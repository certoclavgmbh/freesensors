<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="6.5.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="18" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="18" fill="5" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="pNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="DISIGN_HISTORY" color="3" fill="1" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="111" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="ReferenceLS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="HOT" color="12" fill="15" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="140" name="Mechanik" color="7" fill="1" visible="no" active="yes"/>
<layer number="141" name="Front" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="DrillLegend" color="7" fill="1" visible="no" active="yes"/>
<layer number="150" name="t3Ddata01" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="14" fill="1" visible="no" active="no"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="157" name="tCad" color="7" fill="1" visible="yes" active="yes"/>
<layer number="158" name="bCad" color="7" fill="1" visible="yes" active="yes"/>
<layer number="160" name="b3Ddata01" color="7" fill="1" visible="yes" active="yes"/>
<layer number="198" name="HELP2" color="15" fill="1" visible="no" active="yes"/>
<layer number="199" name="HELP" color="14" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="no"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="no"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="no"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="no"/>
<layer number="207" name="207bmp" color="15" fill="10" visible="no" active="no"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="230" name="EAGLE3D-Animation" color="7" fill="1" visible="no" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="253" name="Info1" color="14" fill="1" visible="yes" active="yes"/>
<layer number="254" name="OrgLBR" color="13" fill="1" visible="no" active="no"/>
<layer number="255" name="description" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="xdevelop-frame">
<packages>
</packages>
<symbols>
<symbol name="XDEVELOP-A3">
<wire x1="0" y1="0" x2="0" y2="4.25" width="0.5" layer="110"/>
<wire x1="0" y1="4.25" x2="0" y2="8.5" width="0.5" layer="110"/>
<wire x1="0" y1="8.5" x2="0" y2="12.75" width="0.5" layer="110"/>
<wire x1="0" y1="12.75" x2="0" y2="17" width="0.5" layer="110"/>
<wire x1="0" y1="17" x2="5" y2="17" width="0.5" layer="110"/>
<wire x1="5" y1="17" x2="12.46" y2="17" width="0.5" layer="110"/>
<wire x1="12.46" y1="17" x2="38" y2="17" width="0.5" layer="110"/>
<wire x1="38" y1="17" x2="49" y2="17" width="0.5" layer="110"/>
<wire x1="49" y1="17" x2="57" y2="17" width="0.5" layer="110"/>
<wire x1="57" y1="17" x2="57" y2="4.25" width="0.5" layer="110"/>
<wire x1="57" y1="4.25" x2="57" y2="0" width="0.5" layer="110"/>
<wire x1="57" y1="0" x2="49" y2="0" width="0.5" layer="110"/>
<wire x1="49" y1="0" x2="38" y2="0" width="0.5" layer="110"/>
<wire x1="38" y1="0" x2="12.46" y2="0" width="0.5" layer="110"/>
<wire x1="12.46" y1="0" x2="0" y2="0" width="0.5" layer="110"/>
<wire x1="57" y1="17" x2="70" y2="17" width="0.5" layer="110"/>
<wire x1="70" y1="17" x2="92" y2="17" width="0.5" layer="110"/>
<wire x1="92" y1="17" x2="92" y2="12.75" width="0.5" layer="110"/>
<wire x1="92" y1="12.75" x2="92" y2="8.5" width="0.5" layer="110"/>
<wire x1="92" y1="8.5" x2="92" y2="0" width="0.5" layer="110"/>
<wire x1="92" y1="0" x2="70" y2="0" width="0.5" layer="110"/>
<wire x1="70" y1="0" x2="57" y2="0" width="0.5" layer="110"/>
<wire x1="92" y1="17" x2="115" y2="17" width="0.5" layer="110"/>
<wire x1="115" y1="17" x2="131" y2="17" width="0.5" layer="110"/>
<wire x1="131" y1="17" x2="170" y2="17" width="0.5" layer="110"/>
<wire x1="170" y1="17" x2="170" y2="4.25" width="0.5" layer="110"/>
<wire x1="170" y1="4.25" x2="170" y2="0" width="0.5" layer="110"/>
<wire x1="170" y1="0" x2="131" y2="0" width="0.5" layer="110"/>
<wire x1="131" y1="0" x2="92" y2="0" width="0.5" layer="110"/>
<wire x1="170" y1="17" x2="209" y2="17" width="0.5" layer="110"/>
<wire x1="209" y1="17" x2="209" y2="0" width="0.5" layer="110"/>
<wire x1="209" y1="0" x2="170" y2="0" width="0.5" layer="110"/>
<wire x1="209" y1="17" x2="274" y2="17" width="0.5" layer="110"/>
<wire x1="274" y1="17" x2="274" y2="8.5" width="0.5" layer="110"/>
<wire x1="274" y1="8.5" x2="274" y2="0" width="0.5" layer="110"/>
<wire x1="274" y1="0" x2="209" y2="0" width="0.5" layer="110"/>
<wire x1="274" y1="17" x2="339" y2="17" width="0.5" layer="110"/>
<wire x1="339" y1="17" x2="385" y2="17" width="0.5" layer="110"/>
<wire x1="385" y1="17" x2="390" y2="17" width="0.5" layer="110"/>
<wire x1="390" y1="17" x2="390" y2="12.75" width="0.5" layer="110"/>
<wire x1="390" y1="12.75" x2="390" y2="8.5" width="0.5" layer="110"/>
<wire x1="390" y1="8.5" x2="390" y2="0" width="0.5" layer="110"/>
<wire x1="390" y1="0" x2="369" y2="0" width="0.5" layer="110"/>
<wire x1="369" y1="0" x2="304" y2="0" width="0.5" layer="110"/>
<wire x1="304" y1="0" x2="274" y2="0" width="0.5" layer="110"/>
<wire x1="0" y1="17" x2="0" y2="46.1667" width="0.5" layer="110"/>
<wire x1="0" y1="46.1667" x2="0" y2="92.3334" width="0.5" layer="110"/>
<wire x1="0" y1="92.3334" x2="0" y2="138.5001" width="0.5" layer="110"/>
<wire x1="0" y1="138.5001" x2="0" y2="184.6668" width="0.5" layer="110"/>
<wire x1="0" y1="184.6668" x2="0" y2="230.8335" width="0.5" layer="110"/>
<wire x1="0" y1="230.8335" x2="0" y2="277.0002" width="0.5" layer="110"/>
<wire x1="0" y1="277.0002" x2="52.5" y2="277.0002" width="0.5" layer="110"/>
<wire x1="52.5" y1="277.0002" x2="100" y2="277.0002" width="0.5" layer="110"/>
<wire x1="100" y1="277.0002" x2="147.5" y2="277.0002" width="0.5" layer="110"/>
<wire x1="147.5" y1="277.0002" x2="195" y2="277.0002" width="0.5" layer="110"/>
<wire x1="195" y1="277.0002" x2="242.5" y2="277.0002" width="0.5" layer="110"/>
<wire x1="242.5" y1="277.0002" x2="290" y2="277.0002" width="0.5" layer="110"/>
<wire x1="290" y1="277.0002" x2="337.5" y2="277.0002" width="0.5" layer="110"/>
<wire x1="337.5" y1="277.0002" x2="390" y2="277.0002" width="0.5" layer="110"/>
<wire x1="390" y1="277.0002" x2="390" y2="230.8335" width="0.5" layer="110"/>
<wire x1="390" y1="230.8335" x2="390" y2="184.6668" width="0.5" layer="110"/>
<wire x1="390" y1="184.6668" x2="390" y2="138.5001" width="0.5" layer="110"/>
<wire x1="390" y1="138.5001" x2="390" y2="92.3334" width="0.5" layer="110"/>
<wire x1="390" y1="92.3334" x2="390" y2="46.1667" width="0.5" layer="110"/>
<wire x1="390" y1="46.1667" x2="390" y2="17" width="0.5" layer="110"/>
<wire x1="5" y1="17" x2="5" y2="46.1667" width="0.18" layer="110"/>
<wire x1="5" y1="46.1667" x2="5" y2="92.3334" width="0.18" layer="110"/>
<wire x1="5" y1="92.3334" x2="5" y2="138.5001" width="0.18" layer="110"/>
<wire x1="5" y1="138.5001" x2="5" y2="184.6668" width="0.18" layer="110"/>
<wire x1="5" y1="184.6668" x2="5" y2="230.8335" width="0.18" layer="110"/>
<wire x1="5" y1="230.8335" x2="5" y2="272.0002" width="0.18" layer="110"/>
<wire x1="5" y1="272.0002" x2="52.5" y2="272.0002" width="0.18" layer="110"/>
<wire x1="52.5" y1="272.0002" x2="100" y2="272.0002" width="0.18" layer="110"/>
<wire x1="100" y1="272.0002" x2="147.5" y2="272.0002" width="0.18" layer="110"/>
<wire x1="147.5" y1="272.0002" x2="195" y2="272.0002" width="0.18" layer="110"/>
<wire x1="195" y1="272.0002" x2="242.5" y2="272.0002" width="0.18" layer="110"/>
<wire x1="242.5" y1="272.0002" x2="290" y2="272.0002" width="0.18" layer="110"/>
<wire x1="290" y1="272.0002" x2="337.5" y2="272.0002" width="0.18" layer="110"/>
<wire x1="337.5" y1="272.0002" x2="385" y2="272.0002" width="0.18" layer="110"/>
<wire x1="385" y1="272.0002" x2="385" y2="230.8335" width="0.18" layer="110"/>
<wire x1="385" y1="230.8335" x2="385" y2="184.6668" width="0.18" layer="110"/>
<wire x1="385" y1="184.6668" x2="385" y2="138.5001" width="0.18" layer="110"/>
<wire x1="385" y1="138.5001" x2="385" y2="92.3334" width="0.18" layer="110"/>
<wire x1="385" y1="92.3334" x2="385" y2="46.1667" width="0.18" layer="110"/>
<wire x1="385" y1="46.1667" x2="385" y2="17" width="0.18" layer="110"/>
<wire x1="0" y1="46.1667" x2="5" y2="46.1667" width="0.18" layer="110"/>
<wire x1="0" y1="92.3334" x2="5" y2="92.3334" width="0.18" layer="110"/>
<wire x1="0" y1="138.5001" x2="5" y2="138.5001" width="0.18" layer="110"/>
<wire x1="0" y1="184.6668" x2="5" y2="184.6668" width="0.18" layer="110"/>
<wire x1="0" y1="230.8335" x2="5" y2="230.8335" width="0.18" layer="110"/>
<wire x1="52.5" y1="272.0002" x2="52.5" y2="277.0002" width="0.18" layer="110"/>
<wire x1="100" y1="272.0002" x2="100" y2="277.0002" width="0.18" layer="110"/>
<wire x1="147.5" y1="272.0002" x2="147.5" y2="277.0002" width="0.18" layer="110"/>
<wire x1="195" y1="272.0002" x2="195" y2="277.0002" width="0.18" layer="110"/>
<wire x1="242.5" y1="272.0002" x2="242.5" y2="277.0002" width="0.18" layer="110"/>
<wire x1="290" y1="272.0002" x2="290" y2="277.0002" width="0.18" layer="110"/>
<wire x1="337.5" y1="272.0002" x2="337.5" y2="277.0002" width="0.18" layer="110"/>
<wire x1="390" y1="46.1667" x2="385" y2="46.1667" width="0.18" layer="110"/>
<wire x1="390" y1="92.3334" x2="385" y2="92.3334" width="0.18" layer="110"/>
<wire x1="390" y1="138.5001" x2="385" y2="138.5001" width="0.18" layer="110"/>
<wire x1="390" y1="184.6668" x2="385" y2="184.6668" width="0.18" layer="110"/>
<wire x1="390" y1="230.8335" x2="385" y2="230.8335" width="0.18" layer="110"/>
<wire x1="12.46" y1="0" x2="12.46" y2="17" width="0.18" layer="110"/>
<wire x1="38" y1="0" x2="38" y2="17" width="0.18" layer="110"/>
<wire x1="49" y1="0" x2="49" y2="17" width="0.18" layer="110"/>
<wire x1="0" y1="4.25" x2="57" y2="4.25" width="0.18" layer="110"/>
<wire x1="57" y1="4.25" x2="115" y2="4.25" width="0.18" layer="110"/>
<wire x1="115" y1="4.25" x2="131" y2="4.25" width="0.18" layer="110"/>
<wire x1="131" y1="4.25" x2="170" y2="4.25" width="0.18" layer="110"/>
<wire x1="0" y1="8.5" x2="92" y2="8.5" width="0.18" layer="110"/>
<wire x1="0" y1="12.75" x2="92" y2="12.75" width="0.18" layer="110"/>
<wire x1="70" y1="0" x2="70" y2="17" width="0.18" layer="110"/>
<wire x1="274" y1="8.5" x2="304" y2="8.5" width="0.18" layer="110"/>
<wire x1="304" y1="8.5" x2="339" y2="8.5" width="0.18" layer="110"/>
<wire x1="339" y1="8.5" x2="369" y2="8.5" width="0.18" layer="110"/>
<wire x1="369" y1="8.5" x2="390" y2="8.5" width="0.18" layer="110"/>
<wire x1="369" y1="8.5" x2="369" y2="0" width="0.18" layer="110"/>
<wire x1="339" y1="8.5" x2="339" y2="12.75" width="0.18" layer="110"/>
<wire x1="339" y1="12.75" x2="339" y2="17" width="0.18" layer="110"/>
<wire x1="304" y1="0" x2="304" y2="8.5" width="0.18" layer="110"/>
<wire x1="339" y1="12.75" x2="390" y2="12.75" width="0.18" layer="110"/>
<text x="1.524" y="30.734" size="2.5" layer="110">A</text>
<text x="1.778" y="70.866" size="2.5" layer="110">B</text>
<text x="1.524" y="119.38" size="2.5" layer="110">C</text>
<text x="1.778" y="162.56" size="2.5" layer="110">D</text>
<text x="1.778" y="208.28" size="2.5" layer="110">E</text>
<text x="1.778" y="251.46" size="2.5" layer="110">F</text>
<text x="27.94" y="273.05" size="2.5" layer="110">8</text>
<text x="77.978" y="273.304" size="2.5" layer="110">7</text>
<text x="123.698" y="273.304" size="2.5" layer="110">6</text>
<text x="171.958" y="273.304" size="2.5" layer="110">5</text>
<text x="218.694" y="273.05" size="2.5" layer="110">4</text>
<text x="265.938" y="273.304" size="2.5" layer="110">3</text>
<text x="312.928" y="273.304" size="2.5" layer="110">2</text>
<text x="361.696" y="273.304" size="2.5" layer="110">1</text>
<text x="370.84" y="5.08" size="2.5" layer="110">Page</text>
<text x="339.598" y="13.462" size="2.5" layer="110">-</text>
<text x="339.598" y="9.144" size="2.5" layer="110">+</text>
<text x="275.336" y="13.462" size="2.5" layer="110">Drawing:</text>
<text x="305.054" y="5.08" size="2.5" layer="110">Object.</text>
<text x="275.082" y="5.08" size="2.5" layer="110">File:</text>
<text x="210.82" y="12.7" size="2.5" layer="110">Title:</text>
<text x="94.488" y="12.7" size="2.5" layer="110">Name:</text>
<text x="57.658" y="13.462" size="2.5" layer="110">Create</text>
<text x="57.658" y="9.398" size="2.5" layer="110">Edit</text>
<text x="57.658" y="5.08" size="2.5" layer="110">Check</text>
<text x="57.658" y="1.27" size="2.5" layer="110">Check</text>
<text x="49.784" y="1.27" size="1.778" layer="110">Name</text>
<text x="40.64" y="1.27" size="1.778" layer="110">Date</text>
<text x="20.828" y="1.27" size="1.778" layer="110">Change</text>
<text x="1.016" y="1.524" size="1.778" layer="110">Version</text>
<text x="386.588" y="251.206" size="2.5" layer="110">F</text>
<text x="386.334" y="208.28" size="2.5" layer="110">E</text>
<text x="386.588" y="160.782" size="2.5" layer="110">D</text>
<text x="386.588" y="114.554" size="2.5" layer="110">C</text>
<text x="386.588" y="67.31" size="2.5" layer="110">B</text>
<text x="386.334" y="30.48" size="2.5" layer="110">A</text>
<text x="210.82" y="6.604" size="3.81" layer="110" font="vector">&gt;TITLE</text>
<text x="210.82" y="2.794" size="2.032" layer="110" font="vector">&gt;SUBTITLE</text>
<text x="73.406" y="13.462" size="2.54" layer="110" font="vector">&gt;CREATE_DATE</text>
<text x="73.406" y="9.398" size="2.54" layer="110" font="vector">&gt;EDIT_DATE</text>
<text x="381" y="1.524" size="2.54" layer="110" font="vector">&gt;SHEET</text>
<text x="98.298" y="6.858" size="3.81" layer="110" font="vector">&gt;USERNAME</text>
<text x="276.86" y="9.906" size="2.54" layer="110" font="vector">&gt;DRAWING</text>
<text x="276.86" y="1.524" size="2.54" layer="110" font="vector">&gt;FILENAME</text>
<text x="305.816" y="1.524" size="2.54" layer="110" font="vector">&gt;OBJECT_NO</text>
<text x="172.72" y="8.89" size="5.08" layer="110" font="vector" ratio="12">xDevelop</text>
<text x="184.15" y="5.334" size="1.778" layer="110" font="vector" ratio="10">Bernhard</text>
<text x="177.292" y="2.794" size="1.778" layer="110" font="vector" ratio="10">Wörndl-Aichriedler</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="XDEVELOP-A3">
<gates>
<gate name="G$1" symbol="XDEVELOP-A3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Basic">
<packages>
<package name="C0402">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.762" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C0504">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.294" y1="0.559" x2="0.294" y2="0.559" width="0.1016" layer="51"/>
<wire x1="-0.294" y1="-0.559" x2="0.294" y2="-0.559" width="0.1016" layer="51"/>
<smd name="1" x="-0.7" y="0" dx="1" dy="1.3" layer="1"/>
<smd name="2" x="0.7" y="0" dx="1" dy="1.3" layer="1"/>
<text x="-0.635" y="0.889" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.159" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.6604" y1="-0.6223" x2="-0.2804" y2="0.6276" layer="51"/>
<rectangle x1="0.2794" y1="-0.6223" x2="0.6594" y2="0.6276" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C0603">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.473" y1="0.8243" x2="1.473" y2="0.8243" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.8243" x2="1.473" y2="-0.8243" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.8243" x2="-1.473" y2="-0.8243" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.8243" x2="-1.473" y2="0.8243" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<wire x1="-1.5288" y1="0.7938" x2="-1.5288" y2="-0.7938" width="0.0762" layer="21"/>
<wire x1="-1.5288" y1="-0.7938" x2="1.5288" y2="-0.7938" width="0.0762" layer="21"/>
<wire x1="1.5288" y1="-0.7938" x2="1.5288" y2="0.7938" width="0.0762" layer="21"/>
<wire x1="1.5288" y1="0.7938" x2="-1.5288" y2="0.7938" width="0.0762" layer="21"/>
<smd name="1" x="-0.85" y="0" dx="1.05" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.05" dy="1" layer="1"/>
<text x="-0.889" y="0.762" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.889" y="-2.032" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C0805">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.8936" y1="0.983" x2="1.8936" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.8936" y1="-0.983" x2="-1.8936" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.8936" y1="-0.983" x2="-1.8936" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<wire x1="1.8936" y1="0.983" x2="1.8936" y2="-0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.889" y="1.016" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.889" y="-2.286" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C1005">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.762" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C1206">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.413" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="C1210">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-1.397" y="1.651" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.397" y="-2.921" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-1.2954" x2="-0.9517" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.3045" x2="1.7018" y2="1.2954" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="C1310">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.294" y1="0.559" x2="0.294" y2="0.559" width="0.1016" layer="51"/>
<wire x1="-0.294" y1="-0.559" x2="0.294" y2="-0.559" width="0.1016" layer="51"/>
<smd name="1" x="-0.7" y="0" dx="1" dy="1.3" layer="1"/>
<smd name="2" x="0.7" y="0" dx="1" dy="1.3" layer="1"/>
<text x="-0.635" y="0.889" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.159" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.6604" y1="-0.6223" x2="-0.2804" y2="0.6276" layer="51"/>
<rectangle x1="0.2794" y1="-0.6223" x2="0.6594" y2="0.6276" layer="51"/>
<rectangle x1="-0.1001" y1="-0.3" x2="0.1001" y2="0.3" layer="35"/>
</package>
<package name="C1608">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.635" y="0.762" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.032" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C1812">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.973" y1="1.983" x2="2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-1.983" x2="-2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-1.983" x2="-2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="2.973" y1="1.983" x2="2.973" y2="-1.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.3" y1="-0.4001" x2="0.3" y2="0.4001" layer="35"/>
</package>
<package name="C1825">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.973" y1="3.483" x2="2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-3.483" x2="-2.973" y2="-3.483" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-3.483" x2="-2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<wire x1="2.973" y1="3.483" x2="2.973" y2="-3.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<text x="-1.905" y="3.683" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-4.826" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.463" y2="3.3472" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3978" y2="3.3472" layer="51"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
</package>
<package name="C2012">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.889" y="1.016" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.889" y="-2.286" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C3216">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.413" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.3" y1="-0.5001" x2="0.3" y2="0.5001" layer="35"/>
</package>
<package name="C3225">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-1.397" y="1.651" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.397" y="-2.921" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-1.2954" x2="-0.9517" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.3045" x2="1.7018" y2="1.2954" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="C4532">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.973" y1="1.983" x2="2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-1.983" x2="-2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-1.983" x2="-2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="2.973" y1="1.983" x2="2.973" y2="-1.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<text x="-1.905" y="2.032" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="C4564">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.973" y1="3.483" x2="2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-3.483" x2="-2.973" y2="-3.483" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-3.483" x2="-2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<wire x1="2.973" y1="3.483" x2="2.973" y2="-3.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<text x="-1.905" y="3.683" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-4.826" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.463" y2="3.3472" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3978" y2="3.3472" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="C025-024X044">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 2.4 x 4.4 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.778" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.778" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-025X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 2.5 x 5 mm</description>
<wire x1="-2.159" y1="1.27" x2="2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.27" x2="-2.159" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.27" x2="2.413" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.016" x2="-2.159" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.27" x2="2.413" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.016" x2="-2.159" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-030X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 3 x 5 mm</description>
<wire x1="-2.159" y1="1.524" x2="2.159" y2="1.524" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.524" x2="-2.159" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.27" x2="2.413" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.27" x2="-2.413" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.524" x2="2.413" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.27" x2="-2.159" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.524" x2="2.413" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.27" x2="-2.159" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-040X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 4 x 5 mm</description>
<wire x1="-2.159" y1="1.905" x2="2.159" y2="1.905" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.905" x2="-2.159" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.651" x2="2.413" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.651" x2="-2.413" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.905" x2="2.413" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.651" x2="-2.159" y2="1.905" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.905" x2="2.413" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.651" x2="-2.159" y2="-1.905" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.429" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-050X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 5 x 5 mm</description>
<wire x1="-2.159" y1="2.286" x2="2.159" y2="2.286" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.286" x2="-2.159" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.032" x2="2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.032" x2="-2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.286" x2="2.413" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.032" x2="-2.159" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.286" x2="2.413" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.032" x2="-2.159" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-060X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 6 x 5 mm</description>
<wire x1="-2.159" y1="2.794" x2="2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.794" x2="-2.159" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.54" x2="2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.54" x2="-2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.794" x2="2.413" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.54" x2="-2.159" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.794" x2="2.413" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.54" x2="-2.159" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-024X070">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm + 5 mm, outline 2.4 x 7 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="-4.191" y1="-1.143" x2="-3.9624" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-4.191" y1="1.143" x2="-3.9624" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-0.635" x2="-4.191" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="-4.699" y1="0.635" x2="-4.191" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.699" y1="-0.635" x2="-4.699" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="1.143" x2="-2.5654" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.143" x2="-2.5654" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.81" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-025X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 2.5 x 7.5 mm</description>
<wire x1="-2.159" y1="1.27" x2="2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.27" x2="-2.159" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.27" x2="2.413" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.016" x2="-2.159" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.27" x2="2.413" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.016" x2="-2.159" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="1.016" x2="4.953" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="4.699" y1="1.27" x2="4.953" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.27" x2="4.953" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="1.27" x2="4.699" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.27" x2="2.794" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.413" y2="0.762" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-0.762" x2="2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.254" x2="2.413" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0" x2="2.286" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.159" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-035X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 3.5 x 7.5 mm</description>
<wire x1="-2.159" y1="1.778" x2="2.159" y2="1.778" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.778" x2="-2.159" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.524" x2="-2.413" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.778" x2="2.413" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.524" x2="-2.159" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.778" x2="2.413" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.524" x2="-2.159" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="1.524" x2="4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.699" y1="1.778" x2="4.953" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.778" x2="4.953" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="1.778" x2="4.699" y2="1.778" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.778" x2="2.794" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.524" x2="2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.413" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.508" x2="2.413" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.302" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-045X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 4.5 x 7.5 mm</description>
<wire x1="-2.159" y1="2.286" x2="2.159" y2="2.286" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.286" x2="-2.159" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.032" x2="-2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.286" x2="2.413" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.032" x2="-2.159" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.286" x2="2.413" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.032" x2="-2.159" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="2.032" x2="4.953" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.286" x2="4.953" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-2.286" x2="4.953" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="2.286" x2="4.699" y2="2.286" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.286" x2="2.794" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.032" x2="2.413" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.397" x2="2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.762" x2="2.413" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-055X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 5.5 x 7.5 mm</description>
<wire x1="-2.159" y1="2.794" x2="2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.794" x2="-2.159" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.54" x2="-2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.794" x2="2.413" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.54" x2="-2.159" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.794" x2="2.413" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.54" x2="-2.159" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="2.54" x2="4.953" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.794" x2="4.953" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-2.794" x2="4.953" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="2.794" x2="4.699" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.794" x2="2.794" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.54" x2="2.413" y2="2.032" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-2.032" x2="2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.762" x2="2.413" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0" x2="2.286" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-024X044">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 2.4 x 4.4 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.159" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.159" y1="-0.381" x2="2.54" y2="0.381" layer="51"/>
<rectangle x1="-2.54" y1="-0.381" x2="-2.159" y2="0.381" layer="51"/>
</package>
<package name="C050-025X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 2.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.016" x2="-3.683" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.27" x2="3.429" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.016" x2="3.683" y2="1.016" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.27" x2="-3.429" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.27" x2="3.683" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.27" x2="3.683" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.016" x2="-3.429" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.016" x2="-3.429" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-045X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 4.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.032" x2="-3.683" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.286" x2="3.429" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.032" x2="3.683" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.286" x2="-3.429" y2="2.286" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.286" x2="3.683" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.286" x2="3.683" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.032" x2="-3.429" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.032" x2="-3.429" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-030X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 3 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.27" x2="-3.683" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.27" x2="3.683" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.524" x2="3.683" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.524" x2="3.683" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.27" x2="-3.429" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.27" x2="-3.429" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-050X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.286" x2="-3.683" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.54" x2="3.429" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.286" x2="3.683" y2="2.286" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.54" x2="-3.429" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.54" x2="3.683" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.54" x2="3.683" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.286" x2="-3.429" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.286" x2="-3.429" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-2.159" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-055X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 5.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.54" x2="-3.683" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.794" x2="3.429" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.54" x2="3.683" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.794" x2="-3.429" y2="2.794" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.794" x2="3.683" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.794" x2="3.683" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.54" x2="-3.429" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.54" x2="-3.429" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.302" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-075X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 7.5 x 7.5 mm</description>
<wire x1="-1.524" y1="0" x2="-0.4572" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.4572" y1="0" x2="-0.4572" y2="0.762" width="0.4064" layer="21"/>
<wire x1="-0.4572" y1="0" x2="-0.4572" y2="-0.762" width="0.4064" layer="21"/>
<wire x1="0.4318" y1="0.762" x2="0.4318" y2="0" width="0.4064" layer="21"/>
<wire x1="0.4318" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.4318" y1="0" x2="0.4318" y2="-0.762" width="0.4064" layer="21"/>
<wire x1="-3.683" y1="3.429" x2="-3.683" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-3.683" x2="3.429" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-3.429" x2="3.683" y2="3.429" width="0.1524" layer="21"/>
<wire x1="3.429" y1="3.683" x2="-3.429" y2="3.683" width="0.1524" layer="21"/>
<wire x1="3.429" y1="3.683" x2="3.683" y2="3.429" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-3.683" x2="3.683" y2="-3.429" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-3.429" x2="-3.429" y2="-3.683" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="3.429" x2="-3.429" y2="3.683" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="4.064" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-2.921" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050H075X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
Horizontal, grid 5 mm, outline 7.5 x 7.5 mm</description>
<wire x1="-3.683" y1="7.112" x2="-3.683" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="0.508" x2="-3.302" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="0.508" x2="-1.778" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="0.508" x2="1.778" y2="0.508" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.508" x2="3.302" y2="0.508" width="0.1524" layer="51"/>
<wire x1="3.302" y1="0.508" x2="3.683" y2="0.508" width="0.1524" layer="21"/>
<wire x1="3.683" y1="0.508" x2="3.683" y2="7.112" width="0.1524" layer="21"/>
<wire x1="3.175" y1="7.62" x2="-3.175" y2="7.62" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="2.413" x2="-0.3048" y2="1.778" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="1.778" x2="-0.3048" y2="1.143" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="1.778" x2="-1.651" y2="1.778" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="2.413" x2="0.3302" y2="1.778" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="1.778" x2="0.3302" y2="1.143" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="1.778" x2="1.651" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="7.112" x2="-3.175" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.175" y1="7.62" x2="3.683" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.254" width="0.508" layer="51"/>
<wire x1="2.54" y1="0" x2="2.54" y2="0.254" width="0.508" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.302" y="8.001" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="3.175" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.794" y1="0.127" x2="-2.286" y2="0.508" layer="51"/>
<rectangle x1="2.286" y1="0.127" x2="2.794" y2="0.508" layer="51"/>
</package>
<package name="C075-032X103">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 3.2 x 10.3 mm</description>
<wire x1="4.826" y1="1.524" x2="-4.826" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-1.524" x2="4.826" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.826" y1="1.524" x2="5.08" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.826" y1="-1.524" x2="5.08" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="-1.27" x2="-4.826" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.27" x2="-4.826" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="0.508" y1="0" x2="2.54" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0" x2="-0.508" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="0.889" x2="-0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="-0.508" y1="0" x2="-0.508" y2="-0.889" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0.889" x2="0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0" x2="0.508" y2="-0.889" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.826" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-042X103">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 4.2 x 10.3 mm</description>
<wire x1="4.826" y1="2.032" x2="-4.826" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.778" x2="-5.08" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-2.032" x2="4.826" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.778" x2="5.08" y2="1.778" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="5.08" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.826" y1="-2.032" x2="5.08" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="-1.778" x2="-4.826" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.778" x2="-4.826" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.699" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-052X106">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 5.2 x 10.6 mm</description>
<wire x1="4.953" y1="2.54" x2="-4.953" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.286" x2="-5.207" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-2.54" x2="4.953" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.286" x2="5.207" y2="2.286" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.54" x2="5.207" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.953" y1="-2.54" x2="5.207" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="-2.286" x2="-4.953" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="2.286" x2="-4.953" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-043X133">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 4.3 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="2.032" x2="6.096" y2="2.032" width="0.1524" layer="21"/>
<wire x1="6.604" y1="1.524" x2="6.604" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-2.032" x2="-6.096" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-1.524" x2="-6.604" y2="1.524" width="0.1524" layer="21"/>
<wire x1="6.096" y1="2.032" x2="6.604" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-2.032" x2="6.604" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-1.524" x2="-6.096" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="1.524" x2="-6.096" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-054X133">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 5.4 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="2.54" x2="6.096" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.604" y1="2.032" x2="6.604" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-2.54" x2="-6.096" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.032" x2="-6.604" y2="2.032" width="0.1524" layer="21"/>
<wire x1="6.096" y1="2.54" x2="6.604" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-2.54" x2="6.604" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.032" x2="-6.096" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.032" x2="-6.096" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-1.905" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-064X133">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 6.4 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="3.048" x2="6.096" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.604" y1="2.54" x2="6.604" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-3.048" x2="-6.096" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.54" x2="-6.604" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.096" y1="3.048" x2="6.604" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-3.048" x2="6.604" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.54" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.54" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102_152-062X184">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm + 15.2 mm, outline 6.2 x 18.4 mm</description>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.683" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="3.683" y2="0" width="0.1524" layer="21"/>
<wire x1="6.477" y1="0" x2="8.636" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="3.048" x2="6.223" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.223" y1="-3.048" x2="-6.096" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.54" x2="-6.604" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.223" y1="3.048" x2="6.731" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.223" y1="-3.048" x2="6.731" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.54" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.54" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.731" y1="2.54" x2="6.731" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="11.176" y1="3.048" x2="11.684" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="11.176" y1="-3.048" x2="11.684" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="11.176" y1="-3.048" x2="7.112" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="7.112" y1="3.048" x2="11.176" y2="3.048" width="0.1524" layer="21"/>
<wire x1="11.684" y1="2.54" x2="11.684" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="3" x="10.033" y="0" drill="1.016" shape="octagon"/>
<text x="-5.969" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-054X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 5.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="2.032" x2="9.017" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-2.54" x2="-8.509" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-2.032" x2="-9.017" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="2.54" x2="8.509" y2="2.54" width="0.1524" layer="21"/>
<wire x1="8.509" y1="2.54" x2="9.017" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-2.54" x2="9.017" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-2.032" x2="-8.509" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="2.032" x2="-8.509" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.382" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-064X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 6.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="2.54" x2="9.017" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.048" x2="-8.509" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-2.54" x2="-9.017" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="3.048" x2="8.509" y2="3.048" width="0.1524" layer="21"/>
<wire x1="8.509" y1="3.048" x2="9.017" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-3.048" x2="9.017" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-2.54" x2="-8.509" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="2.54" x2="-8.509" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-072X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 7.2 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.048" x2="9.017" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.556" x2="-8.509" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.048" x2="-9.017" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="3.556" x2="8.509" y2="3.556" width="0.1524" layer="21"/>
<wire x1="8.509" y1="3.556" x2="9.017" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-3.556" x2="9.017" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.048" x2="-8.509" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.048" x2="-8.509" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="3.937" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-084X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 8.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.556" x2="9.017" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-4.064" x2="-8.509" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.556" x2="-9.017" y2="3.556" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="4.064" x2="8.509" y2="4.064" width="0.1524" layer="21"/>
<wire x1="8.509" y1="4.064" x2="9.017" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-4.064" x2="9.017" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.556" x2="-8.509" y2="-4.064" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.556" x2="-8.509" y2="4.064" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="4.445" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-091X182">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 9.1 x 18.2 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.937" x2="9.017" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-4.445" x2="-8.509" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.937" x2="-9.017" y2="3.937" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="4.445" x2="8.509" y2="4.445" width="0.1524" layer="21"/>
<wire x1="8.509" y1="4.445" x2="9.017" y2="3.937" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-4.445" x2="9.017" y2="-3.937" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.937" x2="-8.509" y2="-4.445" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.937" x2="-8.509" y2="4.445" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="4.826" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-062X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 6.2 x 26.8 mm</description>
<wire x1="-12.827" y1="3.048" x2="12.827" y2="3.048" width="0.1524" layer="21"/>
<wire x1="13.335" y1="2.54" x2="13.335" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-3.048" x2="-12.827" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-2.54" x2="-13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="3.048" x2="13.335" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-3.048" x2="13.335" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-2.54" x2="-12.827" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="2.54" x2="-12.827" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.7" y="3.429" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-074X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 7.4 x 26.8 mm</description>
<wire x1="-12.827" y1="3.556" x2="12.827" y2="3.556" width="0.1524" layer="21"/>
<wire x1="13.335" y1="3.048" x2="13.335" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-3.556" x2="-12.827" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-3.048" x2="-13.335" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="3.556" x2="13.335" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-3.556" x2="13.335" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-3.048" x2="-12.827" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="3.048" x2="-12.827" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.827" y="3.937" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-087X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 8.7 x 26.8 mm</description>
<wire x1="-12.827" y1="4.318" x2="12.827" y2="4.318" width="0.1524" layer="21"/>
<wire x1="13.335" y1="3.81" x2="13.335" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-4.318" x2="-12.827" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-3.81" x2="-13.335" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="4.318" x2="13.335" y2="3.81" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-4.318" x2="13.335" y2="-3.81" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-3.81" x2="-12.827" y2="-4.318" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="3.81" x2="-12.827" y2="4.318" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.827" y="4.699" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-108X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 10.8 x 26.8 mm</description>
<wire x1="-12.827" y1="5.334" x2="12.827" y2="5.334" width="0.1524" layer="21"/>
<wire x1="13.335" y1="4.826" x2="13.335" y2="-4.826" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-5.334" x2="-12.827" y2="-5.334" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-4.826" x2="-13.335" y2="4.826" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="5.334" x2="13.335" y2="4.826" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-5.334" x2="13.335" y2="-4.826" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-4.826" x2="-12.827" y2="-5.334" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="4.826" x2="-12.827" y2="5.334" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.954" y="5.715" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-113X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 11.3 x 26.8 mm</description>
<wire x1="-12.827" y1="5.588" x2="12.827" y2="5.588" width="0.1524" layer="21"/>
<wire x1="13.335" y1="5.08" x2="13.335" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-5.588" x2="-12.827" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-5.08" x2="-13.335" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="5.588" x2="13.335" y2="5.08" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-5.588" x2="13.335" y2="-5.08" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-5.08" x2="-12.827" y2="-5.588" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="5.08" x2="-12.827" y2="5.588" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.954" y="5.969" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-093X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 9.3 x 31.6 mm</description>
<wire x1="-15.24" y1="4.572" x2="15.24" y2="4.572" width="0.1524" layer="21"/>
<wire x1="15.748" y1="4.064" x2="15.748" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-4.572" x2="-15.24" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-4.064" x2="-15.748" y2="4.064" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="4.572" x2="15.748" y2="4.064" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-4.572" x2="15.748" y2="-4.064" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-4.064" x2="-15.24" y2="-4.572" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="4.064" x2="-15.24" y2="4.572" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="4.953" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-113X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 11.3 x 31.6 mm</description>
<wire x1="-15.24" y1="5.588" x2="15.24" y2="5.588" width="0.1524" layer="21"/>
<wire x1="15.748" y1="5.08" x2="15.748" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-5.588" x2="-15.24" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-5.08" x2="-15.748" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="5.588" x2="15.748" y2="5.08" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-5.588" x2="15.748" y2="-5.08" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-5.08" x2="-15.24" y2="-5.588" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="5.08" x2="-15.24" y2="5.588" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="5.969" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-134X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 13.4 x 31.6 mm</description>
<wire x1="-15.24" y1="6.604" x2="15.24" y2="6.604" width="0.1524" layer="21"/>
<wire x1="15.748" y1="6.096" x2="15.748" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-6.604" x2="-15.24" y2="-6.604" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-6.096" x2="-15.748" y2="6.096" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="6.604" x2="15.748" y2="6.096" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-6.604" x2="15.748" y2="-6.096" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-6.096" x2="-15.24" y2="-6.604" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="6.096" x2="-15.24" y2="6.604" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="6.985" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-205X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 20.5 x 31.6 mm</description>
<wire x1="-15.24" y1="10.16" x2="15.24" y2="10.16" width="0.1524" layer="21"/>
<wire x1="15.748" y1="9.652" x2="15.748" y2="-9.652" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-10.16" x2="-15.24" y2="-10.16" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-9.652" x2="-15.748" y2="9.652" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="10.16" x2="15.748" y2="9.652" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-10.16" x2="15.748" y2="-9.652" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-9.652" x2="-15.24" y2="-10.16" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="9.652" x2="-15.24" y2="10.16" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="10.541" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-4.318" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-137X374">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 13.7 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="6.731" x2="18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="6.731" x2="-18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-6.731" x2="18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="18.542" y1="6.731" x2="-18.542" y2="6.731" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.2372" y="7.0612" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-162X374">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 16.2 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="8.001" x2="18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="8.001" x2="-18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-8.001" x2="18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="18.542" y1="8.001" x2="-18.542" y2="8.001" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.3642" y="8.3312" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-182X374">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 18.2 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="9.017" x2="18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="9.017" x2="-18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-9.017" x2="18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="18.542" y1="9.017" x2="-18.542" y2="9.017" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.3642" y="9.3472" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-192X418">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 19.2 x 41.8 mm</description>
<wire x1="-20.32" y1="8.509" x2="20.32" y2="8.509" width="0.1524" layer="21"/>
<wire x1="20.828" y1="8.001" x2="20.828" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-8.509" x2="-20.32" y2="-8.509" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-8.001" x2="-20.828" y2="8.001" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="8.509" x2="20.828" y2="8.001" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-8.509" x2="20.828" y2="-8.001" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-8.001" x2="-20.32" y2="-8.509" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="8.001" x2="-20.32" y2="8.509" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.447" y="8.89" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-203X418">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 20.3 x 41.8 mm</description>
<wire x1="-20.32" y1="10.16" x2="20.32" y2="10.16" width="0.1524" layer="21"/>
<wire x1="20.828" y1="9.652" x2="20.828" y2="-9.652" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-10.16" x2="-20.32" y2="-10.16" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-9.652" x2="-20.828" y2="9.652" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="10.16" x2="20.828" y2="9.652" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-10.16" x2="20.828" y2="-9.652" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-9.652" x2="-20.32" y2="-10.16" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="9.652" x2="-20.32" y2="10.16" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.32" y="10.541" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-035X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 3.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.524" x2="-3.683" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.778" x2="3.429" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.524" x2="3.683" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.778" x2="-3.429" y2="1.778" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.778" x2="3.683" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.778" x2="3.683" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.524" x2="-3.429" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.524" x2="-3.429" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.429" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-155X418">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 15.5 x 41.8 mm</description>
<wire x1="-20.32" y1="7.62" x2="20.32" y2="7.62" width="0.1524" layer="21"/>
<wire x1="20.828" y1="7.112" x2="20.828" y2="-7.112" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-7.62" x2="-20.32" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-7.112" x2="-20.828" y2="7.112" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="7.62" x2="20.828" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-7.62" x2="20.828" y2="-7.112" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-7.112" x2="-20.32" y2="-7.62" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="7.112" x2="-20.32" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.447" y="8.001" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-063X106">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 6.3 x 10.6 mm</description>
<wire x1="4.953" y1="3.048" x2="-4.953" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.794" x2="-5.207" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-3.048" x2="4.953" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.794" x2="5.207" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.953" y1="3.048" x2="5.207" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.953" y1="-3.048" x2="5.207" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="-2.794" x2="-4.953" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="2.794" x2="-4.953" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-154X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 15.4 x 31.6 mm</description>
<wire x1="-15.24" y1="7.62" x2="15.24" y2="7.62" width="0.1524" layer="21"/>
<wire x1="15.748" y1="7.112" x2="15.748" y2="-7.112" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-7.62" x2="-15.24" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-7.112" x2="-15.748" y2="7.112" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="7.62" x2="15.748" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-7.62" x2="15.748" y2="-7.112" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-7.112" x2="-15.24" y2="-7.62" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="7.112" x2="-15.24" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="8.001" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-173X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 17.3 x 31.6 mm</description>
<wire x1="-15.24" y1="8.509" x2="15.24" y2="8.509" width="0.1524" layer="21"/>
<wire x1="15.748" y1="8.001" x2="15.748" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-8.509" x2="-15.24" y2="-8.509" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-8.001" x2="-15.748" y2="8.001" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="8.509" x2="15.748" y2="8.001" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-8.509" x2="15.748" y2="-8.001" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-8.001" x2="-15.24" y2="-8.509" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="8.001" x2="-15.24" y2="8.509" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="8.89" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C0402K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0204 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 1005</description>
<wire x1="-0.425" y1="0.2" x2="0.425" y2="0.2" width="0.1016" layer="51"/>
<wire x1="0.425" y1="-0.2" x2="-0.425" y2="-0.2" width="0.1016" layer="51"/>
<smd name="1" x="-0.6" y="0" dx="0.925" dy="0.74" layer="1"/>
<smd name="2" x="0.6" y="0" dx="0.925" dy="0.74" layer="1"/>
<text x="-0.5" y="0.425" size="1.016" layer="25">&gt;NAME</text>
<text x="-0.5" y="-1.45" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.5" y1="-0.25" x2="-0.225" y2="0.25" layer="51"/>
<rectangle x1="0.225" y1="-0.25" x2="0.5" y2="0.25" layer="51"/>
</package>
<package name="C0603K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0603 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 1608</description>
<wire x1="-0.725" y1="0.35" x2="0.725" y2="0.35" width="0.1016" layer="51"/>
<wire x1="0.725" y1="-0.35" x2="-0.725" y2="-0.35" width="0.1016" layer="51"/>
<smd name="1" x="-0.875" y="0" dx="1.05" dy="1.08" layer="1"/>
<smd name="2" x="0.875" y="0" dx="1.05" dy="1.08" layer="1"/>
<text x="-0.8" y="0.65" size="1.016" layer="25">&gt;NAME</text>
<text x="-0.8" y="-1.65" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8" y1="-0.4" x2="-0.45" y2="0.4" layer="51"/>
<rectangle x1="0.45" y1="-0.4" x2="0.8" y2="0.4" layer="51"/>
</package>
<package name="C0805K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0805 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 2012</description>
<wire x1="-0.925" y1="0.6" x2="0.925" y2="0.6" width="0.1016" layer="51"/>
<wire x1="0.925" y1="-0.6" x2="-0.925" y2="-0.6" width="0.1016" layer="51"/>
<smd name="1" x="-1" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="1" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1" y="0.875" size="1.016" layer="25">&gt;NAME</text>
<text x="-1" y="-1.9" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1" y1="-0.65" x2="-0.5" y2="0.65" layer="51"/>
<rectangle x1="0.5" y1="-0.65" x2="1" y2="0.65" layer="51"/>
</package>
<package name="C1206K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1206 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 3216</description>
<wire x1="-1.525" y1="0.75" x2="1.525" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-0.75" x2="-1.525" y2="-0.75" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2" layer="1"/>
<text x="-1.6" y="1.1" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-2.1" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-0.8" x2="-1.1" y2="0.8" layer="51"/>
<rectangle x1="1.1" y1="-0.8" x2="1.6" y2="0.8" layer="51"/>
</package>
<package name="C1210K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1210 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 3225</description>
<wire x1="-1.525" y1="1.175" x2="1.525" y2="1.175" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-1.175" x2="-1.525" y2="-1.175" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<text x="-1.6" y="1.55" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-2.575" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-1.25" x2="-1.1" y2="1.25" layer="51"/>
<rectangle x1="1.1" y1="-1.25" x2="1.6" y2="1.25" layer="51"/>
</package>
<package name="C1812K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1812 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 4532</description>
<wire x1="-2.175" y1="1.525" x2="2.175" y2="1.525" width="0.1016" layer="51"/>
<wire x1="2.175" y1="-1.525" x2="-2.175" y2="-1.525" width="0.1016" layer="51"/>
<smd name="1" x="-2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<smd name="2" x="2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<text x="-2.25" y="1.95" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.25" y="-2.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.25" y1="-1.6" x2="-1.65" y2="1.6" layer="51"/>
<rectangle x1="1.65" y1="-1.6" x2="2.25" y2="1.6" layer="51"/>
</package>
<package name="C1825K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1825 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 4564</description>
<wire x1="-1.525" y1="3.125" x2="1.525" y2="3.125" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-3.125" x2="-1.525" y2="-3.125" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.8" dy="6.9" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.8" dy="6.9" layer="1"/>
<text x="-1.6" y="3.55" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-4.625" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-3.2" x2="-1.1" y2="3.2" layer="51"/>
<rectangle x1="1.1" y1="-3.2" x2="1.6" y2="3.2" layer="51"/>
</package>
<package name="C2220K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 2220 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 5650</description>
<wire x1="-2.725" y1="2.425" x2="2.725" y2="2.425" width="0.1016" layer="51"/>
<wire x1="2.725" y1="-2.425" x2="-2.725" y2="-2.425" width="0.1016" layer="51"/>
<smd name="1" x="-2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<smd name="2" x="2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<text x="-2.8" y="2.95" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.8" y="-3.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.8" y1="-2.5" x2="-2.2" y2="2.5" layer="51"/>
<rectangle x1="2.2" y1="-2.5" x2="2.8" y2="2.5" layer="51"/>
</package>
<package name="C2225K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 2225 Reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 5664</description>
<wire x1="-2.725" y1="3.075" x2="2.725" y2="3.075" width="0.1016" layer="51"/>
<wire x1="2.725" y1="-3.075" x2="-2.725" y2="-3.075" width="0.1016" layer="51"/>
<smd name="1" x="-2.55" y="0" dx="1.85" dy="6.8" layer="1"/>
<smd name="2" x="2.55" y="0" dx="1.85" dy="6.8" layer="1"/>
<text x="-2.8" y="3.6" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.8" y="-4.575" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.8" y1="-3.15" x2="-2.2" y2="3.15" layer="51"/>
<rectangle x1="2.2" y1="-3.15" x2="2.8" y2="3.15" layer="51"/>
</package>
<package name="R0402">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.762" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.032" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0603">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.8243" x2="1.473" y2="0.8243" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.8243" x2="1.473" y2="-0.8243" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.8243" x2="-1.473" y2="-0.8243" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.8243" x2="-1.473" y2="0.8243" width="0.0508" layer="39"/>
<wire x1="-1.5288" y1="-0.7938" x2="1.5288" y2="-0.7938" width="0.0762" layer="21"/>
<wire x1="1.5288" y1="-0.7938" x2="1.5288" y2="0.7938" width="0.0762" layer="21"/>
<wire x1="1.5288" y1="0.7938" x2="-1.5288" y2="0.7938" width="0.0762" layer="21"/>
<wire x1="-1.5288" y1="0.7938" x2="-1.5288" y2="-0.7938" width="0.0762" layer="21"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-0.889" y="0.889" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.889" y="-2.032" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.8936" y1="0.983" x2="1.8936" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.8936" y1="0.983" x2="1.8936" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.8936" y1="-0.983" x2="-1.8936" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.8936" y1="-0.983" x2="-1.8936" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.762" y="1.016" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.762" y="-2.286" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R0805W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip, wave soldering</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.94" y="0" dx="1.5" dy="1" layer="1"/>
<smd name="2" x="0.94" y="0" dx="1.5" dy="1" layer="1"/>
<text x="-0.635" y="1.016" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.159" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R1005">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.762" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.032" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="R1206">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.397" y="1.143" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.397" y="-2.413" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1206W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip, wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="21"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="21"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.651" y="1.143" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.651" y="-2.413" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1210">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-1.27" y="1.651" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.921" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8999" x2="0.3" y2="0.8999" layer="35"/>
</package>
<package name="R1210W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip, wave soldering</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="21"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="21"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<text x="-1.651" y="1.524" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.651" y="-2.794" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-1.027" y1="1.245" x2="1.027" y2="1.245" width="0.1524" layer="21"/>
<wire x1="-1.002" y1="-1.245" x2="1.016" y2="-1.245" width="0.1524" layer="21"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-2.159" y="1.651" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.159" y="-2.921" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2010W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip, wave soldering</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="21"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="21"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.311" y="0" dx="2" dy="1.8" layer="1"/>
<smd name="2" x="2.311" y="0" dx="2" dy="1.8" layer="1"/>
<text x="-2.286" y="1.524" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.286" y="-2.921" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2012">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.762" y="1.016" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.762" y="-2.286" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
</package>
<package name="R2012W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip, wave soldering</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.94" y="0" dx="1.5" dy="1" layer="1"/>
<smd name="2" x="0.94" y="0" dx="1.5" dy="1" layer="1"/>
<text x="-0.635" y="1.016" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.286" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
</package>
<package name="R2512">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="1.473" x2="1.498" y2="1.473" width="0.1524" layer="21"/>
<wire x1="-1.473" y1="-1.473" x2="1.498" y2="-1.473" width="0.1524" layer="21"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.667" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.667" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R2512W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip, wave soldering</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="21"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="21"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.896" y="0" dx="2" dy="2.1" layer="1"/>
<smd name="2" x="2.896" y="0" dx="2" dy="2.1" layer="1"/>
<text x="-2.794" y="1.778" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.794" y="-3.048" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R3216">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.397" y="1.143" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.397" y="-2.413" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R3216W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip, wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="21"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="21"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.524" y="1.143" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.524" y="-2.413" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R3225">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-1.397" y="1.651" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.397" y="-2.921" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="R3225W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip, wave soldering</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="21"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="21"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<text x="-1.397" y="1.524" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.397" y="-2.921" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="R5025">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-0.9" y1="1.245" x2="0.9" y2="1.245" width="0.1524" layer="21"/>
<wire x1="-0.875" y1="-1.245" x2="0.925" y2="-1.245" width="0.1524" layer="21"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-2.159" y="1.651" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.159" y="-2.921" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R5025W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip, wave soldering</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="21"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="21"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.311" y="0" dx="2" dy="1.8" layer="1"/>
<smd name="2" x="2.311" y="0" dx="2" dy="1.8" layer="1"/>
<text x="-2.286" y="1.651" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.286" y="-2.921" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R6332">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="1.473" x2="1.498" y2="1.473" width="0.1524" layer="21"/>
<wire x1="-1.473" y1="-1.473" x2="1.498" y2="-1.473" width="0.1524" layer="21"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.794" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.794" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R6332W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip, wave soldering</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="21"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="21"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.896" y="0" dx="2" dy="2.1" layer="1"/>
<smd name="2" x="2.896" y="0" dx="2" dy="2.1" layer="1"/>
<text x="-2.921" y="1.778" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.921" y="-3.048" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="M0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.10 W</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="0.7112" y1="0.635" x2="-0.7112" y2="0.635" width="0.1524" layer="51"/>
<wire x1="0.7112" y1="-0.635" x2="-0.7112" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1.016" y="1.016" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.286" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0414" y1="-0.7112" x2="-0.6858" y2="0.7112" layer="51"/>
<rectangle x1="0.6858" y1="-0.7112" x2="1.0414" y2="0.7112" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5999" x2="0.1999" y2="0.5999" layer="35"/>
</package>
<package name="M1206">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.8382" x2="-1.143" y2="0.8382" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.8382" x2="-1.143" y2="-0.8382" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.9144" x2="-1.1176" y2="0.9144" layer="51"/>
<rectangle x1="1.1176" y1="-0.9144" x2="1.7018" y2="0.9144" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="M1406">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.12 W</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.3208" y1="0.762" x2="-1.3208" y2="0.762" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.762" x2="-1.3208" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="0.6858" y1="0.762" x2="-0.6858" y2="0.762" width="0.1524" layer="21"/>
<wire x1="0.6858" y1="-0.762" x2="-0.6858" y2="-0.762" width="0.1524" layer="21"/>
<smd name="1" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-1.651" y="1.143" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.651" y="-2.413" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8382" x2="-1.2954" y2="0.8382" layer="51"/>
<rectangle x1="1.2954" y1="-0.8382" x2="1.8542" y2="0.8382" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="M2012">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.10 W</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="0.7112" y1="0.635" x2="-0.7112" y2="0.635" width="0.1524" layer="51"/>
<wire x1="0.7112" y1="-0.635" x2="-0.7112" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1.016" y="1.016" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.286" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0414" y1="-0.7112" x2="-0.6858" y2="0.7112" layer="51"/>
<rectangle x1="0.6858" y1="-0.7112" x2="1.0414" y2="0.7112" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5999" x2="0.1999" y2="0.5999" layer="35"/>
</package>
<package name="M2309">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-4.473" y1="1.483" x2="4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="-1.483" x2="-4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-4.473" y1="-1.483" x2="-4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="1.483" x2="4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.413" y1="1.1684" x2="-2.4384" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.1684" x2="-2.413" y2="-1.1684" width="0.1524" layer="51"/>
<wire x1="1.651" y1="1.1684" x2="-1.6764" y2="1.1684" width="0.1524" layer="21"/>
<wire x1="1.651" y1="-1.1684" x2="-1.651" y2="-1.1684" width="0.1524" layer="21"/>
<smd name="1" x="-2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<smd name="2" x="2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<text x="-2.794" y="1.651" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.794" y="-2.794" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.048" y1="-1.2446" x2="-2.3876" y2="1.2446" layer="51"/>
<rectangle x1="2.3876" y1="-1.2446" x2="3.048" y2="1.2446" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="M3216">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.8382" x2="-1.143" y2="0.8382" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.8382" x2="-1.143" y2="-0.8382" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.9144" x2="-1.1176" y2="0.9144" layer="51"/>
<rectangle x1="1.1176" y1="-0.9144" x2="1.7018" y2="0.9144" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="M3516">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.12 W</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.3208" y1="0.762" x2="-1.3208" y2="0.762" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.762" x2="-1.3208" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="0.6858" y1="0.762" x2="-0.6858" y2="0.762" width="0.1524" layer="21"/>
<wire x1="0.6858" y1="-0.762" x2="-0.6858" y2="-0.762" width="0.1524" layer="21"/>
<smd name="1" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-1.651" y="1.143" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.651" y="-2.413" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8382" x2="-1.2954" y2="0.8382" layer="51"/>
<rectangle x1="1.2954" y1="-0.8382" x2="1.8542" y2="0.8382" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="M5923">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-4.473" y1="1.483" x2="4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="-1.483" x2="-4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-4.473" y1="-1.483" x2="-4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="1.483" x2="4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.413" y1="1.1684" x2="-2.4384" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.1684" x2="-2.413" y2="-1.1684" width="0.1524" layer="51"/>
<wire x1="1.651" y1="1.1684" x2="-1.6764" y2="1.1684" width="0.1524" layer="21"/>
<wire x1="1.651" y1="-1.1684" x2="-1.651" y2="-1.1684" width="0.1524" layer="21"/>
<smd name="1" x="-2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<smd name="2" x="2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<text x="-2.794" y="1.651" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.794" y="-2.794" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.048" y1="-1.2446" x2="-2.3876" y2="1.2446" layer="51"/>
<rectangle x1="2.3876" y1="-1.2446" x2="3.048" y2="1.2446" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="0204/5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0" x2="-2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-1.778" y1="0.635" x2="-1.524" y2="0.889" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="-1.778" y1="-0.635" x2="-1.524" y2="-0.889" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="1.524" y1="-0.889" x2="1.778" y2="-0.635" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="1.524" y1="0.889" x2="1.778" y2="0.635" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="-1.778" y1="-0.635" x2="-1.778" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-1.524" y1="0.889" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.762" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-0.889" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-0.762" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="-1.143" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="-1.143" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.524" y1="0.889" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-0.889" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.635" x2="1.778" y2="0.635" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.0066" y="1.1684" size="0.9906" layer="25" ratio="12">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="0.9906" layer="27" ratio="12">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-0.254" x2="-1.778" y2="0.254" layer="51"/>
<rectangle x1="1.778" y1="-0.254" x2="2.032" y2="0.254" layer="51"/>
</package>
<package name="0204/7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 7.5 mm</description>
<wire x1="3.81" y1="0" x2="2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-3.81" y1="0" x2="-2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0.762" x2="-2.286" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.286" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.016" x2="2.54" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="1.016" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="0.889" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.016" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="-0.889" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="-1.778" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="-1.778" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="2.286" y1="1.016" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.286" y1="-1.016" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="2.54" y2="0.762" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.2954" size="0.9906" layer="25" ratio="12">&gt;NAME</text>
<text x="-1.6256" y="-0.4826" size="0.9906" layer="27" ratio="12">&gt;VALUE</text>
<rectangle x1="2.54" y1="-0.254" x2="2.921" y2="0.254" layer="21"/>
<rectangle x1="-2.921" y1="-0.254" x2="-2.54" y2="0.254" layer="21"/>
</package>
<package name="0204V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.508" layer="51"/>
<wire x1="-0.127" y1="0" x2="0.127" y2="0" width="0.508" layer="21"/>
<circle x="-1.27" y="0" radius="0.889" width="0.1524" layer="51"/>
<circle x="-1.27" y="0" radius="0.635" width="0.0508" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.1336" y="1.1684" size="1.27" layer="25" ratio="12">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="1.27" layer="27" ratio="12">&gt;VALUE</text>
</package>
<package name="0207/10">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 10 mm</description>
<wire x1="5.08" y1="0" x2="4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="0" x2="-4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.048" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.2606" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
</package>
<package name="0207/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 12 mm</description>
<wire x1="6.35" y1="0" x2="5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="4.445" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-4.445" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.3086" y2="0.3048" layer="21"/>
<rectangle x1="-5.3086" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
</package>
<package name="0207/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 15mm</description>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="5.715" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-5.715" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="5.715" y1="-0.3048" x2="6.5786" y2="0.3048" layer="21"/>
<rectangle x1="-6.5786" y1="-0.3048" x2="-5.715" y2="0.3048" layer="21"/>
</package>
<package name="0207/2V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="-0.381" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.254" y1="0" x2="0.254" y2="0" width="0.6096" layer="21"/>
<wire x1="0.381" y1="0" x2="1.27" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.27" width="0.1524" layer="21"/>
<circle x="-1.27" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-0.0508" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.0508" y="-2.2352" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/5V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 5 mm</description>
<wire x1="-2.54" y1="0" x2="-0.889" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.762" y1="0" x2="0.762" y2="0" width="0.6096" layer="21"/>
<wire x1="0.889" y1="0" x2="2.54" y2="0" width="0.6096" layer="51"/>
<circle x="-2.54" y="0" radius="1.27" width="0.1016" layer="21"/>
<circle x="-2.54" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.143" y="0.889" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.143" y="-2.159" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 7.5 mm</description>
<wire x1="-3.81" y1="0" x2="-3.429" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="3.429" y1="0" x2="3.81" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.5588" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.429" y1="-0.3048" x2="-3.175" y2="0.3048" layer="51"/>
<rectangle x1="3.175" y1="-0.3048" x2="3.429" y2="0.3048" layer="51"/>
</package>
<package name="0309/10">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 10mm</description>
<wire x1="-4.699" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="5.08" y1="0" x2="4.699" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-4.191" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.6228" y1="-0.3048" x2="-4.318" y2="0.3048" layer="51"/>
<rectangle x1="4.318" y1="-0.3048" x2="4.6228" y2="0.3048" layer="51"/>
</package>
<package name="0309/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-4.191" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.318" y1="-0.3048" x2="5.1816" y2="0.3048" layer="21"/>
<rectangle x1="-5.1816" y1="-0.3048" x2="-4.318" y2="0.3048" layer="21"/>
</package>
<package name="0309V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 2.5 mm</description>
<wire x1="1.27" y1="0" x2="0.635" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.524" width="0.1524" layer="21"/>
<circle x="-1.27" y="0" radius="0.762" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="0.254" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.254" y="-2.2098" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.254" y1="-0.3048" x2="0.5588" y2="0.3048" layer="51"/>
<rectangle x1="-0.635" y1="-0.3048" x2="-0.3302" y2="0.3048" layer="51"/>
<rectangle x1="-0.3302" y1="-0.3048" x2="0.254" y2="0.3048" layer="21"/>
</package>
<package name="0411/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.461" y2="0" width="0.762" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.461" y2="0" width="0.762" layer="51"/>
<wire x1="5.08" y1="-1.651" x2="5.08" y2="1.651" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.032" x2="5.08" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.651" x2="-4.699" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="4.699" y1="-2.032" x2="5.08" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.651" x2="-4.699" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="2.032" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="1.905" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.032" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="-1.905" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="3.937" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="3.937" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.651" x2="-5.08" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="2.032" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-2.032" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.9144" shape="octagon"/>
<text x="-5.08" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.5814" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.3594" y1="-0.381" x2="-5.08" y2="0.381" layer="21"/>
<rectangle x1="5.08" y1="-0.381" x2="5.3594" y2="0.381" layer="21"/>
</package>
<package name="0411/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 15 mm</description>
<wire x1="5.08" y1="-1.651" x2="5.08" y2="1.651" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.032" x2="5.08" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.651" x2="-4.699" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="4.699" y1="-2.032" x2="5.08" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.651" x2="-4.699" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="2.032" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="1.905" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.032" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="-1.905" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="3.937" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="3.937" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.651" x2="-5.08" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="2.032" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-2.032" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0" x2="-6.35" y2="0" width="0.762" layer="51"/>
<wire x1="6.35" y1="0" x2="7.62" y2="0" width="0.762" layer="51"/>
<pad name="1" x="-7.62" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.9144" shape="octagon"/>
<text x="-5.08" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.5814" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="5.08" y1="-0.381" x2="6.477" y2="0.381" layer="21"/>
<rectangle x1="-6.477" y1="-0.381" x2="-5.08" y2="0.381" layer="21"/>
</package>
<package name="0411V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 3.81 mm</description>
<wire x1="1.27" y1="0" x2="0.3048" y2="0" width="0.762" layer="51"/>
<wire x1="-1.5748" y1="0" x2="-2.54" y2="0" width="0.762" layer="51"/>
<circle x="-2.54" y="0" radius="2.032" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.9144" shape="octagon"/>
<text x="-0.508" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.5334" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.4732" y1="-0.381" x2="0.2032" y2="0.381" layer="21"/>
</package>
<package name="0414/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0414, grid 15 mm</description>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.096" y1="1.905" x2="-5.842" y2="2.159" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.096" y1="-1.905" x2="-5.842" y2="-2.159" width="0.1524" layer="21" curve="90"/>
<wire x1="5.842" y1="-2.159" x2="6.096" y2="-1.905" width="0.1524" layer="21" curve="90"/>
<wire x1="5.842" y1="2.159" x2="6.096" y2="1.905" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.096" y1="-1.905" x2="-6.096" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="2.159" x2="-4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="2.032" x2="-4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="-2.159" x2="-4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-2.032" x2="-4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="-4.826" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-2.032" x2="4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-2.032" x2="-4.826" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="5.842" y1="2.159" x2="4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="5.842" y1="-2.159" x2="4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-1.905" x2="6.096" y2="1.905" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.5654" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="6.096" y1="-0.4064" x2="6.5024" y2="0.4064" layer="21"/>
<rectangle x1="-6.5024" y1="-0.4064" x2="-6.096" y2="0.4064" layer="21"/>
</package>
<package name="0414V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0414, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-2.54" y1="0" x2="-1.397" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="2.159" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.381" y="1.1684" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.381" y="-2.3622" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.2954" y1="-0.4064" x2="1.2954" y2="0.4064" layer="21"/>
</package>
<package name="0617/17">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 17.5 mm</description>
<wire x1="-8.89" y1="0" x2="-8.636" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.874" y1="3.048" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="2.794" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-3.048" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-2.794" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="-6.731" y2="2.794" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="-6.731" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="7.874" y1="3.048" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-3.048" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="-2.667" x2="-8.255" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="1.016" x2="-8.255" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-8.255" y1="1.016" x2="-8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.667" x2="8.255" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="8.255" y1="1.016" x2="8.255" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="8.255" y1="1.016" x2="8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="8.636" y1="0" x2="8.89" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="2.667" x2="-7.874" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="7.874" y1="3.048" x2="8.255" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.255" y1="-2.667" x2="-7.874" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="7.874" y1="-3.048" x2="8.255" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-8.89" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="8.89" y="0" drill="1.016" shape="octagon"/>
<text x="-8.128" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.096" y="-0.7112" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-8.5344" y1="-0.4064" x2="-8.2296" y2="0.4064" layer="51"/>
<rectangle x1="8.2296" y1="-0.4064" x2="8.5344" y2="0.4064" layer="51"/>
</package>
<package name="0617/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 22.5 mm</description>
<wire x1="-10.287" y1="0" x2="-11.43" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="-2.667" x2="-8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="3.048" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="2.794" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-3.048" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-2.794" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="-6.731" y2="2.794" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="-6.731" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="7.874" y1="3.048" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-3.048" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.667" x2="8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="11.43" y1="0" x2="10.287" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="2.667" x2="-7.874" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.255" y1="-2.667" x2="-7.874" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="7.874" y1="3.048" x2="8.255" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="7.874" y1="-3.048" x2="8.255" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-8.255" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.477" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.1854" y1="-0.4064" x2="-8.255" y2="0.4064" layer="21"/>
<rectangle x1="8.255" y1="-0.4064" x2="10.1854" y2="0.4064" layer="21"/>
</package>
<package name="0617V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 5 mm</description>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.8128" layer="51"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="3.048" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="0.635" y="1.4224" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.635" y="-2.6162" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.3208" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="0922/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0922, grid 22.5 mm</description>
<wire x1="11.43" y1="0" x2="10.795" y2="0" width="0.8128" layer="51"/>
<wire x1="-11.43" y1="0" x2="-10.795" y2="0" width="0.8128" layer="51"/>
<wire x1="-10.16" y1="-4.191" x2="-10.16" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-9.779" y1="4.572" x2="-8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-8.636" y1="4.318" x2="-8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-9.779" y1="-4.572" x2="-8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="-8.636" y1="-4.318" x2="-8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="4.318" x2="8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="4.318" x2="-8.636" y2="4.318" width="0.1524" layer="21"/>
<wire x1="8.636" y1="-4.318" x2="8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="-4.318" x2="-8.636" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="9.779" y1="4.572" x2="8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="9.779" y1="-4.572" x2="8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-4.191" x2="10.16" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-4.191" x2="-9.779" y2="-4.572" width="0.1524" layer="21" curve="90"/>
<wire x1="-10.16" y1="4.191" x2="-9.779" y2="4.572" width="0.1524" layer="21" curve="-90"/>
<wire x1="9.779" y1="-4.572" x2="10.16" y2="-4.191" width="0.1524" layer="21" curve="90"/>
<wire x1="9.779" y1="4.572" x2="10.16" y2="4.191" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-10.16" y="5.1054" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.477" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.7188" y1="-0.4064" x2="-10.16" y2="0.4064" layer="51"/>
<rectangle x1="10.16" y1="-0.4064" x2="10.3124" y2="0.4064" layer="21"/>
<rectangle x1="-10.3124" y1="-0.4064" x2="-10.16" y2="0.4064" layer="21"/>
<rectangle x1="10.16" y1="-0.4064" x2="10.7188" y2="0.4064" layer="51"/>
</package>
<package name="P0613V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0613, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-2.54" y1="0" x2="-1.397" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="2.286" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.254" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.254" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.2954" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="P0613/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0613, grid 15 mm</description>
<wire x1="7.62" y1="0" x2="6.985" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.985" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.477" y1="2.032" x2="-6.223" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.477" y1="-2.032" x2="-6.223" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="6.223" y1="-2.286" x2="6.477" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="6.223" y1="2.286" x2="6.477" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.223" y1="2.286" x2="-5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.159" x2="-5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-6.223" y1="-2.286" x2="-5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="-2.159" x2="-5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="2.159" x2="5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="2.159" x2="-5.207" y2="2.159" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.159" x2="5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.159" x2="-5.207" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="6.223" y1="2.286" x2="5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="6.223" y1="-2.286" x2="5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-0.635" x2="6.477" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-0.635" x2="6.477" y2="0.635" width="0.1524" layer="51"/>
<wire x1="6.477" y1="2.032" x2="6.477" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-2.032" x2="-6.477" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="0.635" x2="-6.477" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="0.635" x2="-6.477" y2="2.032" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.477" y="2.6924" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.7112" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-7.0358" y1="-0.4064" x2="-6.477" y2="0.4064" layer="51"/>
<rectangle x1="6.477" y1="-0.4064" x2="7.0358" y2="0.4064" layer="51"/>
</package>
<package name="P0817/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0817, grid 22.5 mm</description>
<wire x1="-10.414" y1="0" x2="-11.43" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.509" y1="-3.429" x2="-8.509" y2="3.429" width="0.1524" layer="21"/>
<wire x1="-8.128" y1="3.81" x2="-7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="3.556" x2="-7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-8.128" y1="-3.81" x2="-7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-3.556" x2="-7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.556" x2="7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.556" x2="-6.985" y2="3.556" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-3.556" x2="7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-3.556" x2="-6.985" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="8.128" y1="3.81" x2="7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="8.128" y1="-3.81" x2="7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.429" x2="8.509" y2="3.429" width="0.1524" layer="21"/>
<wire x1="11.43" y1="0" x2="10.414" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.509" y1="3.429" x2="-8.128" y2="3.81" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.509" y1="-3.429" x2="-8.128" y2="-3.81" width="0.1524" layer="21" curve="90"/>
<wire x1="8.128" y1="3.81" x2="8.509" y2="3.429" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.128" y1="-3.81" x2="8.509" y2="-3.429" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-8.382" y="4.2164" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.223" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="6.604" y="-2.2606" size="1.27" layer="51" ratio="10" rot="R90">0817</text>
<rectangle x1="8.509" y1="-0.4064" x2="10.3124" y2="0.4064" layer="21"/>
<rectangle x1="-10.3124" y1="-0.4064" x2="-8.509" y2="0.4064" layer="21"/>
</package>
<package name="P0817V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0817, grid 6.35 mm</description>
<wire x1="-3.81" y1="0" x2="-5.08" y2="0" width="0.8128" layer="51"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.8128" layer="51"/>
<circle x="-5.08" y="0" radius="3.81" width="0.1524" layer="21"/>
<circle x="-5.08" y="0" radius="1.27" width="0.1524" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="1.016" shape="octagon"/>
<text x="-1.016" y="1.27" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.016" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.858" y="2.032" size="1.016" layer="21" ratio="12">0817</text>
<rectangle x1="-3.81" y1="-0.4064" x2="0" y2="0.4064" layer="21"/>
</package>
<package name="V234/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V234, grid 12.5 mm</description>
<wire x1="-4.953" y1="1.524" x2="-4.699" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="1.778" x2="4.953" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.778" x2="4.953" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.953" y1="-1.524" x2="-4.699" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="1.778" x2="4.699" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="1.524" x2="-4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.778" x2="-4.699" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="4.953" y1="1.524" x2="4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="6.35" y1="0" x2="5.461" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.461" y2="0" width="0.8128" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.016" shape="octagon"/>
<text x="-4.953" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.953" y1="-0.4064" x2="5.4102" y2="0.4064" layer="21"/>
<rectangle x1="-5.4102" y1="-0.4064" x2="-4.953" y2="0.4064" layer="21"/>
</package>
<package name="V235/17">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V235, grid 17.78 mm</description>
<wire x1="-6.731" y1="2.921" x2="6.731" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-7.112" y1="2.54" x2="-7.112" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.921" x2="-6.731" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="7.112" y1="2.54" x2="7.112" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="8.89" y1="0" x2="7.874" y2="0" width="1.016" layer="51"/>
<wire x1="-7.874" y1="0" x2="-8.89" y2="0" width="1.016" layer="51"/>
<wire x1="-7.112" y1="-2.54" x2="-6.731" y2="-2.921" width="0.1524" layer="21" curve="90"/>
<wire x1="6.731" y1="2.921" x2="7.112" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.731" y1="-2.921" x2="7.112" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-7.112" y1="2.54" x2="-6.731" y2="2.921" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-8.89" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="8.89" y="0" drill="1.1938" shape="octagon"/>
<text x="-6.858" y="3.302" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.842" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="7.112" y1="-0.508" x2="7.747" y2="0.508" layer="21"/>
<rectangle x1="-7.747" y1="-0.508" x2="-7.112" y2="0.508" layer="21"/>
</package>
<package name="V526-0">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V526-0, grid 2.5 mm</description>
<wire x1="-2.54" y1="1.016" x2="-2.286" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="1.27" x2="2.54" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="-1.27" x2="2.54" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.54" y1="-1.016" x2="-2.286" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.27" x2="-2.286" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.016" x2="2.54" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.27" x2="2.286" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.016" x2="-2.54" y2="-1.016" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.413" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.413" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0102R">
<description>&lt;b&gt;CECC Size RC2211&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1" y1="-0.5" x2="1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.5" x2="1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="0.5" x2="-1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="1" x="-0.9" y="0" dx="0.5" dy="1.3" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.5" dy="1.3" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0102W">
<description>&lt;b&gt;CECC Size RC2211&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1" y1="-0.5" x2="1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.5" x2="1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="0.5" x2="-1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="0.6" dy="1.3" layer="1"/>
<smd name="2" x="0.95" y="0" dx="0.6" dy="1.3" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0204R">
<description>&lt;b&gt;CECC Size RC3715&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1.7" y1="-0.6" x2="1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="-0.6" x2="1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="0.6" x2="-1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.6" x2="-1.7" y2="-0.6" width="0.2032" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="0.8" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="0.8" dy="1.6" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0204W">
<description>&lt;b&gt;CECC Size RC3715&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1.7" y1="-0.6" x2="1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="-0.6" x2="1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="0.6" x2="-1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.6" x2="-1.7" y2="-0.6" width="0.2032" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0207R">
<description>&lt;b&gt;CECC Size RC6123&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-2.8" y1="-1" x2="2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="1" x2="-2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1" x2="-2.8" y2="-1" width="0.2032" layer="51"/>
<smd name="1" x="-2.25" y="0" dx="1.6" dy="2.5" layer="1"/>
<smd name="2" x="2.25" y="0" dx="1.6" dy="2.5" layer="1"/>
<text x="-2.2225" y="1.5875" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.2225" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0207W">
<description>&lt;b&gt;CECC Size RC6123&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-2.8" y1="-1" x2="2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="1" x2="-2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1" x2="-2.8" y2="-1" width="0.2032" layer="51"/>
<smd name="1" x="-2.6" y="0" dx="2.4" dy="2.5" layer="1"/>
<smd name="2" x="2.6" y="0" dx="2.4" dy="2.5" layer="1"/>
<text x="-2.54" y="1.5875" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="0922V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0922, grid 7.5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-5.08" y1="0" x2="-3.81" y2="0" width="0.8128" layer="51"/>
<circle x="-5.08" y="0" radius="4.572" width="0.1524" layer="21"/>
<circle x="-5.08" y="0" radius="1.905" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.508" y="1.6764" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.508" y="-2.9972" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.858" y="2.54" size="1.016" layer="21" ratio="12">0922</text>
<rectangle x1="-3.81" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="RDH/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type RDH, grid 15 mm</description>
<wire x1="-7.62" y1="0" x2="-6.858" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.096" y1="3.048" x2="-5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="2.794" x2="-5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="-3.048" x2="-5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-2.794" x2="-5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.794" x2="5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.794" x2="-4.953" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.953" y1="-2.794" x2="5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="-2.794" x2="-4.953" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="6.096" y1="3.048" x2="5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-3.048" x2="5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-2.667" x2="-6.477" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="1.016" x2="-6.477" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="1.016" x2="-6.477" y2="2.667" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-2.667" x2="6.477" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="6.477" y1="1.016" x2="6.477" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="6.477" y1="1.016" x2="6.477" y2="2.667" width="0.1524" layer="21"/>
<wire x1="6.858" y1="0" x2="7.62" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.477" y1="2.667" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="3.048" x2="6.477" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.477" y1="-2.667" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="6.096" y1="-3.048" x2="6.477" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.35" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="4.572" y="-1.7272" size="1.27" layer="51" ratio="10" rot="R90">RDH</text>
<rectangle x1="-6.7564" y1="-0.4064" x2="-6.4516" y2="0.4064" layer="51"/>
<rectangle x1="6.4516" y1="-0.4064" x2="6.7564" y2="0.4064" layer="51"/>
</package>
<package name="MINI_MELF-0102AX">
<description>&lt;b&gt;Mini MELF 0102 Axial&lt;/b&gt;</description>
<circle x="0" y="0" radius="0.6" width="0" layer="51"/>
<circle x="0" y="0" radius="0.6" width="0" layer="52"/>
<smd name="1" x="0" y="0" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="2" x="0" y="0" dx="1.9" dy="1.9" layer="16" roundness="100"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
<hole x="0" y="0" drill="1.3"/>
</package>
<package name="DOUBLE_BRIDGE">
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.8243" x2="1.473" y2="0.8243" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.8243" x2="1.473" y2="-0.8243" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.8243" x2="-1.473" y2="-0.8243" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.8243" x2="-1.473" y2="0.8243" width="0.0508" layer="39"/>
<wire x1="-1.5288" y1="-0.7938" x2="1.5288" y2="-0.7938" width="0.0762" layer="21"/>
<wire x1="1.5288" y1="-0.7938" x2="1.5288" y2="0.7938" width="0.0762" layer="21"/>
<wire x1="1.5288" y1="0.7938" x2="-1.5288" y2="0.7938" width="0.0762" layer="21"/>
<wire x1="-1.5288" y1="0.7938" x2="-1.5288" y2="-0.7938" width="0.0762" layer="21"/>
<smd name="1" x="-0.85" y="0" dx="0.9" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="0.9" dy="1.1" layer="1"/>
<text x="-0.889" y="0.889" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.889" y="-2.032" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
</package>
<package name="SOT663">
<wire x1="0.8" y1="0.6" x2="0.8" y2="-0.6" width="0.127" layer="21"/>
<wire x1="0.8" y1="-0.6" x2="-0.8" y2="-0.6" width="0.127" layer="21"/>
<wire x1="-0.8" y1="-0.6" x2="-0.8" y2="0.6" width="0.127" layer="21"/>
<wire x1="-0.8" y1="0.6" x2="0.8" y2="0.6" width="0.127" layer="21"/>
<smd name="2" x="0.5" y="-0.75" dx="0.6" dy="0.4" layer="1" rot="R90"/>
<smd name="1" x="-0.5" y="-0.75" dx="0.6" dy="0.4" layer="1" rot="R90"/>
<smd name="3" x="0" y="0.75" dx="0.6" dy="0.4" layer="1" rot="R90"/>
<text x="-1.905" y="-2.54" size="1.016" layer="25" font="vector" ratio="12">&gt;NAME</text>
<text x="-1.905" y="-3.81" size="1.016" layer="27" font="vector">&gt;VALUE</text>
</package>
<package name="L2012C">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.864" y1="0.54" x2="0.864" y2="0.54" width="0.1016" layer="51"/>
<wire x1="-0.864" y1="-0.553" x2="0.864" y2="-0.553" width="0.1016" layer="51"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1" y="0" dx="1" dy="1" layer="1"/>
<smd name="2" x="1" y="0" dx="1" dy="1" layer="1"/>
<text x="-1.016" y="0.889" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.159" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.143" y1="-0.6096" x2="-0.843" y2="0.5903" layer="51"/>
<rectangle x1="0.8382" y1="-0.6096" x2="1.1382" y2="0.5903" layer="51"/>
<rectangle x1="-0.3" y1="-0.4001" x2="0.3" y2="0.4001" layer="35"/>
</package>
<package name="L2825P">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;&lt;p&gt;
precision wire wound</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-0.762" y1="1.2" x2="0.762" y2="1.2" width="0.1016" layer="51"/>
<wire x1="-0.762" y1="-1.213" x2="0.762" y2="-1.213" width="0.1016" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="1.34" y1="-0.965" x2="1.34" y2="0.965" width="0.1016" layer="51"/>
<wire x1="-1.34" y1="0.965" x2="-1.34" y2="-0.965" width="0.1016" layer="51"/>
<circle x="0" y="0" radius="0.6604" width="0.1524" layer="51"/>
<smd name="1" x="-1.2" y="0" dx="1.4" dy="2.4" layer="1"/>
<smd name="2" x="1.2" y="0" dx="1.4" dy="2.4" layer="1"/>
<text x="-1.27" y="1.524" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.794" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.7366" y1="-1.27" x2="1.3208" y2="1.27" layer="51"/>
<rectangle x1="-1.3208" y1="-1.27" x2="-0.7366" y2="1.27" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="L3216C">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-1.27" y1="0.896" x2="1.27" y2="0.896" width="0.1016" layer="51"/>
<wire x1="-1.27" y1="-0.883" x2="1.27" y2="-0.883" width="0.1016" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-0.762" y1="0.896" x2="0.762" y2="0.896" width="0.1016" layer="21"/>
<wire x1="-0.762" y1="-0.883" x2="0.762" y2="-0.883" width="0.1016" layer="21"/>
<smd name="1" x="-1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<text x="-1.524" y="1.143" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.524" y="-2.413" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7526" y1="-0.9525" x2="-1.2525" y2="0.9474" layer="51"/>
<rectangle x1="1.2446" y1="-0.9525" x2="1.7447" y2="0.9474" layer="51"/>
<rectangle x1="-0.4001" y1="-0.5999" x2="0.4001" y2="0.5999" layer="35"/>
</package>
<package name="L3225M">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;&lt;p&gt;
molded</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-1.575" y1="1.27" x2="1.575" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.575" y1="1.27" x2="1.575" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.575" y1="-1.27" x2="-1.575" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.575" y1="-1.27" x2="-1.575" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-1.651" y1="0.94" x2="-1.651" y2="-0.94" width="0.1524" layer="51"/>
<wire x1="1.651" y1="0.94" x2="1.651" y2="-0.94" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.2" layer="1"/>
<text x="-1.397" y="1.524" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.397" y="-2.794" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="L3225P">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;&lt;p&gt;
precision wire wound</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-1.676" y1="0.845" x2="1.676" y2="0.845" width="0.1524" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-1.676" y1="0.838" x2="-1.676" y2="-0.838" width="0.1524" layer="51"/>
<wire x1="-1.168" y1="0.838" x2="-1.168" y2="-0.838" width="0.1524" layer="51"/>
<wire x1="1.168" y1="0.838" x2="1.168" y2="-0.838" width="0.1524" layer="51"/>
<wire x1="1.676" y1="0.838" x2="1.676" y2="-0.838" width="0.1524" layer="51"/>
<wire x1="1.676" y1="-0.845" x2="-1.676" y2="-0.845" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="0.7117" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.8" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.8" dy="2" layer="1"/>
<text x="-1.397" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.397" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="L3230M">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;&lt;p&gt;
molded</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-1.575" y1="1.27" x2="1.575" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.575" y1="1.27" x2="1.575" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.575" y1="-1.27" x2="-1.575" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.575" y1="-1.27" x2="-1.575" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-1.651" y1="0.94" x2="-1.651" y2="-0.94" width="0.1524" layer="51"/>
<wire x1="1.651" y1="0.94" x2="1.651" y2="-0.94" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.2" layer="1"/>
<text x="-1.397" y="1.651" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.397" y="-2.921" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="L4035M">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;&lt;p&gt;
molded</description>
<wire x1="-2.973" y1="1.983" x2="2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-1.983" x2="-2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-1.983" x2="-2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="1.983" x2="2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.083" y1="0.686" x2="-2.083" y2="-0.686" width="0.1524" layer="51"/>
<wire x1="2.083" y1="0.686" x2="2.083" y2="-0.686" width="0.1524" layer="51"/>
<wire x1="-1.981" y1="1.524" x2="-1.981" y2="-1.524" width="0.1524" layer="51"/>
<wire x1="-1.981" y1="-1.524" x2="1.981" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="1.981" y1="-1.524" x2="1.981" y2="1.524" width="0.1524" layer="51"/>
<wire x1="1.981" y1="1.524" x2="-1.981" y2="1.524" width="0.1524" layer="21"/>
<smd name="1" x="-1.6" y="0" dx="2.2" dy="1.4" layer="1"/>
<smd name="2" x="1.6" y="0" dx="2.2" dy="1.4" layer="1"/>
<text x="-1.651" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.651" y="-3.048" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="L4516C">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-1.626" y1="0.54" x2="1.626" y2="0.54" width="0.1016" layer="51"/>
<wire x1="-1.626" y1="-0.527" x2="1.626" y2="-0.527" width="0.1016" layer="51"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.372" y1="0.54" x2="1.372" y2="0.54" width="0.1016" layer="21"/>
<wire x1="-1.372" y1="-0.527" x2="1.372" y2="-0.527" width="0.1016" layer="21"/>
<smd name="1" x="-2.1" y="0" dx="1" dy="1.6" layer="1"/>
<smd name="2" x="2.1" y="0" dx="1" dy="1.6" layer="1"/>
<text x="-2.032" y="1.016" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.032" y="-2.286" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4003" y1="-0.5969" x2="-1.6002" y2="0.603" layer="51"/>
<rectangle x1="1.6002" y1="-0.603" x2="2.4003" y2="0.5969" layer="51"/>
<rectangle x1="-0.7" y1="-0.3" x2="0.7" y2="0.3" layer="35"/>
</package>
<package name="L4532M">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;&lt;p&gt;
molded</description>
<wire x1="-3.473" y1="1.983" x2="3.473" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.983" x2="-3.473" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.983" x2="-3.473" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.983" x2="3.473" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.21" y1="-1.626" x2="2.21" y2="-1.626" width="0.1524" layer="21"/>
<wire x1="2.21" y1="1.626" x2="-2.21" y2="1.626" width="0.1524" layer="21"/>
<wire x1="-2.337" y1="1.041" x2="-2.337" y2="-1.041" width="0.1524" layer="51"/>
<wire x1="2.337" y1="1.041" x2="2.337" y2="-1.041" width="0.1524" layer="51"/>
<wire x1="-2.21" y1="1.626" x2="-2.21" y2="-1.626" width="0.1524" layer="51"/>
<wire x1="2.21" y1="1.626" x2="2.21" y2="-1.626" width="0.1524" layer="51"/>
<smd name="1" x="-1.9" y="0" dx="2" dy="2.4" layer="1"/>
<smd name="2" x="1.9" y="0" dx="2" dy="2.4" layer="1"/>
<text x="-1.905" y="2.032" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.302" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="L4532P">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;&lt;p&gt;
precision wire wound</description>
<wire x1="-2.973" y1="1.983" x2="2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-1.983" x2="-2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-1.983" x2="-2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-2.311" y1="1.675" x2="2.311" y2="1.675" width="0.1524" layer="51"/>
<wire x1="2.973" y1="1.983" x2="2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.311" y1="-1.653" x2="2.311" y2="-1.653" width="0.1524" layer="51"/>
<wire x1="-2.311" y1="1.626" x2="-2.311" y2="-1.626" width="0.1524" layer="51"/>
<wire x1="2.311" y1="1.675" x2="2.311" y2="-1.626" width="0.1524" layer="51"/>
<wire x1="-1.448" y1="1.651" x2="-1.448" y2="-1.626" width="0.1524" layer="51"/>
<wire x1="1.448" y1="1.626" x2="1.448" y2="-1.626" width="0.1524" layer="51"/>
<wire x1="-0.66" y1="1.675" x2="0.66" y2="1.675" width="0.1524" layer="21"/>
<wire x1="-0.66" y1="-1.653" x2="0.66" y2="-1.653" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="1.3211" width="0.1524" layer="51"/>
<smd name="1" x="-2" y="0" dx="1.8" dy="3.6" layer="1"/>
<smd name="2" x="2" y="0" dx="1.8" dy="3.6" layer="1"/>
<text x="-1.905" y="2.032" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.302" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="L5038P">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt; &lt;p&gt;
precision wire wound</description>
<wire x1="-2.973" y1="1.983" x2="2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-1.983" x2="-2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-1.983" x2="-2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-2.286" y1="1.853" x2="2.311" y2="1.853" width="0.1016" layer="21"/>
<wire x1="2.973" y1="1.983" x2="2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.286" y1="-1.856" x2="2.311" y2="-1.856" width="0.1016" layer="21"/>
<wire x1="2.389" y1="-1.27" x2="2.389" y2="1.27" width="0.1016" layer="51"/>
<wire x1="-2.386" y1="-1.27" x2="-2.386" y2="1.27" width="0.1016" layer="51"/>
<wire x1="1.602" y1="-1.854" x2="1.602" y2="1.854" width="0.1016" layer="51"/>
<wire x1="-1.624" y1="-1.854" x2="-1.624" y2="1.854" width="0.1016" layer="51"/>
<wire x1="-2.31" y1="-1.854" x2="-2.31" y2="1.854" width="0.1016" layer="51"/>
<wire x1="2.313" y1="-1.854" x2="2.313" y2="1.854" width="0.1016" layer="51"/>
<circle x="0" y="0" radius="1.4732" width="0.1524" layer="51"/>
<smd name="1" x="-2.2" y="0" dx="1.4" dy="2.8" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.4" dy="2.8" layer="1"/>
<text x="-2.159" y="2.159" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.159" y="-3.429" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="L5650M">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt; &lt;p&gt;
molded</description>
<wire x1="-3.973" y1="2.983" x2="3.973" y2="2.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-2.983" x2="-3.973" y2="-2.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-2.983" x2="-3.973" y2="2.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="2.983" x2="3.973" y2="-2.983" width="0.0508" layer="39"/>
<wire x1="-2.108" y1="-2.591" x2="2.083" y2="-2.591" width="0.1524" layer="21"/>
<wire x1="2.083" y1="2.591" x2="-2.108" y2="2.591" width="0.1524" layer="21"/>
<wire x1="2.184" y1="2.032" x2="2.184" y2="-2.032" width="0.1524" layer="51"/>
<wire x1="-2.21" y1="2.032" x2="-2.21" y2="-2.032" width="0.1524" layer="51"/>
<wire x1="-2.108" y1="2.591" x2="-2.108" y2="-2.591" width="0.1524" layer="51"/>
<wire x1="2.083" y1="2.591" x2="2.083" y2="-2.591" width="0.1524" layer="51"/>
<smd name="1" x="-2.5" y="0" dx="1.8" dy="4" layer="1"/>
<smd name="2" x="2.5" y="0" dx="1.8" dy="4" layer="1"/>
<text x="-2.54" y="2.921" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-4.191" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="L8530M">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt; &lt;p&gt;
molded</description>
<wire x1="-5.473" y1="1.983" x2="5.473" y2="1.983" width="0.0508" layer="39"/>
<wire x1="5.473" y1="-1.983" x2="-5.473" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-5.473" y1="-1.983" x2="-5.473" y2="1.983" width="0.0508" layer="39"/>
<wire x1="5.473" y1="1.983" x2="5.473" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-4.191" y1="-1.524" x2="-4.191" y2="1.524" width="0.1524" layer="51"/>
<wire x1="4.191" y1="1.524" x2="-4.191" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.191" y1="-1.524" x2="-4.191" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-4.293" y1="-0.66" x2="-4.293" y2="0.66" width="0.1524" layer="51"/>
<wire x1="4.293" y1="-0.66" x2="4.293" y2="0.66" width="0.1524" layer="51"/>
<wire x1="4.191" y1="-1.524" x2="4.191" y2="1.524" width="0.1524" layer="51"/>
<smd name="1" x="-3.7" y="0" dx="2.4" dy="1.4" layer="1"/>
<smd name="2" x="3.7" y="0" dx="2.4" dy="1.4" layer="1"/>
<text x="-3.683" y="1.778" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.683" y="-3.048" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="L1812">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
</package>
<package name="TJ3-U1">
<description>&lt;b&gt;Inductor&lt;/b&gt;&lt;p&gt;
Source: TJ-Serie Vishay.pdf</description>
<wire x1="-3.65" y1="8.15" x2="3.65" y2="8.15" width="0.2032" layer="21"/>
<wire x1="3.65" y1="-8.15" x2="-3.65" y2="-8.15" width="0.2032" layer="21"/>
<wire x1="-3.65" y1="-8.15" x2="-3.65" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="-3.65" y1="-1.1" x2="-3.65" y2="1.1" width="0.2032" layer="51"/>
<wire x1="-3.65" y1="1.1" x2="-3.65" y2="8.15" width="0.2032" layer="21"/>
<wire x1="3.65" y1="8.15" x2="3.65" y2="1.1" width="0.2032" layer="21"/>
<wire x1="3.65" y1="1.1" x2="3.65" y2="-1.1" width="0.2032" layer="51"/>
<wire x1="3.65" y1="-1.1" x2="3.65" y2="-8.15" width="0.2032" layer="21"/>
<pad name="1" x="-3.3" y="0" drill="0.9"/>
<pad name="2" x="3.3" y="0" drill="0.9"/>
<text x="-0.635" y="-5.08" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-5.08" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="TJ3-U2">
<description>&lt;b&gt;Inductor&lt;/b&gt;&lt;p&gt;
Source: TJ-Serie Vishay.pdf</description>
<circle x="0" y="0" radius="8.3" width="0.2032" layer="27"/>
<pad name="1" x="-7.35" y="0" drill="0.9"/>
<pad name="2" x="7.35" y="0" drill="0.9"/>
<text x="-5.08" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.08" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="TJ4-U1">
<description>&lt;b&gt;Inductor&lt;/b&gt;&lt;p&gt;
Source: TJ-Serie Vishay.pdf</description>
<wire x1="-4.95" y1="11.05" x2="4.95" y2="11.05" width="0.2032" layer="21"/>
<wire x1="4.95" y1="11.05" x2="4.95" y2="-11.05" width="0.2032" layer="21"/>
<wire x1="4.95" y1="-11.05" x2="-4.95" y2="-11.05" width="0.2032" layer="21"/>
<wire x1="-4.95" y1="-11.05" x2="-4.95" y2="11.05" width="0.2032" layer="21"/>
<pad name="1" x="-3.935" y="0" drill="0.9"/>
<pad name="2" x="3.935" y="0" drill="0.9"/>
<text x="-0.635" y="-5.08" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-5.08" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="TJ4-U2">
<description>&lt;b&gt;Inductor&lt;/b&gt;&lt;p&gt;
Source: TJ-Serie Vishay.pdf</description>
<circle x="0" y="0" radius="11.1" width="0.2032" layer="27"/>
<pad name="1" x="-9.9" y="0" drill="0.9"/>
<pad name="2" x="9.9" y="0" drill="0.9"/>
<text x="-5.08" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.08" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="TJ5-U1">
<description>&lt;b&gt;Inductor&lt;/b&gt;&lt;p&gt;
Source: TJ-Serie Vishay.pdf</description>
<wire x1="-5.55" y1="12.55" x2="5.55" y2="12.55" width="0.2032" layer="21"/>
<wire x1="5.55" y1="12.55" x2="5.55" y2="-12.55" width="0.2032" layer="21"/>
<wire x1="5.55" y1="-12.55" x2="-5.55" y2="-12.55" width="0.2032" layer="21"/>
<wire x1="-5.55" y1="-12.55" x2="-5.55" y2="12.55" width="0.2032" layer="21"/>
<pad name="1" x="-4.7" y="0" drill="0.9"/>
<pad name="2" x="4.7" y="0" drill="0.9"/>
<text x="-0.635" y="-5.08" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-5.08" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="TJ5-U2">
<description>&lt;b&gt;Inductor&lt;/b&gt;&lt;p&gt;
Source: TJ-Serie Vishay.pdf</description>
<circle x="0" y="0" radius="12.6" width="0.2032" layer="27"/>
<pad name="1" x="-11.45" y="0" drill="0.9"/>
<pad name="2" x="11.45" y="0" drill="0.9"/>
<text x="-5.08" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.08" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="TJ6-U1">
<description>&lt;b&gt;Inductor&lt;/b&gt;&lt;p&gt;
Source: TJ-Serie Vishay.pdf</description>
<wire x1="-10.95" y1="17.45" x2="10.95" y2="17.45" width="0.2032" layer="21"/>
<wire x1="10.95" y1="17.45" x2="10.95" y2="-17.45" width="0.2032" layer="21"/>
<wire x1="10.95" y1="-17.45" x2="-10.95" y2="-17.45" width="0.2032" layer="21"/>
<wire x1="-10.95" y1="-17.45" x2="-10.95" y2="17.45" width="0.2032" layer="21"/>
<pad name="1" x="-9.25" y="0" drill="1.3"/>
<pad name="2" x="9.25" y="0" drill="1.3"/>
<text x="-0.635" y="-5.08" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-5.08" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="TJ6-U2">
<description>&lt;b&gt;Inductor&lt;/b&gt;&lt;p&gt;
Source: TJ-Serie Vishay.pdf</description>
<circle x="0" y="0" radius="17.5" width="0.2032" layer="27"/>
<pad name="1" x="-15.5" y="0" drill="1.3"/>
<pad name="2" x="15.5" y="0" drill="1.3"/>
<text x="-5.08" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.08" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="TJ7-U1">
<description>&lt;b&gt;Inductor&lt;/b&gt;&lt;p&gt;
Source: TJ-Serie Vishay.pdf</description>
<wire x1="-9.45" y1="20.85" x2="9.43" y2="20.85" width="0.2032" layer="21"/>
<wire x1="9.43" y1="20.85" x2="9.45" y2="-20.85" width="0.2032" layer="21"/>
<wire x1="9.45" y1="-20.85" x2="-9.45" y2="-20.85" width="0.2032" layer="21"/>
<wire x1="-9.45" y1="-20.85" x2="-9.45" y2="20.85" width="0.2032" layer="21"/>
<pad name="1" x="-7.9" y="0" drill="1.3"/>
<pad name="2" x="7.9" y="0" drill="1.3"/>
<text x="-0.635" y="-5.08" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-5.08" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="TJ7-U2">
<description>&lt;b&gt;Inductor&lt;/b&gt;&lt;p&gt;
Source: TJ-Serie Vishay.pdf</description>
<circle x="0" y="0" radius="20.9" width="0.2032" layer="27"/>
<pad name="1" x="-18.8" y="0" drill="1.3"/>
<pad name="2" x="18.8" y="0" drill="1.3"/>
<text x="-5.08" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.08" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="TJ8-U1">
<description>&lt;b&gt;Inductor&lt;/b&gt;&lt;p&gt;
Source: TJ-Serie Vishay.pdf</description>
<wire x1="-12.55" y1="24.25" x2="12.55" y2="24.25" width="0.2032" layer="21"/>
<wire x1="12.55" y1="24.25" x2="12.55" y2="-24.25" width="0.2032" layer="21"/>
<wire x1="12.55" y1="-24.25" x2="-12.55" y2="-24.25" width="0.2032" layer="21"/>
<wire x1="-12.55" y1="-24.25" x2="-12.55" y2="24.25" width="0.2032" layer="21"/>
<pad name="1" x="-10.4" y="0" drill="1.5"/>
<pad name="2" x="10.4" y="0" drill="1.5"/>
<text x="-0.635" y="-5.08" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-5.08" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="TJ8-U2">
<description>&lt;b&gt;Inductor&lt;/b&gt;&lt;p&gt;
Source: TJ-Serie Vishay.pdf</description>
<circle x="0" y="0" radius="24.6" width="0.2032" layer="27"/>
<pad name="1" x="-22.35" y="0" drill="1.5"/>
<pad name="2" x="22.35" y="0" drill="1.5"/>
<text x="-5.08" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.08" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="TJ9-U1">
<description>&lt;b&gt;Inductor&lt;/b&gt;&lt;p&gt;
Source: TJ-Serie Vishay.pdf</description>
<wire x1="-17.95" y1="33.75" x2="17.95" y2="33.75" width="0.2032" layer="21"/>
<wire x1="17.95" y1="33.75" x2="17.95" y2="-33.75" width="0.2032" layer="21"/>
<wire x1="17.95" y1="-33.75" x2="-17.95" y2="-33.75" width="0.2032" layer="21"/>
<wire x1="-17.95" y1="-33.75" x2="-17.95" y2="33.75" width="0.2032" layer="21"/>
<pad name="1" x="-15.9" y="0" drill="1.8"/>
<pad name="2" x="15.9" y="0" drill="1.8"/>
<text x="-0.635" y="-5.08" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-5.08" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="TJ9-U2">
<description>&lt;b&gt;Inductor&lt;/b&gt;&lt;p&gt;
Source: TJ-Serie Vishay.pdf</description>
<circle x="0" y="0" radius="34.5" width="0.2032" layer="27"/>
<pad name="1" x="-31.6" y="0" drill="1.8"/>
<pad name="2" x="31.6" y="0" drill="1.8"/>
<text x="-5.08" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.08" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="POWER-CHOKE_WE-TPC">
<description>&lt;b&gt;POWER-CHOKE WE-TPC&lt;/b&gt;&lt;p&gt;
Würth Elektronik, Partnumber: 744053220&lt;br&gt;
Source: WE-TPC 744053220.pdf</description>
<wire x1="-2.8" y1="2.3" x2="-2.3" y2="2.8" width="0.2" layer="51" curve="-90"/>
<wire x1="-2.3" y1="2.8" x2="2.3" y2="2.8" width="0.2" layer="51"/>
<wire x1="2.3" y1="2.8" x2="2.8" y2="2.3" width="0.2" layer="51" curve="-90"/>
<wire x1="2.8" y1="2.3" x2="2.8" y2="-2.3" width="0.2" layer="51"/>
<wire x1="2.8" y1="-2.3" x2="2.3" y2="-2.8" width="0.2" layer="51" curve="-90"/>
<wire x1="2.3" y1="-2.8" x2="-2.3" y2="-2.8" width="0.2" layer="51"/>
<wire x1="-2.3" y1="-2.8" x2="-2.8" y2="-2.3" width="0.2" layer="51" curve="-90"/>
<wire x1="-2.8" y1="-2.3" x2="-2.8" y2="2.3" width="0.2" layer="51"/>
<wire x1="-2.8" y1="0.65" x2="-2.8" y2="-0.65" width="0.2" layer="21"/>
<wire x1="-2" y1="0.65" x2="-2" y2="-0.65" width="0.2" layer="21" curve="36.008323"/>
<wire x1="2.8" y1="-0.65" x2="2.8" y2="0.65" width="0.2" layer="21"/>
<wire x1="2" y1="-0.65" x2="2" y2="0.65" width="0.2" layer="21" curve="36.008323"/>
<circle x="0" y="0" radius="2.1" width="0.2" layer="51"/>
<smd name="1" x="0" y="2.05" dx="6.3" dy="2.2" layer="1" roundness="25"/>
<smd name="2" x="0" y="-2.05" dx="6.3" dy="2.2" layer="1" roundness="25" rot="R180"/>
<text x="-3.5" y="3.5" size="1.778" layer="25">&gt;NAME</text>
<text x="-3.5" y="-5.3" size="1.778" layer="27">&gt;VALUE</text>
</package>
<package name="2200-12.7">
<description>&lt;b&gt;newport components&lt;/b&gt; 2200 Serie RM 12.7 mm&lt;p&gt;
Miniatur Axial Lead Inductors&lt;br&gt;
Source: www.rsonline.de&lt;br&gt;
Order code 240-517</description>
<wire x1="-4.9" y1="1.9" x2="4.9" y2="1.9" width="0.2032" layer="51"/>
<wire x1="4.9" y1="1.9" x2="4.9" y2="-1.9" width="0.2032" layer="51"/>
<wire x1="4.9" y1="-1.9" x2="-4.9" y2="-1.9" width="0.2032" layer="51"/>
<wire x1="-4.9" y1="-1.9" x2="-4.9" y2="1.9" width="0.2032" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.08" y2="0" width="0.5" layer="51"/>
<wire x1="6.35" y1="0" x2="5.08" y2="0" width="0.5" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="0.8" diameter="1.27"/>
<pad name="2" x="6.35" y="0" drill="0.8" diameter="1.27"/>
<text x="-4.445" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.81" y="-0.635" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="2200-15.24">
<description>&lt;b&gt;newport components&lt;/b&gt; 2200 Serie RM 15.24 mm&lt;p&gt;
Miniatur Axial Lead Inductors&lt;br&gt;
Source: www.rsonline.de&lt;br&gt;
Order code 240-517</description>
<wire x1="-4.9" y1="1.9" x2="4.9" y2="1.9" width="0.2032" layer="51"/>
<wire x1="4.9" y1="1.9" x2="4.9" y2="-1.9" width="0.2032" layer="51"/>
<wire x1="4.9" y1="-1.9" x2="-4.9" y2="-1.9" width="0.2032" layer="51"/>
<wire x1="-4.9" y1="-1.9" x2="-4.9" y2="1.9" width="0.2032" layer="51"/>
<wire x1="-7.62" y1="0" x2="-5.08" y2="0" width="0.5" layer="51"/>
<wire x1="7.62" y1="0" x2="5.08" y2="0" width="0.5" layer="51"/>
<pad name="1" x="-7.62" y="0" drill="0.8" diameter="1.27"/>
<pad name="2" x="7.62" y="0" drill="0.8" diameter="1.27"/>
<text x="-4.445" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.81" y="-0.635" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="2200-11.43">
<description>&lt;b&gt;newport components&lt;/b&gt; 2200 Serie RM 11.43 mm&lt;p&gt;
Miniatur Axial Lead Inductors&lt;br&gt;
Source: www.rsonline.de&lt;br&gt;
Order code 240-517</description>
<wire x1="-4.9" y1="1.9" x2="4.9" y2="1.9" width="0.2032" layer="51"/>
<wire x1="4.9" y1="1.9" x2="4.9" y2="-1.9" width="0.2032" layer="51"/>
<wire x1="4.9" y1="-1.9" x2="-4.9" y2="-1.9" width="0.2032" layer="51"/>
<wire x1="-4.9" y1="-1.9" x2="-4.9" y2="1.9" width="0.2032" layer="51"/>
<wire x1="-5.715" y1="0" x2="-5.08" y2="0" width="0.5" layer="51"/>
<wire x1="5.715" y1="0" x2="5.08" y2="0" width="0.5" layer="51"/>
<pad name="1" x="-5.715" y="0" drill="0.8" diameter="1.27"/>
<pad name="2" x="5.715" y="0" drill="0.8" diameter="1.27"/>
<text x="-4.445" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.81" y="-0.635" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="CEP125">
<description>&lt;b&gt;POWER INDUCTORS&lt;/b&gt; (SMT Type)&lt;p&gt;
Source: www.sumida.com/products/pdf/CEP125.pdf</description>
<wire x1="-1.5796" y1="6.3448" x2="-1.5533" y2="6.3448" width="0.2032" layer="21"/>
<wire x1="-1.5533" y1="6.3448" x2="-1.29" y2="6.0815" width="0.2032" layer="21"/>
<wire x1="-1.29" y1="6.0815" x2="-1.1584" y2="6.0816" width="0.2032" layer="21"/>
<wire x1="-1.1584" y1="6.0816" x2="-1.1584" y2="6.2922" width="0.2032" layer="21"/>
<wire x1="-1.1584" y1="6.2922" x2="-1.1583" y2="6.2922" width="0.2032" layer="21"/>
<wire x1="-1.1583" y1="6.2922" x2="-1.1057" y2="6.3448" width="0.2032" layer="21"/>
<wire x1="-1.1057" y1="6.3448" x2="1.1057" y2="6.3448" width="0.2032" layer="21"/>
<wire x1="1.1057" y1="6.3448" x2="1.1057" y2="6.0815" width="0.2032" layer="21"/>
<wire x1="1.1057" y1="6.0815" x2="1.211" y2="6.0815" width="0.2032" layer="21"/>
<wire x1="1.211" y1="6.0815" x2="1.4217" y2="6.3448" width="0.2032" layer="21"/>
<wire x1="1.4217" y1="6.3448" x2="6.1079" y2="6.3448" width="0.2032" layer="51"/>
<wire x1="6.1079" y1="6.3448" x2="6.3448" y2="6.1079" width="0.2032" layer="21" curve="-96.645912"/>
<wire x1="6.3448" y1="6.1079" x2="6.3448" y2="1.8166" width="0.2032" layer="21"/>
<wire x1="6.3448" y1="1.8166" x2="6.2658" y2="1.7376" width="0.2032" layer="21"/>
<wire x1="6.2658" y1="1.7376" x2="6.2658" y2="-1.7376" width="0.2032" layer="21"/>
<wire x1="6.2658" y1="-1.7376" x2="6.3448" y2="-1.8166" width="0.2032" layer="21"/>
<wire x1="6.3448" y1="-1.8166" x2="6.3448" y2="-6.0289" width="0.2032" layer="21"/>
<wire x1="6.3448" y1="-6.0289" x2="6.0289" y2="-6.3448" width="0.2032" layer="21" curve="-91.024745"/>
<wire x1="6.0289" y1="-6.3448" x2="-6.0289" y2="-6.3448" width="0.2032" layer="51"/>
<wire x1="-6.3448" y1="-6.0289" x2="-6.3448" y2="6.1342" width="0.2032" layer="21"/>
<wire x1="-6.3448" y1="6.1342" x2="-6.1342" y2="6.3448" width="0.2032" layer="21" curve="-83.297108"/>
<wire x1="-6.1342" y1="6.3448" x2="-1.5533" y2="6.3448" width="0.2032" layer="51"/>
<wire x1="-6.2395" y1="5.7393" x2="-2.0535" y2="5.7393" width="0.2032" layer="51"/>
<wire x1="-2.0535" y1="5.7393" x2="-1.5534" y2="3.9754" width="0.2032" layer="51"/>
<wire x1="-1.5534" y1="3.9754" x2="1.527" y2="3.9754" width="0.2032" layer="21"/>
<wire x1="1.527" y1="3.9754" x2="2.0535" y2="5.7393" width="0.2032" layer="51"/>
<wire x1="2.0535" y1="5.7393" x2="6.2395" y2="5.7393" width="0.2032" layer="51"/>
<wire x1="6.2395" y1="-3.4752" x2="-6.2658" y2="-3.4752" width="0.2032" layer="21"/>
<wire x1="-5.6077" y1="-3.5805" x2="-5.6077" y2="-6.2395" width="0.2032" layer="21"/>
<wire x1="-4.8968" y1="-6.2395" x2="-4.8968" y2="-3.5805" width="0.2032" layer="21"/>
<wire x1="-4.7915" y1="-6.0552" x2="-2.2115" y2="-6.0552" width="0.2032" layer="21"/>
<wire x1="-2.9486" y1="-5.9499" x2="-2.9223" y2="-5.9499" width="0.2032" layer="21"/>
<wire x1="-2.9223" y1="-5.9499" x2="-2.9223" y2="-3.5805" width="0.2032" layer="21"/>
<wire x1="-2.1588" y1="-6.2395" x2="-2.1588" y2="-3.8701" width="0.2032" layer="21"/>
<wire x1="2.1325" y1="-6.2395" x2="2.1325" y2="-3.8701" width="0.2032" layer="21"/>
<wire x1="2.2378" y1="-6.0289" x2="4.8968" y2="-6.0289" width="0.2032" layer="21"/>
<wire x1="4.8968" y1="-3.5805" x2="4.8968" y2="-6.2395" width="0.2032" layer="21"/>
<wire x1="2.9486" y1="-5.9236" x2="2.9486" y2="-3.5805" width="0.2032" layer="21"/>
<wire x1="5.5813" y1="-6.2395" x2="5.5813" y2="-3.5805" width="0.2032" layer="21"/>
<wire x1="2.5011" y1="-3.5805" x2="-2.4747" y2="-3.5805" width="0.2032" layer="51" curve="-63.906637"/>
<wire x1="1.1057" y1="6.0815" x2="-1.1584" y2="6.0815" width="0.2032" layer="21"/>
<wire x1="-1.7902" y1="5.1601" x2="1.7376" y2="5.1863" width="0.2032" layer="21" curve="-37.134171"/>
<wire x1="-6.0289" y1="-6.3448" x2="-6.3448" y2="-6.0289" width="0.2032" layer="21" curve="-90.91408"/>
<smd name="1" x="-3.5" y="5.375" dx="3" dy="2.75" layer="1"/>
<smd name="2" x="3.5" y="5.375" dx="3" dy="2.75" layer="1"/>
<smd name="3" x="0" y="-5.48" dx="3" dy="2.55" layer="1"/>
<text x="-5.08" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.08" y="-1.27" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="EPCOS10X10">
<wire x1="0" y1="0" x2="10.3" y2="0" width="0.127" layer="21"/>
<wire x1="10.3" y1="0" x2="10.3" y2="10.4" width="0.127" layer="21"/>
<wire x1="10.3" y1="10.4" x2="0" y2="10.4" width="0.127" layer="21"/>
<wire x1="0" y1="10.4" x2="0" y2="0" width="0.127" layer="21"/>
<circle x="5.1" y="5.2" radius="4.0706" width="0.127" layer="21"/>
<smd name="P$1" x="-0.4" y="5.2" dx="6.4516" dy="3.2512" layer="1" rot="R90"/>
<smd name="P$2" x="10.6" y="5.2" dx="6.4516" dy="3.2512" layer="1" rot="R90"/>
<text x="3.175" y="-1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="COILCRAFT-MSS1278">
<wire x1="-6.15" y1="4.15" x2="-4.15" y2="6.15" width="0.127" layer="21" curve="-90"/>
<wire x1="-4.15" y1="6.15" x2="4.15" y2="6.15" width="0.127" layer="21"/>
<wire x1="4.15" y1="6.15" x2="6.15" y2="4.15" width="0.127" layer="21" curve="-90"/>
<wire x1="6.15" y1="4.15" x2="6.15" y2="-4.15" width="0.127" layer="21"/>
<wire x1="6.15" y1="-4.15" x2="4.15" y2="-6.15" width="0.127" layer="21" curve="-90"/>
<wire x1="4.15" y1="-6.15" x2="-4.15" y2="-6.15" width="0.127" layer="21"/>
<wire x1="-4.15" y1="-6.15" x2="-6.15" y2="-4.15" width="0.127" layer="21" curve="-90"/>
<wire x1="-6.15" y1="-4.15" x2="-6.15" y2="4.15" width="0.127" layer="21"/>
<circle x="0" y="0" radius="5.2363" width="0.127" layer="21"/>
<circle x="3.81" y="3.81" radius="1.27" width="0.127" layer="21"/>
<circle x="-3.81" y="3.81" radius="1.27" width="0.127" layer="21"/>
<circle x="-3.81" y="-3.81" radius="1.27" width="0.127" layer="21"/>
<circle x="3.81" y="-3.81" radius="1.27" width="0.127" layer="21"/>
<smd name="P$1" x="0" y="4.75" dx="4.5" dy="5.5" layer="1" rot="R90"/>
<smd name="P$2" x="0" y="-4.75" dx="4.5" dy="5.5" layer="1" rot="R90"/>
</package>
<package name="BOURNS_SRU5028">
<wire x1="-2.6" y1="-1" x2="-2.6" y2="1" width="0.127" layer="21"/>
<wire x1="-2.6" y1="1" x2="-1" y2="2.6" width="0.127" layer="21"/>
<wire x1="-1" y1="2.6" x2="1" y2="2.6" width="0.127" layer="21"/>
<wire x1="1" y1="2.6" x2="2.6" y2="1" width="0.127" layer="21"/>
<wire x1="2.6" y1="1" x2="2.6" y2="-1" width="0.127" layer="21"/>
<wire x1="2.6" y1="-1" x2="1" y2="-2.6" width="0.127" layer="21"/>
<wire x1="1" y1="-2.6" x2="-1" y2="-2.6" width="0.127" layer="21"/>
<wire x1="-1" y1="-2.6" x2="-2.6" y2="-1" width="0.127" layer="21"/>
<wire x1="1.27" y1="0" x2="1.8" y2="0" width="0.127" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.8" y2="0" width="0.127" layer="21"/>
<circle x="0" y="0" radius="1.796" width="0.127" layer="21"/>
<circle x="0.8" y="0" radius="0.05" width="0.127" layer="21"/>
<smd name="P$1" x="0" y="2.55" dx="2" dy="1.4" layer="1"/>
<smd name="P$2" x="0" y="-2.55" dx="2" dy="1.4" layer="1"/>
<smd name="L2" x="2.55" y="0" dx="2" dy="1.4" layer="1" rot="R90"/>
<smd name="L1" x="-2.55" y="0" dx="2" dy="1.4" layer="1" rot="R90"/>
</package>
<package name="COILCRAFT-LPS4018">
<wire x1="-1.6" y1="-2.2" x2="1.6" y2="-2.2" width="0.127" layer="21"/>
<wire x1="1.6" y1="-2.2" x2="2.2" y2="-1.6" width="0.127" layer="21"/>
<wire x1="2.2" y1="-1.6" x2="2.2" y2="1.6" width="0.127" layer="21"/>
<wire x1="2.2" y1="1.6" x2="1.6" y2="2.2" width="0.127" layer="21"/>
<wire x1="1.6" y1="2.2" x2="-1.6" y2="2.2" width="0.127" layer="21"/>
<wire x1="-1.6" y1="2.2" x2="-2.2" y2="1.6" width="0.127" layer="21"/>
<wire x1="-2.2" y1="1.6" x2="-2.2" y2="-1.6" width="0.127" layer="21"/>
<wire x1="-2.2" y1="-1.6" x2="-1.6" y2="-2.2" width="0.127" layer="21"/>
<circle x="-1.27" y="0" radius="0.122" width="0.127" layer="21"/>
<smd name="P$1" x="-1.55" y="0" dx="1.6" dy="4.5" layer="1" rot="R180"/>
<smd name="P$2" x="1.55" y="0" dx="1.6" dy="4.5" layer="1" rot="R180"/>
</package>
<package name="COILCRAFT-LPO6610">
<wire x1="-3.45" y1="1.9" x2="-0.5" y2="2.7" width="0.127" layer="21"/>
<wire x1="-0.5" y1="2.7" x2="0" y2="2.75" width="0.127" layer="21"/>
<wire x1="0" y1="2.75" x2="0.55" y2="2.7" width="0.127" layer="21"/>
<wire x1="0.55" y1="2.7" x2="3.45" y2="1.9" width="0.127" layer="21"/>
<wire x1="3.45" y1="1.9" x2="3.45" y2="-1.9" width="0.127" layer="21"/>
<wire x1="3.45" y1="-1.9" x2="0.55" y2="-2.7" width="0.127" layer="21"/>
<wire x1="0.55" y1="-2.7" x2="0" y2="-2.75" width="0.127" layer="21"/>
<wire x1="0" y1="-2.75" x2="-0.55" y2="-2.7" width="0.127" layer="21"/>
<wire x1="-0.55" y1="-2.7" x2="-3.45" y2="-1.9" width="0.127" layer="21"/>
<wire x1="-3.45" y1="-1.9" x2="-3.45" y2="1.9" width="0.127" layer="21"/>
<circle x="0" y="0" radius="2.75" width="0.127" layer="21"/>
<smd name="P$3" x="2.9" y="0" dx="1.1" dy="3.8" layer="1"/>
<smd name="P$4" x="-2.9" y="0" dx="1.1" dy="3.8" layer="1"/>
<text x="-2.54" y="-4.445" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-6.35" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="EPCOS-SMTB8">
<wire x1="-6.15" y1="4.15" x2="-4.15" y2="6.15" width="0.127" layer="21" curve="-90"/>
<wire x1="-4.15" y1="6.15" x2="4.15" y2="6.15" width="0.127" layer="21"/>
<wire x1="4.15" y1="6.15" x2="6.15" y2="4.15" width="0.127" layer="21" curve="-90"/>
<wire x1="6.15" y1="4.15" x2="6.15" y2="-4.15" width="0.127" layer="21"/>
<wire x1="6.15" y1="-4.15" x2="4.15" y2="-6.15" width="0.127" layer="21" curve="-90"/>
<wire x1="4.15" y1="-6.15" x2="-4.15" y2="-6.15" width="0.127" layer="21"/>
<wire x1="-4.15" y1="-6.15" x2="-6.15" y2="-4.15" width="0.127" layer="21" curve="-90"/>
<wire x1="-6.15" y1="-4.15" x2="-6.15" y2="4.15" width="0.127" layer="21"/>
<circle x="0" y="0" radius="5.2363" width="0.127" layer="21"/>
<circle x="3.81" y="3.81" radius="1.27" width="0.127" layer="21"/>
<circle x="-3.81" y="3.81" radius="1.27" width="0.127" layer="21"/>
<circle x="-3.81" y="-3.81" radius="1.27" width="0.127" layer="21"/>
<circle x="3.81" y="-3.81" radius="1.27" width="0.127" layer="21"/>
<smd name="P$1" x="0" y="4.75" dx="4.5" dy="5.5" layer="1" rot="R90"/>
<smd name="P$2" x="0" y="-4.75" dx="4.5" dy="5.5" layer="1" rot="R90"/>
</package>
<package name="SUMIDA-CDRH3D16">
<wire x1="-1.9" y1="-1.9" x2="-1.9" y2="-1" width="0.127" layer="21"/>
<wire x1="-1.9" y1="-1" x2="-1.9" y2="1" width="0.127" layer="21"/>
<wire x1="-1" y1="1.9" x2="1" y2="1.9" width="0.127" layer="21"/>
<wire x1="1.9" y1="1" x2="1.9" y2="-1" width="0.127" layer="21"/>
<wire x1="1" y1="-1.9" x2="-1" y2="-1.9" width="0.127" layer="21"/>
<wire x1="-1" y1="-1.9" x2="-1.9" y2="-1.9" width="0.127" layer="21"/>
<wire x1="1" y1="1.9" x2="1.9" y2="1" width="0.127" layer="21"/>
<wire x1="-1.9" y1="1" x2="-1" y2="1.9" width="0.127" layer="21"/>
<wire x1="1" y1="-1.9" x2="1.9" y2="-1" width="0.127" layer="21"/>
<smd name="P$1" x="-1.4" y="1.4" dx="1.5" dy="1.4" layer="1" rot="R45"/>
<smd name="P$2" x="1.4" y="-1.4" dx="1.5" dy="1.4" layer="1" rot="R45"/>
<text x="-1.8" y="-4" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.8" y="-5.1" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="WUERTH-744774122">
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-1.7797" y2="2.0303" width="0.127" layer="21"/>
<wire x1="-1.7797" y1="2.0303" x2="-0.6253" y2="2.2176" width="0.127" layer="21" curve="-71.569954"/>
<wire x1="-0.6253" y1="2.2176" x2="0" y2="1.905" width="0.127" layer="21"/>
<wire x1="0" y1="1.905" x2="0.6253" y2="2.2176" width="0.127" layer="21"/>
<wire x1="0.6253" y1="2.2176" x2="1.7797" y2="2.0303" width="0.127" layer="21" curve="-71.569954"/>
<wire x1="1.7797" y1="2.0303" x2="2.54" y2="1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="1.7797" y2="-2.0303" width="0.127" layer="21"/>
<wire x1="1.7797" y1="-2.0303" x2="0.6253" y2="-2.2176" width="0.127" layer="21" curve="-71.569954"/>
<wire x1="0.6253" y1="-2.2176" x2="0" y2="-1.905" width="0.127" layer="21"/>
<wire x1="0" y1="-1.905" x2="-0.6253" y2="-2.2176" width="0.127" layer="21"/>
<wire x1="-0.6253" y1="-2.2176" x2="-1.7797" y2="-2.0303" width="0.127" layer="21" curve="-71.569954"/>
<wire x1="-1.7797" y1="-2.0303" x2="-2.54" y2="-1.27" width="0.127" layer="21"/>
<circle x="0" y="1.27" radius="0.2" width="0.127" layer="21"/>
<smd name="1" x="0" y="1.925" dx="5.5" dy="2.15" layer="1"/>
<smd name="2" x="0" y="-1.925" dx="5.5" dy="2.15" layer="1"/>
</package>
<package name="WUERTH-744065470">
<wire x1="-2.3" y1="5" x2="2.3" y2="5" width="0.127" layer="21"/>
<wire x1="2.3" y1="5" x2="5" y2="2.3" width="0.127" layer="21"/>
<wire x1="5" y1="2.3" x2="5" y2="-2.3" width="0.127" layer="21"/>
<wire x1="5" y1="-2.3" x2="2.3" y2="-5" width="0.127" layer="21"/>
<wire x1="2.3" y1="-5" x2="-2.3" y2="-5" width="0.127" layer="21"/>
<wire x1="-2.3" y1="-5" x2="-5" y2="-2.3" width="0.127" layer="21"/>
<wire x1="-5" y1="-2.3" x2="-5" y2="2.3" width="0.127" layer="21"/>
<wire x1="-5" y1="2.3" x2="-2.3" y2="5" width="0.127" layer="21"/>
<circle x="0" y="0" radius="3.175" width="0.127" layer="21"/>
<circle x="0" y="1.905" radius="0.381" width="0.127" layer="21"/>
<smd name="P$1" x="0" y="4.5" dx="4.6" dy="1.8" layer="1"/>
<smd name="P$2" x="0" y="-4.5" dx="4.6" dy="1.8" layer="1"/>
<text x="-5.588" y="1.524" size="1.016" layer="25" font="vector" rot="R180">&gt;NAME</text>
<text x="-5.588" y="0" size="1.016" layer="27" font="vector" rot="R180">&gt;VALUE</text>
</package>
<package name="1206">
<description>&lt;b&gt;CHICAGO MINIATURE LAMP, INC.&lt;/b&gt;&lt;p&gt;
7022X Series SMT LEDs 1206 Package Size</description>
<wire x1="1.55" y1="-0.75" x2="-1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="-0.75" x2="-1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="0.75" x2="1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="0.75" x2="1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-0.55" y1="-0.5" x2="0.55" y2="-0.5" width="0.1016" layer="21" curve="95.452622" cap="flat"/>
<wire x1="-0.55" y1="-0.5" x2="-0.55" y2="0.5" width="0.1016" layer="51" curve="-84.547378" cap="flat"/>
<wire x1="-0.55" y1="0.5" x2="0.55" y2="0.5" width="0.1016" layer="21" curve="-95.452622" cap="flat"/>
<wire x1="0.55" y1="0.5" x2="0.55" y2="-0.5" width="0.1016" layer="51" curve="-84.547378" cap="flat"/>
<smd name="A" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="C" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1" y1="-0.1" x2="0.1" y2="0.1" layer="21"/>
<rectangle x1="0.45" y1="-0.7" x2="0.8" y2="-0.45" layer="51"/>
<rectangle x1="0.8" y1="-0.7" x2="0.9" y2="0.5" layer="51"/>
<rectangle x1="0.8" y1="0.55" x2="0.9" y2="0.7" layer="51"/>
<rectangle x1="-0.9" y1="-0.7" x2="-0.8" y2="0.5" layer="51"/>
<rectangle x1="-0.9" y1="0.55" x2="-0.8" y2="0.7" layer="51"/>
<rectangle x1="0.45" y1="-0.7" x2="0.6" y2="-0.45" layer="21"/>
</package>
<package name="LD260">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
5 mm, square, Siemens</description>
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="-1.27" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.889" x2="1.27" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.889" x2="-1.27" y2="0.889" width="0.1524" layer="51"/>
<wire x1="0" y1="1.27" x2="0.9917" y2="0.7934" width="0.1524" layer="21" curve="-51.33923" cap="flat"/>
<wire x1="-0.9917" y1="0.7934" x2="0" y2="1.27" width="0.1524" layer="21" curve="-51.33923" cap="flat"/>
<wire x1="0" y1="-1.27" x2="0.9917" y2="-0.7934" width="0.1524" layer="21" curve="51.33923" cap="flat"/>
<wire x1="-0.9917" y1="-0.7934" x2="0" y2="-1.27" width="0.1524" layer="21" curve="51.33923" cap="flat"/>
<wire x1="0.9558" y1="-0.8363" x2="1.27" y2="0" width="0.1524" layer="51" curve="41.185419" cap="flat"/>
<wire x1="0.9756" y1="0.813" x2="1.2699" y2="0" width="0.1524" layer="51" curve="-39.806332" cap="flat"/>
<wire x1="-1.27" y1="0" x2="-0.9643" y2="-0.8265" width="0.1524" layer="51" curve="40.600331" cap="flat"/>
<wire x1="-1.27" y1="0" x2="-0.9643" y2="0.8265" width="0.1524" layer="51" curve="-40.600331" cap="flat"/>
<wire x1="-0.889" y1="0" x2="0" y2="0.889" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="-0.508" y1="0" x2="0" y2="0.508" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-0.508" x2="0.508" y2="0" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="0" y1="-0.889" x2="0.889" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.2954" y="1.4732" size="1.016" layer="25" ratio="14">&gt;NAME</text>
<text x="-1.27" y="-2.4892" size="1.016" layer="27" ratio="14">&gt;VALUE</text>
<rectangle x1="1.27" y1="-0.635" x2="2.032" y2="0.635" layer="51"/>
<rectangle x1="1.905" y1="-0.635" x2="2.032" y2="0.635" layer="21"/>
</package>
<package name="LED2X5">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
2 x 5 mm, rectangle</description>
<wire x1="-2.54" y1="-1.27" x2="2.54" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.27" x2="-2.54" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="0" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="0.381" x2="-0.508" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="0.381" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0" x2="-0.508" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0.381" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0" x2="0.508" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="0.889" y1="-0.254" x2="1.143" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.762" x2="1.143" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.762" x2="0.9398" y2="-0.6096" width="0.1524" layer="51"/>
<wire x1="0.9398" y1="-0.6096" x2="1.143" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="1.397" y1="-0.254" x2="1.651" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-0.762" x2="1.651" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-0.762" x2="1.4478" y2="-0.6096" width="0.1524" layer="51"/>
<wire x1="1.4478" y1="-0.6096" x2="1.651" y2="-0.508" width="0.1524" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.397" size="1.016" layer="25" ratio="14">&gt;NAME</text>
<text x="-2.54" y="-2.413" size="1.016" layer="27" ratio="14">&gt;VALUE</text>
<rectangle x1="2.159" y1="-1.27" x2="2.413" y2="1.27" layer="21"/>
</package>
<package name="LED3MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
3 mm, round</description>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="-1.524" y1="0" x2="-1.1708" y2="0.9756" width="0.1524" layer="51" curve="-39.80361" cap="flat"/>
<wire x1="-1.524" y1="0" x2="-1.1391" y2="-1.0125" width="0.1524" layer="51" curve="41.633208" cap="flat"/>
<wire x1="1.1571" y1="0.9918" x2="1.524" y2="0" width="0.1524" layer="51" curve="-40.601165" cap="flat"/>
<wire x1="1.1708" y1="-0.9756" x2="1.524" y2="0" width="0.1524" layer="51" curve="39.80361" cap="flat"/>
<wire x1="0" y1="1.524" x2="1.2401" y2="0.8858" width="0.1524" layer="21" curve="-54.461337" cap="flat"/>
<wire x1="-1.2192" y1="0.9144" x2="0" y2="1.524" width="0.1524" layer="21" curve="-53.130102" cap="flat"/>
<wire x1="0" y1="-1.524" x2="1.203" y2="-0.9356" width="0.1524" layer="21" curve="52.126876" cap="flat"/>
<wire x1="-1.203" y1="-0.9356" x2="0" y2="-1.524" width="0.1524" layer="21" curve="52.126876" cap="flat"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="-1.016" x2="1.016" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="21" curve="-50.193108" cap="flat"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="21" curve="-61.926949" cap="flat"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="21" curve="49.763022" cap="flat"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="21" curve="60.255215" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822" cap="flat"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="1.905" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.905" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED5MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
5 mm, round</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LSU260">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
1 mm, round, Siemens</description>
<wire x1="0" y1="-0.508" x2="-1.143" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="-0.508" x2="-1.143" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="0.508" x2="0" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="-0.254" x2="-1.397" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="-0.254" x2="-1.143" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.397" y1="-0.254" x2="-1.397" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.397" y1="0.254" x2="-1.143" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="0.254" x2="-1.143" y2="0.508" width="0.1524" layer="51"/>
<wire x1="0.508" y1="-0.254" x2="1.397" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="1.397" y1="-0.254" x2="1.397" y2="0.254" width="0.1524" layer="51"/>
<wire x1="1.397" y1="0.254" x2="0.508" y2="0.254" width="0.1524" layer="51"/>
<wire x1="0.381" y1="-0.381" x2="0.254" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-0.508" x2="-0.254" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="-0.381" x2="-0.254" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.381" y1="0.381" x2="0.254" y2="0.508" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.508" x2="-0.254" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0.381" x2="-0.254" y2="0.508" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.254" x2="0.254" y2="0" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="-0.254" y1="0" x2="0" y2="0.254" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="0.381" y1="-0.381" x2="0.381" y2="0.381" width="0.1524" layer="21" curve="90"/>
<circle x="0" y="0" radius="0.508" width="0.1524" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.2954" y="0.8382" size="1.016" layer="25" ratio="14">&gt;NAME</text>
<text x="-1.27" y="-1.8542" size="1.016" layer="27" ratio="14">&gt;VALUE</text>
<rectangle x1="-1.397" y1="-0.254" x2="-1.143" y2="0.254" layer="51"/>
<rectangle x1="0.508" y1="-0.254" x2="1.397" y2="0.254" layer="51"/>
</package>
<package name="LZR181">
<description>&lt;B&gt;LED BLOCK&lt;/B&gt;&lt;p&gt;
1 LED, Siemens</description>
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.889" x2="1.27" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.889" x2="-1.27" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="0" x2="0" y2="0.889" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="-0.508" y1="0" x2="0" y2="0.508" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-0.508" x2="0.508" y2="0" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="0" y1="-0.889" x2="0.889" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-0.8678" y1="0.7439" x2="0" y2="1.143" width="0.1524" layer="21" curve="-49.396139" cap="flat"/>
<wire x1="0" y1="1.143" x2="0.8678" y2="0.7439" width="0.1524" layer="21" curve="-49.396139" cap="flat"/>
<wire x1="-0.8678" y1="-0.7439" x2="0" y2="-1.143" width="0.1524" layer="21" curve="49.396139" cap="flat"/>
<wire x1="0" y1="-1.143" x2="0.8678" y2="-0.7439" width="0.1524" layer="21" curve="49.396139" cap="flat"/>
<wire x1="0.8678" y1="0.7439" x2="1.143" y2="0" width="0.1524" layer="51" curve="-40.604135" cap="flat"/>
<wire x1="0.8678" y1="-0.7439" x2="1.143" y2="0" width="0.1524" layer="51" curve="40.604135" cap="flat"/>
<wire x1="-1.143" y1="0" x2="-0.8678" y2="0.7439" width="0.1524" layer="51" curve="-40.604135" cap="flat"/>
<wire x1="-1.143" y1="0" x2="-0.8678" y2="-0.7439" width="0.1524" layer="51" curve="40.604135" cap="flat"/>
<wire x1="-1.27" y1="1.27" x2="1.27" y2="1.27" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.2954" y="1.4732" size="1.016" layer="25" ratio="14">&gt;NAME</text>
<text x="-1.27" y="-2.4892" size="1.016" layer="27" ratio="14">&gt;VALUE</text>
<rectangle x1="1.27" y1="-0.889" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.27" y2="0.254" layer="51"/>
</package>
<package name="Q62902-B152">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="2.9718" y1="-1.8542" x2="-2.1082" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-2.1082" y1="1.8542" x2="2.9718" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.9718" y1="-1.8542" x2="-2.9718" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.1082" y1="-1.8542" x2="-2.54" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-2.1082" y1="1.8542" x2="-2.54" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.8542" x2="-2.54" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.8542" x2="-2.9718" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.8542" x2="-2.9718" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.9718" y1="0.254" x2="-2.9718" y2="-0.254" width="0.1524" layer="21" curve="180"/>
<wire x1="-1.1486" y1="0.8814" x2="0" y2="1.4478" width="0.1524" layer="21" curve="-52.498642" cap="flat"/>
<wire x1="0" y1="1.4478" x2="1.1476" y2="0.8827" width="0.1524" layer="21" curve="-52.433716" cap="flat"/>
<wire x1="-1.1351" y1="-0.8987" x2="0" y2="-1.4478" width="0.1524" layer="21" curve="51.629985" cap="flat"/>
<wire x1="0" y1="-1.4478" x2="1.1305" y2="-0.9044" width="0.1524" layer="21" curve="51.339172" cap="flat"/>
<wire x1="1.1281" y1="-0.9074" x2="1.4478" y2="0" width="0.1524" layer="51" curve="38.811177" cap="flat"/>
<wire x1="1.1401" y1="0.8923" x2="1.4478" y2="0" width="0.1524" layer="51" curve="-38.048073" cap="flat"/>
<wire x1="-1.4478" y1="0" x2="-1.1305" y2="-0.9044" width="0.1524" layer="51" curve="38.659064" cap="flat"/>
<wire x1="-1.4478" y1="0" x2="-1.1456" y2="0.8853" width="0.1524" layer="51" curve="-37.696376" cap="flat"/>
<wire x1="0" y1="1.7018" x2="1.4674" y2="0.8618" width="0.1524" layer="21" curve="-59.573488" cap="flat"/>
<wire x1="-1.4618" y1="0.8714" x2="0" y2="1.7018" width="0.1524" layer="21" curve="-59.200638" cap="flat"/>
<wire x1="0" y1="-1.7018" x2="1.4571" y2="-0.8793" width="0.1524" layer="21" curve="58.891781" cap="flat"/>
<wire x1="-1.4571" y1="-0.8793" x2="0" y2="-1.7018" width="0.1524" layer="21" curve="58.891781" cap="flat"/>
<wire x1="-1.7018" y1="0" x2="-1.4447" y2="0.8995" width="0.1524" layer="51" curve="-31.907626" cap="flat"/>
<wire x1="-1.7018" y1="0" x2="-1.4502" y2="-0.8905" width="0.1524" layer="51" curve="31.551992" cap="flat"/>
<wire x1="1.4521" y1="0.8874" x2="1.7018" y2="0" width="0.1524" layer="51" curve="-31.429586" cap="flat"/>
<wire x1="1.4459" y1="-0.8975" x2="1.7018" y2="0" width="0.1524" layer="51" curve="31.828757" cap="flat"/>
<wire x1="-2.1082" y1="1.8542" x2="-2.1082" y2="0.4826" width="0.1524" layer="21"/>
<wire x1="-2.1082" y1="0.4826" x2="-2.1082" y2="-0.4826" width="0.1524" layer="21"/>
<wire x1="-2.1082" y1="-0.4826" x2="-2.1082" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="-1.016" x2="1.016" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="2.9718" y1="1.8542" x2="2.9718" y2="0.4826" width="0.1524" layer="21"/>
<wire x1="2.9718" y1="0.4826" x2="2.9718" y2="-0.4826" width="0.1524" layer="21"/>
<wire x1="2.9718" y1="-0.4826" x2="2.9718" y2="-1.8542" width="0.1524" layer="21"/>
<pad name="K" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="A" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-1.905" y="2.286" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.905" y="-3.556" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="Q62902-B153">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="5.5118" y1="-3.5052" x2="-4.6482" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-4.6482" y1="3.5052" x2="5.5118" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-5.5118" y1="-3.5052" x2="-5.5118" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-4.6482" y1="-3.5052" x2="-5.08" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-4.6482" y1="3.5052" x2="-5.08" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-3.5052" x2="-5.08" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-3.5052" x2="-5.5118" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="3.5052" x2="-5.5118" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-5.5118" y1="0.254" x2="-5.5118" y2="-0.254" width="0.1524" layer="21" curve="180"/>
<wire x1="-4.6482" y1="3.5052" x2="-4.6482" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="5.5118" y1="3.5052" x2="5.5118" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="-1.905" width="0.254" layer="21"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="3.175" width="0.254" layer="21"/>
<pad name="A" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="K" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-4.191" y="3.937" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-5.08" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="Q62902-B155">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="-1.27" y1="-3.048" x2="-1.27" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.033" y1="3.048" x2="2.921" y2="3.048" width="0.1524" layer="21"/>
<wire x1="10.033" y1="3.048" x2="10.033" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-3.048" x2="2.921" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-3.048" x2="2.921" y2="3.048" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-3.048" x2="10.033" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="2.921" y1="3.048" x2="-1.27" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="-5.207" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="-2.54" x2="-1.27" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.54" x2="-5.207" y2="-2.54" width="0.1524" layer="21" curve="180"/>
<wire x1="-6.985" y1="0.635" x2="-6.985" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="1.397" x2="-6.096" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="1.905" x2="-5.207" y2="-1.905" width="0.1524" layer="21"/>
<pad name="K" x="7.62" y="1.27" drill="0.8128" shape="long"/>
<pad name="A" x="7.62" y="-1.27" drill="0.8128" shape="long"/>
<text x="3.302" y="-2.794" size="1.016" layer="21" ratio="14">A+</text>
<text x="3.302" y="1.778" size="1.016" layer="21" ratio="14">K-</text>
<text x="11.684" y="-2.794" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="0.635" y="-4.445" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.921" y1="1.016" x2="6.731" y2="1.524" layer="21"/>
<rectangle x1="2.921" y1="-1.524" x2="6.731" y2="-1.016" layer="21"/>
<hole x="0" y="0" drill="0.8128"/>
</package>
<package name="Q62902-B156">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="2.54" y1="3.81" x2="3.81" y2="2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="3.81" x2="-3.81" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-3.81" x2="-3.81" y2="3.81" width="0.1524" layer="21"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-3.81" x2="-2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-3.302" x2="-2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-3.81" x2="2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-3.302" x2="2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-3.302" x2="-2.54" y2="-3.302" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="3.175" width="0.254" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="K" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-3.81" y="4.0894" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.7846" y="-5.3594" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-3.556" y="-3.302" size="1.016" layer="21" ratio="14">+</text>
<text x="2.794" y="-3.302" size="1.016" layer="21" ratio="14">-</text>
</package>
<package name="SFH480">
<description>&lt;B&gt;IR LED&lt;/B&gt;&lt;p&gt;
infrared emitting diode, Infineon
TO-18, lead spacing 2.54 mm, cathode marking&lt;p&gt;
Inifineon</description>
<wire x1="-2.159" y1="1.524" x2="-2.794" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="0" y1="1.778" x2="1.5358" y2="0.8959" width="0.1524" layer="21" curve="-59.743278" cap="flat"/>
<wire x1="-1.5358" y1="0.8959" x2="0" y2="1.778" width="0.1524" layer="21" curve="-59.743278" cap="flat"/>
<wire x1="-1.5358" y1="-0.8959" x2="0" y2="-1.778" width="0.1524" layer="21" curve="59.743278" cap="flat"/>
<wire x1="0" y1="-1.778" x2="1.5358" y2="-0.8959" width="0.1524" layer="21" curve="59.743278" cap="flat"/>
<wire x1="1.5142" y1="0.9318" x2="1.778" y2="0" width="0.1524" layer="51" curve="-31.606487" cap="flat"/>
<wire x1="1.5" y1="-0.9546" x2="1.778" y2="0" width="0.1524" layer="51" curve="32.472615" cap="flat"/>
<wire x1="-1.778" y1="0" x2="-1.5142" y2="-0.9318" width="0.1524" layer="51" curve="31.606487" cap="flat"/>
<wire x1="-1.778" y1="0" x2="-1.5" y2="0.9546" width="0.1524" layer="51" curve="-32.472615" cap="flat"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="-1.016" x2="1.016" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<circle x="0" y="0" radius="2.667" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.413" width="0.254" layer="21"/>
<pad name="K" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="A" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-1.27" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-4.318" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="SFH482">
<description>&lt;B&gt;IR LED&lt;/B&gt;&lt;p&gt;
infrared emitting diode, Infineon
TO-18, lead spacing 2.54 mm, cathode marking&lt;p&gt;
Inifineon</description>
<wire x1="-2.159" y1="1.524" x2="-2.794" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="0" y1="1.778" x2="1.5358" y2="0.8959" width="0.1524" layer="21" curve="-59.743278" cap="flat"/>
<wire x1="-1.5358" y1="0.8959" x2="0" y2="1.778" width="0.1524" layer="21" curve="-59.743278" cap="flat"/>
<wire x1="-1.5358" y1="-0.8959" x2="0" y2="-1.778" width="0.1524" layer="21" curve="59.743278" cap="flat"/>
<wire x1="0" y1="-1.778" x2="1.5358" y2="-0.8959" width="0.1524" layer="21" curve="59.743278" cap="flat"/>
<wire x1="1.5142" y1="0.9318" x2="1.778" y2="0" width="0.1524" layer="51" curve="-31.606487" cap="flat"/>
<wire x1="1.5" y1="-0.9546" x2="1.778" y2="0" width="0.1524" layer="51" curve="32.472615" cap="flat"/>
<wire x1="-1.778" y1="0" x2="-1.5142" y2="-0.9318" width="0.1524" layer="51" curve="31.606487" cap="flat"/>
<wire x1="-1.778" y1="0" x2="-1.5" y2="0.9546" width="0.1524" layer="51" curve="-32.472615" cap="flat"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="-1.016" x2="1.016" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<circle x="0" y="0" radius="2.667" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.413" width="0.254" layer="21"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-1.27" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-4.318" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="U57X32">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
rectangle, 5.7 x 3.2 mm</description>
<wire x1="-3.175" y1="1.905" x2="3.175" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.905" x2="3.175" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.905" x2="-3.175" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.905" x2="-3.175" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="1.397" x2="2.667" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.667" y1="-1.397" x2="2.667" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.667" y1="-1.397" x2="-2.667" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="1.397" x2="-2.667" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.016" x2="2.54" y2="1.016" width="0.1524" layer="51"/>
<wire x1="2.286" y1="1.27" x2="2.286" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="0.508" x2="2.54" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="-0.508" x2="2.54" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="-1.016" x2="2.54" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="1.27" x2="-1.778" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="1.27" x2="-0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="1.27" x2="-0.254" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="0.254" y1="1.27" x2="0.254" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="0.762" y1="1.27" x2="0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.778" y1="1.27" x2="1.778" y2="-1.27" width="0.1524" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="3.683" y="0.254" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.683" y="-1.524" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="IRL80A">
<description>&lt;B&gt;IR LED&lt;/B&gt;&lt;p&gt;
IR transmitter Siemens</description>
<wire x1="0.889" y1="2.286" x2="0.889" y2="1.778" width="0.1524" layer="21"/>
<wire x1="0.889" y1="1.778" x2="0.889" y2="0.762" width="0.1524" layer="51"/>
<wire x1="0.889" y1="0.762" x2="0.889" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.889" y1="-0.635" x2="0.889" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="0.889" y1="-1.778" x2="0.889" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="0.889" y1="-2.286" x2="-0.889" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="2.286" x2="-0.889" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="1.778" x2="-0.889" y2="0.762" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="0.762" x2="-0.889" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-0.762" x2="-0.889" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="-1.778" x2="-0.889" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="2.286" x2="0.889" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-0.762" x2="-0.889" y2="0.762" width="0.1524" layer="21" curve="-180"/>
<wire x1="-1.397" y1="0.254" x2="-1.397" y2="-0.254" width="0.0508" layer="21"/>
<wire x1="-1.143" y1="0.508" x2="-1.143" y2="-0.508" width="0.0508" layer="21"/>
<pad name="K" x="0" y="1.27" drill="0.8128" shape="octagon"/>
<pad name="A" x="0" y="-1.27" drill="0.8128" shape="octagon"/>
<text x="1.27" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.27" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="P-LCC-2">
<description>&lt;b&gt;TOPLED® High-optical Power LED (HOP)&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... ls_t675.pdf</description>
<wire x1="-1.4" y1="1.6" x2="-1.4" y2="-0.65" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.6" x2="-1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-0.85" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.6" x2="1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-1.6" x2="1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="1.6" x2="-1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="1.8" x2="1.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="1.1" y1="1.8" x2="1.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-1.1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="-1.8" x2="1.1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="1.1" y1="-1.8" x2="1.1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-0.65" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.1" width="0.2032" layer="51"/>
<smd name="C" x="0" y="-2.75" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="2.75" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="-2.54" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-0.635" y="2.54" size="1.27" layer="21">A</text>
<text x="-0.635" y="-3.81" size="1.27" layer="21">C</text>
<rectangle x1="-1.3" y1="-2.25" x2="1.3" y2="-0.75" layer="31"/>
<rectangle x1="-1.3" y1="0.75" x2="1.3" y2="2.25" layer="31"/>
<rectangle x1="-0.25" y1="-0.25" x2="0.25" y2="0.25" layer="21"/>
<rectangle x1="-1.4" y1="0.65" x2="1.4" y2="2.35" layer="29"/>
<rectangle x1="-1.4" y1="-2.35" x2="1.4" y2="-0.65" layer="29"/>
</package>
<package name="OSRAM-MINI-TOP-LED">
<description>&lt;b&gt;BLUE LINETM Hyper Mini TOPLED® Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LB M676.pdf</description>
<wire x1="-0.6" y1="0.9" x2="-0.6" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="-0.9" x2="-0.4" y2="-0.9" width="0.1016" layer="51"/>
<wire x1="-0.4" y1="-0.9" x2="0.6" y2="-0.9" width="0.1016" layer="51"/>
<wire x1="0.6" y1="-0.9" x2="0.6" y2="0.9" width="0.1016" layer="51"/>
<wire x1="0.6" y1="0.9" x2="-0.6" y2="0.9" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="0.95" x2="-0.45" y2="1.1" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="1.1" x2="0.45" y2="1.1" width="0.1016" layer="51"/>
<wire x1="0.45" y1="1.1" x2="0.45" y2="0.95" width="0.1016" layer="51"/>
<wire x1="-0.6" y1="-0.7" x2="-0.4" y2="-0.9" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="-0.9" x2="-0.45" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="-1.1" x2="0.45" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="0.45" y1="-1.1" x2="0.45" y2="-0.95" width="0.1016" layer="51"/>
<smd name="A" x="0" y="2.6" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="C" x="0" y="-2.6" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="-0.635" y="1.905" size="1.27" layer="21">A</text>
<text x="-0.635" y="-3.175" size="1.27" layer="21">C</text>
<text x="-2.54" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.5" y1="0.6" x2="0.5" y2="1.4" layer="31"/>
<rectangle x1="-0.5" y1="-1.4" x2="0.5" y2="-0.6" layer="31"/>
<rectangle x1="-0.15" y1="-0.6" x2="0.15" y2="-0.3" layer="51"/>
<rectangle x1="-0.45" y1="0.65" x2="0.45" y2="1.35" layer="31"/>
<rectangle x1="-0.45" y1="-1.35" x2="0.45" y2="-0.65" layer="31"/>
</package>
<package name="OSRAM-SIDELED">
<description>&lt;b&gt;Super SIDELED® High-Current LED&lt;/b&gt;&lt;p&gt;
LG A672, LP A672 &lt;br&gt;
Source: http://www.osram.convergy.de/ ... LG_LP_A672.pdf (2004.05.13)</description>
<wire x1="-1.85" y1="-2.05" x2="-1.85" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.85" y1="-0.75" x2="-1.7" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.7" y1="-0.75" x2="-1.7" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.7" y1="0.75" x2="-1.85" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.85" y1="0.75" x2="-1.85" y2="2.05" width="0.1016" layer="51"/>
<wire x1="-1.85" y1="2.05" x2="0.9" y2="2.05" width="0.1016" layer="51"/>
<wire x1="0.9" y1="2.05" x2="0.9" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="0.9" y1="-2.05" x2="-1.85" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="0.9" y1="-2.05" x2="1.05" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="1.05" y1="-2.05" x2="1.85" y2="-1.85" width="0.1016" layer="51"/>
<wire x1="1.85" y1="-1.85" x2="1.85" y2="1.85" width="0.1016" layer="51"/>
<wire x1="1.85" y1="1.85" x2="1.05" y2="2.05" width="0.1016" layer="51"/>
<wire x1="1.05" y1="2.05" x2="0.9" y2="2.05" width="0.1016" layer="51"/>
<wire x1="1.05" y1="2.05" x2="1.05" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="-0.55" y1="-0.9" x2="-0.55" y2="0.9" width="0.1016" layer="51" curve="-167.319617"/>
<wire x1="-0.55" y1="-0.9" x2="0.85" y2="-1.2" width="0.1016" layer="51" style="shortdash"/>
<wire x1="-0.55" y1="0.9" x2="0.85" y2="1.2" width="0.1016" layer="51" style="shortdash"/>
<smd name="C" x="0" y="-2.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="2.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="0.635" y="-3.175" size="1.27" layer="21" rot="R90">C</text>
<text x="0.635" y="2.54" size="1.27" layer="21" rot="R90">A</text>
<text x="-2.54" y="-2.54" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-2.54" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.1" y1="-2.2" x2="2.1" y2="-0.4" layer="29"/>
<rectangle x1="-2.1" y1="0.4" x2="2.1" y2="2.2" layer="29"/>
<rectangle x1="-1.9" y1="-2.1" x2="1.9" y2="-0.6" layer="31"/>
<rectangle x1="-1.9" y1="0.6" x2="1.9" y2="2.1" layer="31"/>
<rectangle x1="-1.85" y1="-2.05" x2="-0.7" y2="-1" layer="51"/>
</package>
<package name="SMART-LED">
<description>&lt;b&gt;SmartLEDTM Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY L896.pdf</description>
<wire x1="-0.35" y1="0.6" x2="0.35" y2="0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.35" y1="0.6" x2="0.35" y2="-0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.6" x2="-0.35" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="-0.35" y1="-0.6" x2="-0.35" y2="0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.4" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<smd name="A" x="0" y="0.725" dx="0.35" dy="0.35" layer="1"/>
<smd name="B" x="0" y="-0.725" dx="0.35" dy="0.35" layer="1"/>
<text x="-0.635" y="-0.635" size="1.016" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-0.635" size="1.016" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.15" y1="-0.35" x2="0.15" y2="-0.05" layer="21"/>
<rectangle x1="-0.15" y1="0.6" x2="0.15" y2="0.85" layer="51"/>
<rectangle x1="-0.15" y1="-0.85" x2="0.15" y2="-0.6" layer="51"/>
</package>
<package name="P-LCC-2-TOPLED-RG">
<description>&lt;b&gt;Hyper TOPLED® RG Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY T776.pdf</description>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.6" x2="-1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-0.85" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.6" x2="1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-1.6" x2="1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="1.6" x2="-1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.1" y2="2.45" width="0.1016" layer="51"/>
<wire x1="1.1" y1="2.45" x2="1.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-1.1" y2="-2.45" width="0.1016" layer="51"/>
<wire x1="1.1" y1="-2.45" x2="1.1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.1" width="0.2032" layer="21"/>
<smd name="C" x="0" y="-3.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="3.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="-2.54" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-0.635" y="3.29" size="1.27" layer="21">A</text>
<text x="-0.635" y="-4.56" size="1.27" layer="21">C</text>
<rectangle x1="-1.3" y1="-3" x2="1.3" y2="-1.5" layer="31"/>
<rectangle x1="-1.3" y1="1.5" x2="1.3" y2="3" layer="31"/>
<rectangle x1="-0.25" y1="-0.25" x2="0.25" y2="0.25" layer="21"/>
<rectangle x1="-1.15" y1="2.4" x2="1.15" y2="2.7" layer="51"/>
<rectangle x1="-1.15" y1="-2.7" x2="1.15" y2="-2.4" layer="51"/>
<rectangle x1="-1.5" y1="1.5" x2="1.5" y2="3.2" layer="29"/>
<rectangle x1="-1.5" y1="-3.2" x2="1.5" y2="-1.5" layer="29"/>
<hole x="0" y="0" drill="2.8"/>
</package>
<package name="MICRO-SIDELED">
<description>&lt;b&gt;Hyper Micro SIDELED®&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY Y876.pdf</description>
<wire x1="0.65" y1="1.1" x2="-0.1" y2="1.1" width="0.1016" layer="51"/>
<wire x1="-0.1" y1="1.1" x2="-0.35" y2="1" width="0.1016" layer="51"/>
<wire x1="-0.35" y1="1" x2="-0.35" y2="-0.9" width="0.1016" layer="21"/>
<wire x1="-0.35" y1="-0.9" x2="-0.1" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="-0.1" y1="-1.1" x2="0.65" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="0.65" y1="-1.1" x2="0.65" y2="1.1" width="0.1016" layer="21"/>
<wire x1="0.6" y1="0.9" x2="0.25" y2="0.7" width="0.0508" layer="21"/>
<wire x1="0.25" y1="0.7" x2="0.25" y2="-0.7" width="0.0508" layer="21"/>
<wire x1="0.25" y1="-0.7" x2="0.6" y2="-0.9" width="0.0508" layer="21"/>
<smd name="A" x="0" y="1.95" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<smd name="C" x="0" y="-1.95" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<text x="-1.27" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.4" y1="1.1" x2="0.4" y2="1.8" layer="29"/>
<rectangle x1="-0.4" y1="-1.8" x2="0.4" y2="-1.1" layer="29"/>
<rectangle x1="-0.35" y1="-1.75" x2="0.35" y2="-1.15" layer="31"/>
<rectangle x1="-0.35" y1="1.15" x2="0.35" y2="1.75" layer="31"/>
<rectangle x1="-0.125" y1="1.125" x2="0.125" y2="1.75" layer="51"/>
<rectangle x1="-0.125" y1="-1.75" x2="0.125" y2="-1.125" layer="51"/>
</package>
<package name="P-LCC-4">
<description>&lt;b&gt;Power TOPLED®&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LA_LY E67B.pdf</description>
<wire x1="-1.4" y1="1.6" x2="-1.4" y2="-0.65" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.6" x2="-1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="-1.6" x2="-0.85" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1" y1="-1.6" x2="1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-1.6" x2="1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="1.6" x2="-1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="1.6" x2="-1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="1.6" x2="-1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-1" y1="1.8" x2="-0.5" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-0.5" y1="1.8" x2="-0.5" y2="1.65" width="0.1016" layer="51"/>
<wire x1="0.5" y1="1.65" x2="0.5" y2="1.8" width="0.1016" layer="51"/>
<wire x1="0.5" y1="1.8" x2="1.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="1.1" y1="1.8" x2="1.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-1" y1="-1.6" x2="-1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-1" y1="-1.8" x2="-0.5" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-0.5" y1="-1.8" x2="-0.5" y2="-1.65" width="0.1016" layer="51"/>
<wire x1="0.5" y1="-1.65" x2="0.5" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="0.5" y1="-1.8" x2="1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="1" y1="-1.8" x2="1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-0.65" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.1" width="0.2032" layer="51"/>
<smd name="A" x="-2" y="3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<smd name="C@3" x="2" y="3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<smd name="C@4" x="2" y="-3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<smd name="C@1" x="-2" y="-3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<text x="-3.81" y="-2.54" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="5.08" y="-2.54" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-1.905" y="-3.81" size="1.27" layer="21">C</text>
<text x="-1.905" y="2.54" size="1.27" layer="21">A</text>
<text x="1.27" y="2.54" size="1.27" layer="21">C</text>
<text x="1.27" y="-3.81" size="1.27" layer="21">C</text>
<rectangle x1="-1.15" y1="0.75" x2="-0.35" y2="1.85" layer="29"/>
<rectangle x1="0.35" y1="0.75" x2="1.15" y2="1.85" layer="29"/>
<rectangle x1="0.35" y1="-1.85" x2="1.15" y2="-0.75" layer="29"/>
<rectangle x1="-1.15" y1="-1.85" x2="-0.35" y2="-0.75" layer="29"/>
<rectangle x1="-1.1" y1="-1.8" x2="-0.4" y2="-0.8" layer="31"/>
<rectangle x1="0.4" y1="-1.8" x2="1.1" y2="-0.8" layer="31"/>
<rectangle x1="0.4" y1="0.8" x2="1.1" y2="1.8" layer="31"/>
<rectangle x1="-1.1" y1="0.8" x2="-0.4" y2="1.8" layer="31"/>
<rectangle x1="-0.2" y1="-0.2" x2="0.2" y2="0.2" layer="21"/>
</package>
<package name="CHIP-LED0603">
<description>&lt;b&gt;Hyper CHIPLED Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
LB Q993&lt;br&gt;
Source: http://www.osram.convergy.de/ ... Lb_q993.pdf</description>
<wire x1="-0.4" y1="0.45" x2="-0.4" y2="-0.45" width="0.1016" layer="51"/>
<wire x1="0.4" y1="0.45" x2="0.4" y2="-0.45" width="0.1016" layer="51"/>
<smd name="C" x="0" y="0.75" dx="0.8" dy="0.8" layer="1"/>
<smd name="A" x="0" y="-0.75" dx="0.8" dy="0.8" layer="1"/>
<text x="-0.635" y="-0.635" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-0.635" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.45" y1="0.45" x2="0.45" y2="0.85" layer="51"/>
<rectangle x1="-0.45" y1="-0.85" x2="0.45" y2="-0.45" layer="51"/>
<rectangle x1="-0.45" y1="0" x2="-0.3" y2="0.3" layer="21"/>
<rectangle x1="0.3" y1="0" x2="0.45" y2="0.3" layer="21"/>
<rectangle x1="-0.15" y1="0" x2="0.15" y2="0.3" layer="21"/>
</package>
<package name="CHIP-LED0805">
<description>&lt;b&gt;Hyper CHIPLED Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
LB R99A&lt;br&gt;
Source: http://www.osram.convergy.de/ ... lb_r99a.pdf</description>
<wire x1="-0.625" y1="0.45" x2="-0.625" y2="-0.45" width="0.1016" layer="51"/>
<wire x1="0.625" y1="0.45" x2="0.625" y2="-0.475" width="0.1016" layer="51"/>
<smd name="C" x="0" y="1.05" dx="1.2" dy="1.2" layer="1"/>
<smd name="A" x="0" y="-1.05" dx="1.2" dy="1.2" layer="1"/>
<text x="-1.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.675" y1="0" x2="-0.525" y2="0.3" layer="21"/>
<rectangle x1="0.525" y1="0" x2="0.675" y2="0.3" layer="21"/>
<rectangle x1="-0.15" y1="0" x2="0.15" y2="0.3" layer="21"/>
<rectangle x1="-0.675" y1="0.45" x2="0.675" y2="1.05" layer="51"/>
<rectangle x1="-0.675" y1="-1.05" x2="0.675" y2="-0.45" layer="51"/>
</package>
<package name="MINI-TOPLED-SANTANA">
<description>&lt;b&gt;Mini TOPLED Santana®&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG M470.pdf</description>
<wire x1="0.7" y1="-1" x2="0.35" y2="-1" width="0.1016" layer="21"/>
<wire x1="0.35" y1="-1" x2="-0.45" y2="-1" width="0.1016" layer="21"/>
<wire x1="-0.45" y1="-1" x2="-0.7" y2="-1" width="0.1016" layer="21"/>
<wire x1="-0.7" y1="-1" x2="-0.7" y2="-0.75" width="0.1016" layer="21"/>
<wire x1="-0.7" y1="-0.75" x2="-0.7" y2="1" width="0.1016" layer="21"/>
<wire x1="-0.7" y1="1" x2="0.7" y2="1" width="0.1016" layer="21"/>
<wire x1="0.7" y1="1" x2="0.7" y2="-0.65" width="0.1016" layer="21"/>
<wire x1="0.7" y1="-0.65" x2="0.7" y2="-1" width="0.1016" layer="21"/>
<wire x1="0.45" y1="-0.7" x2="-0.45" y2="-0.7" width="0.1016" layer="21"/>
<wire x1="-0.45" y1="-0.7" x2="-0.45" y2="0.7" width="0.1016" layer="21"/>
<wire x1="-0.45" y1="0.7" x2="0.45" y2="0.7" width="0.1016" layer="21"/>
<wire x1="0.45" y1="0.7" x2="0.45" y2="-0.7" width="0.1016" layer="21"/>
<wire x1="0.7" y1="-0.65" x2="0.35" y2="-1" width="0.1016" layer="21"/>
<smd name="C" x="0" y="-2.2" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="2.2" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<text x="-1.27" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.55" y1="1.5" x2="0.55" y2="2.1" layer="29"/>
<rectangle x1="-0.55" y1="-2.1" x2="0.55" y2="-1.5" layer="29"/>
<rectangle x1="-0.5" y1="-2.05" x2="0.5" y2="-1.55" layer="31"/>
<rectangle x1="-0.5" y1="1.55" x2="0.5" y2="2.05" layer="31"/>
<rectangle x1="-0.2" y1="-0.4" x2="0.15" y2="-0.05" layer="21"/>
<rectangle x1="-0.5" y1="-2.1" x2="0.5" y2="-1.4" layer="51"/>
<rectangle x1="-0.5" y1="1.4" x2="0.5" y2="2.05" layer="51"/>
<rectangle x1="-0.5" y1="1" x2="0.5" y2="1.4" layer="21"/>
<rectangle x1="-0.5" y1="-1.4" x2="0.5" y2="-1.05" layer="21"/>
<hole x="0" y="0" drill="2.7"/>
</package>
<package name="CHIPLED_0805">
<description>&lt;b&gt;CHIPLED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG_R971.pdf</description>
<wire x1="-0.35" y1="1" x2="0.35" y2="1" width="0.1016" layer="51" curve="180" cap="flat"/>
<wire x1="-0.35" y1="-1" x2="0.35" y2="-1" width="0.1016" layer="51" curve="-180" cap="flat"/>
<wire x1="0.575" y1="0.525" x2="0.575" y2="-0.525" width="0.1016" layer="51"/>
<wire x1="-0.575" y1="-0.5" x2="-0.575" y2="0.925" width="0.1016" layer="51"/>
<circle x="-0.45" y="0.85" radius="0.103" width="0.1016" layer="51"/>
<smd name="C" x="0" y="1.05" dx="1.2" dy="1.2" layer="1"/>
<smd name="A" x="0" y="-1.05" dx="1.2" dy="1.2" layer="1"/>
<text x="-1.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="0.3" y1="0.5" x2="0.625" y2="1" layer="51"/>
<rectangle x1="-0.325" y1="0.5" x2="-0.175" y2="0.75" layer="51"/>
<rectangle x1="0.175" y1="0.5" x2="0.325" y2="0.75" layer="51"/>
<rectangle x1="-0.2" y1="0.5" x2="0.2" y2="0.675" layer="51"/>
<rectangle x1="0.3" y1="-1" x2="0.625" y2="-0.5" layer="51"/>
<rectangle x1="-0.625" y1="-1" x2="-0.3" y2="-0.5" layer="51"/>
<rectangle x1="0.175" y1="-0.75" x2="0.325" y2="-0.5" layer="51"/>
<rectangle x1="-0.325" y1="-0.75" x2="-0.175" y2="-0.5" layer="51"/>
<rectangle x1="-0.2" y1="-0.675" x2="0.2" y2="-0.5" layer="51"/>
<rectangle x1="-0.1" y1="0" x2="0.1" y2="0.2" layer="21"/>
<rectangle x1="-0.6" y1="0.5" x2="-0.3" y2="0.8" layer="51"/>
<rectangle x1="-0.625" y1="0.925" x2="-0.4" y2="1" layer="51"/>
</package>
<package name="CHIPLED_1206">
<description>&lt;b&gt;CHIPLED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG_LY N971.pdf</description>
<wire x1="-0.4" y1="1.625" x2="0.4" y2="1.625" width="0.1016" layer="51" curve="180" cap="flat"/>
<wire x1="-0.8" y1="-0.95" x2="-0.8" y2="0.95" width="0.1016" layer="51"/>
<wire x1="0.8" y1="0.95" x2="0.8" y2="-0.95" width="0.1016" layer="51"/>
<circle x="-0.55" y="1.425" radius="0.1" width="0.1016" layer="51"/>
<smd name="C" x="0" y="1.75" dx="1.5" dy="1.5" layer="1"/>
<smd name="A" x="0" y="-1.75" dx="1.5" dy="1.5" layer="1"/>
<text x="-1.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.85" y1="1.525" x2="-0.35" y2="1.65" layer="51"/>
<rectangle x1="-0.85" y1="1.225" x2="-0.625" y2="1.55" layer="51"/>
<rectangle x1="-0.45" y1="1.225" x2="-0.325" y2="1.45" layer="51"/>
<rectangle x1="-0.65" y1="1.225" x2="-0.225" y2="1.35" layer="51"/>
<rectangle x1="0.35" y1="1.3" x2="0.85" y2="1.65" layer="51"/>
<rectangle x1="0.25" y1="1.225" x2="0.85" y2="1.35" layer="51"/>
<rectangle x1="-0.85" y1="0.95" x2="0.85" y2="1.25" layer="51"/>
<rectangle x1="-0.85" y1="-1.65" x2="0.85" y2="-0.95" layer="51"/>
<rectangle x1="-0.85" y1="0.35" x2="-0.525" y2="0.775" layer="21"/>
<rectangle x1="0.525" y1="0.35" x2="0.85" y2="0.775" layer="21"/>
<rectangle x1="-0.175" y1="0" x2="0.175" y2="0.35" layer="21"/>
</package>
<package name="CHIPLED_0603">
<description>&lt;b&gt;CHIPLED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG_LY Q971.pdf</description>
<wire x1="-0.3" y1="0.825" x2="0.3" y2="0.825" width="0.1016" layer="51" curve="180" cap="flat"/>
<wire x1="-0.275" y1="-0.825" x2="0.275" y2="-0.825" width="0.0508" layer="51" curve="-180" cap="flat"/>
<wire x1="-0.4" y1="0.375" x2="-0.4" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="0.4" y1="0.35" x2="0.4" y2="-0.35" width="0.1016" layer="51"/>
<circle x="-0.35" y="0.625" radius="0.075" width="0.0508" layer="51"/>
<smd name="C" x="0" y="0.75" dx="0.8" dy="0.8" layer="1"/>
<smd name="A" x="0" y="-0.75" dx="0.8" dy="0.8" layer="1"/>
<text x="-0.635" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.45" y1="0.7" x2="-0.25" y2="0.85" layer="51"/>
<rectangle x1="-0.275" y1="0.55" x2="-0.225" y2="0.6" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="-0.4" y2="0.725" layer="51"/>
<rectangle x1="0.25" y1="0.55" x2="0.45" y2="0.85" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="0.45" y2="0.575" layer="51"/>
<rectangle x1="-0.45" y1="-0.85" x2="-0.25" y2="-0.35" layer="51"/>
<rectangle x1="0.25" y1="-0.85" x2="0.45" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.575" x2="0.275" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.65" x2="-0.175" y2="-0.55" layer="51"/>
<rectangle x1="0.175" y1="-0.65" x2="0.275" y2="-0.55" layer="51"/>
<rectangle x1="-0.125" y1="0" x2="0.125" y2="0.25" layer="21"/>
</package>
<package name="CHIPLED-0603-TTW">
<description>&lt;b&gt;CHIPLED-0603&lt;/b&gt;&lt;p&gt;
Recommended Solder Pad useable for SmartLEDTM and Chipled - Package 0603&lt;br&gt;
Package able to withstand TTW-soldering heat&lt;br&gt;
Package suitable for TTW-soldering&lt;br&gt;
Source: http://www.osram.convergy.de/ ... LO_LS_LY L89K.pdf</description>
<wire x1="-0.3" y1="0.825" x2="0.3" y2="0.825" width="0.1016" layer="51" curve="180" cap="flat"/>
<wire x1="-0.275" y1="-0.825" x2="0.275" y2="-0.825" width="0.0508" layer="51" curve="-180" cap="flat"/>
<wire x1="-0.4" y1="0.375" x2="-0.4" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="0.4" y1="0.35" x2="0.4" y2="-0.35" width="0.1016" layer="51"/>
<circle x="-0.35" y="0.625" radius="0.075" width="0.0508" layer="51"/>
<smd name="C" x="0" y="0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="-0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A@1" x="0" y="-0.5" dx="0.35" dy="0.35" layer="1"/>
<smd name="C@1" x="0" y="0.5" dx="0.35" dy="0.35" layer="1"/>
<text x="-0.635" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.45" y1="0.7" x2="-0.25" y2="0.85" layer="51"/>
<rectangle x1="-0.275" y1="0.55" x2="-0.225" y2="0.6" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="-0.4" y2="0.725" layer="51"/>
<rectangle x1="0.25" y1="0.55" x2="0.45" y2="0.85" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="0.45" y2="0.575" layer="51"/>
<rectangle x1="-0.45" y1="-0.85" x2="-0.25" y2="-0.35" layer="51"/>
<rectangle x1="0.25" y1="-0.85" x2="0.45" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.575" x2="0.275" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.65" x2="-0.175" y2="-0.55" layer="51"/>
<rectangle x1="0.175" y1="-0.65" x2="0.275" y2="-0.55" layer="51"/>
<rectangle x1="-0.125" y1="0" x2="0.125" y2="0.25" layer="21"/>
<rectangle x1="-0.175" y1="0.325" x2="0.175" y2="0.7" layer="29"/>
<rectangle x1="-0.4" y1="0.625" x2="0.4" y2="1.125" layer="29"/>
<rectangle x1="-0.4" y1="-1.125" x2="0.4" y2="-0.625" layer="29"/>
<rectangle x1="-0.175" y1="-0.675" x2="0.175" y2="-0.325" layer="29"/>
</package>
<package name="SMARTLED-TTW">
<description>&lt;b&gt;SmartLED TTW&lt;/b&gt;&lt;p&gt;
Recommended Solder Pad useable for SmartLEDTM and Chipled - Package 0603&lt;br&gt;
Package able to withstand TTW-soldering heat&lt;br&gt;
Package suitable for TTW-soldering&lt;br&gt;
Source: http://www.osram.convergy.de/ ... LO_LS_LY L89K.pdf</description>
<wire x1="-0.35" y1="0.6" x2="0.35" y2="0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.35" y1="0.6" x2="0.35" y2="-0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.6" x2="-0.35" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="-0.35" y1="-0.6" x2="-0.35" y2="0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.4" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<smd name="C" x="0" y="0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="-0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A@1" x="0" y="-0.5" dx="0.35" dy="0.35" layer="1" stop="no" cream="no"/>
<smd name="C@1" x="0" y="0.5" dx="0.35" dy="0.35" layer="1" stop="no" cream="no"/>
<text x="-0.635" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.175" y1="0.325" x2="0.175" y2="0.7" layer="29"/>
<rectangle x1="-0.15" y1="-0.35" x2="0.15" y2="-0.05" layer="21"/>
<rectangle x1="-0.15" y1="0.6" x2="0.15" y2="0.85" layer="51"/>
<rectangle x1="-0.15" y1="-0.85" x2="0.15" y2="-0.6" layer="51"/>
<rectangle x1="-0.225" y1="0.3" x2="0.225" y2="0.975" layer="31"/>
<rectangle x1="-0.175" y1="-0.7" x2="0.175" y2="-0.325" layer="29" rot="R180"/>
<rectangle x1="-0.225" y1="-0.975" x2="0.225" y2="-0.3" layer="31" rot="R180"/>
</package>
<package name="SIDE-LED-KINGBRIGHT">
<wire x1="1.5" y1="0.5" x2="1.5" y2="-0.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="-0.5" x2="-0.5" y2="-0.5" width="0.127" layer="21"/>
<wire x1="-0.5" y1="-0.5" x2="-1.5" y2="-0.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-0.5" x2="-1.5" y2="0.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="0.5" x2="-1" y2="0.5" width="0.127" layer="21"/>
<wire x1="-1" y1="0.5" x2="1" y2="0.5" width="0.127" layer="21"/>
<wire x1="1" y1="0.5" x2="1.5" y2="0.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-0.5" x2="-1.5" y2="-0.8" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-0.8" x2="-0.5" y2="-0.8" width="0.127" layer="21"/>
<wire x1="-0.5" y1="-0.8" x2="-0.5" y2="-0.5" width="0.127" layer="21"/>
<wire x1="-1" y1="0.5" x2="-1" y2="1" width="0.127" layer="21"/>
<wire x1="-1" y1="1" x2="-0.5" y2="1.5" width="0.127" layer="21" curve="-90"/>
<wire x1="-0.5" y1="1.5" x2="0.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="0.5" y1="1.5" x2="1" y2="1" width="0.127" layer="21" curve="-90"/>
<wire x1="1" y1="1" x2="1" y2="0.5" width="0.127" layer="21"/>
<smd name="A" x="1.75" y="0" dx="1.5" dy="1.5" layer="1"/>
<smd name="K" x="-1.75" y="0" dx="1.5" dy="1.5" layer="1"/>
<smd name="P$3" x="0" y="0.65" dx="0.9" dy="0.9" layer="1"/>
<text x="-1.905" y="-1.905" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.016" layer="27" font="vector">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.032" y="-1.778" size="1.524" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="C-EU">
<wire x1="0" y1="0" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<text x="-2.286" y="0.381" size="1.524" layer="95" rot="R180">&gt;NAME</text>
<text x="-2.286" y="-1.651" size="1.27" layer="96" rot="R180">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-2.032" x2="2.032" y2="-1.524" layer="94"/>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="-0.508" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="R-EU">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-1.27" y="-3.0734" size="1.524" layer="95">&gt;NAME</text>
<text x="-2.032" y="-5.08" size="1.27" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="+5V">
<text x="-1.778" y="0.254" size="1.524" layer="96">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="ZENER_DUAL">
<wire x1="-1.27" y1="-1.27" x2="-2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="-2.54" y1="1.27" x2="-3.81" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-3.81" y1="1.27" x2="-2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-2.54" y1="1.27" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="-3.81" y1="1.27" x2="-3.81" y2="0.762" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="1.778" width="0.254" layer="94"/>
<pin name="A" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="C" x="-2.54" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<wire x1="3.81" y1="-1.27" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="1.27" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="3.81" y2="-1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="1.27" x2="3.81" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0.762" width="0.254" layer="94"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="1.778" width="0.254" layer="94"/>
<text x="-4.318" y="2.3114" size="1.524" layer="95" rot="R180">&gt;NAME</text>
<text x="-4.318" y="0.5334" size="1.27" layer="96" rot="R180">&gt;VALUE</text>
<pin name="C1" x="2.54" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="0" width="0.1524" layer="94"/>
</symbol>
<symbol name="+3.3V">
<text x="-3.302" y="0.254" size="1.27" layer="96">&gt;VALUE</text>
<pin name="+3.3V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="L-EU">
<text x="-1.27" y="1.8034" size="1.524" layer="95" rot="R180">&gt;NAME</text>
<text x="-1.27" y="-0.254" size="1.27" layer="96" rot="R180">&gt;VALUE</text>
<rectangle x1="-1.016" y1="-3.556" x2="1.016" y2="3.556" layer="94"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
</symbol>
<symbol name="LED">
<wire x1="-1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.032" y1="-0.762" x2="3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="1.905" y1="-1.905" x2="3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="-1.778" y="0.254" size="1.524" layer="95" rot="R180">&gt;NAME</text>
<text x="-1.778" y="-1.651" size="1.27" layer="96" rot="R180">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="3.429" y="-2.159"/>
<vertex x="3.048" y="-1.27"/>
<vertex x="2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="3.302" y="-3.302"/>
<vertex x="2.921" y="-2.413"/>
<vertex x="2.413" y="-2.921"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C-EU" prefix="C" uservalue="yes">
<description>&lt;B&gt;CAPACITOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="G$1" symbol="C-EU" x="0" y="0"/>
</gates>
<devices>
<device name="C0402" package="C0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0504" package="C0504">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0603" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0805" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1005" package="C1005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1206" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1210" package="C1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1310" package="C1310">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1608" package="C1608">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1812" package="C1812">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1825" package="C1825">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C2012" package="C2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C3216" package="C3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C3225" package="C3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C4532" package="C4532">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C4564" package="C4564">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-024X044" package="C025-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-025X050" package="C025-025X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-030X050" package="C025-030X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-040X050" package="C025-040X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-050X050" package="C025-050X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-060X050" package="C025-060X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C025_050-024X070" package="C025_050-024X070">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-025X075" package="C025_050-025X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-035X075" package="C025_050-035X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-045X075" package="C025_050-045X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-055X075" package="C025_050-055X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-024X044" package="C050-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-025X075" package="C050-025X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-045X075" package="C050-045X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-030X075" package="C050-030X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-050X075" package="C050-050X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-055X075" package="C050-055X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-075X075" package="C050-075X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050H075X075" package="C050H075X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-032X103" package="C075-032X103">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-042X103" package="C075-042X103">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-052X106" package="C075-052X106">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102-043X133" package="C102-043X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102-054X133" package="C102-054X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102-064X133" package="C102-064X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102_152-062X184" package="C102_152-062X184">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-054X183" package="C150-054X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-064X183" package="C150-064X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-072X183" package="C150-072X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-084X183" package="C150-084X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-091X182" package="C150-091X182">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-062X268" package="C225-062X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-074X268" package="C225-074X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-087X268" package="C225-087X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-108X268" package="C225-108X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-113X268" package="C225-113X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-093X316" package="C275-093X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-113X316" package="C275-113X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-134X316" package="C275-134X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-205X316" package="C275-205X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="325-137X374" package="C325-137X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="325-162X374" package="C325-162X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="325-182X374" package="C325-182X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="375-192X418" package="C375-192X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="375-203X418" package="C375-203X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-035X075" package="C050-035X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="375-155X418" package="C375-155X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-063X106" package="C075-063X106">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-154X316" package="C275-154X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-173X316" package="C275-173X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0402K" package="C0402K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0603K" package="C0603K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0805K" package="C0805K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1206K" package="C1206K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1210K" package="C1210K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1812K" package="C1812K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1825K" package="C1825K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C2220K" package="C2220K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C2225K" package="C2225K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="R-EU_" prefix="R" uservalue="yes">
<description>&lt;B&gt;RESISTOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="G$1" symbol="R-EU" x="0" y="0"/>
</gates>
<devices>
<device name="R0402" package="R0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0603" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0805" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0805W" package="R0805W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1005" package="R1005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1206" package="R1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1206W" package="R1206W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1210" package="R1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1210W" package="R1210W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2010W" package="R2010W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2012" package="R2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2012W" package="R2012W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2512W" package="R2512W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3216" package="R3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3216W" package="R3216W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3225" package="R3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3225W" package="R3225W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R5025" package="R5025">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R5025W" package="R5025W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R6332" package="R6332">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R6332W" package="R6332W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M0805" package="M0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M1206" package="M1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M1406" package="M1406">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2012" package="M2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2309" package="M2309">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3216" package="M3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3516" package="M3516">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M5923" package="M5923">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/5" package="0204/5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/7" package="0204/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/2V" package="0204V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/10" package="0207/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/12" package="0207/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/15" package="0207/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/2V" package="0207/2V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/5V" package="0207/5V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/7" package="0207/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/10" package="0309/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/12" package="0309/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/V" package="0309V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/12" package="0411/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/15" package="0411/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/3V" package="0411V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0414/15" package="0414/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0414/5V" package="0414V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/17" package="0617/17">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/22" package="0617/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/5V" package="0617V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0922/22" package="0922/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0613/5V" package="P0613V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0613/15" package="P0613/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0817/22" package="P0817/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0817/7V" package="P0817V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V234/12" package="V234/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V235/17" package="V235/17">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V526-0" package="V526-0">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102R" package="MINI_MELF-0102R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102W" package="MINI_MELF-0102W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0204R" package="MINI_MELF-0204R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0204W" package="MINI_MELF-0204W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0207R" package="MINI_MELF-0207R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0207W" package="MINI_MELF-0207W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0922V" package="0922V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RDH/15" package="RDH/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102AX" package="MINI_MELF-0102AX">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="DOUBLE-BRIDGE" package="DOUBLE_BRIDGE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ZENER-DIODE-DUAL" prefix="V" uservalue="yes">
<gates>
<gate name="G$1" symbol="ZENER_DUAL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT663">
<connects>
<connect gate="G$1" pin="A" pad="3"/>
<connect gate="G$1" pin="C" pad="1"/>
<connect gate="G$1" pin="C1" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+3.3V" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+3.3V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="L-EU" prefix="L" uservalue="yes">
<description>&lt;B&gt;INDUCTOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="G$1" symbol="L-EU" x="0" y="0"/>
</gates>
<devices>
<device name="L2012C" package="L2012C">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="L2825P" package="L2825P">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="L3216C" package="L3216C">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="L3225M" package="L3225M">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="L3225P" package="L3225P">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="L3230M" package="L3230M">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="L4035M" package="L4035M">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="L4516C" package="L4516C">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="L4532C" package="L4532M">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="L4532P" package="L4532P">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="L5038P" package="L5038P">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="L5650M" package="L5650M">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="L8530M" package="L8530M">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/5" package="0204/5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/10" package="0207/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/12" package="0207/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/2V" package="0207/2V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/5V" package="0207/5V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/7" package="0207/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/15" package="0207/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/7" package="0204/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204V" package="0204V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="L1812" package="L1812">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TJ3-U1" package="TJ3-U1">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TJ3-U2" package="TJ3-U2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TJ4-U1" package="TJ4-U1">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TJ4-U2" package="TJ4-U2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TJ5-U1" package="TJ5-U1">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TJ5-U2" package="TJ5-U2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TJ6-U1" package="TJ6-U1">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TJ6-U2" package="TJ6-U2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TJ7-U1" package="TJ7-U1">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TJ7-U2" package="TJ7-U2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TJ8-U1" package="TJ8-U1">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TJ8-U2" package="TJ8-U2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TJ9-U1" package="TJ9-U1">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TJ9-U2" package="TJ9-U2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WE-TPC" package="POWER-CHOKE_WE-TPC">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2200-12.7" package="2200-12.7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2200-15.24" package="2200-15.24">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2200-11.43" package="2200-11.43">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CEP125" package="CEP125">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="EPCOS10X10">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CC-MSS1278" package="COILCRAFT-MSS1278">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="BOURNS-SRU5028" package="BOURNS_SRU5028">
<connects>
<connect gate="G$1" pin="1" pad="L1"/>
<connect gate="G$1" pin="2" pad="L2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CC-LPS4018" package="COILCRAFT-LPS4018">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="L0805" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CC-LP06610" package="COILCRAFT-LPO6610">
<connects>
<connect gate="G$1" pin="1" pad="P$3"/>
<connect gate="G$1" pin="2" pad="P$4"/>
</connects>
<technologies>
<technology name="">
<attribute name="DISTRIBUTOR" value="coilcraft.com" constant="no"/>
<attribute name="MANUFACTURER" value="coilcraft" constant="no"/>
<attribute name="ORDER_NO" value="LPO6610-" constant="no"/>
<attribute name="PART_NO" value="LPO6610-" constant="no"/>
<attribute name="REFERENCE_NO" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="EPCOS-B8" package="EPCOS-SMTB8">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SUMIDA-CDRH3D16" package="SUMIDA-CDRH3D16">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-WUERTH-744774122" package="WUERTH-744774122">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-WUERTH-744065470" package="WUERTH-744065470">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED" prefix="H" uservalue="yes">
<description>&lt;b&gt;LED&lt;/b&gt;&lt;p&gt;
&lt;u&gt;OSRAM&lt;/u&gt;:&lt;br&gt;

- &lt;u&gt;CHIPLED&lt;/u&gt;&lt;br&gt;
LG R971, LG N971, LY N971, LG Q971, LY Q971, LO R971, LY R971
LH N974, LH R974&lt;br&gt;
LS Q976, LO Q976, LY Q976&lt;br&gt;
LO Q996&lt;br&gt;


- &lt;u&gt;Hyper CHIPLED&lt;/u&gt;&lt;br&gt;
LW Q18S&lt;br&gt;
LB Q993, LB Q99A, LB R99A&lt;br&gt;

- &lt;u&gt;SideLED&lt;/u&gt;&lt;br&gt;
LS A670, LO A670, LY A670, LG A670, LP A670&lt;br&gt;
LB A673, LV A673, LT A673, LW A673&lt;br&gt;
LH A674&lt;br&gt;
LY A675&lt;br&gt;
LS A676, LA A676, LO A676, LY A676, LW A676&lt;br&gt;
LS A679, LY A679, LG A679&lt;br&gt;

-  &lt;u&gt;Hyper Micro SIDELED®&lt;/u&gt;&lt;br&gt;
LS Y876, LA Y876, LO Y876, LY Y876&lt;br&gt;
LT Y87S&lt;br&gt;

- &lt;u&gt;SmartLED&lt;/u&gt;&lt;br&gt;
LW L88C, LW L88S&lt;br&gt;
LB L89C, LB L89S, LG L890&lt;br&gt;
LS L89K, LO L89K, LY L89K&lt;br&gt;
LS L896, LA L896, LO L896, LY L896&lt;br&gt;

- &lt;u&gt;TOPLED&lt;/u&gt;&lt;br&gt;
LS T670, LO T670, LY T670, LG T670, LP T670&lt;br&gt;
LSG T670, LSP T670, LSY T670, LOP T670, LYG T670&lt;br&gt;
LG T671, LOG T671, LSG T671&lt;br&gt;
LB T673, LV T673, LT T673, LW T673&lt;br&gt;
LH T674&lt;br&gt;
LS T676, LA T676, LO T676, LY T676, LB T676, LH T676, LSB T676, LW T676&lt;br&gt;
LB T67C, LV T67C, LT T67C, LS T67K, LO T67K, LY T67K, LW E67C&lt;br&gt;
LS E67B, LA E67B, LO E67B, LY E67B, LB E67C, LV E67C, LT E67C&lt;br&gt;
LW T67C&lt;br&gt;
LS T679, LY T679, LG T679&lt;br&gt;
LS T770, LO T770, LY T770, LG T770, LP T770&lt;br&gt;
LB T773, LV T773, LT T773, LW T773&lt;br&gt;
LH T774&lt;br&gt;
LS E675, LA E675, LY E675, LS T675&lt;br&gt;
LS T776, LA T776, LO T776, LY T776, LB T776&lt;br&gt;
LHGB T686&lt;br&gt;
LT T68C, LB T68C&lt;br&gt;

- &lt;u&gt;Hyper Mini TOPLED®&lt;/u&gt;&lt;br&gt;
LB M676&lt;br&gt;

- &lt;u&gt;Mini TOPLED Santana®&lt;/u&gt;&lt;br&gt;
LG M470&lt;br&gt;
LS M47K, LO M47K, LY M47K&lt;br&gt;

&lt;p&gt;
Source: http://www.osram.convergy.de/</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="SMT1206" package="1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LD260" package="LD260">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SQR2X5" package="LED2X5">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3MM" package="LED3MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM" package="LED5MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LSU260" package="LSU260">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LZR181" package="LZR181">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B152" package="Q62902-B152">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B153" package="Q62902-B153">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B155" package="Q62902-B155">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B156" package="Q62902-B156">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SFH480" package="SFH480">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SFH482" package="SFH482">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SQR5.7X3.2" package="U57X32">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="IRL80A" package="IRL80A">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="P-LCC-2" package="P-LCC-2">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MINI-TOP" package="OSRAM-MINI-TOP-LED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SIDELED" package="OSRAM-SIDELED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMART-LED" package="SMART-LED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="B"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="P-LCC-2-BACK" package="P-LCC-2-TOPLED-RG">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MICRO-SIDELED" package="MICRO-SIDELED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="P-LCC-4" package="P-LCC-4">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C@4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIP-LED0603" package="CHIP-LED0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIP-LED0805" package="CHIP-LED0805">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TOPLED-SANTANA" package="MINI-TOPLED-SANTANA">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED_0805" package="CHIPLED_0805">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED_1206" package="CHIPLED_1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED_0603" package="CHIPLED_0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED-0603-TTW" package="CHIPLED-0603-TTW">
<connects>
<connect gate="G$1" pin="A" pad="A@1"/>
<connect gate="G$1" pin="C" pad="C@1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="SMARTLED-TTW">
<connects>
<connect gate="G$1" pin="A" pad="A@1"/>
<connect gate="G$1" pin="C" pad="C@1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SIDE-LED" package="SIDE-LED-KINGBRIGHT">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="certoclav">
<packages>
<package name="SOT23">
<description>&lt;b&gt;SOT-23&lt;/b&gt;</description>
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.4224" y1="-0.6604" x2="-1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.6604" x2="-1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="51"/>
<smd name="3" x="0" y="1.1" dx="1" dy="1.4" layer="1"/>
<smd name="2" x="0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<smd name="1" x="-0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
</package>
<package name="PN61729-S">
<description>&lt;b&gt;USB connector&lt;/b&gt; with shield&lt;p&gt;</description>
<wire x1="-5.9" y1="5.6" x2="-5.9" y2="-10.15" width="0.254" layer="51"/>
<wire x1="-5.9" y1="-10.15" x2="5.9" y2="-10.15" width="0.254" layer="21"/>
<wire x1="5.9" y1="-10.15" x2="5.9" y2="5.6" width="0.254" layer="51"/>
<wire x1="5.9" y1="5.6" x2="-5.9" y2="5.6" width="0.254" layer="21"/>
<wire x1="-2.46" y1="-0.27" x2="-2.46" y2="0.73" width="0.0508" layer="21" curve="180"/>
<wire x1="-2.46" y1="-1.27" x2="-2.46" y2="-0.27" width="0.0508" layer="21" curve="180"/>
<wire x1="3.665" y1="0.23" x2="3.665" y2="-0.77" width="0.0508" layer="21" curve="180"/>
<wire x1="3.415" y1="0.73" x2="3.415" y2="-1.27" width="0.0508" layer="21" curve="180"/>
<wire x1="3.665" y1="0.23" x2="4.165" y2="0.23" width="0.0508" layer="21" curve="-15.189287"/>
<wire x1="3.415" y1="0.73" x2="4.175" y2="0.675" width="0.0508" layer="21" curve="-12.057134"/>
<wire x1="3.415" y1="-1.27" x2="4.165" y2="-1.145" width="0.0508" layer="21" curve="18.422836"/>
<wire x1="1.665" y1="-0.52" x2="1.665" y2="0.73" width="0.0508" layer="21" curve="180"/>
<wire x1="1.29" y1="-0.145" x2="1.29" y2="0.23" width="0.0508" layer="21" curve="180"/>
<wire x1="-3.835" y1="0.73" x2="-3.835" y2="-1.27" width="0.0508" layer="21"/>
<wire x1="-3.835" y1="-1.27" x2="-3.21" y2="-1.27" width="0.0508" layer="21"/>
<wire x1="-3.21" y1="-1.27" x2="-3.21" y2="-0.27" width="0.0508" layer="21"/>
<wire x1="-3.21" y1="-0.27" x2="-3.21" y2="0.73" width="0.0508" layer="21"/>
<wire x1="-3.21" y1="0.73" x2="-3.835" y2="0.73" width="0.0508" layer="21"/>
<wire x1="-3.21" y1="0.73" x2="-2.46" y2="0.23" width="0.0508" layer="21"/>
<wire x1="-2.46" y1="0.23" x2="-3.21" y2="-0.27" width="0.0508" layer="21"/>
<wire x1="-3.21" y1="-0.27" x2="-2.46" y2="-0.77" width="0.0508" layer="21"/>
<wire x1="-2.46" y1="-0.77" x2="-3.21" y2="-1.27" width="0.0508" layer="21"/>
<wire x1="-2.46" y1="-1.27" x2="-3.21" y2="-1.27" width="0.0508" layer="21"/>
<wire x1="-2.46" y1="0.73" x2="-3.21" y2="0.73" width="0.0508" layer="21"/>
<wire x1="-1.71" y1="0.73" x2="-1.71" y2="-1.27" width="0.0508" layer="21"/>
<wire x1="-1.71" y1="-1.27" x2="0.04" y2="-1.27" width="0.0508" layer="21"/>
<wire x1="0.04" y1="-1.27" x2="0.04" y2="-0.77" width="0.0508" layer="21"/>
<wire x1="0.04" y1="-0.77" x2="-1.085" y2="-0.77" width="0.0508" layer="21"/>
<wire x1="-1.085" y1="-0.77" x2="-1.085" y2="-0.52" width="0.0508" layer="21"/>
<wire x1="-1.085" y1="-0.52" x2="0.04" y2="-0.52" width="0.0508" layer="21"/>
<wire x1="0.04" y1="-0.52" x2="0.04" y2="-0.02" width="0.0508" layer="21"/>
<wire x1="0.04" y1="-0.02" x2="-1.085" y2="-0.02" width="0.0508" layer="21"/>
<wire x1="-1.085" y1="-0.02" x2="-1.085" y2="0.23" width="0.0508" layer="21"/>
<wire x1="-1.085" y1="0.23" x2="0.04" y2="0.23" width="0.0508" layer="21"/>
<wire x1="0.04" y1="0.23" x2="0.04" y2="0.73" width="0.0508" layer="21"/>
<wire x1="0.04" y1="0.73" x2="-1.71" y2="0.73" width="0.0508" layer="21"/>
<wire x1="0.29" y1="0.73" x2="0.29" y2="-1.27" width="0.0508" layer="21"/>
<wire x1="0.29" y1="-1.27" x2="0.915" y2="-1.27" width="0.0508" layer="21"/>
<wire x1="0.915" y1="-1.27" x2="0.915" y2="-0.52" width="0.0508" layer="21"/>
<wire x1="0.915" y1="-0.52" x2="1.415" y2="-1.27" width="0.0508" layer="21"/>
<wire x1="1.415" y1="-1.27" x2="2.165" y2="-1.27" width="0.0508" layer="21"/>
<wire x1="2.165" y1="-1.27" x2="1.665" y2="-0.52" width="0.0508" layer="21"/>
<wire x1="0.915" y1="0.23" x2="0.915" y2="-0.145" width="0.0508" layer="21"/>
<wire x1="0.29" y1="0.73" x2="1.665" y2="0.73" width="0.0508" layer="21"/>
<wire x1="0.915" y1="0.23" x2="1.29" y2="0.23" width="0.0508" layer="21"/>
<wire x1="0.915" y1="-0.145" x2="1.29" y2="-0.145" width="0.0508" layer="21"/>
<wire x1="3.665" y1="-0.27" x2="4.165" y2="-0.27" width="0.0508" layer="21"/>
<wire x1="3.665" y1="-0.27" x2="3.665" y2="-0.77" width="0.0508" layer="21"/>
<wire x1="4.16" y1="0.23" x2="4.16" y2="0.675" width="0.0508" layer="21"/>
<wire x1="4.165" y1="-0.27" x2="4.165" y2="-1.145" width="0.0508" layer="21"/>
<wire x1="-5.9" y1="-2.02" x2="-5.9" y2="-10.15" width="0.254" layer="21"/>
<wire x1="5.9" y1="1.915" x2="5.9" y2="5.6" width="0.254" layer="21"/>
<wire x1="-5.9" y1="5.6" x2="-5.9" y2="1.915" width="0.254" layer="21"/>
<wire x1="5.9" y1="-10.15" x2="5.9" y2="-2.02" width="0.254" layer="21"/>
<pad name="1" x="1.25" y="4.71" drill="0.95" shape="octagon"/>
<pad name="2" x="-1.25" y="4.71" drill="0.95" shape="octagon"/>
<pad name="3" x="-1.25" y="2.71" drill="0.95" shape="octagon"/>
<pad name="4" x="1.25" y="2.71" drill="0.95" shape="octagon"/>
<pad name="S1" x="-6.02" y="0" drill="2.3" diameter="2.9"/>
<pad name="S2" x="6.02" y="0" drill="2.3" diameter="2.9"/>
<text x="-6.35" y="6.35" size="1.27" layer="25">&gt;NAME</text>
<text x="7.62" y="-8.89" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-3.81" y="-2.04" size="0.4064" layer="21">E L E C T R O N I C S</text>
</package>
<package name="AMPHENOL-MUSBD11130">
<pad name="1" x="1.25" y="1" drill="0.92"/>
<pad name="4" x="1.25" y="-1" drill="0.92"/>
<pad name="3" x="-1.25" y="-1" drill="0.92"/>
<pad name="2" x="-1.25" y="1" drill="0.92"/>
<pad name="S1" x="-4" y="-5.49" drill="2.3"/>
<pad name="S2" x="4" y="-5.49" drill="2.3"/>
<wire x1="-9" y1="-8.74" x2="9" y2="-8.74" width="0.127" layer="20"/>
<wire x1="-14" y1="-11" x2="-7" y2="-11" width="0.127" layer="21"/>
<wire x1="-7" y1="-11" x2="7" y2="-11" width="0.127" layer="21"/>
<wire x1="7" y1="-11" x2="14" y2="-11" width="0.127" layer="21"/>
<wire x1="14" y1="-11" x2="14" y2="-9" width="0.127" layer="21"/>
<wire x1="14" y1="-9" x2="13.5" y2="-9" width="0.127" layer="21"/>
<wire x1="13.5" y1="-9" x2="8.5" y2="-9" width="0.127" layer="21"/>
<wire x1="8.5" y1="-9" x2="7" y2="-9" width="0.127" layer="21"/>
<wire x1="7" y1="-9" x2="-7" y2="-9" width="0.127" layer="21"/>
<wire x1="-7" y1="-9" x2="-8.5" y2="-9" width="0.127" layer="21"/>
<wire x1="-8.5" y1="-9" x2="-13.5" y2="-9" width="0.127" layer="21"/>
<wire x1="-13.5" y1="-9" x2="-14" y2="-9" width="0.127" layer="21"/>
<wire x1="-14" y1="-9" x2="-14" y2="-11" width="0.127" layer="21"/>
<wire x1="-7" y1="-9" x2="-7" y2="3" width="0.127" layer="21"/>
<wire x1="-7" y1="3" x2="7" y2="3" width="0.127" layer="21"/>
<wire x1="7" y1="3" x2="7" y2="-9" width="0.127" layer="21"/>
<wire x1="-7" y1="-11" x2="-7" y2="-15" width="0.127" layer="21"/>
<wire x1="-7" y1="-15" x2="7" y2="-15" width="0.127" layer="21"/>
<wire x1="7" y1="-15" x2="7" y2="-11" width="0.127" layer="21"/>
<wire x1="-13" y1="-11.1" x2="-8" y2="-11.1" width="0.254" layer="21"/>
<wire x1="-8" y1="-11.1" x2="13" y2="-11.1" width="0.254" layer="21"/>
<wire x1="-13.5" y1="-9" x2="-13.5" y2="-5" width="0.127" layer="21"/>
<wire x1="-13.5" y1="-5" x2="-11.5" y2="-3" width="0.127" layer="21" curve="-90"/>
<wire x1="-11.5" y1="-3" x2="-10.5" y2="-3" width="0.127" layer="21"/>
<wire x1="-10.5" y1="-3" x2="-8.5" y2="-5" width="0.127" layer="21" curve="-90"/>
<wire x1="-8.5" y1="-5" x2="-8.5" y2="-9" width="0.127" layer="21"/>
<wire x1="8.5" y1="-9" x2="8.5" y2="-5" width="0.127" layer="21"/>
<wire x1="8.5" y1="-5" x2="10.5" y2="-3" width="0.127" layer="21" curve="-90"/>
<wire x1="10.5" y1="-3" x2="11.5" y2="-3" width="0.127" layer="21"/>
<wire x1="11.5" y1="-3" x2="13.5" y2="-5" width="0.127" layer="21" curve="-90"/>
<wire x1="13.5" y1="-5" x2="13.5" y2="-9" width="0.127" layer="21"/>
</package>
<package name="MA05-1">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-5.715" y1="1.27" x2="-4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-0.635" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="0.635" x2="-6.35" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="1.27" x2="-6.35" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="-0.635" x2="-5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-1.27" x2="-5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="0.635" x2="6.35" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="-2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-6.35" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.715" y="-2.921" size="1.27" layer="21" ratio="10">1</text>
<text x="4.445" y="1.651" size="1.27" layer="21" ratio="10">5</text>
<text x="-2.54" y="-2.921" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="51"/>
<rectangle x1="-5.334" y1="-0.254" x2="-4.826" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
</package>
<package name="SO16L">
<description>&lt;b&gt;Small Outline Package&lt;/b&gt; .300 SIOC&lt;p&gt;
Source: http://www.maxim-ic.com/cgi-bin/packages?pkg=16%2FSOIC%2E300&amp;Type=Max</description>
<wire x1="4.8768" y1="3.7338" x2="-4.8768" y2="3.7338" width="0.1524" layer="21"/>
<wire x1="4.8768" y1="-3.7338" x2="5.2578" y2="-3.3528" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.2578" y1="3.3528" x2="-4.8768" y2="3.7338" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.8768" y1="3.7338" x2="5.2578" y2="3.3528" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.2578" y1="-3.3528" x2="-4.8768" y2="-3.7338" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.8768" y1="-3.7338" x2="4.8768" y2="-3.7338" width="0.1524" layer="21"/>
<wire x1="5.2578" y1="-3.3528" x2="5.2578" y2="3.3528" width="0.1524" layer="21"/>
<wire x1="-5.2578" y1="3.3528" x2="-5.2578" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-5.2578" y1="1.27" x2="-5.2578" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-5.2578" y1="-1.27" x2="-5.2578" y2="-3.3528" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="-3.3782" x2="5.2324" y2="-3.3782" width="0.0508" layer="21"/>
<wire x1="-5.2578" y1="1.27" x2="-5.2578" y2="-1.27" width="0.1524" layer="21" curve="-180"/>
<smd name="1" x="-4.445" y="-4.9022" dx="0.6604" dy="2.032" layer="1"/>
<smd name="2" x="-3.175" y="-4.9022" dx="0.6604" dy="2.032" layer="1"/>
<smd name="3" x="-1.905" y="-4.9022" dx="0.6604" dy="2.032" layer="1"/>
<smd name="4" x="-0.635" y="-4.9022" dx="0.6604" dy="2.032" layer="1"/>
<smd name="5" x="0.635" y="-4.9022" dx="0.6604" dy="2.032" layer="1"/>
<smd name="6" x="1.905" y="-4.9022" dx="0.6604" dy="2.032" layer="1"/>
<smd name="7" x="3.175" y="-4.9022" dx="0.6604" dy="2.032" layer="1"/>
<smd name="8" x="4.445" y="-4.9022" dx="0.6604" dy="2.032" layer="1"/>
<smd name="9" x="4.445" y="4.9022" dx="0.6604" dy="2.032" layer="1"/>
<smd name="10" x="3.175" y="4.9022" dx="0.6604" dy="2.032" layer="1"/>
<smd name="11" x="1.905" y="4.9022" dx="0.6604" dy="2.032" layer="1"/>
<smd name="12" x="0.635" y="4.9022" dx="0.6604" dy="2.032" layer="1"/>
<smd name="13" x="-0.635" y="4.9022" dx="0.6604" dy="2.032" layer="1"/>
<smd name="14" x="-1.905" y="4.9022" dx="0.6604" dy="2.032" layer="1"/>
<smd name="15" x="-3.175" y="4.9022" dx="0.6604" dy="2.032" layer="1"/>
<smd name="16" x="-4.445" y="4.9022" dx="0.6604" dy="2.032" layer="1"/>
<text x="-6.223" y="-4.191" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<text x="-8.128" y="-3.556" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<rectangle x1="-4.699" y1="-3.8608" x2="-4.191" y2="-3.7338" layer="21"/>
<rectangle x1="-4.699" y1="-5.334" x2="-4.191" y2="-3.8608" layer="51"/>
<rectangle x1="-3.429" y1="-3.8608" x2="-2.921" y2="-3.7338" layer="21"/>
<rectangle x1="-3.429" y1="-5.334" x2="-2.921" y2="-3.8608" layer="51"/>
<rectangle x1="-2.159" y1="-3.8608" x2="-1.651" y2="-3.7338" layer="21"/>
<rectangle x1="-2.159" y1="-5.334" x2="-1.651" y2="-3.8608" layer="51"/>
<rectangle x1="-0.889" y1="-3.8608" x2="-0.381" y2="-3.7338" layer="21"/>
<rectangle x1="-0.889" y1="-5.334" x2="-0.381" y2="-3.8608" layer="51"/>
<rectangle x1="0.381" y1="-5.334" x2="0.889" y2="-3.8608" layer="51"/>
<rectangle x1="0.381" y1="-3.8608" x2="0.889" y2="-3.7338" layer="21"/>
<rectangle x1="1.651" y1="-3.8608" x2="2.159" y2="-3.7338" layer="21"/>
<rectangle x1="1.651" y1="-5.334" x2="2.159" y2="-3.8608" layer="51"/>
<rectangle x1="2.921" y1="-3.8608" x2="3.429" y2="-3.7338" layer="21"/>
<rectangle x1="2.921" y1="-5.334" x2="3.429" y2="-3.8608" layer="51"/>
<rectangle x1="4.191" y1="-3.8608" x2="4.699" y2="-3.7338" layer="21"/>
<rectangle x1="4.191" y1="-5.334" x2="4.699" y2="-3.8608" layer="51"/>
<rectangle x1="-4.699" y1="3.8608" x2="-4.191" y2="5.334" layer="51"/>
<rectangle x1="-4.699" y1="3.7338" x2="-4.191" y2="3.8608" layer="21"/>
<rectangle x1="-3.429" y1="3.7338" x2="-2.921" y2="3.8608" layer="21"/>
<rectangle x1="-3.429" y1="3.8608" x2="-2.921" y2="5.334" layer="51"/>
<rectangle x1="-2.159" y1="3.7338" x2="-1.651" y2="3.8608" layer="21"/>
<rectangle x1="-2.159" y1="3.8608" x2="-1.651" y2="5.334" layer="51"/>
<rectangle x1="-0.889" y1="3.7338" x2="-0.381" y2="3.8608" layer="21"/>
<rectangle x1="-0.889" y1="3.8608" x2="-0.381" y2="5.334" layer="51"/>
<rectangle x1="0.381" y1="3.7338" x2="0.889" y2="3.8608" layer="21"/>
<rectangle x1="0.381" y1="3.8608" x2="0.889" y2="5.334" layer="51"/>
<rectangle x1="1.651" y1="3.7338" x2="2.159" y2="3.8608" layer="21"/>
<rectangle x1="1.651" y1="3.8608" x2="2.159" y2="5.334" layer="51"/>
<rectangle x1="2.921" y1="3.7338" x2="3.429" y2="3.8608" layer="21"/>
<rectangle x1="2.921" y1="3.8608" x2="3.429" y2="5.334" layer="51"/>
<rectangle x1="4.191" y1="3.7338" x2="4.699" y2="3.8608" layer="21"/>
<rectangle x1="4.191" y1="3.8608" x2="4.699" y2="5.334" layer="51"/>
<wire x1="-5.395" y1="3.9" x2="5.395" y2="3.9" width="0.1998" layer="39"/>
<wire x1="5.395" y1="-3.9" x2="-5.395" y2="-3.9" width="0.1998" layer="39"/>
<wire x1="-5.395" y1="-3.9" x2="-5.395" y2="3.9" width="0.1998" layer="39"/>
<wire x1="4.94" y1="-1.9" x2="-4.94" y2="-1.9" width="0.2032" layer="51"/>
<wire x1="-4.94" y1="-1.9" x2="-4.94" y2="-1.4" width="0.2032" layer="51"/>
<wire x1="-4.94" y1="-1.4" x2="-4.94" y2="1.9" width="0.2032" layer="51"/>
<wire x1="-4.94" y1="1.9" x2="4.94" y2="1.9" width="0.2032" layer="51"/>
<wire x1="4.94" y1="-1.4" x2="-4.94" y2="-1.4" width="0.2032" layer="51"/>
<wire x1="4.94" y1="1.9" x2="4.94" y2="-1.4" width="0.2032" layer="51"/>
<wire x1="4.94" y1="-1.4" x2="4.94" y2="-1.9" width="0.2032" layer="51"/>
<wire x1="5.395" y1="3.9" x2="5.395" y2="-3.9" width="0.1998" layer="39"/>
<smd name="2-2" x="-3.175" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="2-13" x="-0.635" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="2-1" x="-4.445" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="2-3" x="-1.905" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="2-4" x="-0.635" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="2-14" x="-1.905" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="2-12" x="0.635" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="2-11" x="1.905" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="2-6" x="1.905" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="2-9" x="4.445" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="2-5" x="0.635" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="2-7" x="3.175" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="2-10" x="3.175" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="2-8" x="4.445" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="2-15" x="-3.175" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="2-16" x="-4.445" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<rectangle x1="-4.6901" y1="-3.1001" x2="-4.1999" y2="-2" layer="51"/>
<rectangle x1="-3.4201" y1="-3.1001" x2="-2.9299" y2="-2" layer="51"/>
<rectangle x1="-2.1501" y1="-3.1001" x2="-1.6599" y2="-2" layer="51"/>
<rectangle x1="-0.8801" y1="-3.1001" x2="-0.3899" y2="-2" layer="51"/>
<rectangle x1="1.6599" y1="2" x2="2.1501" y2="3.1001" layer="51"/>
<rectangle x1="0.3899" y1="2" x2="0.8801" y2="3.1001" layer="51"/>
<rectangle x1="-0.8801" y1="2" x2="-0.3899" y2="3.1001" layer="51"/>
<rectangle x1="-2.1501" y1="2" x2="-1.6599" y2="3.1001" layer="51"/>
<rectangle x1="0.3899" y1="-3.1001" x2="0.8801" y2="-2" layer="51"/>
<rectangle x1="1.6599" y1="-3.1001" x2="2.1501" y2="-2" layer="51"/>
<rectangle x1="2.9299" y1="-3.1001" x2="3.4201" y2="-2" layer="51"/>
<rectangle x1="4.1999" y1="-3.1001" x2="4.6901" y2="-2" layer="51"/>
<rectangle x1="4.1999" y1="2" x2="4.6901" y2="3.1001" layer="51"/>
<rectangle x1="2.9299" y1="2" x2="3.4201" y2="3.1001" layer="51"/>
<rectangle x1="-3.4201" y1="2" x2="-2.9299" y2="3.1001" layer="51"/>
<rectangle x1="-4.6901" y1="2" x2="-4.1999" y2="3.1001" layer="51"/>
</package>
<package name="M09D">
<description>&lt;b&gt;SUB-D&lt;/b&gt;</description>
<wire x1="-10.414" y1="0.508" x2="-10.414" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-10.414" y1="0.508" x2="-10.287" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-10.287" y1="-1.905" x2="-9.652" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-10.287" y1="-1.905" x2="-10.287" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-9.671" y1="-2.54" x2="9.671" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.414" y1="0.508" x2="10.414" y2="1.016" width="0.1524" layer="21"/>
<wire x1="10.287" y1="-1.905" x2="10.287" y2="0.508" width="0.1524" layer="21"/>
<wire x1="10.287" y1="0.508" x2="10.414" y2="0.508" width="0.1524" layer="21"/>
<wire x1="9.652" y1="-2.54" x2="10.287" y2="-1.905" width="0.1524" layer="21" curve="90"/>
<wire x1="-16.4592" y1="-2.7432" x2="-15.0876" y2="-4.1148" width="0.254" layer="48"/>
<wire x1="-15.0876" y1="-4.1148" x2="-13.716" y2="-2.7432" width="0.254" layer="48"/>
<wire x1="-13.716" y1="-2.7432" x2="-12.3444" y2="-4.1148" width="0.254" layer="48"/>
<wire x1="-12.3444" y1="-4.1148" x2="-10.9728" y2="-2.7432" width="0.254" layer="48"/>
<wire x1="10.9728" y1="-2.7432" x2="12.3444" y2="-4.1148" width="0.254" layer="48"/>
<wire x1="12.3444" y1="-4.1148" x2="13.716" y2="-2.7432" width="0.254" layer="48"/>
<wire x1="13.716" y1="-2.7432" x2="15.0876" y2="-4.1148" width="0.254" layer="48"/>
<wire x1="15.0876" y1="-4.1148" x2="16.4592" y2="-2.7432" width="0.254" layer="48"/>
<wire x1="-8.763" y1="6.717" x2="-8.255" y2="7.225" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.255" y1="7.225" x2="8.763" y2="6.717" width="0.1524" layer="21" curve="-90"/>
<wire x1="5.08" y1="3.669" x2="2.794" y2="3.669" width="0.1524" layer="21"/>
<wire x1="2.794" y1="3.669" x2="2.794" y2="5.955" width="0.1524" layer="21"/>
<wire x1="5.08" y1="5.955" x2="5.08" y2="3.669" width="0.1524" layer="21"/>
<wire x1="1.143" y1="3.669" x2="-1.143" y2="3.669" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="3.669" x2="-1.143" y2="5.955" width="0.1524" layer="21"/>
<wire x1="1.143" y1="5.955" x2="1.143" y2="3.669" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="3.669" x2="-5.08" y2="3.669" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="3.669" x2="-5.08" y2="5.955" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="5.955" x2="-2.794" y2="3.669" width="0.1524" layer="21"/>
<wire x1="8.763" y1="2.018" x2="8.763" y2="6.717" width="0.1524" layer="21"/>
<wire x1="8.763" y1="2.018" x2="9.398" y2="1.383" width="0.1524" layer="21" curve="90"/>
<wire x1="-8.763" y1="2.018" x2="-8.763" y2="6.717" width="0.1524" layer="21"/>
<wire x1="-9.398" y1="1.383" x2="-8.763" y2="2.018" width="0.1524" layer="21" curve="90"/>
<wire x1="-8.255" y1="7.225" x2="8.255" y2="7.225" width="0.1524" layer="21"/>
<smd name="1" x="5.4862" y="-6.0448" dx="1.7" dy="3.5" layer="1"/>
<smd name="2" x="2.7434" y="-6.0448" dx="1.7" dy="3.5" layer="1"/>
<smd name="3" x="0" y="-6.045" dx="1.7" dy="3.5" layer="1"/>
<smd name="4" x="-2.7434" y="-6.0448" dx="1.7" dy="3.5" layer="1"/>
<smd name="5" x="-5.4862" y="-6.0448" dx="1.7" dy="3.5" layer="1"/>
<smd name="6" x="4.1148" y="-6.0452" dx="1.7" dy="3.5" layer="16"/>
<smd name="7" x="1.3716" y="-6.0452" dx="1.7" dy="3.5" layer="16"/>
<smd name="8" x="-1.3716" y="-6.0452" dx="1.7" dy="3.5" layer="16"/>
<smd name="9" x="-4.1148" y="-6.0452" dx="1.7" dy="3.5" layer="16"/>
<text x="-8.89" y="-10.16" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="1.27" y="-10.16" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<text x="5.0528" y="-2.1786" size="1.27" layer="21" ratio="10">1</text>
<text x="-5.8878" y="-2.1786" size="1.27" layer="21" ratio="10">5</text>
<text x="-3.6878" y="-2.1966" size="1.27" layer="22" ratio="10" rot="MR0">9</text>
<text x="4.6528" y="-2.1966" size="1.27" layer="22" ratio="10" rot="MR0">6</text>
<text x="11.43" y="-2.54" size="1.27" layer="48">Board</text>
<text x="-7.62" y="5.469" size="1.27" layer="51" ratio="10">M09D</text>
<text x="-16.51" y="-2.54" size="1.27" layer="48">Board </text>
<rectangle x1="-15.494" y1="0.916" x2="15.494" y2="1.424" layer="21"/>
<rectangle x1="-6.0864" y1="-6.56" x2="-4.8864" y2="-4.06" layer="51"/>
<rectangle x1="3.5148" y1="-6.56" x2="4.7148" y2="-4.06" layer="52"/>
<rectangle x1="0.7716" y1="-6.56" x2="1.9716" y2="-4.06" layer="52"/>
<rectangle x1="-1.9716" y1="-6.56" x2="-0.7716" y2="-4.06" layer="52"/>
<rectangle x1="-3.3432" y1="-6.56" x2="-2.1432" y2="-4.06" layer="51"/>
<rectangle x1="-0.6" y1="-6.56" x2="0.6" y2="-4.06" layer="51"/>
<rectangle x1="2.1432" y1="-6.56" x2="3.3432" y2="-4.06" layer="51"/>
<rectangle x1="4.8864" y1="-6.56" x2="6.0864" y2="-4.06" layer="51"/>
<rectangle x1="-4.7148" y1="-6.56" x2="-3.5148" y2="-4.06" layer="52"/>
<rectangle x1="-9.2" y1="-4.16" x2="9.2" y2="-2.56" layer="21"/>
</package>
<package name="M09H">
<description>&lt;b&gt;SUB-D&lt;/b&gt;</description>
<wire x1="8.255" y1="-17.526" x2="8.001" y2="-17.526" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-17.526" x2="8.763" y2="-17.018" width="0.1524" layer="21" curve="90"/>
<wire x1="-8.763" y1="-17.018" x2="-8.255" y2="-17.526" width="0.1524" layer="21" curve="90"/>
<wire x1="8.382" y1="-11.684" x2="-8.382" y2="-11.684" width="0.1524" layer="21"/>
<wire x1="15.494" y1="-7.62" x2="10.414" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="15.494" y1="-7.62" x2="15.494" y2="-10.668" width="0.1524" layer="21"/>
<wire x1="15.494" y1="-7.62" x2="15.494" y2="-7.493" width="0.1524" layer="21"/>
<wire x1="15.494" y1="-10.668" x2="15.494" y2="-11.176" width="0.1524" layer="21"/>
<wire x1="15.494" y1="-11.176" x2="15.494" y2="-11.684" width="0.1524" layer="21"/>
<wire x1="-15.494" y1="-11.684" x2="-15.494" y2="-11.176" width="0.1524" layer="21"/>
<wire x1="-15.494" y1="-11.176" x2="-15.494" y2="-10.668" width="0.1524" layer="21"/>
<wire x1="-15.494" y1="-10.668" x2="-15.494" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-15.494" y1="-7.62" x2="-15.494" y2="-7.493" width="0.1524" layer="21"/>
<wire x1="15.494" y1="-11.176" x2="12.954" y2="-11.176" width="0.1524" layer="21"/>
<wire x1="12.954" y1="-11.176" x2="10.414" y2="-11.176" width="0.1524" layer="21"/>
<wire x1="10.414" y1="-10.668" x2="10.414" y2="-11.176" width="0.1524" layer="21"/>
<wire x1="10.414" y1="-10.668" x2="10.287" y2="-10.668" width="0.1524" layer="21"/>
<wire x1="9.652" y1="-7.62" x2="10.287" y2="-8.255" width="0.1524" layer="21" curve="-90"/>
<wire x1="10.414" y1="-7.62" x2="9.525" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="9.525" y1="-7.62" x2="9.271" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="10.287" y1="-8.255" x2="10.287" y2="-10.668" width="0.1524" layer="21"/>
<wire x1="9.271" y1="-7.62" x2="9.271" y2="-6.858" width="0.1524" layer="21"/>
<wire x1="9.271" y1="-7.62" x2="-9.271" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="9.271" y1="-6.858" x2="-9.271" y2="-6.858" width="0.1524" layer="21"/>
<wire x1="-9.271" y1="-7.62" x2="-9.271" y2="-6.858" width="0.1524" layer="21"/>
<wire x1="-9.271" y1="-7.62" x2="-9.525" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="-7.62" x2="-10.414" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-12.954" y1="-11.176" x2="-15.494" y2="-11.176" width="0.1524" layer="21"/>
<wire x1="10.414" y1="-11.176" x2="-10.414" y2="-11.176" width="0.1524" layer="21"/>
<wire x1="-10.414" y1="-11.176" x2="-12.954" y2="-11.176" width="0.1524" layer="21"/>
<wire x1="-10.414" y1="-10.668" x2="-10.414" y2="-11.176" width="0.1524" layer="21"/>
<wire x1="-10.287" y1="-8.255" x2="-10.287" y2="-10.668" width="0.1524" layer="21"/>
<wire x1="-10.287" y1="-10.668" x2="-10.414" y2="-10.668" width="0.1524" layer="21"/>
<wire x1="-10.287" y1="-8.255" x2="-9.652" y2="-7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="14.859" y1="3.175" x2="15.494" y2="2.54" width="0.1524" layer="21"/>
<wire x1="15.494" y1="2.54" x2="15.494" y2="-7.493" width="0.1524" layer="21"/>
<wire x1="10.033" y1="3.175" x2="14.859" y2="3.175" width="0.1524" layer="21"/>
<wire x1="10.033" y1="3.175" x2="9.525" y2="2.667" width="0.1524" layer="21"/>
<wire x1="9.525" y1="2.667" x2="9.525" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="2.667" x2="-9.525" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="2.667" x2="-10.033" y2="3.175" width="0.1524" layer="21"/>
<wire x1="-10.033" y1="3.175" x2="-14.986" y2="3.175" width="0.1524" layer="21"/>
<wire x1="-14.986" y1="3.175" x2="-15.494" y2="2.667" width="0.1524" layer="21"/>
<wire x1="-15.494" y1="2.667" x2="-15.494" y2="-7.493" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-13.97" x2="-2.794" y2="-13.97" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="-13.97" x2="-2.794" y2="-16.256" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-16.256" x2="-5.08" y2="-13.97" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-13.97" x2="1.143" y2="-13.97" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-13.97" x2="1.143" y2="-16.256" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-16.256" x2="-1.143" y2="-13.97" width="0.1524" layer="21"/>
<wire x1="2.794" y1="-13.97" x2="5.08" y2="-13.97" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-13.97" x2="5.08" y2="-16.256" width="0.1524" layer="21"/>
<wire x1="2.794" y1="-16.256" x2="2.794" y2="-13.97" width="0.1524" layer="21"/>
<wire x1="-10.414" y1="-7.62" x2="-15.494" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-8.382" y1="-11.684" x2="-15.494" y2="-11.684" width="0.1524" layer="21"/>
<wire x1="-8.763" y1="-12.319" x2="-8.763" y2="-17.018" width="0.1524" layer="21"/>
<wire x1="-9.398" y1="-11.684" x2="-8.763" y2="-12.319" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.494" y1="-11.684" x2="8.382" y2="-11.684" width="0.1524" layer="21"/>
<wire x1="8.763" y1="-12.319" x2="8.763" y2="-17.018" width="0.1524" layer="21"/>
<wire x1="8.763" y1="-12.319" x2="9.398" y2="-11.684" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.001" y1="-17.526" x2="-8.255" y2="-17.526" width="0.1524" layer="21"/>
<wire x1="8.001" y1="-17.526" x2="-8.001" y2="-17.526" width="0.1524" layer="21"/>
<wire x1="0" y1="1.143" x2="0" y2="0.127" width="0.8128" layer="51"/>
<wire x1="2.7432" y1="1.143" x2="2.7432" y2="0.127" width="0.8128" layer="51"/>
<wire x1="5.4864" y1="1.143" x2="5.4864" y2="0.127" width="0.8128" layer="51"/>
<wire x1="4.1148" y1="-1.397" x2="4.1148" y2="-2.413" width="0.8128" layer="51"/>
<wire x1="1.3716" y1="-1.397" x2="1.3716" y2="-2.413" width="0.8128" layer="51"/>
<wire x1="-1.3716" y1="-1.397" x2="-1.3716" y2="-2.413" width="0.8128" layer="51"/>
<wire x1="-2.7432" y1="1.143" x2="-2.7432" y2="0.127" width="0.8128" layer="51"/>
<wire x1="-4.1148" y1="-1.397" x2="-4.1148" y2="-2.413" width="0.8128" layer="51"/>
<wire x1="-5.4864" y1="1.143" x2="-5.4864" y2="0.127" width="0.8128" layer="51"/>
<circle x="12.5222" y="0" radius="1.651" width="0.1524" layer="21"/>
<circle x="12.5222" y="0" radius="2.667" width="0" layer="42"/>
<circle x="12.5222" y="0" radius="2.667" width="0" layer="43"/>
<circle x="-12.5222" y="0" radius="2.667" width="0" layer="42"/>
<circle x="-12.5222" y="0" radius="2.667" width="0" layer="43"/>
<circle x="-12.5222" y="0" radius="1.651" width="0.1524" layer="21"/>
<pad name="5" x="5.4864" y="1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="2.7432" y="1.27" drill="1.016" shape="octagon"/>
<pad name="3" x="0" y="1.27" drill="1.016" shape="octagon"/>
<pad name="2" x="-2.7432" y="1.27" drill="1.016" shape="octagon"/>
<pad name="1" x="-5.4864" y="1.27" drill="1.016" shape="octagon"/>
<pad name="9" x="4.1148" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="8" x="1.3716" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="7" x="-1.3716" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="6" x="-4.1148" y="-1.27" drill="1.016" shape="octagon"/>
<text x="-15.24" y="4.445" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-7.62" y="-20.447" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<text x="-7.62" y="0.635" size="1.27" layer="21" ratio="10">1</text>
<text x="7.239" y="0.635" size="1.27" layer="21" ratio="10">5</text>
<text x="7.239" y="-1.905" size="1.27" layer="21" ratio="10">9</text>
<text x="-7.62" y="-2.032" size="1.27" layer="21" ratio="10">6</text>
<text x="-13.589" y="-6.858" size="1.27" layer="21" ratio="10" rot="R90">2,54</text>
<text x="-7.62" y="-9.779" size="1.27" layer="21" ratio="10">M09</text>
<rectangle x1="-9.271" y1="-7.62" x2="9.271" y2="-6.858" layer="21"/>
<rectangle x1="-15.494" y1="-11.684" x2="15.494" y2="-11.176" layer="21"/>
<rectangle x1="-5.8928" y1="-6.858" x2="-5.08" y2="0.381" layer="21"/>
<rectangle x1="-4.5212" y1="-6.858" x2="-3.7084" y2="-2.159" layer="21"/>
<rectangle x1="-3.1496" y1="-6.858" x2="-2.3368" y2="0.381" layer="21"/>
<rectangle x1="-1.778" y1="-6.858" x2="-0.9652" y2="-2.159" layer="21"/>
<rectangle x1="-0.4064" y1="-6.858" x2="0.4064" y2="0.381" layer="21"/>
<rectangle x1="0.9652" y1="-6.858" x2="1.778" y2="-2.159" layer="21"/>
<rectangle x1="2.3368" y1="-6.858" x2="3.1496" y2="0.381" layer="21"/>
<rectangle x1="3.7084" y1="-6.858" x2="4.5212" y2="-2.159" layer="21"/>
<rectangle x1="5.08" y1="-6.858" x2="5.8928" y2="0.381" layer="21"/>
<hole x="12.5222" y="0" drill="3.302"/>
<hole x="-12.5222" y="0" drill="3.302"/>
</package>
<package name="M09HP">
<description>&lt;b&gt;SUB-D&lt;/b&gt;</description>
<wire x1="8.255" y1="-17.526" x2="8.001" y2="-17.526" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-17.526" x2="8.763" y2="-17.018" width="0.1524" layer="21" curve="90"/>
<wire x1="-8.763" y1="-17.018" x2="-8.255" y2="-17.526" width="0.1524" layer="21" curve="90"/>
<wire x1="8.382" y1="-11.684" x2="8.382" y2="-17.018" width="0.1524" layer="21"/>
<wire x1="8.382" y1="-11.684" x2="-8.382" y2="-11.684" width="0.1524" layer="21"/>
<wire x1="-8.382" y1="-11.684" x2="-8.382" y2="-17.018" width="0.1524" layer="21"/>
<wire x1="15.494" y1="-7.62" x2="10.414" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="15.494" y1="-7.62" x2="15.494" y2="-10.668" width="0.1524" layer="21"/>
<wire x1="15.494" y1="-7.62" x2="15.494" y2="-7.493" width="0.1524" layer="21"/>
<wire x1="15.494" y1="-10.668" x2="12.954" y2="-10.668" width="0.1524" layer="21"/>
<wire x1="15.494" y1="-10.668" x2="15.494" y2="-11.176" width="0.1524" layer="21"/>
<wire x1="15.494" y1="-11.176" x2="15.494" y2="-11.684" width="0.1524" layer="21"/>
<wire x1="-15.494" y1="-11.684" x2="-15.494" y2="-11.176" width="0.1524" layer="21"/>
<wire x1="-15.494" y1="-11.176" x2="-15.494" y2="-10.668" width="0.1524" layer="21"/>
<wire x1="-15.494" y1="-10.668" x2="-15.494" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-15.494" y1="-7.62" x2="-15.494" y2="-7.493" width="0.1524" layer="21"/>
<wire x1="12.954" y1="-10.414" x2="10.414" y2="-10.414" width="0.1524" layer="21"/>
<wire x1="15.494" y1="-11.176" x2="12.954" y2="-11.176" width="0.1524" layer="21"/>
<wire x1="12.954" y1="-11.176" x2="10.414" y2="-11.176" width="0.1524" layer="21"/>
<wire x1="12.954" y1="-10.668" x2="12.954" y2="-11.176" width="0.1524" layer="21"/>
<wire x1="12.954" y1="-10.668" x2="12.954" y2="-10.414" width="0.1524" layer="21"/>
<wire x1="10.414" y1="-10.414" x2="10.414" y2="-10.668" width="0.1524" layer="21"/>
<wire x1="10.414" y1="-10.668" x2="10.414" y2="-11.176" width="0.1524" layer="21"/>
<wire x1="15.494" y1="-7.493" x2="10.414" y2="-7.493" width="0.1524" layer="21"/>
<wire x1="10.414" y1="-7.493" x2="10.414" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="10.414" y1="-10.668" x2="10.287" y2="-10.668" width="0.1524" layer="21"/>
<wire x1="10.287" y1="-10.668" x2="-10.287" y2="-10.668" width="0.1524" layer="21"/>
<wire x1="9.652" y1="-7.62" x2="10.287" y2="-8.255" width="0.1524" layer="21" curve="-90"/>
<wire x1="10.414" y1="-7.62" x2="9.525" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="9.525" y1="-7.62" x2="9.271" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="10.287" y1="-8.255" x2="10.287" y2="-10.668" width="0.1524" layer="21"/>
<wire x1="9.271" y1="-7.62" x2="9.271" y2="-6.858" width="0.1524" layer="21"/>
<wire x1="9.271" y1="-7.62" x2="-9.271" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="9.271" y1="-6.858" x2="-9.271" y2="-6.858" width="0.1524" layer="21"/>
<wire x1="-9.271" y1="-7.62" x2="-9.271" y2="-6.858" width="0.1524" layer="21"/>
<wire x1="-9.271" y1="-7.62" x2="-9.525" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="-7.62" x2="-10.414" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-10.414" y1="-7.493" x2="-15.494" y2="-7.493" width="0.1524" layer="21"/>
<wire x1="-12.954" y1="-10.668" x2="-12.954" y2="-11.176" width="0.1524" layer="21"/>
<wire x1="-12.954" y1="-10.668" x2="-15.494" y2="-10.668" width="0.1524" layer="21"/>
<wire x1="-12.954" y1="-11.176" x2="-15.494" y2="-11.176" width="0.1524" layer="21"/>
<wire x1="-10.414" y1="-10.414" x2="-12.954" y2="-10.414" width="0.1524" layer="21"/>
<wire x1="-12.954" y1="-10.668" x2="-12.954" y2="-10.414" width="0.1524" layer="21"/>
<wire x1="10.414" y1="-11.176" x2="-10.414" y2="-11.176" width="0.1524" layer="21"/>
<wire x1="-10.414" y1="-11.176" x2="-12.954" y2="-11.176" width="0.1524" layer="21"/>
<wire x1="-10.414" y1="-10.414" x2="-10.414" y2="-10.668" width="0.1524" layer="21"/>
<wire x1="-10.414" y1="-10.668" x2="-10.414" y2="-11.176" width="0.1524" layer="21"/>
<wire x1="-10.287" y1="-8.255" x2="-10.287" y2="-10.668" width="0.1524" layer="21"/>
<wire x1="-10.287" y1="-10.668" x2="-10.414" y2="-10.668" width="0.1524" layer="21"/>
<wire x1="-10.287" y1="-8.255" x2="-9.652" y2="-7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="14.859" y1="3.175" x2="15.494" y2="2.54" width="0.1524" layer="21"/>
<wire x1="15.494" y1="2.54" x2="15.494" y2="-7.493" width="0.1524" layer="21"/>
<wire x1="10.033" y1="3.175" x2="14.859" y2="3.175" width="0.1524" layer="21"/>
<wire x1="10.033" y1="3.175" x2="9.525" y2="2.667" width="0.1524" layer="21"/>
<wire x1="9.525" y1="2.667" x2="9.525" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="2.667" x2="-9.525" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="2.667" x2="-10.033" y2="3.175" width="0.1524" layer="21"/>
<wire x1="-10.033" y1="3.175" x2="-14.986" y2="3.175" width="0.1524" layer="21"/>
<wire x1="-14.986" y1="3.175" x2="-15.494" y2="2.667" width="0.1524" layer="21"/>
<wire x1="-15.494" y1="2.667" x2="-15.494" y2="-7.493" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-13.97" x2="-2.794" y2="-13.97" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="-13.97" x2="-2.794" y2="-16.256" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="-16.256" x2="-3.175" y2="-16.256" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-16.256" x2="-3.175" y2="-14.351" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-14.351" x2="-4.699" y2="-14.351" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-14.351" x2="-4.699" y2="-16.256" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-16.256" x2="-5.08" y2="-16.256" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-16.256" x2="-5.08" y2="-13.97" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-13.97" x2="1.143" y2="-13.97" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-13.97" x2="1.143" y2="-16.256" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-16.256" x2="0.762" y2="-16.256" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-16.256" x2="0.762" y2="-14.351" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-14.351" x2="-0.762" y2="-14.351" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-14.351" x2="-0.762" y2="-16.256" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-16.256" x2="-1.143" y2="-16.256" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-16.256" x2="-1.143" y2="-13.97" width="0.1524" layer="21"/>
<wire x1="2.794" y1="-13.97" x2="5.08" y2="-13.97" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-13.97" x2="5.08" y2="-16.256" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-16.256" x2="4.699" y2="-16.256" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-16.256" x2="4.699" y2="-14.351" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-14.351" x2="3.175" y2="-14.351" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-14.351" x2="3.175" y2="-16.256" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-16.256" x2="2.794" y2="-16.256" width="0.1524" layer="21"/>
<wire x1="2.794" y1="-16.256" x2="2.794" y2="-13.97" width="0.1524" layer="21"/>
<wire x1="-10.414" y1="-7.493" x2="-10.414" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-10.414" y1="-7.62" x2="-15.494" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-8.382" y1="-11.684" x2="-15.494" y2="-11.684" width="0.1524" layer="21"/>
<wire x1="-8.763" y1="-12.319" x2="-8.763" y2="-17.018" width="0.1524" layer="21"/>
<wire x1="-9.398" y1="-11.684" x2="-8.763" y2="-12.319" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.494" y1="-11.684" x2="8.382" y2="-11.684" width="0.1524" layer="21"/>
<wire x1="8.763" y1="-12.319" x2="8.763" y2="-17.018" width="0.1524" layer="21"/>
<wire x1="8.763" y1="-12.319" x2="9.398" y2="-11.684" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.001" y1="-12.192" x2="-8.001" y2="-17.526" width="0.1524" layer="21"/>
<wire x1="-8.001" y1="-17.526" x2="-8.255" y2="-17.526" width="0.1524" layer="21"/>
<wire x1="8.001" y1="-12.192" x2="8.001" y2="-17.526" width="0.1524" layer="21"/>
<wire x1="8.001" y1="-17.526" x2="-8.001" y2="-17.526" width="0.1524" layer="21"/>
<wire x1="0" y1="1.143" x2="0" y2="0.127" width="0.8128" layer="51"/>
<wire x1="2.7432" y1="1.143" x2="2.7432" y2="0.127" width="0.8128" layer="51"/>
<wire x1="5.4864" y1="1.143" x2="5.4864" y2="0.127" width="0.8128" layer="51"/>
<wire x1="4.1148" y1="-1.397" x2="4.1148" y2="-2.413" width="0.8128" layer="51"/>
<wire x1="1.3716" y1="-1.397" x2="1.3716" y2="-2.413" width="0.8128" layer="51"/>
<wire x1="-1.3716" y1="-1.397" x2="-1.3716" y2="-2.413" width="0.8128" layer="51"/>
<wire x1="-2.7432" y1="1.143" x2="-2.7432" y2="0.127" width="0.8128" layer="51"/>
<wire x1="-4.1148" y1="-1.397" x2="-4.1148" y2="-2.413" width="0.8128" layer="51"/>
<wire x1="-5.4864" y1="1.143" x2="-5.4864" y2="0.127" width="0.8128" layer="51"/>
<circle x="12.5222" y="0" radius="1.651" width="0.1524" layer="21"/>
<circle x="-12.5222" y="0" radius="1.651" width="0.1524" layer="21"/>
<pad name="5" x="5.4864" y="1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="2.7432" y="1.27" drill="1.016" shape="octagon"/>
<pad name="3" x="0" y="1.27" drill="1.016" shape="octagon"/>
<pad name="2" x="-2.7432" y="1.27" drill="1.016" shape="octagon"/>
<pad name="1" x="-5.4864" y="1.27" drill="1.016" shape="octagon"/>
<pad name="9" x="4.1148" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="8" x="1.3716" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="7" x="-1.3716" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="6" x="-4.1148" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="G1" x="-12.5222" y="0" drill="3.302" diameter="5.08"/>
<pad name="G2" x="12.5222" y="0" drill="3.302" diameter="5.08"/>
<text x="-15.24" y="4.445" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-7.62" y="-20.447" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<text x="-7.62" y="0.635" size="1.27" layer="21" ratio="10">1</text>
<text x="7.239" y="0.635" size="1.27" layer="21" ratio="10">5</text>
<text x="7.239" y="-1.905" size="1.27" layer="21" ratio="10">9</text>
<text x="-7.62" y="-2.032" size="1.27" layer="21" ratio="10">6</text>
<text x="-13.589" y="-6.858" size="1.27" layer="21" ratio="10" rot="R90">2,54</text>
<text x="-7.62" y="-9.779" size="1.27" layer="21" ratio="10">M09</text>
<rectangle x1="9.652" y1="-10.668" x2="9.906" y2="-8.255" layer="21"/>
<rectangle x1="-9.906" y1="-10.668" x2="-9.652" y2="-8.255" layer="21"/>
<rectangle x1="12.0142" y1="-7.62" x2="13.0302" y2="-5.969" layer="21"/>
<rectangle x1="-13.0302" y1="-7.62" x2="-12.0142" y2="-5.969" layer="21"/>
<rectangle x1="-9.271" y1="-7.62" x2="9.271" y2="-6.858" layer="21"/>
<rectangle x1="-15.494" y1="-11.684" x2="15.494" y2="-11.176" layer="21"/>
<rectangle x1="-5.8928" y1="-6.858" x2="-5.08" y2="0.381" layer="21"/>
<rectangle x1="-4.5212" y1="-6.858" x2="-3.7084" y2="-2.159" layer="21"/>
<rectangle x1="-3.1496" y1="-6.858" x2="-2.3368" y2="0.381" layer="21"/>
<rectangle x1="-1.778" y1="-6.858" x2="-0.9652" y2="-2.159" layer="21"/>
<rectangle x1="-0.4064" y1="-6.858" x2="0.4064" y2="0.381" layer="21"/>
<rectangle x1="0.9652" y1="-6.858" x2="1.778" y2="-2.159" layer="21"/>
<rectangle x1="2.3368" y1="-6.858" x2="3.1496" y2="0.381" layer="21"/>
<rectangle x1="3.7084" y1="-6.858" x2="4.5212" y2="-2.159" layer="21"/>
<rectangle x1="5.08" y1="-6.858" x2="5.8928" y2="0.381" layer="21"/>
</package>
<package name="M09V">
<description>&lt;b&gt;SUB-D&lt;/b&gt;</description>
<wire x1="-7.5184" y1="-2.9464" x2="-8.3058" y2="2.3368" width="0.1524" layer="21"/>
<wire x1="6.223" y1="-3.937" x2="7.5317" y2="-2.905" width="0.1524" layer="21" curve="76.489196"/>
<wire x1="6.985" y1="3.937" x2="8.3005" y2="2.3038" width="0.1524" layer="21" curve="-102.298925"/>
<wire x1="8.3058" y1="2.3114" x2="7.5184" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="3.937" x2="6.985" y2="3.937" width="0.1524" layer="21"/>
<wire x1="-8.3051" y1="2.3268" x2="-6.985" y2="3.937" width="0.1524" layer="21" curve="-101.30773"/>
<wire x1="-7.5259" y1="-2.9295" x2="-6.223" y2="-3.937" width="0.1524" layer="21" curve="75.428151"/>
<wire x1="-6.223" y1="-3.937" x2="6.223" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-6.223" x2="-12.7" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-6.223" x2="-10.16" y2="-6.223" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-6.096" x2="-10.16" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-6.096" x2="-10.16" y2="-6.223" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-6.223" x2="10.16" y2="-6.223" width="0.1524" layer="21"/>
<wire x1="-14.859" y1="-6.223" x2="-12.7" y2="-6.223" width="0.1524" layer="21"/>
<wire x1="-15.494" y1="-5.588" x2="-14.859" y2="-6.223" width="0.1524" layer="21" curve="90"/>
<wire x1="14.859" y1="-6.223" x2="15.494" y2="-5.588" width="0.1524" layer="21" curve="90"/>
<wire x1="14.859" y1="6.223" x2="12.7" y2="6.223" width="0.1524" layer="21"/>
<wire x1="15.494" y1="5.588" x2="15.494" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="14.859" y1="6.223" x2="15.494" y2="5.588" width="0.1524" layer="21" curve="-90"/>
<wire x1="-15.494" y1="5.588" x2="-15.494" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="-15.494" y1="5.588" x2="-14.859" y2="6.223" width="0.1524" layer="21" curve="-90"/>
<wire x1="10.16" y1="-6.223" x2="10.16" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-6.223" x2="12.7" y2="-6.223" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-6.096" x2="12.7" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-6.096" x2="12.7" y2="-6.223" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-6.223" x2="14.859" y2="-6.223" width="0.1524" layer="21"/>
<wire x1="10.16" y1="6.223" x2="10.16" y2="6.096" width="0.1524" layer="21"/>
<wire x1="10.16" y1="6.223" x2="-10.16" y2="6.223" width="0.1524" layer="21"/>
<wire x1="10.16" y1="6.096" x2="12.7" y2="6.096" width="0.1524" layer="21"/>
<wire x1="12.7" y1="6.096" x2="12.7" y2="6.223" width="0.1524" layer="21"/>
<wire x1="12.7" y1="6.223" x2="10.16" y2="6.223" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="6.223" x2="-12.7" y2="6.096" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="6.223" x2="-14.859" y2="6.223" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="6.096" x2="-10.16" y2="6.096" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="6.096" x2="-10.16" y2="6.223" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="6.223" x2="-12.7" y2="6.223" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="4.318" x2="6.985" y2="4.318" width="0.1524" layer="21"/>
<wire x1="8.6868" y1="2.3114" x2="7.9248" y2="-2.8702" width="0.1524" layer="21"/>
<wire x1="6.985" y1="4.318" x2="8.679" y2="2.2521" width="0.1524" layer="21" curve="-101.297755"/>
<wire x1="-7.8994" y1="-2.9464" x2="-8.6868" y2="2.3368" width="0.1524" layer="21"/>
<wire x1="-8.6787" y1="2.2521" x2="-6.985" y2="4.318" width="0.1524" layer="21" curve="-101.307706"/>
<wire x1="-6.223" y1="-4.318" x2="6.223" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-7.935" y1="-2.8191" x2="-6.223" y2="-4.318" width="0.1524" layer="21" curve="82.402958"/>
<wire x1="6.223" y1="-4.318" x2="7.9473" y2="-2.6849" width="0.1524" layer="21" curve="86.865803"/>
<circle x="-12.5222" y="0" radius="2.667" width="0" layer="42"/>
<circle x="-12.5222" y="0" radius="2.667" width="0" layer="43"/>
<circle x="12.5222" y="0" radius="2.667" width="0" layer="42"/>
<circle x="12.5222" y="0" radius="2.667" width="0" layer="43"/>
<circle x="-12.5222" y="0" radius="1.651" width="0.1524" layer="21"/>
<circle x="12.5222" y="0" radius="1.651" width="0.1524" layer="21"/>
<circle x="0" y="1.4224" radius="0.254" width="0.4064" layer="51"/>
<circle x="1.3716" y="-1.4224" radius="0.254" width="0.4064" layer="51"/>
<circle x="2.7432" y="1.4224" radius="0.254" width="0.4064" layer="51"/>
<circle x="4.1148" y="-1.4224" radius="0.254" width="0.4064" layer="51"/>
<circle x="5.4864" y="1.4224" radius="0.254" width="0.4064" layer="51"/>
<circle x="-1.3716" y="-1.4224" radius="0.254" width="0.4064" layer="51"/>
<circle x="-2.7432" y="1.4224" radius="0.254" width="0.4064" layer="51"/>
<circle x="-4.1148" y="-1.4224" radius="0.254" width="0.4064" layer="51"/>
<circle x="-5.4864" y="1.4224" radius="0.254" width="0.4064" layer="51"/>
<pad name="5" x="5.4864" y="1.4224" drill="1.016" shape="octagon"/>
<pad name="4" x="2.7432" y="1.4224" drill="1.016" shape="octagon"/>
<pad name="3" x="0" y="1.4224" drill="1.016" shape="octagon"/>
<pad name="2" x="-2.7432" y="1.4224" drill="1.016" shape="octagon"/>
<pad name="1" x="-5.4864" y="1.4224" drill="1.016" shape="octagon"/>
<pad name="9" x="4.1148" y="-1.4224" drill="1.016" shape="octagon"/>
<pad name="8" x="1.3716" y="-1.4224" drill="1.016" shape="octagon"/>
<pad name="7" x="-1.3716" y="-1.4224" drill="1.016" shape="octagon"/>
<pad name="6" x="-4.1148" y="-1.4224" drill="1.016" shape="octagon"/>
<text x="-0.381" y="2.4384" size="0.9906" layer="21" ratio="12">3</text>
<text x="-15.24" y="6.985" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="6.985" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<text x="-5.842" y="2.4384" size="0.9906" layer="21" ratio="12">1</text>
<text x="-3.175" y="2.4384" size="0.9906" layer="21" ratio="12">2</text>
<text x="2.286" y="2.4384" size="0.9906" layer="21" ratio="12">4</text>
<text x="5.08" y="2.4384" size="0.9906" layer="21" ratio="12">5</text>
<text x="-4.572" y="-3.4544" size="0.9906" layer="21" ratio="12">6</text>
<text x="-1.778" y="-3.5814" size="0.9906" layer="21" ratio="12">7</text>
<text x="1.016" y="-3.5814" size="0.9906" layer="21" ratio="12">8</text>
<text x="3.81" y="-3.5814" size="0.9906" layer="21" ratio="12">9</text>
<hole x="-12.5222" y="0" drill="3.302"/>
<hole x="12.5222" y="0" drill="3.302"/>
</package>
<package name="M09VP">
<description>&lt;b&gt;SUB-D&lt;/b&gt;</description>
<wire x1="-7.5184" y1="-2.9464" x2="-8.3058" y2="2.3368" width="0.1524" layer="21"/>
<wire x1="6.223" y1="-3.937" x2="7.5317" y2="-2.905" width="0.1524" layer="21" curve="76.489196"/>
<wire x1="6.985" y1="3.937" x2="8.3005" y2="2.3038" width="0.1524" layer="21" curve="-102.298925"/>
<wire x1="8.3058" y1="2.3114" x2="7.5184" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="3.937" x2="6.985" y2="3.937" width="0.1524" layer="21"/>
<wire x1="-8.3051" y1="2.3268" x2="-6.985" y2="3.937" width="0.1524" layer="21" curve="-101.30773"/>
<wire x1="-7.5259" y1="-2.9295" x2="-6.223" y2="-3.937" width="0.1524" layer="21" curve="75.428151"/>
<wire x1="-6.223" y1="-3.937" x2="6.223" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-6.223" x2="-12.7" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-6.223" x2="-10.16" y2="-6.223" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-6.096" x2="-10.16" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-6.096" x2="-10.16" y2="-6.223" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-6.223" x2="10.16" y2="-6.223" width="0.1524" layer="21"/>
<wire x1="-14.859" y1="-6.223" x2="-12.7" y2="-6.223" width="0.1524" layer="21"/>
<wire x1="-15.494" y1="-5.588" x2="-14.859" y2="-6.223" width="0.1524" layer="21" curve="90"/>
<wire x1="14.859" y1="-6.223" x2="15.494" y2="-5.588" width="0.1524" layer="21" curve="90"/>
<wire x1="14.859" y1="6.223" x2="12.7" y2="6.223" width="0.1524" layer="21"/>
<wire x1="15.494" y1="5.588" x2="15.494" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="14.859" y1="6.223" x2="15.494" y2="5.588" width="0.1524" layer="21" curve="-90"/>
<wire x1="-15.494" y1="5.588" x2="-15.494" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="-15.494" y1="5.588" x2="-14.859" y2="6.223" width="0.1524" layer="21" curve="-90"/>
<wire x1="10.16" y1="-6.223" x2="10.16" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-6.223" x2="12.7" y2="-6.223" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-6.096" x2="12.7" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-6.096" x2="12.7" y2="-6.223" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-6.223" x2="14.859" y2="-6.223" width="0.1524" layer="21"/>
<wire x1="10.16" y1="6.223" x2="10.16" y2="6.096" width="0.1524" layer="21"/>
<wire x1="10.16" y1="6.223" x2="-10.16" y2="6.223" width="0.1524" layer="21"/>
<wire x1="10.16" y1="6.096" x2="12.7" y2="6.096" width="0.1524" layer="21"/>
<wire x1="12.7" y1="6.096" x2="12.7" y2="6.223" width="0.1524" layer="21"/>
<wire x1="12.7" y1="6.223" x2="10.16" y2="6.223" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="6.223" x2="-12.7" y2="6.096" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="6.223" x2="-14.859" y2="6.223" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="6.096" x2="-10.16" y2="6.096" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="6.096" x2="-10.16" y2="6.223" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="6.223" x2="-12.7" y2="6.223" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="4.318" x2="6.985" y2="4.318" width="0.1524" layer="21"/>
<wire x1="8.6868" y1="2.3114" x2="7.9248" y2="-2.8702" width="0.1524" layer="21"/>
<wire x1="6.985" y1="4.318" x2="8.679" y2="2.2521" width="0.1524" layer="21" curve="-101.297755"/>
<wire x1="-7.8994" y1="-2.9464" x2="-8.6868" y2="2.3368" width="0.1524" layer="21"/>
<wire x1="-8.6787" y1="2.2521" x2="-6.985" y2="4.318" width="0.1524" layer="21" curve="-101.307706"/>
<wire x1="-6.223" y1="-4.318" x2="6.223" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-7.935" y1="-2.8191" x2="-6.223" y2="-4.318" width="0.1524" layer="21" curve="82.402958"/>
<wire x1="6.223" y1="-4.318" x2="7.9473" y2="-2.6849" width="0.1524" layer="21" curve="86.865803"/>
<circle x="-12.5222" y="0" radius="1.651" width="0.1524" layer="21"/>
<circle x="12.5222" y="0" radius="1.651" width="0.1524" layer="21"/>
<circle x="0" y="1.4224" radius="0.254" width="0.4064" layer="51"/>
<circle x="1.3716" y="-1.4224" radius="0.254" width="0.4064" layer="51"/>
<circle x="2.7432" y="1.4224" radius="0.254" width="0.4064" layer="51"/>
<circle x="4.1148" y="-1.4224" radius="0.254" width="0.4064" layer="51"/>
<circle x="5.4864" y="1.4224" radius="0.254" width="0.4064" layer="51"/>
<circle x="-1.3716" y="-1.4224" radius="0.254" width="0.4064" layer="51"/>
<circle x="-2.7432" y="1.4224" radius="0.254" width="0.4064" layer="51"/>
<circle x="-4.1148" y="-1.4224" radius="0.254" width="0.4064" layer="51"/>
<circle x="-5.4864" y="1.4224" radius="0.254" width="0.4064" layer="51"/>
<pad name="5" x="5.4864" y="1.4224" drill="1.016" shape="octagon"/>
<pad name="4" x="2.7432" y="1.4224" drill="1.016" shape="octagon"/>
<pad name="3" x="0" y="1.4224" drill="1.016" shape="octagon"/>
<pad name="2" x="-2.7432" y="1.4224" drill="1.016" shape="octagon"/>
<pad name="1" x="-5.4864" y="1.4224" drill="1.016" shape="octagon"/>
<pad name="9" x="4.1148" y="-1.4224" drill="1.016" shape="octagon"/>
<pad name="8" x="1.3716" y="-1.4224" drill="1.016" shape="octagon"/>
<pad name="7" x="-1.3716" y="-1.4224" drill="1.016" shape="octagon"/>
<pad name="6" x="-4.1148" y="-1.4224" drill="1.016" shape="octagon"/>
<pad name="G1" x="-12.5222" y="0" drill="3.302" diameter="5.08"/>
<pad name="G2" x="12.5222" y="0" drill="3.302" diameter="5.08"/>
<text x="-0.381" y="2.4384" size="0.9906" layer="21" ratio="12">3</text>
<text x="-15.24" y="6.985" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="6.985" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<text x="-5.842" y="2.4384" size="0.9906" layer="21" ratio="12">1</text>
<text x="-3.175" y="2.4384" size="0.9906" layer="21" ratio="12">2</text>
<text x="2.286" y="2.4384" size="0.9906" layer="21" ratio="12">4</text>
<text x="5.08" y="2.4384" size="0.9906" layer="21" ratio="12">5</text>
<text x="-4.572" y="-3.4544" size="0.9906" layer="21" ratio="12">6</text>
<text x="-1.778" y="-3.5814" size="0.9906" layer="21" ratio="12">7</text>
<text x="1.016" y="-3.5814" size="0.9906" layer="21" ratio="12">8</text>
<text x="3.81" y="-3.5814" size="0.9906" layer="21" ratio="12">9</text>
</package>
<package name="M09VB">
<description>&lt;b&gt;SUB-D&lt;/b&gt;</description>
<wire x1="7.5184" y1="2.9464" x2="8.3058" y2="-2.3368" width="0.1524" layer="21"/>
<wire x1="-7.5317" y1="2.905" x2="-6.223" y2="3.937" width="0.1524" layer="21" curve="-76.489196"/>
<wire x1="-8.3005" y1="-2.3038" x2="-6.985" y2="-3.937" width="0.1524" layer="21" curve="102.298925"/>
<wire x1="-8.3058" y1="-2.3114" x2="-7.5184" y2="2.921" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-3.937" x2="-6.985" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-3.937" x2="8.3051" y2="-2.3268" width="0.1524" layer="21" curve="101.30773"/>
<wire x1="6.223" y1="3.937" x2="7.5259" y2="2.9295" width="0.1524" layer="21" curve="-75.428151"/>
<wire x1="6.223" y1="3.937" x2="-6.223" y2="3.937" width="0.1524" layer="21"/>
<wire x1="12.7" y1="6.223" x2="12.7" y2="6.096" width="0.1524" layer="21"/>
<wire x1="12.7" y1="6.223" x2="10.16" y2="6.223" width="0.1524" layer="21"/>
<wire x1="12.7" y1="6.096" x2="10.16" y2="6.096" width="0.1524" layer="21"/>
<wire x1="10.16" y1="6.096" x2="10.16" y2="6.223" width="0.1524" layer="21"/>
<wire x1="10.16" y1="6.223" x2="-10.16" y2="6.223" width="0.1524" layer="21"/>
<wire x1="14.859" y1="6.223" x2="12.7" y2="6.223" width="0.1524" layer="21"/>
<wire x1="14.859" y1="6.223" x2="15.494" y2="5.588" width="0.1524" layer="21" curve="-90"/>
<wire x1="-15.494" y1="5.588" x2="-14.859" y2="6.223" width="0.1524" layer="21" curve="-90"/>
<wire x1="-14.859" y1="-6.223" x2="-12.7" y2="-6.223" width="0.1524" layer="21"/>
<wire x1="-15.494" y1="-5.588" x2="-15.494" y2="5.588" width="0.1524" layer="21"/>
<wire x1="-15.494" y1="-5.588" x2="-14.859" y2="-6.223" width="0.1524" layer="21" curve="90"/>
<wire x1="15.494" y1="-5.588" x2="15.494" y2="5.588" width="0.1524" layer="21"/>
<wire x1="14.859" y1="-6.223" x2="15.494" y2="-5.588" width="0.1524" layer="21" curve="90"/>
<wire x1="-10.16" y1="6.223" x2="-10.16" y2="6.096" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="6.223" x2="-12.7" y2="6.223" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="6.096" x2="-12.7" y2="6.096" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="6.096" x2="-12.7" y2="6.223" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="6.223" x2="-14.859" y2="6.223" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-6.223" x2="-10.16" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-6.223" x2="10.16" y2="-6.223" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-6.096" x2="-12.7" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-6.096" x2="-12.7" y2="-6.223" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-6.223" x2="-10.16" y2="-6.223" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-6.223" x2="12.7" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-6.223" x2="14.859" y2="-6.223" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-6.096" x2="10.16" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-6.096" x2="10.16" y2="-6.223" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-6.223" x2="12.7" y2="-6.223" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-4.318" x2="-6.985" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-8.6868" y1="-2.3114" x2="-7.9248" y2="2.8702" width="0.1524" layer="21"/>
<wire x1="-8.679" y1="-2.2521" x2="-6.985" y2="-4.318" width="0.1524" layer="21" curve="101.297755"/>
<wire x1="7.8994" y1="2.9464" x2="8.6868" y2="-2.3368" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-4.318" x2="8.6787" y2="-2.2521" width="0.1524" layer="21" curve="101.307706"/>
<wire x1="6.223" y1="4.318" x2="-6.223" y2="4.318" width="0.1524" layer="21"/>
<wire x1="6.223" y1="4.318" x2="7.935" y2="2.8191" width="0.1524" layer="21" curve="-82.402958"/>
<wire x1="-7.9473" y1="2.6849" x2="-6.223" y2="4.318" width="0.1524" layer="21" curve="-86.865803"/>
<circle x="0" y="-1.4224" radius="0.254" width="0.4064" layer="51"/>
<circle x="-1.3716" y="1.4224" radius="0.254" width="0.4064" layer="51"/>
<circle x="-2.7432" y="-1.4224" radius="0.254" width="0.4064" layer="51"/>
<circle x="-4.1148" y="1.4224" radius="0.254" width="0.4064" layer="51"/>
<circle x="-5.4864" y="-1.4224" radius="0.254" width="0.4064" layer="51"/>
<circle x="1.3716" y="1.4224" radius="0.254" width="0.4064" layer="51"/>
<circle x="2.7432" y="-1.4224" radius="0.254" width="0.4064" layer="51"/>
<circle x="4.1148" y="1.4224" radius="0.254" width="0.4064" layer="51"/>
<circle x="5.4864" y="-1.4224" radius="0.254" width="0.4064" layer="51"/>
<pad name="5" x="-5.4864" y="-1.4224" drill="1.016" shape="octagon"/>
<pad name="4" x="-2.7432" y="-1.4224" drill="1.016" shape="octagon"/>
<pad name="3" x="0" y="-1.4224" drill="1.016" shape="octagon"/>
<pad name="2" x="2.7432" y="-1.4224" drill="1.016" shape="octagon"/>
<pad name="1" x="5.4864" y="-1.4224" drill="1.016" shape="octagon"/>
<pad name="9" x="-4.1148" y="1.4224" drill="1.016" shape="octagon"/>
<pad name="8" x="-1.3716" y="1.4224" drill="1.016" shape="octagon"/>
<pad name="7" x="1.3716" y="1.4224" drill="1.016" shape="octagon"/>
<pad name="6" x="4.1148" y="1.4224" drill="1.016" shape="octagon"/>
<text x="0.381" y="-2.4384" size="0.9906" layer="21" ratio="12" rot="R180">3</text>
<text x="-15.24" y="6.985" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="6.985" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<text x="5.842" y="-2.4384" size="0.9906" layer="21" ratio="12" rot="R180">1</text>
<text x="3.175" y="-2.4384" size="0.9906" layer="21" ratio="12" rot="R180">2</text>
<text x="-2.286" y="-2.4384" size="0.9906" layer="21" ratio="12" rot="R180">4</text>
<text x="-5.08" y="-2.4384" size="0.9906" layer="21" ratio="12" rot="R180">5</text>
<text x="4.572" y="3.4544" size="0.9906" layer="21" ratio="12" rot="R180">6</text>
<text x="1.778" y="3.5814" size="0.9906" layer="21" ratio="12" rot="R180">7</text>
<text x="-1.016" y="3.5814" size="0.9906" layer="21" ratio="12" rot="R180">8</text>
<text x="-3.81" y="3.5814" size="0.9906" layer="21" ratio="12" rot="R180">9</text>
</package>
<package name="182-009-MALE">
<description>&lt;b&gt;NORCOMP Right Angle D-Sub Connector&lt;/b&gt;, 182 Economy Series (.318" Footprint)&lt;p&gt;
Source: http://www.norcomp.net/ .. 182-yyy-113Ryy1Rev3.pdf</description>
<wire x1="8.255" y1="-5.751" x2="8.763" y2="-5.243" width="0.1524" layer="21" curve="90"/>
<wire x1="-8.763" y1="-5.243" x2="-8.255" y2="-5.751" width="0.1524" layer="21" curve="90"/>
<wire x1="15.494" y1="12.875" x2="15.494" y2="2.885" width="0.1524" layer="21"/>
<wire x1="15.494" y1="2.885" x2="15.494" y2="0.091" width="0.1524" layer="21"/>
<wire x1="-15.494" y1="12.875" x2="-15.494" y2="0.091" width="0.1524" layer="21"/>
<wire x1="15.494" y1="0.091" x2="-15.494" y2="0.091" width="0.1524" layer="21"/>
<wire x1="-8.763" y1="-0.544" x2="-8.763" y2="-5.243" width="0.1524" layer="21"/>
<wire x1="-9.398" y1="0.091" x2="-8.763" y2="-0.544" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.763" y1="-0.544" x2="8.763" y2="-5.243" width="0.1524" layer="21"/>
<wire x1="8.763" y1="-0.544" x2="9.398" y2="0.091" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.255" y1="-5.751" x2="-8.255" y2="-5.751" width="0.1524" layer="21"/>
<wire x1="15.494" y1="2.885" x2="-15.494" y2="2.885" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="12.875" x2="-9.525" y2="3.082" width="0.1524" layer="21"/>
<wire x1="9.525" y1="3.082" x2="9.525" y2="12.875" width="0.1524" layer="21"/>
<wire x1="15.494" y1="12.8926" x2="-15.494" y2="12.8926" width="0.1524" layer="21"/>
<pad name="5" x="5.5372" y="10.9233" drill="1.1938" diameter="1.524" shape="octagon"/>
<pad name="4" x="2.7686" y="10.9233" drill="1.1938" diameter="1.524" shape="octagon"/>
<pad name="3" x="0" y="10.9233" drill="1.1938" diameter="1.524" shape="octagon"/>
<pad name="2" x="-2.7686" y="10.9233" drill="1.1938" diameter="1.524" shape="octagon"/>
<pad name="1" x="-5.5372" y="10.9233" drill="1.1938" diameter="1.524" shape="octagon"/>
<pad name="6" x="-4.1529" y="8.0797" drill="1.1938" diameter="1.524" shape="octagon"/>
<pad name="7" x="-1.3843" y="8.0797" drill="1.1938" diameter="1.524" shape="octagon"/>
<pad name="8" x="1.3843" y="8.0797" drill="1.1938" diameter="1.524" shape="octagon"/>
<pad name="9" x="4.1529" y="8.0797" drill="1.1938" diameter="1.524" shape="octagon"/>
<text x="-9.2456" y="13.335" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="2.1844" y="13.335" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<text x="-7.62" y="9.9782" size="1.27" layer="21" ratio="10">1</text>
<text x="-7.62" y="7.3112" size="1.27" layer="21" ratio="10">6</text>
<hole x="-12.446" y="9.5009" drill="3.048"/>
<hole x="12.446" y="9.5009" drill="3.048"/>
</package>
<package name="M09SMT">
<description>&lt;b&gt;D-Sub Steckverbinder&lt;/b&gt; Abgewinkelte Messerleisten SMT, Einbauhöhe 3,6mm&lt;p&gt;
Source: ERNI-D-Sub-Conmnectors-d.pdf / www.erni.com</description>
<wire x1="8.319" y1="-17.526" x2="8.065" y2="-17.526" width="0.1524" layer="21"/>
<wire x1="8.319" y1="-17.526" x2="8.827" y2="-17.018" width="0.1524" layer="21" curve="90"/>
<wire x1="-7.457" y1="-17.018" x2="-6.949" y2="-17.526" width="0.1524" layer="21" curve="90"/>
<wire x1="8.446" y1="-11.684" x2="8.446" y2="-17.018" width="0.1524" layer="21"/>
<wire x1="8.446" y1="-11.684" x2="-7.076" y2="-11.684" width="0.1524" layer="21"/>
<wire x1="-7.076" y1="-11.684" x2="-7.076" y2="-17.018" width="0.1524" layer="21"/>
<wire x1="15.558" y1="-8.92" x2="9.335" y2="-8.92" width="0.1524" layer="21"/>
<wire x1="15.558" y1="-8.92" x2="15.558" y2="-11.684" width="0.1524" layer="21"/>
<wire x1="15.558" y1="-8.92" x2="15.51" y2="-8.793" width="0.1524" layer="21"/>
<wire x1="-14.188" y1="-11.684" x2="-14.188" y2="-8.92" width="0.1524" layer="21"/>
<wire x1="-14.188" y1="-8.92" x2="-14.14" y2="-8.793" width="0.1524" layer="21"/>
<wire x1="9.335" y1="-8.92" x2="9.335" y2="-8.158" width="0.1524" layer="21"/>
<wire x1="9.335" y1="-8.92" x2="-7.965" y2="-8.92" width="0.1524" layer="21"/>
<wire x1="9.335" y1="-8.158" x2="-7.965" y2="-8.158" width="0.1524" layer="21"/>
<wire x1="-7.965" y1="-8.92" x2="-7.965" y2="-8.158" width="0.1524" layer="21"/>
<wire x1="15.51" y1="3.016" x2="15.51" y2="-8.793" width="0.1524" layer="21"/>
<wire x1="-14.14" y1="3.016" x2="-14.14" y2="-8.793" width="0.1524" layer="21"/>
<wire x1="-7.965" y1="-8.92" x2="-14.188" y2="-8.92" width="0.1524" layer="21"/>
<wire x1="-7.076" y1="-11.684" x2="-14.188" y2="-11.684" width="0.1524" layer="21"/>
<wire x1="-7.457" y1="-12.319" x2="-7.457" y2="-17.018" width="0.1524" layer="21"/>
<wire x1="-8.092" y1="-11.684" x2="-7.457" y2="-12.319" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.558" y1="-11.684" x2="8.446" y2="-11.684" width="0.1524" layer="21"/>
<wire x1="8.827" y1="-12.319" x2="8.827" y2="-17.018" width="0.1524" layer="21"/>
<wire x1="8.827" y1="-12.319" x2="9.462" y2="-11.684" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.695" y1="-12.192" x2="-6.695" y2="-17.526" width="0.1524" layer="21"/>
<wire x1="-6.695" y1="-17.526" x2="-6.949" y2="-17.526" width="0.1524" layer="21"/>
<wire x1="8.065" y1="-12.192" x2="8.065" y2="-17.526" width="0.1524" layer="21"/>
<wire x1="8.065" y1="-17.526" x2="-6.695" y2="-17.526" width="0.1524" layer="21"/>
<text x="3.51" y="5.08" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-7.855" y="5.08" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<text x="-7.62" y="3.4925" size="1.27" layer="21" ratio="10">8</text>
<text x="7.32" y="3.4925" size="1.27" layer="21" ratio="10">1</text>
<text x="-7.62" y="-2.54" size="1.27" layer="21" ratio="10">15</text>
<text x="6.3675" y="-2.54" size="1.27" layer="21" ratio="10">9</text>
<text x="-5.989" y="-15.104" size="1.27" layer="21" ratio="10">M15</text>
<rectangle x1="-7.9375" y1="-8.92" x2="9.325" y2="-8.158" layer="21"/>
<rectangle x1="-14.175" y1="-11.684" x2="15.575" y2="-11.04" layer="21"/>
<hole x="-10.24" y="0" drill="1.7"/>
<hole x="11.61" y="0" drill="1.7"/>
<smd name="1" x="5.48" y="1.975" dx="1" dy="2.8" layer="1"/>
<smd name="6" x="4.11" y="-1.975" dx="1" dy="2.8" layer="1"/>
<smd name="S@2" x="-11.115" y="-2.625" dx="1" dy="1" layer="1"/>
<smd name="S@1" x="12.485" y="-2.65" dx="1" dy="1" layer="1"/>
<wire x1="-14.3175" y1="-8.1" x2="15.6875" y2="-8.1" width="0" layer="20"/>
<polygon width="0.2" layer="1">
<vertex x="-13.9975" y="-7.9375"/>
<vertex x="-8.19" y="-7.9375"/>
<vertex x="-8.19" y="-1.5875"/>
<vertex x="-10.1875" y="-1.5875" curve="-180"/>
<vertex x="-10.1875" y="1.5875"/>
<vertex x="-8.165" y="1.5875"/>
<vertex x="-8.165" y="3.325"/>
<vertex x="-13.9975" y="3.325"/>
</polygon>
<polygon width="0.2" layer="1">
<vertex x="15.3675" y="-7.9375"/>
<vertex x="9.56" y="-7.9375"/>
<vertex x="9.56" y="-1.5875"/>
<vertex x="11.5575" y="-1.5875" curve="180"/>
<vertex x="11.5575" y="1.5875"/>
<vertex x="9.535" y="1.5875"/>
<vertex x="9.535" y="3.325"/>
<vertex x="15.3675" y="3.325"/>
</polygon>
<wire x1="15.51" y1="3.016" x2="-14.14" y2="3.016" width="0.1524" layer="21"/>
<smd name="2" x="2.74" y="1.975" dx="1" dy="2.8" layer="1"/>
<smd name="3" x="0" y="1.975" dx="1" dy="2.8" layer="1"/>
<smd name="4" x="-2.74" y="1.975" dx="1" dy="2.8" layer="1"/>
<smd name="5" x="-5.48" y="1.975" dx="1" dy="2.8" layer="1"/>
<smd name="7" x="1.37" y="-1.975" dx="1" dy="2.8" layer="1"/>
<smd name="8" x="-1.37" y="-1.975" dx="1" dy="2.8" layer="1"/>
<smd name="9" x="-4.11" y="-1.975" dx="1" dy="2.8" layer="1"/>
</package>
<package name="ASSMANN-A-DS09AA-WP">
<pad name="3" x="0" y="0" drill="1"/>
<pad name="2" x="-2.77" y="0" drill="1"/>
<pad name="4" x="2.77" y="0" drill="1"/>
<pad name="5" x="5.54" y="0" drill="1"/>
<pad name="1" x="-5.54" y="0" drill="1"/>
<pad name="8" x="1.385" y="-2.84" drill="1"/>
<pad name="7" x="-1.385" y="-2.84" drill="1"/>
<pad name="6" x="-4.155" y="-2.84" drill="1"/>
<pad name="9" x="4.155" y="-2.84" drill="1"/>
<wire x1="-10" y1="-6" x2="10" y2="-6" width="0.127" layer="20"/>
<wire x1="-19.7" y1="-9.84" x2="-18" y2="-9.84" width="0.127" layer="21"/>
<wire x1="-18" y1="-9.84" x2="18" y2="-9.84" width="0.127" layer="21"/>
<wire x1="18" y1="-9.84" x2="19.7" y2="-9.84" width="0.127" layer="21"/>
<wire x1="19.7" y1="-9.84" x2="19.7" y2="-13.69" width="0.127" layer="21"/>
<wire x1="19.7" y1="-13.69" x2="8.46" y2="-13.69" width="0.127" layer="21"/>
<wire x1="8.46" y1="-13.69" x2="-8.46" y2="-13.69" width="0.127" layer="21"/>
<wire x1="-8.46" y1="-13.69" x2="-19.7" y2="-13.69" width="0.127" layer="21"/>
<wire x1="-19.7" y1="-13.69" x2="-19.7" y2="-9.84" width="0.127" layer="21"/>
<wire x1="-18" y1="-9.84" x2="-18" y2="-9" width="0.127" layer="21"/>
<wire x1="-18" y1="-9" x2="-16" y2="-7" width="0.127" layer="21" curve="-90"/>
<wire x1="-16" y1="-7" x2="-5" y2="-8" width="0.127" layer="21"/>
<wire x1="-5" y1="-8" x2="5" y2="-8" width="0.127" layer="21"/>
<wire x1="5" y1="-8" x2="16" y2="-7" width="0.127" layer="21"/>
<wire x1="16" y1="-7" x2="18" y2="-9" width="0.127" layer="21" curve="-90"/>
<wire x1="18" y1="-9" x2="18" y2="-9.84" width="0.127" layer="21"/>
<wire x1="-5.5" y1="0" x2="-5.5" y2="-7.8" width="0.4064" layer="21"/>
<wire x1="-4.2" y1="-2.9" x2="-4.2" y2="-7.8" width="0.4064" layer="21"/>
<wire x1="-2.8" y1="0" x2="-2.8" y2="-7.8" width="0.4064" layer="21"/>
<wire x1="-1.4" y1="-2.8" x2="-1.4" y2="-7.8" width="0.4064" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="-7.8" width="0.4064" layer="21"/>
<wire x1="1.4" y1="-2.8" x2="1.4" y2="-7.8" width="0.4064" layer="21"/>
<wire x1="2.8" y1="0" x2="2.8" y2="-7.8" width="0.4064" layer="21"/>
<wire x1="4.1" y1="-2.8" x2="4.1" y2="-7.8" width="0.4064" layer="21"/>
<wire x1="5.6" y1="0" x2="5.6" y2="-7.8" width="0.4064" layer="21"/>
<wire x1="-8.46" y1="-13.69" x2="-8.46" y2="-19.69" width="0.127" layer="21"/>
<wire x1="-8.46" y1="-19.69" x2="8.46" y2="-19.69" width="0.127" layer="21"/>
<wire x1="8.46" y1="-19.69" x2="8.46" y2="-13.69" width="0.127" layer="21"/>
<wire x1="-1" y1="-17" x2="-1" y2="-16" width="0.127" layer="21"/>
<wire x1="-1" y1="-16" x2="1" y2="-16" width="0.127" layer="21"/>
<wire x1="1" y1="-16" x2="1" y2="-17" width="0.127" layer="21"/>
<wire x1="-6" y1="-17" x2="-6" y2="-16" width="0.127" layer="21"/>
<wire x1="-6" y1="-16" x2="-4" y2="-16" width="0.127" layer="21"/>
<wire x1="-4" y1="-16" x2="-4" y2="-17" width="0.127" layer="21"/>
<wire x1="4" y1="-17" x2="4" y2="-16" width="0.127" layer="21"/>
<wire x1="4" y1="-16" x2="6" y2="-16" width="0.127" layer="21"/>
<wire x1="6" y1="-16" x2="6" y2="-17" width="0.127" layer="21"/>
</package>
<package name="MA03-1">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-3.175" y1="1.27" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-3.81" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-0.635" size="1.27" layer="21" ratio="10">1</text>
<text x="-3.81" y="-2.921" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
</package>
<package name="YB2004C">
<wire x1="-73" y1="-30.5" x2="-73" y2="32" width="0.127" layer="21"/>
<wire x1="-73" y1="32" x2="73" y2="32" width="0.127" layer="21"/>
<wire x1="73" y1="32" x2="73" y2="-30.5" width="0.127" layer="21"/>
<wire x1="73" y1="-30.5" x2="-73" y2="-30.5" width="0.127" layer="21"/>
<pad name="P$1" x="-69.5" y="28.5" drill="2.5" diameter="6"/>
<pad name="P$2" x="-69.5" y="-27" drill="2.5" diameter="6"/>
<pad name="P$3" x="69.5" y="-27" drill="2.5" diameter="6"/>
<pad name="P$4" x="69.5" y="28.5" drill="2.5" diameter="6"/>
<pad name="1" x="-55" y="29.5" drill="1" shape="long" rot="R90"/>
<pad name="2" x="-52.46" y="29.5" drill="1" shape="long" rot="R90"/>
<pad name="3" x="-49.92" y="29.5" drill="1" shape="long" rot="R90"/>
<pad name="4" x="-47.38" y="29.5" drill="1" shape="long" rot="R90"/>
<pad name="5" x="-44.84" y="29.5" drill="1" shape="long" rot="R90"/>
<pad name="6" x="-42.3" y="29.5" drill="1" shape="long" rot="R90"/>
<pad name="7" x="-39.76" y="29.5" drill="1" shape="long" rot="R90"/>
<pad name="8" x="-37.22" y="29.5" drill="1" shape="long" rot="R90"/>
<pad name="9" x="-34.68" y="29.5" drill="1" shape="long" rot="R90"/>
<pad name="10" x="-32.14" y="29.5" drill="1" shape="long" rot="R90"/>
<pad name="11" x="-29.6" y="29.5" drill="1" shape="long" rot="R90"/>
<pad name="12" x="-27.06" y="29.5" drill="1" shape="long" rot="R90"/>
<pad name="13" x="-24.52" y="29.5" drill="1" shape="long" rot="R90"/>
<pad name="14" x="-21.98" y="29.5" drill="1" shape="long" rot="R90"/>
<pad name="15" x="-19.44" y="29.5" drill="1" shape="long" rot="R90"/>
<wire x1="-67" y1="27.5" x2="67" y2="27.5" width="0.127" layer="21"/>
<wire x1="67" y1="27.5" x2="67" y2="-27.5" width="0.127" layer="21"/>
<wire x1="67" y1="-27.5" x2="-67" y2="-27.5" width="0.127" layer="21"/>
<wire x1="-67" y1="-27.5" x2="-67" y2="27.5" width="0.127" layer="21"/>
<wire x1="-61.75" y1="19.5" x2="-61.75" y2="-19.5" width="0.127" layer="21"/>
<wire x1="-61.75" y1="-19.5" x2="-59.75" y2="-21.5" width="0.127" layer="21" curve="90"/>
<wire x1="-59.75" y1="-21.5" x2="59.75" y2="-21.5" width="0.127" layer="21"/>
<wire x1="59.75" y1="-21.5" x2="61.75" y2="-19.5" width="0.127" layer="21" curve="90"/>
<wire x1="61.75" y1="-19.5" x2="61.75" y2="19.5" width="0.127" layer="21"/>
<wire x1="61.75" y1="19.5" x2="59.75" y2="21.5" width="0.127" layer="21" curve="90"/>
<wire x1="59.75" y1="21.5" x2="-59.75" y2="21.5" width="0.127" layer="21"/>
<wire x1="-59.75" y1="21.5" x2="-61.75" y2="19.5" width="0.127" layer="21" curve="90"/>
<wire x1="-59.42" y1="19.235" x2="59.42" y2="19.235" width="0.127" layer="21"/>
<wire x1="59.42" y1="19.235" x2="59.42" y2="-19.235" width="0.127" layer="21"/>
<wire x1="59.42" y1="-19.235" x2="-59.42" y2="-19.235" width="0.127" layer="21"/>
<wire x1="-59.42" y1="-19.235" x2="-59.42" y2="19.235" width="0.127" layer="21"/>
<rectangle x1="-69" y1="-28.25" x2="69" y2="28.75" layer="39"/>
<pad name="16" x="-16.9" y="29.5" drill="1" shape="long" rot="R90"/>
</package>
<package name="MA05-2">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-5.715" y1="2.54" x2="-4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="2.54" x2="-3.81" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.905" x2="-3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="2.54" x2="-6.35" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="3.81" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.905" x2="4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="2.54" x2="5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.905" x2="-4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="-3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="1.905" x2="-6.35" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="-1.905" x2="-5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-2.54" x2="-5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-2.54" x2="4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-2.54" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-2.54" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.35" y1="1.905" x2="6.35" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.715" y1="2.54" x2="6.35" y2="1.905" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-1.905" x2="5.715" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="3" x="-2.54" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="5" x="0" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="7" x="2.54" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="9" x="5.08" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="2" x="-5.08" y="1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="-2.54" y="1.27" drill="1.016" shape="octagon"/>
<pad name="6" x="0" y="1.27" drill="1.016" shape="octagon"/>
<pad name="8" x="2.54" y="1.27" drill="1.016" shape="octagon"/>
<pad name="10" x="5.08" y="1.27" drill="1.016" shape="octagon"/>
<text x="-5.588" y="-4.191" size="1.27" layer="21" ratio="10">1</text>
<text x="-6.35" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="-4.191" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="4.572" y="2.921" size="1.27" layer="21" ratio="10">10</text>
<rectangle x1="-2.794" y1="-1.524" x2="-2.286" y2="-1.016" layer="51"/>
<rectangle x1="-5.334" y1="-1.524" x2="-4.826" y2="-1.016" layer="51"/>
<rectangle x1="-0.254" y1="-1.524" x2="0.254" y2="-1.016" layer="51"/>
<rectangle x1="4.826" y1="-1.524" x2="5.334" y2="-1.016" layer="51"/>
<rectangle x1="2.286" y1="-1.524" x2="2.794" y2="-1.016" layer="51"/>
<rectangle x1="-5.334" y1="1.016" x2="-4.826" y2="1.524" layer="51"/>
<rectangle x1="-2.794" y1="1.016" x2="-2.286" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="1.016" x2="0.254" y2="1.524" layer="51"/>
<rectangle x1="2.286" y1="1.016" x2="2.794" y2="1.524" layer="51"/>
<rectangle x1="4.826" y1="1.016" x2="5.334" y2="1.524" layer="51"/>
</package>
<package name="TACTILE-PTH">
<description>&lt;b&gt;OMRON SWITCH&lt;/b&gt;</description>
<wire x1="3.048" y1="1.016" x2="3.048" y2="2.54" width="0.2032" layer="51"/>
<wire x1="3.048" y1="2.54" x2="2.54" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.54" y1="-3.048" x2="3.048" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="3.048" y1="-2.54" x2="3.048" y2="-1.016" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="3.048" x2="-3.048" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-3.048" y1="2.54" x2="-3.048" y2="1.016" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-3.048" x2="-3.048" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="-3.048" y1="-2.54" x2="-3.048" y2="-1.016" width="0.2032" layer="51"/>
<wire x1="2.54" y1="-3.048" x2="2.159" y2="-3.048" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-3.048" x2="-2.159" y2="-3.048" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="3.048" x2="-2.159" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.54" y1="3.048" x2="2.159" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.159" y1="3.048" x2="-2.159" y2="3.048" width="0.2032" layer="21"/>
<wire x1="-2.159" y1="-3.048" x2="2.159" y2="-3.048" width="0.2032" layer="21"/>
<wire x1="3.048" y1="0.998" x2="3.048" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="-3.048" y1="1.028" x2="-3.048" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="0.508" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-0.508" x2="-2.54" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="0.508" x2="-2.159" y2="-0.381" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.778" width="0.2032" layer="21"/>
<pad name="1" x="-3.2512" y="2.2606" drill="1.016"/>
<pad name="2" x="3.2512" y="2.2606" drill="1.016"/>
<pad name="3" x="-3.2512" y="-2.2606" drill="1.016"/>
<pad name="4" x="3.2512" y="-2.2606" drill="1.016"/>
<text x="-2.54" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="TACTILE_SWITCH_SMD">
<wire x1="-1.54" y1="-2.54" x2="-2.54" y2="-1.54" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-1.24" x2="-2.54" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="1.54" x2="-1.54" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-1.54" y1="2.54" x2="1.54" y2="2.54" width="0.2032" layer="21"/>
<wire x1="1.54" y1="2.54" x2="2.54" y2="1.54" width="0.2032" layer="51"/>
<wire x1="2.54" y1="1.24" x2="2.54" y2="-1.24" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.54" x2="1.54" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="1.54" y1="-2.54" x2="-1.54" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="1.905" y2="0.445" width="0.127" layer="51"/>
<wire x1="1.905" y1="0.445" x2="2.16" y2="-0.01" width="0.127" layer="51"/>
<wire x1="1.905" y1="-0.23" x2="1.905" y2="-1.115" width="0.127" layer="51"/>
<circle x="0" y="0" radius="1.27" width="0.2032" layer="21"/>
<smd name="1" x="-2.54" y="1.905" dx="0.762" dy="1.524" layer="1" rot="R90"/>
<smd name="2" x="2.54" y="1.905" dx="0.762" dy="1.524" layer="1" rot="R90"/>
<smd name="3" x="-2.54" y="-1.905" dx="0.762" dy="1.524" layer="1" rot="R90"/>
<smd name="4" x="2.54" y="-1.905" dx="0.762" dy="1.524" layer="1" rot="R90"/>
<text x="-0.889" y="1.778" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.889" y="-2.032" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="TACTILE_SWITCH_SMD-2">
<wire x1="1.905" y1="1.27" x2="1.905" y2="0.445" width="0.127" layer="51"/>
<wire x1="1.905" y1="0.445" x2="2.16" y2="-0.01" width="0.127" layer="51"/>
<wire x1="1.905" y1="-0.23" x2="1.905" y2="-1.115" width="0.127" layer="51"/>
<wire x1="-2.25" y1="2.25" x2="2.25" y2="2.25" width="0.127" layer="51"/>
<wire x1="2.25" y1="2.25" x2="2.25" y2="-2.25" width="0.127" layer="51"/>
<wire x1="2.25" y1="-2.25" x2="-2.25" y2="-2.25" width="0.127" layer="51"/>
<wire x1="-2.25" y1="-2.25" x2="-2.25" y2="2.25" width="0.127" layer="51"/>
<wire x1="-2.2" y1="0.8" x2="-2.2" y2="-0.8" width="0.2032" layer="21"/>
<wire x1="1.3" y1="2.2" x2="-1.3" y2="2.2" width="0.2032" layer="21"/>
<wire x1="2.2" y1="-0.8" x2="2.2" y2="0.8" width="0.2032" layer="21"/>
<wire x1="-1.3" y1="-2.2" x2="1.3" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="2.2" y1="0.8" x2="1.8" y2="0.8" width="0.2032" layer="21"/>
<wire x1="2.2" y1="-0.8" x2="1.8" y2="-0.8" width="0.2032" layer="21"/>
<wire x1="-1.8" y1="0.8" x2="-2.2" y2="0.8" width="0.2032" layer="21"/>
<wire x1="-1.8" y1="-0.8" x2="-2.2" y2="-0.8" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="1.27" width="0.2032" layer="21"/>
<smd name="1" x="2.225" y="1.75" dx="1.1" dy="0.7" layer="1" rot="R90"/>
<smd name="2" x="2.225" y="-1.75" dx="1.1" dy="0.7" layer="1" rot="R90"/>
<smd name="3" x="-2.225" y="-1.75" dx="1.1" dy="0.7" layer="1" rot="R90"/>
<smd name="4" x="-2.225" y="1.75" dx="1.1" dy="0.7" layer="1" rot="R90"/>
<text x="-0.889" y="1.778" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.889" y="-2.032" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="SCHURTER-1301.93">
<smd name="1" x="-5" y="2.25" dx="2.1" dy="1.4" layer="1"/>
<smd name="2" x="5" y="2.25" dx="2.1" dy="1.4" layer="1"/>
<smd name="4" x="5" y="-2.25" dx="2.1" dy="1.4" layer="1"/>
<smd name="3" x="-5" y="-2.25" dx="2.1" dy="1.4" layer="1"/>
<circle x="0" y="0" radius="1.6" width="0.127" layer="21"/>
<wire x1="-3" y1="-3" x2="-3" y2="3" width="0.127" layer="21"/>
<wire x1="-3" y1="3" x2="3" y2="3" width="0.127" layer="21"/>
<wire x1="3" y1="3" x2="3" y2="-3" width="0.127" layer="21"/>
<wire x1="3" y1="-3" x2="-3" y2="-3" width="0.127" layer="21"/>
<wire x1="-5" y1="2.25" x2="-3.25" y2="2.25" width="0.7" layer="21"/>
<wire x1="-5" y1="-2.25" x2="-3.25" y2="-2.25" width="0.7" layer="21"/>
<wire x1="5" y1="-2.25" x2="3.25" y2="-2.25" width="0.7" layer="21"/>
<wire x1="3.25" y1="2.25" x2="5" y2="2.25" width="0.7" layer="21"/>
<circle x="2.25" y="2.25" radius="0.5" width="0.127" layer="21"/>
<circle x="-2.25" y="2.25" radius="0.5" width="0.127" layer="21"/>
<circle x="-2.25" y="-2.25" radius="0.5" width="0.127" layer="21"/>
<circle x="2.25" y="-2.25" radius="0.5" width="0.127" layer="21"/>
</package>
<package name="TTL-FT232R">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-6.985" y1="1.27" x2="-5.715" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="1.27" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-0.635" x2="-5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-1.27" x2="-5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0.635" x2="-7.62" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="1.27" x2="-7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-0.635" x2="-6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-1.27" x2="-6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="5.715" y2="1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.985" y2="1.27" width="0.1524" layer="21"/>
<wire x1="6.985" y1="1.27" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-0.635" x2="6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-1.27" x2="5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="0.635" x2="7.62" y2="-0.635" width="0.1524" layer="21"/>
<pad name="GND" x="-6.35" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="!CTS" x="-3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="VCC" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="TXD" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="RXD" x="3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="!RTS" x="6.35" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-7.62" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.985" y="-2.921" size="1.27" layer="21" ratio="10">1</text>
<text x="5.715" y="1.651" size="1.27" layer="21" ratio="10">6</text>
<text x="-2.54" y="-2.921" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="-6.604" y1="-0.254" x2="-6.096" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="6.096" y1="-0.254" x2="6.604" y2="0.254" layer="51"/>
</package>
<package name="10-PIN-IDC">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;&lt;p&gt; JTAG 10 Pin, 0.1" Straight</description>
<wire x1="-10" y1="4.2" x2="10" y2="4.2" width="0.2032" layer="21"/>
<wire x1="10" y1="4.2" x2="10" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="10" y1="-4.2" x2="5.938" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="5.938" y1="-4.2" x2="5.938" y2="-3.9" width="0.2032" layer="21"/>
<wire x1="5.938" y1="-3.9" x2="4.459" y2="-3.9" width="0.2032" layer="21"/>
<wire x1="4.459" y1="-3.9" x2="4.459" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="4.459" y1="-4.2" x2="1.883" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="1.883" y1="-4.2" x2="1.883" y2="-2.65" width="0.2032" layer="21"/>
<wire x1="1.883" y1="-2.65" x2="-1.883" y2="-2.65" width="0.2032" layer="21"/>
<wire x1="-1.883" y1="-2.65" x2="-1.883" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="1.883" y1="-4.2" x2="-1.883" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="-1.883" y1="-4.2" x2="-10" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="-10" y1="-4.2" x2="-10" y2="4.2" width="0.2032" layer="21"/>
<wire x1="-8.875" y1="3.275" x2="8.875" y2="3.275" width="0.2032" layer="21"/>
<wire x1="8.875" y1="3.275" x2="8.875" y2="-3.275" width="0.2032" layer="21"/>
<wire x1="8.875" y1="-3.275" x2="1.883" y2="-3.275" width="0.2032" layer="21"/>
<wire x1="-1.883" y1="-3.275" x2="-8.875" y2="-3.275" width="0.2032" layer="21"/>
<wire x1="-8.875" y1="-3.275" x2="-8.875" y2="3.275" width="0.2032" layer="21"/>
<pad name="1" x="-5.08" y="-1.27" drill="0.9" shape="square"/>
<pad name="2" x="-5.08" y="1.27" drill="0.9" shape="octagon"/>
<pad name="3" x="-2.54" y="-1.27" drill="0.9" shape="octagon"/>
<pad name="4" x="-2.54" y="1.27" drill="0.9" shape="octagon"/>
<pad name="5" x="0" y="-1.27" drill="0.9" shape="octagon"/>
<pad name="6" x="0" y="1.27" drill="0.9" shape="octagon"/>
<pad name="7" x="2.54" y="-1.27" drill="0.9" shape="octagon"/>
<pad name="8" x="2.54" y="1.27" drill="0.9" shape="octagon"/>
<pad name="9" x="5.08" y="-1.27" drill="0.9" shape="octagon"/>
<pad name="10" x="5.08" y="1.27" drill="0.9" shape="octagon"/>
<text x="-2.54" y="-5.715" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-7.62" size="1.27" layer="27" ratio="12">&gt;VALUE</text>
</package>
<package name="MICRO-SD-SOCKET-PP">
<wire x1="-14" y1="0" x2="-14" y2="13.2" width="0.2032" layer="21"/>
<wire x1="0" y1="12.1" x2="0" y2="0" width="0.2032" layer="21"/>
<wire x1="-11.7" y1="15.3" x2="-12.5" y2="15.3" width="0.2032" layer="21"/>
<wire x1="-11" y1="16" x2="0" y2="16" width="0.2032" layer="51"/>
<wire x1="-10" y1="13.6" x2="-1.6" y2="13.6" width="0.2032" layer="21"/>
<wire x1="-14" y1="0" x2="-9.1" y2="0" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="0" x2="-6.4" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="-0.7" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="17.6" x2="-11" y2="17.6" width="0.2032" layer="51"/>
<wire x1="0" y1="20.7" x2="-11" y2="20.7" width="0.2032" layer="51"/>
<wire x1="-11" y1="14.6" x2="-11.7" y2="15.3" width="0.2032" layer="21" curve="98.797411"/>
<wire x1="-11" y1="14.6" x2="-10" y2="13.6" width="0.2032" layer="21" curve="87.205638"/>
<smd name="1" x="-8.94" y="10.7" dx="0.8" dy="1.5" layer="1"/>
<smd name="2" x="-7.84" y="10.3" dx="0.8" dy="1.5" layer="1"/>
<smd name="3" x="-6.74" y="10.7" dx="0.8" dy="1.5" layer="1"/>
<smd name="4" x="-5.64" y="10.9" dx="0.8" dy="1.5" layer="1"/>
<smd name="5" x="-4.54" y="10.7" dx="0.8" dy="1.5" layer="1"/>
<smd name="6" x="-3.44" y="10.9" dx="0.8" dy="1.5" layer="1"/>
<smd name="7" x="-2.34" y="10.7" dx="0.8" dy="1.5" layer="1"/>
<smd name="8" x="-1.24" y="10.7" dx="0.8" dy="1.5" layer="1"/>
<smd name="CD1" x="-7.75" y="0.4" dx="1.8" dy="1.4" layer="1"/>
<smd name="CD2" x="-2.05" y="0.4" dx="1.8" dy="1.4" layer="1"/>
<smd name="GND1" x="-13.6" y="14.55" dx="1.4" dy="1.9" layer="1"/>
<smd name="GND2" x="-0.45" y="13.55" dx="1.4" dy="1.9" layer="1"/>
<text x="-8.89" y="6.35" size="0.8128" layer="25">&gt;Name</text>
<text x="-8.89" y="5.08" size="0.8128" layer="27">&gt;Value</text>
</package>
<package name="MA07-1">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-8.255" y1="1.27" x2="-6.985" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="1.27" x2="-6.35" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="-0.635" x2="-6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="0.635" x2="-5.715" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="1.27" x2="-4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-0.635" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-1.27" x2="-5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-1.27" x2="-6.35" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-8.89" y1="0.635" x2="-8.89" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="1.27" x2="-8.89" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-8.89" y1="-0.635" x2="-8.255" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-1.27" x2="-8.255" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.1524" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.1524" layer="21"/>
<wire x1="8.89" y1="0.635" x2="8.89" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="-5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="-2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="6" x="5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="7" x="7.62" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-8.89" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-8.255" y="-2.921" size="1.27" layer="21" ratio="10">1</text>
<text x="-2.54" y="-2.921" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="7.239" y="1.651" size="1.27" layer="21" ratio="10">7</text>
<rectangle x1="-5.334" y1="-0.254" x2="-4.826" y2="0.254" layer="51"/>
<rectangle x1="-7.874" y1="-0.254" x2="-7.366" y2="0.254" layer="51"/>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
</package>
<package name="SOD882">
<smd name="P$1" x="0.35" y="0" dx="0.7" dy="0.4" layer="1" rot="R90"/>
<smd name="P$2" x="-0.35" y="0" dx="0.7" dy="0.4" layer="1" rot="R90"/>
<wire x1="-0.65" y1="-0.45" x2="-0.65" y2="0.45" width="0.127" layer="21"/>
<wire x1="-0.65" y1="0.45" x2="0.65" y2="0.45" width="0.127" layer="21"/>
<wire x1="0.65" y1="0.45" x2="0.65" y2="-0.45" width="0.127" layer="21"/>
<wire x1="0.65" y1="-0.45" x2="-0.65" y2="-0.45" width="0.127" layer="21"/>
<text x="-1.905" y="-1.905" size="1.016" layer="25" font="vector" ratio="12">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.016" layer="27" font="vector">&gt;VALUE</text>
</package>
<package name="NOTHING">
</package>
<package name="SONITRON-SMAT-13">
<circle x="0" y="0" radius="2" width="0.127" layer="21"/>
<wire x1="-7" y1="-4.5" x2="-7" y2="4.5" width="0.127" layer="21"/>
<wire x1="-7" y1="4.5" x2="-4.5" y2="7" width="0.127" layer="21"/>
<wire x1="-4.5" y1="7" x2="4.5" y2="7" width="0.127" layer="21"/>
<wire x1="4.5" y1="7" x2="7" y2="4.5" width="0.127" layer="21"/>
<wire x1="7" y1="4.5" x2="7" y2="-4.5" width="0.127" layer="21"/>
<wire x1="7" y1="-4.5" x2="4.5" y2="-7" width="0.127" layer="21"/>
<wire x1="4.5" y1="-7" x2="-4.5" y2="-7" width="0.127" layer="21"/>
<wire x1="-4.5" y1="-7" x2="-7" y2="-4.5" width="0.127" layer="21"/>
<smd name="1" x="-7.375" y="0" dx="7.25" dy="3" layer="1"/>
<smd name="2" x="7.375" y="0" dx="7.25" dy="3" layer="1"/>
<text x="-1.905" y="-8.89" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-2.54" y="-10.16" size="1.016" layer="27" font="vector">&gt;VALUE</text>
</package>
<package name="SONITRON-SMAT-24-P10">
<circle x="0" y="0" radius="3.2" width="0.127" layer="21"/>
<wire x1="-12" y1="-6" x2="-12" y2="6" width="0.127" layer="21"/>
<wire x1="-12" y1="6" x2="-6" y2="12" width="0.127" layer="21"/>
<wire x1="-6" y1="12" x2="6" y2="12" width="0.127" layer="21"/>
<wire x1="6" y1="12" x2="12" y2="6" width="0.127" layer="21"/>
<wire x1="12" y1="6" x2="12" y2="-6" width="0.127" layer="21"/>
<wire x1="12" y1="-6" x2="6" y2="-12" width="0.127" layer="21"/>
<wire x1="6" y1="-12" x2="-6" y2="-12" width="0.127" layer="21"/>
<wire x1="-6" y1="-12" x2="-12" y2="-6" width="0.127" layer="21"/>
<text x="-1.905" y="-13.97" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-2.54" y="-15.24" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<pad name="1" x="5" y="0" drill="1" diameter="2" shape="octagon"/>
<pad name="2" x="-5" y="0" drill="1" diameter="2" shape="octagon"/>
<smd name="1S" x="10" y="0" dx="3" dy="12" layer="1" rot="R90"/>
<smd name="2-S" x="-10" y="0" dx="3" dy="12" layer="1" rot="R90"/>
</package>
<package name="SONITRON-SMAT-17-P10">
<circle x="0" y="0" radius="3.2" width="0.127" layer="21"/>
<wire x1="-7.5" y1="-4.5" x2="-7.5" y2="4.5" width="0.127" layer="21"/>
<wire x1="-7.5" y1="4.5" x2="-4.5" y2="7.5" width="0.127" layer="21"/>
<wire x1="-4.5" y1="7.5" x2="4.5" y2="7.5" width="0.127" layer="21"/>
<wire x1="4.5" y1="7.5" x2="7.5" y2="4.5" width="0.127" layer="21"/>
<wire x1="7.5" y1="4.5" x2="7.5" y2="-4.5" width="0.127" layer="21"/>
<wire x1="7.5" y1="-4.5" x2="4.5" y2="-7.5" width="0.127" layer="21"/>
<wire x1="4.5" y1="-7.5" x2="-4.5" y2="-7.5" width="0.127" layer="21"/>
<wire x1="-4.5" y1="-7.5" x2="-7.5" y2="-4.5" width="0.127" layer="21"/>
<text x="-1.905" y="-8.89" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-2.54" y="-10.16" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<pad name="1" x="5" y="0" drill="1" diameter="2" shape="octagon"/>
<pad name="2" x="-5" y="0" drill="1" diameter="2" shape="octagon"/>
<smd name="1S" x="8.125" y="0" dx="3" dy="7.25" layer="1" rot="R90" cream="no"/>
<smd name="2-S" x="-8.125" y="0" dx="3" dy="7.25" layer="1" rot="R90" cream="no"/>
</package>
<package name="618009231221">
<description>WR-DSUB 8.08mm Male Angled PCB Connector with Hex Screw, 9 Pins</description>
<wire x1="-15.4" y1="3" x2="-9.525" y2="3" width="0.127" layer="21"/>
<wire x1="-9.525" y1="3" x2="9.525" y2="3" width="0.127" layer="21"/>
<wire x1="9.525" y1="3" x2="15.4" y2="3" width="0.127" layer="21"/>
<pad name="3" x="0" y="1.42" drill="1"/>
<pad name="2" x="-2.77" y="1.42" drill="1"/>
<pad name="1" x="-5.54" y="1.42" drill="1"/>
<pad name="4" x="2.77" y="1.42" drill="1"/>
<pad name="5" x="5.54" y="1.42" drill="1"/>
<pad name="Z1" x="-12.5" y="0" drill="3.2" diameter="5"/>
<pad name="Z2" x="12.5" y="0" drill="3.2" diameter="5"/>
<pad name="6" x="-4.155" y="-1.42" drill="1"/>
<pad name="7" x="-1.385" y="-1.42" drill="1"/>
<pad name="8" x="1.385" y="-1.42" drill="1"/>
<pad name="9" x="4.155" y="-1.42" drill="1"/>
<text x="-7.62" y="0.635" size="1.27" layer="21">1</text>
<text x="6.35" y="-1.905" size="1.27" layer="21">9</text>
<text x="17.355" y="-4.12" size="1.27" layer="25">&gt;NAME</text>
<text x="17.355" y="-6.96" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="8.255" y1="-15.621" x2="8.001" y2="-15.621" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-15.621" x2="8.763" y2="-15.113" width="0.1524" layer="21" curve="90"/>
<wire x1="-8.763" y1="-15.113" x2="-8.255" y2="-15.621" width="0.1524" layer="21" curve="90"/>
<wire x1="8.382" y1="-9.779" x2="8.382" y2="-15.113" width="0.1524" layer="21"/>
<wire x1="8.382" y1="-9.779" x2="-8.382" y2="-9.779" width="0.1524" layer="21"/>
<wire x1="-8.382" y1="-9.779" x2="-8.382" y2="-15.113" width="0.1524" layer="21"/>
<wire x1="15.494" y1="-5.715" x2="10.414" y2="-5.715" width="0.1524" layer="21"/>
<wire x1="15.494" y1="-5.715" x2="15.494" y2="-8.763" width="0.1524" layer="21"/>
<wire x1="15.494" y1="-5.715" x2="15.494" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="15.494" y1="-8.763" x2="12.954" y2="-8.763" width="0.1524" layer="21"/>
<wire x1="15.494" y1="-8.763" x2="15.494" y2="-9.271" width="0.1524" layer="21"/>
<wire x1="15.494" y1="-9.271" x2="15.494" y2="-9.779" width="0.1524" layer="21"/>
<wire x1="-15.494" y1="-9.779" x2="-15.494" y2="-9.271" width="0.1524" layer="21"/>
<wire x1="-15.494" y1="-9.271" x2="-15.494" y2="-8.763" width="0.1524" layer="21"/>
<wire x1="-15.494" y1="-8.763" x2="-15.494" y2="-5.715" width="0.1524" layer="21"/>
<wire x1="-15.494" y1="-5.715" x2="-15.494" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="12.954" y1="-8.509" x2="10.414" y2="-8.509" width="0.1524" layer="21"/>
<wire x1="15.494" y1="-9.271" x2="12.954" y2="-9.271" width="0.1524" layer="21"/>
<wire x1="12.954" y1="-9.271" x2="10.414" y2="-9.271" width="0.1524" layer="21"/>
<wire x1="12.954" y1="-8.763" x2="12.954" y2="-9.271" width="0.1524" layer="21"/>
<wire x1="12.954" y1="-8.763" x2="12.954" y2="-8.509" width="0.1524" layer="21"/>
<wire x1="10.414" y1="-8.509" x2="10.414" y2="-8.763" width="0.1524" layer="21"/>
<wire x1="10.414" y1="-8.763" x2="10.414" y2="-9.271" width="0.1524" layer="21"/>
<wire x1="15.494" y1="-5.588" x2="10.414" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="10.414" y1="-5.588" x2="10.414" y2="-5.715" width="0.1524" layer="21"/>
<wire x1="10.414" y1="-8.763" x2="10.287" y2="-8.763" width="0.1524" layer="21"/>
<wire x1="10.287" y1="-8.763" x2="-10.287" y2="-8.763" width="0.1524" layer="21"/>
<wire x1="9.652" y1="-5.715" x2="10.287" y2="-6.35" width="0.1524" layer="21" curve="-90"/>
<wire x1="10.414" y1="-5.715" x2="9.525" y2="-5.715" width="0.1524" layer="21"/>
<wire x1="9.525" y1="-5.715" x2="9.271" y2="-5.715" width="0.1524" layer="21"/>
<wire x1="10.287" y1="-6.35" x2="10.287" y2="-8.763" width="0.1524" layer="21"/>
<wire x1="9.271" y1="-5.715" x2="9.271" y2="-4.953" width="0.1524" layer="21"/>
<wire x1="9.271" y1="-5.715" x2="-9.271" y2="-5.715" width="0.1524" layer="21"/>
<wire x1="9.271" y1="-4.953" x2="-9.271" y2="-4.953" width="0.1524" layer="21"/>
<wire x1="-9.271" y1="-5.715" x2="-9.271" y2="-4.953" width="0.1524" layer="21"/>
<wire x1="-9.271" y1="-5.715" x2="-9.525" y2="-5.715" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="-5.715" x2="-10.414" y2="-5.715" width="0.1524" layer="21"/>
<wire x1="-10.414" y1="-5.588" x2="-15.494" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="-12.954" y1="-8.763" x2="-12.954" y2="-9.271" width="0.1524" layer="21"/>
<wire x1="-12.954" y1="-8.763" x2="-15.494" y2="-8.763" width="0.1524" layer="21"/>
<wire x1="-12.954" y1="-9.271" x2="-15.494" y2="-9.271" width="0.1524" layer="21"/>
<wire x1="-10.414" y1="-8.509" x2="-12.954" y2="-8.509" width="0.1524" layer="21"/>
<wire x1="-12.954" y1="-8.763" x2="-12.954" y2="-8.509" width="0.1524" layer="21"/>
<wire x1="10.414" y1="-9.271" x2="-10.414" y2="-9.271" width="0.1524" layer="21"/>
<wire x1="-10.414" y1="-9.271" x2="-12.954" y2="-9.271" width="0.1524" layer="21"/>
<wire x1="-10.414" y1="-8.509" x2="-10.414" y2="-8.763" width="0.1524" layer="21"/>
<wire x1="-10.414" y1="-8.763" x2="-10.414" y2="-9.271" width="0.1524" layer="21"/>
<wire x1="-10.287" y1="-6.35" x2="-10.287" y2="-8.763" width="0.1524" layer="21"/>
<wire x1="-10.287" y1="-8.763" x2="-10.414" y2="-8.763" width="0.1524" layer="21"/>
<wire x1="-10.287" y1="-6.35" x2="-9.652" y2="-5.715" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.494" y1="3" x2="15.494" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="9.525" y1="3" x2="9.525" y2="-5.715" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="3" x2="-9.525" y2="-5.715" width="0.1524" layer="21"/>
<wire x1="-15.494" y1="3" x2="-15.494" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-12.065" x2="-2.794" y2="-12.065" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="-12.065" x2="-2.794" y2="-14.351" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="-14.351" x2="-3.175" y2="-14.351" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-14.351" x2="-3.175" y2="-12.446" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-12.446" x2="-4.699" y2="-12.446" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-12.446" x2="-4.699" y2="-14.351" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-14.351" x2="-5.08" y2="-14.351" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-14.351" x2="-5.08" y2="-12.065" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-12.065" x2="1.143" y2="-12.065" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-12.065" x2="1.143" y2="-14.351" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-14.351" x2="0.762" y2="-14.351" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-14.351" x2="0.762" y2="-12.446" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-12.446" x2="-0.762" y2="-12.446" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-12.446" x2="-0.762" y2="-14.351" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-14.351" x2="-1.143" y2="-14.351" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-14.351" x2="-1.143" y2="-12.065" width="0.1524" layer="21"/>
<wire x1="2.794" y1="-12.065" x2="5.08" y2="-12.065" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-12.065" x2="5.08" y2="-14.351" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-14.351" x2="4.699" y2="-14.351" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-14.351" x2="4.699" y2="-12.446" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-12.446" x2="3.175" y2="-12.446" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-12.446" x2="3.175" y2="-14.351" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-14.351" x2="2.794" y2="-14.351" width="0.1524" layer="21"/>
<wire x1="2.794" y1="-14.351" x2="2.794" y2="-12.065" width="0.1524" layer="21"/>
<wire x1="-10.414" y1="-5.588" x2="-10.414" y2="-5.715" width="0.1524" layer="21"/>
<wire x1="-10.414" y1="-5.715" x2="-15.494" y2="-5.715" width="0.1524" layer="21"/>
<wire x1="-8.382" y1="-9.779" x2="-15.494" y2="-9.779" width="0.1524" layer="21"/>
<wire x1="-8.763" y1="-10.414" x2="-8.763" y2="-15.113" width="0.1524" layer="21"/>
<wire x1="-9.398" y1="-9.779" x2="-8.763" y2="-10.414" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.494" y1="-9.779" x2="8.382" y2="-9.779" width="0.1524" layer="21"/>
<wire x1="8.763" y1="-10.414" x2="8.763" y2="-15.113" width="0.1524" layer="21"/>
<wire x1="8.763" y1="-10.414" x2="9.398" y2="-9.779" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.001" y1="-10.287" x2="-8.001" y2="-15.621" width="0.1524" layer="21"/>
<wire x1="-8.001" y1="-15.621" x2="-8.255" y2="-15.621" width="0.1524" layer="21"/>
<wire x1="8.001" y1="-10.287" x2="8.001" y2="-15.621" width="0.1524" layer="21"/>
<wire x1="8.001" y1="-15.621" x2="-8.001" y2="-15.621" width="0.1524" layer="21"/>
<rectangle x1="9.652" y1="-8.763" x2="9.906" y2="-6.35" layer="21"/>
<rectangle x1="-9.906" y1="-8.763" x2="-9.652" y2="-6.35" layer="21"/>
<rectangle x1="12.0142" y1="-5.715" x2="13.0302" y2="-4.064" layer="21"/>
<rectangle x1="-13.0302" y1="-5.715" x2="-12.0142" y2="-4.064" layer="21"/>
<rectangle x1="-9.271" y1="-5.715" x2="9.271" y2="-4.953" layer="21"/>
<rectangle x1="-15.494" y1="-9.779" x2="15.494" y2="-9.271" layer="21"/>
<rectangle x1="-5.8928" y1="-4.953" x2="-5.08" y2="2.286" layer="21"/>
<rectangle x1="-4.5212" y1="-4.953" x2="-3.7084" y2="-0.254" layer="21"/>
<rectangle x1="-3.1496" y1="-4.953" x2="-2.3368" y2="2.286" layer="21"/>
<rectangle x1="-1.778" y1="-4.953" x2="-0.9652" y2="-0.254" layer="21"/>
<rectangle x1="-0.4064" y1="-4.953" x2="0.4064" y2="2.286" layer="21"/>
<rectangle x1="0.9652" y1="-4.953" x2="1.778" y2="-0.254" layer="21"/>
<rectangle x1="2.3368" y1="-4.953" x2="3.1496" y2="2.286" layer="21"/>
<rectangle x1="3.7084" y1="-4.953" x2="4.5212" y2="-0.254" layer="21"/>
<rectangle x1="5.08" y1="-4.953" x2="5.8928" y2="2.286" layer="21"/>
</package>
<package name="61400416121">
<description>WR-COM Horizontal USB Type B</description>
<wire x1="-6" y1="-13.99" x2="6" y2="-13.99" width="0.127" layer="21"/>
<wire x1="6" y1="-13.99" x2="6" y2="2.21" width="0.127" layer="51"/>
<wire x1="6" y1="2.21" x2="-6" y2="2.21" width="0.127" layer="21"/>
<wire x1="-6" y1="2.21" x2="-6" y2="-13.99" width="0.127" layer="51"/>
<pad name="Z2" x="6.02" y="-3.71" drill="2.3" diameter="4"/>
<pad name="Z1" x="-6.02" y="-3.71" drill="2.3" diameter="4"/>
<pad name="4" x="1.25" y="-1" drill="0.92"/>
<pad name="3" x="-1.25" y="-1" drill="0.92"/>
<pad name="1" x="1.25" y="1" drill="0.92"/>
<pad name="2" x="-1.25" y="1" drill="0.92"/>
<text x="-13.4144" y="10.8533" size="1.27" layer="25">&gt;NAME</text>
<text x="-13.4144" y="-12.273" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="61900411121">
<description>WR-WTB 2.54mm Male Locking Header, 4 Pins</description>
<wire x1="-5.23" y1="-3.2" x2="-5.23" y2="2.6" width="0.127" layer="21"/>
<wire x1="-5.23" y1="2.6" x2="5.23" y2="2.6" width="0.127" layer="21"/>
<wire x1="5.23" y1="2.6" x2="5.23" y2="-3.2" width="0.127" layer="21"/>
<wire x1="5.23" y1="-3.2" x2="-5.23" y2="-3.2" width="0.127" layer="21"/>
<wire x1="-5.23" y1="2.6" x2="5.23" y2="2.6" width="0.127" layer="21"/>
<wire x1="5.23" y1="2.6" x2="5.23" y2="-3.2" width="0.127" layer="21"/>
<wire x1="5.23" y1="-3.2" x2="-5.23" y2="-3.2" width="0.127" layer="21"/>
<wire x1="-5.23" y1="-3.2" x2="-5.23" y2="2.6" width="0.127" layer="21"/>
<pad name="1" x="3.81" y="0" drill="1"/>
<pad name="2" x="1.27" y="0" drill="1"/>
<pad name="3" x="-1.27" y="0" drill="1"/>
<pad name="4" x="-3.81" y="0" drill="1"/>
<text x="5.78" y="-0.6" size="1.27" layer="51">1</text>
<text x="-6.8" y="-0.7" size="1.27" layer="51">4</text>
<wire x1="-3.81" y1="2.54" x2="-3.81" y2="1.905" width="0.127" layer="21"/>
<wire x1="-3.81" y1="1.905" x2="3.81" y2="1.905" width="0.127" layer="21"/>
<wire x1="3.81" y1="1.905" x2="3.81" y2="2.54" width="0.127" layer="21"/>
<text x="-2.54" y="-5.08" size="1.016" layer="25" font="vector" ratio="12">&gt;NAME</text>
<text x="-2.54" y="-6.35" size="1.016" layer="27" font="vector">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="N-MOS-1">
<wire x1="-1.27" y1="0" x2="-0.254" y2="0.381" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="0.381" x2="-0.254" y2="-0.381" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-0.381" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="0" x2="-0.889" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.032" x2="0" y2="-2.794" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="3.048" x2="1.27" y2="3.048" width="0.1524" layer="94"/>
<wire x1="1.27" y1="3.048" x2="1.27" y2="0.762" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0.762" x2="1.27" y2="0.508" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0.508" x2="1.27" y2="-2.794" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-2.794" x2="0" y2="-2.794" width="0.1524" layer="94"/>
<wire x1="1.778" y1="0" x2="0.762" y2="0" width="0.1524" layer="94"/>
<wire x1="0.762" y1="0" x2="1.27" y2="0.762" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0.762" x2="1.778" y2="0" width="0.1524" layer="94"/>
<wire x1="1.778" y1="0.762" x2="0.762" y2="0.762" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="2.032" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="2.032" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-2.032" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="0" x2="-0.381" y2="-0.254" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-0.254" x2="-0.381" y2="0.254" width="0.254" layer="94"/>
<wire x1="-0.381" y1="0.254" x2="-0.889" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0.508" x2="1.016" y2="0.127" width="0.254" layer="94"/>
<wire x1="1.016" y1="0.127" x2="1.524" y2="0.127" width="0.254" layer="94"/>
<wire x1="1.524" y1="0.127" x2="1.27" y2="0.508" width="0.254" layer="94"/>
<circle x="0" y="-2.794" radius="0.3592" width="0" layer="94"/>
<circle x="0" y="-2.032" radius="0.3592" width="0" layer="94"/>
<circle x="0" y="3.048" radius="0.3592" width="0" layer="94"/>
<text x="-3.048" y="1.778" size="1.524" layer="95" rot="R180">&gt;NAME</text>
<text x="-3.048" y="0" size="1.27" layer="96" rot="R180">&gt;VALUE</text>
<rectangle x1="-2.032" y1="1.27" x2="-1.524" y2="2.54" layer="94"/>
<rectangle x1="-2.032" y1="-2.54" x2="-1.524" y2="-1.27" layer="94"/>
<rectangle x1="-2.032" y1="-0.762" x2="-1.524" y2="0.762" layer="94"/>
<pin name="G" x="-5.08" y="-2.54" visible="off" length="short" direction="pas"/>
<pin name="D" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="S" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
</symbol>
<symbol name="USB-SHIELD">
<wire x1="0" y1="7.62" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="7.62" x2="5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="-10.16" x2="2.54" y2="-9.398" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-8.89" x2="2.54" y2="-8.128" width="0.1524" layer="94"/>
<wire x1="6.35" y1="-5.334" x2="6.35" y2="-4.572" width="0.1524" layer="94"/>
<wire x1="6.35" y1="-4.064" x2="6.35" y2="-3.302" width="0.1524" layer="94"/>
<wire x1="6.35" y1="-2.794" x2="6.35" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="6.35" y1="-1.524" x2="6.35" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="6.35" y1="-0.254" x2="6.35" y2="0.508" width="0.1524" layer="94"/>
<wire x1="6.35" y1="1.016" x2="6.35" y2="1.778" width="0.1524" layer="94"/>
<wire x1="6.35" y1="2.286" x2="6.35" y2="3.048" width="0.1524" layer="94"/>
<wire x1="6.35" y1="3.556" x2="6.35" y2="4.318" width="0.1524" layer="94"/>
<wire x1="6.35" y1="4.826" x2="6.35" y2="5.588" width="0.1524" layer="94"/>
<wire x1="6.35" y1="6.096" x2="6.35" y2="6.858" width="0.1524" layer="94"/>
<wire x1="6.35" y1="7.366" x2="6.35" y2="8.128" width="0.1524" layer="94"/>
<wire x1="6.35" y1="8.382" x2="5.588" y2="8.382" width="0.1524" layer="94"/>
<wire x1="5.08" y1="8.382" x2="4.318" y2="8.382" width="0.1524" layer="94"/>
<wire x1="3.81" y1="8.382" x2="3.048" y2="8.382" width="0.1524" layer="94"/>
<wire x1="2.54" y1="8.382" x2="1.778" y2="8.382" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-7.112" x2="2.54" y2="-6.35" width="0.1524" layer="94"/>
<wire x1="3.556" y1="-5.842" x2="2.794" y2="-5.842" width="0.1524" layer="94"/>
<wire x1="4.826" y1="-5.842" x2="4.064" y2="-5.842" width="0.1524" layer="94"/>
<wire x1="6.096" y1="-5.842" x2="5.334" y2="-5.842" width="0.1524" layer="94"/>
<text x="0" y="-12.7" size="1.524" layer="95">&gt;NAME</text>
<text x="0" y="-14.478" size="1.27" layer="96">&gt;VALUE</text>
<text x="5.08" y="-2.54" size="2.54" layer="94" rot="R90">USB</text>
<pin name="1" x="-2.54" y="5.08" visible="pad" length="short" direction="pas"/>
<pin name="2" x="-2.54" y="2.54" visible="pad" length="short" direction="pas"/>
<pin name="3" x="-2.54" y="0" visible="pad" length="short" direction="pas"/>
<pin name="4" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas"/>
<pin name="S1" x="0" y="-7.62" visible="off" length="short" direction="pas"/>
<pin name="S2" x="0" y="-10.16" visible="off" length="short" direction="pas"/>
</symbol>
<symbol name="MA05-1">
<wire x1="3.81" y1="-7.62" x2="-1.27" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="7.62" x2="-1.27" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<text x="-0.762" y="-11.684" size="1.27" layer="96">&gt;VALUE</text>
<text x="-1.016" y="-10.16" size="1.524" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="MAX232">
<wire x1="-10.16" y1="15.24" x2="10.16" y2="15.24" width="0.4064" layer="94"/>
<wire x1="10.16" y1="-17.78" x2="10.16" y2="15.24" width="0.4064" layer="94"/>
<wire x1="10.16" y1="-17.78" x2="-10.16" y2="-17.78" width="0.4064" layer="94"/>
<wire x1="-10.16" y1="15.24" x2="-10.16" y2="-17.78" width="0.4064" layer="94"/>
<text x="-3.81" y="-19.939" size="1.524" layer="95">&gt;NAME</text>
<text x="-3.556" y="-21.59" size="1.27" layer="96">&gt;VALUE</text>
<pin name="C1+" x="-15.24" y="12.7" length="middle"/>
<pin name="C1-" x="-15.24" y="7.62" length="middle"/>
<pin name="C2+" x="-15.24" y="2.54" length="middle"/>
<pin name="C2-" x="-15.24" y="-2.54" length="middle"/>
<pin name="T1IN" x="-15.24" y="-7.62" length="middle" direction="in"/>
<pin name="T2IN" x="-15.24" y="-10.16" length="middle" direction="in"/>
<pin name="R1OUT" x="-15.24" y="-12.7" length="middle" direction="out"/>
<pin name="R2OUT" x="-15.24" y="-15.24" length="middle" direction="out"/>
<pin name="V+" x="15.24" y="10.16" length="middle" rot="R180"/>
<pin name="V-" x="15.24" y="5.08" length="middle" rot="R180"/>
<pin name="T1OUT" x="15.24" y="-7.62" length="middle" direction="out" rot="R180"/>
<pin name="T2OUT" x="15.24" y="-10.16" length="middle" direction="out" rot="R180"/>
<pin name="R1IN" x="15.24" y="-12.7" length="middle" direction="in" rot="R180"/>
<pin name="R2IN" x="15.24" y="-15.24" length="middle" direction="in" rot="R180"/>
</symbol>
<symbol name="VCC-GND">
<text x="1.524" y="-5.08" size="1.016" layer="95" rot="R90">GND</text>
<text x="1.524" y="2.54" size="1.016" layer="95" rot="R90">VCC</text>
<text x="-0.762" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<pin name="VCC" x="0" y="7.62" visible="pad" length="middle" direction="pwr" rot="R270"/>
<pin name="GND" x="0" y="-7.62" visible="pad" length="middle" direction="pwr" rot="R90"/>
</symbol>
<symbol name="M09">
<wire x1="-2.54" y1="5.08" x2="-1.27" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-1.27" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.397" y1="5.08" x2="2.667" y2="5.08" width="0.6096" layer="94"/>
<wire x1="1.397" y1="2.54" x2="2.667" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-1.27" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.397" y1="0" x2="2.667" y2="0" width="0.6096" layer="94"/>
<wire x1="1.397" y1="-2.54" x2="2.667" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="2.5226" y1="-8.1718" x2="4.0637" y2="-6.9312" width="0.4064" layer="94" curve="102.322193" cap="flat"/>
<wire x1="2.5226" y1="-8.1718" x2="-3.0654" y2="-6.9494" width="0.4064" layer="94"/>
<wire x1="-4.064" y1="-5.7088" x2="-3.0654" y2="-6.9494" width="0.4064" layer="94" curve="77.657889"/>
<wire x1="-4.064" y1="5.7088" x2="-4.064" y2="-5.7088" width="0.4064" layer="94"/>
<wire x1="-4.064" y1="5.7088" x2="-3.0654" y2="6.9494" width="0.4064" layer="94" curve="-77.657889"/>
<wire x1="4.064" y1="6.9312" x2="4.064" y2="-6.9312" width="0.4064" layer="94"/>
<wire x1="2.5226" y1="8.1718" x2="-3.0654" y2="6.9494" width="0.4064" layer="94"/>
<wire x1="2.5226" y1="8.1719" x2="4.064" y2="6.9312" width="0.4064" layer="94" curve="-102.337599" cap="flat"/>
<text x="-3.81" y="-10.795" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="8.89" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="6" x="-7.62" y="5.08" visible="pad" length="middle" direction="pas"/>
<pin name="2" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="7" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas"/>
<pin name="3" x="7.62" y="0" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="8" x="-7.62" y="0" visible="pad" length="middle" direction="pas"/>
<pin name="4" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="9" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas"/>
<pin name="5" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" rot="R180"/>
</symbol>
<symbol name="MA03-1">
<wire x1="3.81" y1="-5.08" x2="-1.27" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="-1.27" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<text x="-0.762" y="-8.89" size="1.27" layer="96">&gt;VALUE</text>
<text x="-1.27" y="-7.112" size="1.524" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="DISPLAY">
<wire x1="-20.32" y1="15.24" x2="30.48" y2="15.24" width="0.254" layer="94"/>
<wire x1="30.48" y1="15.24" x2="30.48" y2="-7.62" width="0.254" layer="94"/>
<wire x1="30.48" y1="-7.62" x2="-30.48" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-30.48" y1="-7.62" x2="-30.48" y2="15.24" width="0.254" layer="94"/>
<wire x1="-30.48" y1="15.24" x2="-20.32" y2="15.24" width="0.254" layer="94"/>
<wire x1="-20.32" y1="12.7" x2="-20.32" y2="0" width="0.254" layer="94"/>
<wire x1="-20.32" y1="0" x2="20.32" y2="0" width="0.254" layer="94"/>
<wire x1="20.32" y1="0" x2="20.32" y2="12.7" width="0.254" layer="94"/>
<wire x1="20.32" y1="12.7" x2="-20.32" y2="12.7" width="0.254" layer="94"/>
<text x="-20.32" y="-8.128" size="1.524" layer="95" font="vector" rot="R180">&gt;NAME</text>
<text x="-20.32" y="-10.16" size="1.27" layer="96" font="vector" rot="R180">&gt;VALUE</text>
<pin name="VSS@1" x="-35.56" y="0" length="middle"/>
<pin name="VDD@2" x="-35.56" y="10.16" length="middle"/>
<pin name="VO@3" x="-35.56" y="5.08" length="middle"/>
<pin name="RS@4" x="-15.24" y="-12.7" length="middle" rot="R90"/>
<pin name="R/W@5" x="-12.7" y="-12.7" length="middle" rot="R90"/>
<pin name="E@6" x="-10.16" y="-12.7" length="middle" rot="R90"/>
<pin name="DB0@7" x="-2.54" y="-12.7" length="middle" rot="R90"/>
<pin name="DB1@8" x="0" y="-12.7" length="middle" rot="R90"/>
<pin name="DB2@9" x="2.54" y="-12.7" length="middle" rot="R90"/>
<pin name="K-@16" x="35.56" y="0" length="middle" rot="R180"/>
<pin name="A+@15" x="35.56" y="10.16" length="middle" rot="R180"/>
<pin name="DB3@10" x="5.08" y="-12.7" length="middle" rot="R90"/>
<pin name="DB4@11" x="7.62" y="-12.7" length="middle" rot="R90"/>
<pin name="DB5@12" x="10.16" y="-12.7" length="middle" rot="R90"/>
<pin name="DB6@13" x="12.7" y="-12.7" length="middle" rot="R90"/>
<pin name="DB7@14" x="15.24" y="-12.7" length="middle" rot="R90"/>
<pin name="DRL" x="-35.56" y="-2.54" visible="pin" length="middle"/>
</symbol>
<symbol name="MA05-2">
<wire x1="3.81" y1="-7.62" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-1.27" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-1.27" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-1.27" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-1.27" y2="2.54" width="0.6096" layer="94"/>
<text x="-3.302" y="-11.938" size="1.27" layer="96">&gt;VALUE</text>
<text x="-3.302" y="-9.906" size="1.524" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="4" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="6" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="7" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="9" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="8" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="10" x="-7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="BUTTON">
<wire x1="1.905" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="1.905" y2="1.27" width="0.254" layer="94"/>
<circle x="-2.54" y="0" radius="0.127" width="0.4064" layer="94"/>
<circle x="2.54" y="0" radius="0.127" width="0.4064" layer="94"/>
<text x="-2.286" y="-2.286" size="1.524" layer="95">&gt;NAME</text>
<text x="-2.032" y="-3.81" size="1.27" layer="96">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="2"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="TTL-FT232R">
<wire x1="3.81" y1="-10.16" x2="-1.27" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="2.54" y2="-7.62" width="0.6096" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="7.62" x2="-1.27" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-10.16" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<text x="-2.032" y="-14.224" size="1.27" layer="96">&gt;VALUE</text>
<text x="-2.286" y="-12.446" size="1.524" layer="95">&gt;NAME</text>
<pin name="GND" x="7.62" y="-7.62" visible="pad" length="middle" direction="sup" swaplevel="1" rot="R180"/>
<pin name="!CTS" x="7.62" y="-5.08" visible="pad" length="middle" direction="in" swaplevel="1" rot="R180"/>
<pin name="VCC" x="7.62" y="-2.54" visible="pad" length="middle" direction="sup" swaplevel="1" rot="R180"/>
<pin name="TXD" x="7.62" y="0" visible="pad" length="middle" direction="out" swaplevel="1" rot="R180"/>
<pin name="RXD" x="7.62" y="2.54" visible="pad" length="middle" direction="in" swaplevel="1" rot="R180"/>
<pin name="!RTS" x="7.62" y="5.08" visible="pad" length="middle" direction="out" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="USDCARD">
<wire x1="-2.54" y1="15.24" x2="10.16" y2="15.24" width="0.254" layer="94"/>
<wire x1="10.16" y1="15.24" x2="10.16" y2="-20.32" width="0.254" layer="94"/>
<wire x1="10.16" y1="-20.32" x2="-2.54" y2="-20.32" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-20.32" x2="-2.54" y2="15.24" width="0.254" layer="94"/>
<pin name="CD1" x="-7.62" y="-10.16" length="middle"/>
<pin name="CD2" x="-7.62" y="-12.7" length="middle"/>
<pin name="CS" x="-7.62" y="10.16" length="middle"/>
<pin name="DI" x="-7.62" y="7.62" length="middle"/>
<pin name="DO" x="-7.62" y="-2.54" length="middle"/>
<pin name="GND" x="-7.62" y="0" length="middle"/>
<pin name="NC" x="-7.62" y="12.7" length="middle"/>
<pin name="RSV" x="-7.62" y="-5.08" length="middle"/>
<pin name="SCK" x="-7.62" y="2.54" length="middle"/>
<pin name="SHIELD1" x="-7.62" y="-15.24" length="middle"/>
<pin name="SHIELD2" x="-7.62" y="-17.78" length="middle"/>
<pin name="VCC" x="-7.62" y="5.08" length="middle"/>
<text x="0.254" y="-22.606" size="1.524" layer="95">&gt;NAME</text>
<text x="0.508" y="-24.13" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="MA07-1">
<wire x1="3.81" y1="-10.16" x2="-1.27" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="2.54" y2="-7.62" width="0.6096" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="7.62" x2="2.54" y2="7.62" width="0.6096" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="10.16" x2="-1.27" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-10.16" x2="3.81" y2="10.16" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="10.16" x2="3.81" y2="10.16" width="0.4064" layer="94"/>
<text x="-1.524" y="-13.97" size="1.27" layer="96">&gt;VALUE</text>
<text x="-2.032" y="-12.192" size="1.524" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="7" x="7.62" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="TVS-DIODE">
<wire x1="1.27" y1="-2.54" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<pin name="A" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<wire x1="-1.27" y1="2.54" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.524" y="1.5494" size="1.524" layer="95" rot="R180">&gt;NAME</text>
<text x="-1.524" y="-0.2286" size="1.27" layer="96" rot="R180">&gt;VALUE</text>
<pin name="A1" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<wire x1="0" y1="2.54" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-0.508" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="0.508" width="0.254" layer="94"/>
</symbol>
<symbol name="ADDITONAL">
<wire x1="-5.08" y1="2.54" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="-5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<text x="-3.81" y="-4.572" size="1.524" layer="95">&gt;NAME</text>
<text x="-3.81" y="-6.35" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="BUZZER">
<wire x1="-1.905" y1="-5.715" x2="1.905" y2="-5.715" width="0.254" layer="94"/>
<wire x1="1.905" y1="-5.715" x2="1.905" y2="-5.08" width="0.254" layer="94"/>
<wire x1="1.905" y1="-5.08" x2="1.905" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.905" y1="0" x2="-1.905" y2="0" width="0.254" layer="94"/>
<wire x1="-1.905" y1="-5.715" x2="-1.905" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-1.905" y1="-5.08" x2="-1.905" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.905" y1="0" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.905" y1="0" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="1.905" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.905" y1="-2.54" x2="1.905" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-1.905" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-2.54" x2="-1.905" y2="0" width="0.254" layer="94"/>
<pin name="1" x="-5.08" y="-2.54" visible="off" length="short" direction="pas"/>
<pin name="2" x="5.08" y="-2.54" visible="off" length="short" direction="pas" rot="R180"/>
<text x="-3.81" y="-8.636" size="1.524" layer="95">&gt;NAME</text>
<text x="-3.556" y="-10.287" size="1.27" layer="96">&gt;VALUE</text>
<pin name="1S" x="-5.08" y="-5.08" visible="off" length="short" direction="pas"/>
<pin name="2S" x="5.08" y="-5.08" visible="off" length="short" direction="pas" rot="R180"/>
<wire x1="-2.54" y1="-5.08" x2="-1.905" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="1.905" y2="-5.08" width="0.1524" layer="94"/>
</symbol>
<symbol name="4">
<wire x1="-15.24" y1="2.54" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="-15.24" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-2.54" x2="-15.24" y2="2.54" width="0.254" layer="94"/>
<text x="7.62" y="0" size="1.27" layer="95">&gt;NAME</text>
<text x="7.62" y="-2.54" size="1.27" layer="96">&gt;VALUE</text>
<pin name="1" x="2.54" y="-7.62" length="middle" rot="R90"/>
<pin name="2" x="-2.54" y="-7.62" length="middle" rot="R90"/>
<pin name="3" x="-7.62" y="-7.62" length="middle" rot="R90"/>
<pin name="4" x="-12.7" y="-7.62" length="middle" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FDN337N" prefix="V">
<description>&lt;b&gt;N-CHANNEL MOS FET&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="N-MOS-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DISTRIBUTOR" value="farnell.com" constant="no"/>
<attribute name="MANUFACTURER" value="FAIRCHILD SEMICONDUCTOR" constant="no"/>
<attribute name="ORDER_NO" value="9845356" constant="no"/>
<attribute name="PART_NO" value="FDN337N" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="USB-B" prefix="X">
<description>&lt;b&gt;BERG&lt;/b&gt; USB connector</description>
<gates>
<gate name="G$1" symbol="USB-SHIELD" x="0" y="0"/>
</gates>
<devices>
<device name="PN61729-S" package="PN61729-S">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="S1" pad="S1"/>
<connect gate="G$1" pin="S2" pad="S2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="MUSBD11130" package="AMPHENOL-MUSBD11130">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="S1" pad="S1"/>
<connect gate="G$1" pin="S2" pad="S2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WE61400416121" package="61400416121">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="S1" pad="Z1"/>
<connect gate="G$1" pin="S2" pad="Z2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MA05-1" prefix="X" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="MA05-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MA05-1">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MAX232ECWE" prefix="D">
<description>&lt;b&gt;RS232 TRANSCEIVER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="MAX232" x="0" y="0"/>
<gate name="P" symbol="VCC-GND" x="25.4" y="0" addlevel="request"/>
</gates>
<devices>
<device name="" package="SO16L">
<connects>
<connect gate="G$1" pin="C1+" pad="1 2-1"/>
<connect gate="G$1" pin="C1-" pad="2-3 3"/>
<connect gate="G$1" pin="C2+" pad="2-4 4"/>
<connect gate="G$1" pin="C2-" pad="2-5 5"/>
<connect gate="G$1" pin="R1IN" pad="2-13 13"/>
<connect gate="G$1" pin="R1OUT" pad="2-12 12"/>
<connect gate="G$1" pin="R2IN" pad="2-8 8"/>
<connect gate="G$1" pin="R2OUT" pad="2-9 9"/>
<connect gate="G$1" pin="T1IN" pad="2-11 11"/>
<connect gate="G$1" pin="T1OUT" pad="2-14 14"/>
<connect gate="G$1" pin="T2IN" pad="2-10 10"/>
<connect gate="G$1" pin="T2OUT" pad="2-7 7"/>
<connect gate="G$1" pin="V+" pad="2 2-2"/>
<connect gate="G$1" pin="V-" pad="2-6 6"/>
<connect gate="P" pin="GND" pad="2-15 15"/>
<connect gate="P" pin="VCC" pad="2-16 16"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="MAXIM" constant="no"/>
<attribute name="MPN" value="MAX232ECWE+" constant="no"/>
<attribute name="OC_FARNELL" value="9724435" constant="no"/>
<attribute name="OC_NEWARK" value="67K4440" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="M09" prefix="X" uservalue="yes">
<description>&lt;b&gt;SUB-D&lt;/b&gt;</description>
<gates>
<gate name="-1" symbol="M09" x="0" y="0"/>
</gates>
<devices>
<device name="D" package="M09D">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-1" pin="2" pad="2"/>
<connect gate="-1" pin="3" pad="3"/>
<connect gate="-1" pin="4" pad="4"/>
<connect gate="-1" pin="5" pad="5"/>
<connect gate="-1" pin="6" pad="6"/>
<connect gate="-1" pin="7" pad="7"/>
<connect gate="-1" pin="8" pad="8"/>
<connect gate="-1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="H" package="M09H">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-1" pin="2" pad="2"/>
<connect gate="-1" pin="3" pad="3"/>
<connect gate="-1" pin="4" pad="4"/>
<connect gate="-1" pin="5" pad="5"/>
<connect gate="-1" pin="6" pad="6"/>
<connect gate="-1" pin="7" pad="7"/>
<connect gate="-1" pin="8" pad="8"/>
<connect gate="-1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="HP" package="M09HP">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-1" pin="2" pad="2"/>
<connect gate="-1" pin="3" pad="3"/>
<connect gate="-1" pin="4" pad="4"/>
<connect gate="-1" pin="5" pad="5"/>
<connect gate="-1" pin="6" pad="6"/>
<connect gate="-1" pin="7" pad="7"/>
<connect gate="-1" pin="8" pad="8"/>
<connect gate="-1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="V" package="M09V">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-1" pin="2" pad="2"/>
<connect gate="-1" pin="3" pad="3"/>
<connect gate="-1" pin="4" pad="4"/>
<connect gate="-1" pin="5" pad="5"/>
<connect gate="-1" pin="6" pad="6"/>
<connect gate="-1" pin="7" pad="7"/>
<connect gate="-1" pin="8" pad="8"/>
<connect gate="-1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="VP" package="M09VP">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-1" pin="2" pad="2"/>
<connect gate="-1" pin="3" pad="3"/>
<connect gate="-1" pin="4" pad="4"/>
<connect gate="-1" pin="5" pad="5"/>
<connect gate="-1" pin="6" pad="6"/>
<connect gate="-1" pin="7" pad="7"/>
<connect gate="-1" pin="8" pad="8"/>
<connect gate="-1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="VB" package="M09VB">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-1" pin="2" pad="2"/>
<connect gate="-1" pin="3" pad="3"/>
<connect gate="-1" pin="4" pad="4"/>
<connect gate="-1" pin="5" pad="5"/>
<connect gate="-1" pin="6" pad="6"/>
<connect gate="-1" pin="7" pad="7"/>
<connect gate="-1" pin="8" pad="8"/>
<connect gate="-1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="-182" package="182-009-MALE">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-1" pin="2" pad="2"/>
<connect gate="-1" pin="3" pad="3"/>
<connect gate="-1" pin="4" pad="4"/>
<connect gate="-1" pin="5" pad="5"/>
<connect gate="-1" pin="6" pad="6"/>
<connect gate="-1" pin="7" pad="7"/>
<connect gate="-1" pin="8" pad="8"/>
<connect gate="-1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="SMT" package="M09SMT">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-1" pin="2" pad="2"/>
<connect gate="-1" pin="3" pad="3"/>
<connect gate="-1" pin="4" pad="4"/>
<connect gate="-1" pin="5" pad="5"/>
<connect gate="-1" pin="6" pad="6"/>
<connect gate="-1" pin="7" pad="7"/>
<connect gate="-1" pin="8" pad="8"/>
<connect gate="-1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SEALED" package="ASSMANN-A-DS09AA-WP">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-1" pin="2" pad="2"/>
<connect gate="-1" pin="3" pad="3"/>
<connect gate="-1" pin="4" pad="4"/>
<connect gate="-1" pin="5" pad="5"/>
<connect gate="-1" pin="6" pad="6"/>
<connect gate="-1" pin="7" pad="7"/>
<connect gate="-1" pin="8" pad="8"/>
<connect gate="-1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="618009231221">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-1" pin="2" pad="2"/>
<connect gate="-1" pin="3" pad="3"/>
<connect gate="-1" pin="4" pad="4"/>
<connect gate="-1" pin="5" pad="5"/>
<connect gate="-1" pin="6" pad="6"/>
<connect gate="-1" pin="7" pad="7"/>
<connect gate="-1" pin="8" pad="8"/>
<connect gate="-1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MA03-1" prefix="X" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="MA03-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MA03-1">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DISPLAY" prefix="H">
<gates>
<gate name="G$1" symbol="DISPLAY" x="0" y="-2.54"/>
</gates>
<devices>
<device name="YB2004C" package="YB2004C">
<connects>
<connect gate="G$1" pin="A+@15" pad="15"/>
<connect gate="G$1" pin="DB0@7" pad="7"/>
<connect gate="G$1" pin="DB1@8" pad="8"/>
<connect gate="G$1" pin="DB2@9" pad="9"/>
<connect gate="G$1" pin="DB3@10" pad="10"/>
<connect gate="G$1" pin="DB4@11" pad="11"/>
<connect gate="G$1" pin="DB5@12" pad="12"/>
<connect gate="G$1" pin="DB6@13" pad="13"/>
<connect gate="G$1" pin="DB7@14" pad="14"/>
<connect gate="G$1" pin="DRL" pad="P$1 P$2 P$3 P$4"/>
<connect gate="G$1" pin="E@6" pad="6"/>
<connect gate="G$1" pin="K-@16" pad="16"/>
<connect gate="G$1" pin="R/W@5" pad="5"/>
<connect gate="G$1" pin="RS@4" pad="4"/>
<connect gate="G$1" pin="VDD@2" pad="2"/>
<connect gate="G$1" pin="VO@3" pad="3"/>
<connect gate="G$1" pin="VSS@1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MA05-2" prefix="X" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="MA05-2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MA05-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="-LOCKED" package="10-PIN-IDC">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SWITCH-MOMENTARY-2" prefix="S">
<gates>
<gate name="G$1" symbol="BUTTON" x="0" y="0"/>
</gates>
<devices>
<device name="PTH" package="TACTILE-PTH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="TACTILE_SWITCH_SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-2" package="TACTILE_SWITCH_SMD-2">
<connects>
<connect gate="G$1" pin="1" pad="2"/>
<connect gate="G$1" pin="2" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SCHURTER-1301.93" package="SCHURTER-1301.93">
<connects>
<connect gate="G$1" pin="1" pad="2"/>
<connect gate="G$1" pin="2" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FTDI-TTL232R" prefix="X">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="TTL-FT232R" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TTL-FT232R">
<connects>
<connect gate="1" pin="!CTS" pad="!CTS"/>
<connect gate="1" pin="!RTS" pad="!RTS"/>
<connect gate="1" pin="GND" pad="GND"/>
<connect gate="1" pin="RXD" pad="RXD"/>
<connect gate="1" pin="TXD" pad="TXD"/>
<connect gate="1" pin="VCC" pad="VCC"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="USD-SOCKET" prefix="X">
<description>&lt;b&gt;microSD Socket&lt;/b&gt;
Push-push type uSD socket. Schematic element and footprint production proven. Spark Fun Electronics SKU : PRT-00127. tDoc lines correctly indicate media card edge positions when inserting (unlocked, locked, depressed).</description>
<gates>
<gate name="G$1" symbol="USDCARD" x="-2.54" y="5.08"/>
</gates>
<devices>
<device name="SMD" package="MICRO-SD-SOCKET-PP">
<connects>
<connect gate="G$1" pin="CD1" pad="CD1"/>
<connect gate="G$1" pin="CD2" pad="CD2"/>
<connect gate="G$1" pin="CS" pad="2"/>
<connect gate="G$1" pin="DI" pad="3"/>
<connect gate="G$1" pin="DO" pad="7"/>
<connect gate="G$1" pin="GND" pad="6"/>
<connect gate="G$1" pin="NC" pad="1"/>
<connect gate="G$1" pin="RSV" pad="8"/>
<connect gate="G$1" pin="SCK" pad="5"/>
<connect gate="G$1" pin="SHIELD1" pad="GND1"/>
<connect gate="G$1" pin="SHIELD2" pad="GND2"/>
<connect gate="G$1" pin="VCC" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MA07-1" prefix="X" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="MA07-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MA07-1">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
<connect gate="1" pin="3" pad="3"/>
<connect gate="1" pin="4" pad="4"/>
<connect gate="1" pin="5" pad="5"/>
<connect gate="1" pin="6" pad="6"/>
<connect gate="1" pin="7" pad="7"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TVS-DIODE" prefix="V" uservalue="yes">
<gates>
<gate name="G$1" symbol="TVS-DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOD882">
<connects>
<connect gate="G$1" pin="A" pad="P$1"/>
<connect gate="G$1" pin="A1" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ADDTIONAL" prefix="A" uservalue="yes">
<gates>
<gate name="G$1" symbol="ADDITONAL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="NOTHING">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BUZZER" prefix="H">
<gates>
<gate name="G$1" symbol="BUZZER" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SONITRON-SMAT-13">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-24-P10" package="SONITRON-SMAT-24-P10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="1S" pad="1S"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="2S" pad="2-S"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-17-P10" package="SONITRON-SMAT-17-P10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="1S" pad="1S"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="2S" pad="2-S"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="61900411121" prefix="X">
<description>WR-WTB 2.54mm Male Locking Header, 4 Pins</description>
<gates>
<gate name="G$1" symbol="4" x="0" y="0"/>
</gates>
<devices>
<device name="" package="61900411121">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="linemetrics">
<packages>
<package name="SOT323">
<description>&lt;b&gt;Small Outline Transistor&lt;/b&gt;</description>
<wire x1="-1" y1="-0.55" x2="1" y2="-0.55" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.55" x2="1" y2="0.55" width="0.2032" layer="21"/>
<wire x1="1" y1="0.55" x2="-1" y2="0.55" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.55" x2="-1" y2="-0.55" width="0.2032" layer="21"/>
<smd name="3" x="0" y="0.9" dx="0.5" dy="1" layer="1"/>
<smd name="1" x="-0.65" y="-0.9" dx="0.5" dy="1" layer="1"/>
<smd name="2" x="0.65" y="-0.9" dx="0.5" dy="1" layer="1"/>
<text x="1.225" y="0.03" size="1.016" layer="25">&gt;NAME</text>
<text x="1.225" y="-1.33" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.15" y1="0.65" x2="0.15" y2="1.2" layer="51"/>
<rectangle x1="-0.8" y1="-1.2" x2="-0.5" y2="-0.65" layer="51"/>
<rectangle x1="0.5" y1="-1.2" x2="0.8" y2="-0.65" layer="51"/>
</package>
<package name="QFN64">
<description>&lt;b&gt;64M1&lt;/b&gt; Micro Lead Frame package (MLF)</description>
<wire x1="-4.5" y1="4.5" x2="4.5" y2="4.5" width="0.127" layer="51"/>
<wire x1="4.5" y1="4.5" x2="4.5" y2="-4.5" width="0.127" layer="51"/>
<wire x1="4.5" y1="-4.5" x2="-4.5" y2="-4.5" width="0.127" layer="51"/>
<wire x1="-4.5" y1="-4.5" x2="-4.5" y2="4.5" width="0.127" layer="51"/>
<circle x="-3.4" y="3.4" radius="0.2" width="0.254" layer="21"/>
<smd name="1" x="-4.4" y="3.75" dx="0.7" dy="0.3" layer="1"/>
<smd name="2" x="-4.4" y="3.25" dx="0.7" dy="0.3" layer="1"/>
<smd name="3" x="-4.4" y="2.75" dx="0.7" dy="0.3" layer="1"/>
<smd name="4" x="-4.4" y="2.25" dx="0.7" dy="0.3" layer="1"/>
<smd name="5" x="-4.4" y="1.75" dx="0.7" dy="0.3" layer="1"/>
<smd name="6" x="-4.4" y="1.25" dx="0.7" dy="0.3" layer="1"/>
<smd name="7" x="-4.4" y="0.75" dx="0.7" dy="0.3" layer="1"/>
<smd name="8" x="-4.4" y="0.25" dx="0.7" dy="0.3" layer="1"/>
<smd name="9" x="-4.4" y="-0.25" dx="0.7" dy="0.3" layer="1"/>
<smd name="10" x="-4.4" y="-0.75" dx="0.7" dy="0.3" layer="1"/>
<smd name="11" x="-4.4" y="-1.25" dx="0.7" dy="0.3" layer="1"/>
<smd name="12" x="-4.4" y="-1.75" dx="0.7" dy="0.3" layer="1"/>
<smd name="13" x="-4.4" y="-2.25" dx="0.7" dy="0.3" layer="1"/>
<smd name="14" x="-4.4" y="-2.75" dx="0.7" dy="0.3" layer="1"/>
<smd name="15" x="-4.4" y="-3.25" dx="0.7" dy="0.3" layer="1"/>
<smd name="16" x="-4.4" y="-3.75" dx="0.7" dy="0.3" layer="1"/>
<smd name="17" x="-3.75" y="-4.4" dx="0.3" dy="0.7" layer="1"/>
<smd name="18" x="-3.25" y="-4.4" dx="0.3" dy="0.7" layer="1"/>
<smd name="19" x="-2.75" y="-4.4" dx="0.3" dy="0.7" layer="1"/>
<smd name="20" x="-2.25" y="-4.4" dx="0.3" dy="0.7" layer="1"/>
<smd name="21" x="-1.75" y="-4.4" dx="0.3" dy="0.7" layer="1"/>
<smd name="22" x="-1.25" y="-4.4" dx="0.3" dy="0.7" layer="1"/>
<smd name="23" x="-0.75" y="-4.4" dx="0.3" dy="0.7" layer="1"/>
<smd name="24" x="-0.25" y="-4.4" dx="0.3" dy="0.7" layer="1"/>
<smd name="25" x="0.25" y="-4.4" dx="0.3" dy="0.7" layer="1"/>
<smd name="26" x="0.75" y="-4.4" dx="0.3" dy="0.7" layer="1"/>
<smd name="27" x="1.25" y="-4.4" dx="0.3" dy="0.7" layer="1"/>
<smd name="28" x="1.75" y="-4.4" dx="0.3" dy="0.7" layer="1"/>
<smd name="29" x="2.25" y="-4.4" dx="0.3" dy="0.7" layer="1"/>
<smd name="30" x="2.75" y="-4.4" dx="0.3" dy="0.7" layer="1"/>
<smd name="31" x="3.25" y="-4.4" dx="0.3" dy="0.7" layer="1"/>
<smd name="32" x="3.75" y="-4.4" dx="0.3" dy="0.7" layer="1"/>
<smd name="33" x="4.4" y="-3.75" dx="0.7" dy="0.3" layer="1"/>
<smd name="34" x="4.4" y="-3.25" dx="0.7" dy="0.3" layer="1"/>
<smd name="35" x="4.4" y="-2.75" dx="0.7" dy="0.3" layer="1"/>
<smd name="36" x="4.4" y="-2.25" dx="0.7" dy="0.3" layer="1"/>
<smd name="37" x="4.4" y="-1.75" dx="0.7" dy="0.3" layer="1"/>
<smd name="38" x="4.4" y="-1.25" dx="0.7" dy="0.3" layer="1"/>
<smd name="39" x="4.4" y="-0.75" dx="0.7" dy="0.3" layer="1"/>
<smd name="40" x="4.4" y="-0.25" dx="0.7" dy="0.3" layer="1"/>
<smd name="41" x="4.4" y="0.25" dx="0.7" dy="0.3" layer="1"/>
<smd name="42" x="4.4" y="0.75" dx="0.7" dy="0.3" layer="1"/>
<smd name="43" x="4.4" y="1.25" dx="0.7" dy="0.3" layer="1"/>
<smd name="44" x="4.4" y="1.75" dx="0.7" dy="0.3" layer="1"/>
<smd name="45" x="4.4" y="2.25" dx="0.7" dy="0.3" layer="1"/>
<smd name="46" x="4.4" y="2.75" dx="0.7" dy="0.3" layer="1"/>
<smd name="47" x="4.4" y="3.25" dx="0.7" dy="0.3" layer="1"/>
<smd name="48" x="4.4" y="3.75" dx="0.7" dy="0.3" layer="1"/>
<smd name="49" x="3.75" y="4.4" dx="0.3" dy="0.7" layer="1"/>
<smd name="50" x="3.25" y="4.4" dx="0.3" dy="0.7" layer="1"/>
<smd name="51" x="2.75" y="4.4" dx="0.3" dy="0.7" layer="1"/>
<smd name="52" x="2.25" y="4.4" dx="0.3" dy="0.7" layer="1"/>
<smd name="53" x="1.75" y="4.4" dx="0.3" dy="0.7" layer="1"/>
<smd name="54" x="1.25" y="4.4" dx="0.3" dy="0.7" layer="1"/>
<smd name="55" x="0.75" y="4.4" dx="0.3" dy="0.7" layer="1"/>
<smd name="56" x="0.25" y="4.4" dx="0.3" dy="0.7" layer="1"/>
<smd name="57" x="-0.25" y="4.4" dx="0.3" dy="0.7" layer="1"/>
<smd name="58" x="-0.75" y="4.4" dx="0.3" dy="0.7" layer="1"/>
<smd name="59" x="-1.25" y="4.4" dx="0.3" dy="0.7" layer="1"/>
<smd name="60" x="-1.75" y="4.4" dx="0.3" dy="0.7" layer="1"/>
<smd name="61" x="-2.25" y="4.4" dx="0.3" dy="0.7" layer="1"/>
<smd name="62" x="-2.75" y="4.4" dx="0.3" dy="0.7" layer="1"/>
<smd name="63" x="-3.25" y="4.4" dx="0.3" dy="0.7" layer="1"/>
<smd name="64" x="-3.75" y="4.4" dx="0.3" dy="0.7" layer="1"/>
<text x="-2.032" y="-6.427" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-2.667" y="-7.57" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-4.5" y1="3.625" x2="-4" y2="3.875" layer="51"/>
<rectangle x1="-4.5" y1="3.125" x2="-4" y2="3.375" layer="51"/>
<rectangle x1="-4.5" y1="2.625" x2="-4" y2="2.875" layer="51"/>
<rectangle x1="-4.5" y1="2.125" x2="-4" y2="2.375" layer="51"/>
<rectangle x1="-4.5" y1="1.625" x2="-4" y2="1.875" layer="51"/>
<rectangle x1="-4.5" y1="1.125" x2="-4" y2="1.375" layer="51"/>
<rectangle x1="-4.5" y1="0.625" x2="-4" y2="0.875" layer="51"/>
<rectangle x1="-4.5" y1="0.125" x2="-4" y2="0.375" layer="51"/>
<rectangle x1="-4.5" y1="-0.375" x2="-4" y2="-0.125" layer="51"/>
<rectangle x1="-4.5" y1="-0.875" x2="-4" y2="-0.625" layer="51"/>
<rectangle x1="-4.5" y1="-1.375" x2="-4" y2="-1.125" layer="51"/>
<rectangle x1="-4.5" y1="-1.875" x2="-4" y2="-1.625" layer="51"/>
<rectangle x1="-4.5" y1="-2.375" x2="-4" y2="-2.125" layer="51"/>
<rectangle x1="-4.5" y1="-2.875" x2="-4" y2="-2.625" layer="51"/>
<rectangle x1="-4.5" y1="-3.375" x2="-4" y2="-3.125" layer="51"/>
<rectangle x1="-4.5" y1="-3.875" x2="-4" y2="-3.625" layer="51"/>
<rectangle x1="-3.875" y1="-4.5" x2="-3.625" y2="-4" layer="51"/>
<rectangle x1="-3.375" y1="-4.5" x2="-3.125" y2="-4" layer="51"/>
<rectangle x1="-2.875" y1="-4.5" x2="-2.625" y2="-4" layer="51"/>
<rectangle x1="-2.375" y1="-4.5" x2="-2.125" y2="-4" layer="51"/>
<rectangle x1="-1.875" y1="-4.5" x2="-1.625" y2="-4" layer="51"/>
<rectangle x1="-1.375" y1="-4.5" x2="-1.125" y2="-4" layer="51"/>
<rectangle x1="-0.875" y1="-4.5" x2="-0.625" y2="-4" layer="51"/>
<rectangle x1="-0.375" y1="-4.5" x2="-0.125" y2="-4" layer="51"/>
<rectangle x1="0.125" y1="-4.5" x2="0.375" y2="-4" layer="51"/>
<rectangle x1="0.625" y1="-4.5" x2="0.875" y2="-4" layer="51"/>
<rectangle x1="1.125" y1="-4.5" x2="1.375" y2="-4" layer="51"/>
<rectangle x1="1.625" y1="-4.5" x2="1.875" y2="-4" layer="51"/>
<rectangle x1="2.125" y1="-4.5" x2="2.375" y2="-4" layer="51"/>
<rectangle x1="2.625" y1="-4.5" x2="2.875" y2="-4" layer="51"/>
<rectangle x1="3.125" y1="-4.5" x2="3.375" y2="-4" layer="51"/>
<rectangle x1="3.625" y1="-4.5" x2="3.875" y2="-4" layer="51"/>
<rectangle x1="4" y1="-3.875" x2="4.5" y2="-3.625" layer="51"/>
<rectangle x1="4" y1="-3.375" x2="4.5" y2="-3.125" layer="51"/>
<rectangle x1="4" y1="-2.875" x2="4.5" y2="-2.625" layer="51"/>
<rectangle x1="4" y1="-2.375" x2="4.5" y2="-2.125" layer="51"/>
<rectangle x1="4" y1="-1.875" x2="4.5" y2="-1.625" layer="51"/>
<rectangle x1="4" y1="-1.375" x2="4.5" y2="-1.125" layer="51"/>
<rectangle x1="4" y1="-0.875" x2="4.5" y2="-0.625" layer="51"/>
<rectangle x1="4" y1="-0.375" x2="4.5" y2="-0.125" layer="51"/>
<rectangle x1="4" y1="0.125" x2="4.5" y2="0.375" layer="51"/>
<rectangle x1="4" y1="0.625" x2="4.5" y2="0.875" layer="51"/>
<rectangle x1="4" y1="1.125" x2="4.5" y2="1.375" layer="51"/>
<rectangle x1="4" y1="1.625" x2="4.5" y2="1.875" layer="51"/>
<rectangle x1="4" y1="2.125" x2="4.5" y2="2.375" layer="51"/>
<rectangle x1="4" y1="2.625" x2="4.5" y2="2.875" layer="51"/>
<rectangle x1="4" y1="3.125" x2="4.5" y2="3.375" layer="51"/>
<rectangle x1="4" y1="3.625" x2="4.5" y2="3.875" layer="51"/>
<rectangle x1="3.625" y1="4" x2="3.875" y2="4.5" layer="51"/>
<rectangle x1="3.125" y1="4" x2="3.375" y2="4.5" layer="51"/>
<rectangle x1="2.625" y1="4" x2="2.875" y2="4.5" layer="51"/>
<rectangle x1="2.125" y1="4" x2="2.375" y2="4.5" layer="51"/>
<rectangle x1="1.625" y1="4" x2="1.875" y2="4.5" layer="51"/>
<rectangle x1="1.125" y1="4" x2="1.375" y2="4.5" layer="51"/>
<rectangle x1="0.625" y1="4" x2="0.875" y2="4.5" layer="51"/>
<rectangle x1="0.125" y1="4" x2="0.375" y2="4.5" layer="51"/>
<rectangle x1="-0.375" y1="4" x2="-0.125" y2="4.5" layer="51"/>
<rectangle x1="-0.875" y1="4" x2="-0.625" y2="4.5" layer="51"/>
<rectangle x1="-1.375" y1="4" x2="-1.125" y2="4.5" layer="51"/>
<rectangle x1="-1.875" y1="4" x2="-1.625" y2="4.5" layer="51"/>
<rectangle x1="-2.375" y1="4" x2="-2.125" y2="4.5" layer="51"/>
<rectangle x1="-2.875" y1="4" x2="-2.625" y2="4.5" layer="51"/>
<rectangle x1="-3.375" y1="4" x2="-3.125" y2="4.5" layer="51"/>
<rectangle x1="-3.875" y1="4" x2="-3.625" y2="4.5" layer="51"/>
<smd name="PAD" x="0" y="0" dx="7.7" dy="7.7" layer="1" cream="no"/>
<circle x="-4.5" y="4.5" radius="0.3" width="0.127" layer="21"/>
<rectangle x1="-3.81" y1="0.635" x2="-0.635" y2="3.81" layer="31"/>
<rectangle x1="0.635" y1="0.635" x2="3.81" y2="3.81" layer="31"/>
<rectangle x1="-3.81" y1="-3.81" x2="-0.635" y2="-0.635" layer="31"/>
<rectangle x1="0.635" y1="-3.81" x2="3.81" y2="-0.635" layer="31"/>
</package>
<package name="MA03-2">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-3.175" y1="2.54" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-3.81" y2="1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="3.81" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.905" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.905" x2="-3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.905" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-2.54" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="-1.27" drill="1.016"/>
<pad name="3" x="0" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="5" x="2.54" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="2" x="-2.54" y="1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="0" y="1.27" drill="1.016" shape="octagon"/>
<pad name="6" x="2.54" y="1.27" drill="1.016" shape="octagon"/>
<text x="-3.81" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-4.191" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-1.524" x2="0.254" y2="-1.016" layer="51"/>
<rectangle x1="-2.794" y1="-1.524" x2="-2.286" y2="-1.016" layer="51"/>
<rectangle x1="2.286" y1="-1.524" x2="2.794" y2="-1.016" layer="51"/>
<rectangle x1="-2.794" y1="1.016" x2="-2.286" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="1.016" x2="0.254" y2="1.524" layer="51"/>
<rectangle x1="2.286" y1="1.016" x2="2.794" y2="1.524" layer="51"/>
<circle x="-3.429" y="-2.159" radius="0.457903125" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="SCHOTTKY-DUAL-SERIES">
<wire x1="1.27" y1="1.27" x2="0" y2="3.81" width="0.254" layer="94"/>
<wire x1="0" y1="3.81" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="3.81" x2="0" y2="3.81" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="3.81" x2="1.27" y2="3.81" width="0.254" layer="94"/>
<wire x1="-1.27" y1="3.81" x2="-1.27" y2="3.302" width="0.254" layer="94"/>
<wire x1="1.27" y1="3.81" x2="1.27" y2="4.318" width="0.254" layer="94"/>
<pin name="IO" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="VCC" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<wire x1="1.27" y1="-3.81" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="-3.81" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-1.27" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-3.81" x2="1.27" y2="-3.81" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-1.778" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="-0.762" width="0.254" layer="94"/>
<text x="-1.778" y="-0.2286" size="1.524" layer="95" rot="R180">&gt;NAME</text>
<text x="-1.778" y="-2.0066" size="1.27" layer="96" rot="R180">&gt;VALUE</text>
<pin name="GND" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<wire x1="0" y1="2.54" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.27" y1="4.318" x2="0.762" y2="4.318" width="0.254" layer="94"/>
<wire x1="-0.762" y1="3.302" x2="-1.27" y2="3.302" width="0.254" layer="94"/>
<wire x1="-0.762" y1="-1.778" x2="-1.27" y2="-1.778" width="0.254" layer="94"/>
<wire x1="1.27" y1="-0.762" x2="0.762" y2="-0.762" width="0.254" layer="94"/>
</symbol>
<symbol name="XMEGA-A3U">
<wire x1="-27.94" y1="53.34" x2="30.48" y2="53.34" width="0.254" layer="94"/>
<wire x1="30.48" y1="53.34" x2="30.48" y2="-50.8" width="0.254" layer="94"/>
<wire x1="30.48" y1="-50.8" x2="-27.94" y2="-50.8" width="0.254" layer="94"/>
<wire x1="-27.94" y1="-50.8" x2="-27.94" y2="53.34" width="0.254" layer="94"/>
<text x="-27.94" y="54.61" size="1.9304" layer="95" ratio="5">&gt;NAME</text>
<text x="-9.906" y="51.054" size="1.9304" layer="96" ratio="5">&gt;VALUE</text>
<pin name="PA3(ADC3)" x="-33.02" y="40.64" length="middle"/>
<pin name="PA4(ADC4)" x="-33.02" y="38.1" length="middle"/>
<pin name="PA5(ADC5)" x="-33.02" y="35.56" length="middle"/>
<pin name="PB1(ADC1)" x="-33.02" y="20.32" length="middle"/>
<pin name="GND@1" x="10.16" y="-55.88" length="middle" direction="pwr" rot="R90"/>
<pin name="VCC@1" x="35.56" y="-33.02" length="middle" direction="pwr" rot="R180"/>
<pin name="PC1(OC0B/XCK0/SCL)" x="-33.02" y="-5.08" length="middle"/>
<pin name="AVCC" x="35.56" y="-27.94" length="middle" direction="pwr" rot="R180"/>
<pin name="PA1(ADC1)" x="-33.02" y="45.72" length="middle"/>
<pin name="PA2(ADC2)" x="-33.02" y="43.18" length="middle"/>
<pin name="PB2(ADC2/DAC0)" x="-33.02" y="17.78" length="middle"/>
<pin name="GND@2" x="7.62" y="-55.88" length="middle" direction="pwr" rot="R90"/>
<pin name="PC0(OC0A/SDA)" x="-33.02" y="-2.54" length="middle"/>
<pin name="PC2(OC0C/RXD0)" x="-33.02" y="-7.62" length="middle"/>
<pin name="GND@3" x="5.08" y="-55.88" length="middle" direction="pwr" rot="R90"/>
<pin name="PA0(ADC0/AREFA)" x="-33.02" y="48.26" length="middle"/>
<pin name="PA6(ADC6/AC1-OUT)" x="-33.02" y="33.02" length="middle"/>
<pin name="PB3(ADC3/DAC1)" x="-33.02" y="15.24" length="middle"/>
<pin name="PC3(OC0D/TXD0)" x="-33.02" y="-10.16" length="middle"/>
<pin name="PC4(OC1A/!SS!)" x="-33.02" y="-12.7" length="middle"/>
<pin name="PC5(OC1B/MOSI/XCK1)" x="-33.02" y="-15.24" length="middle"/>
<pin name="PR1(XTAL1)" x="-33.02" y="-43.18" length="middle"/>
<pin name="GND@4" x="2.54" y="-55.88" length="middle" direction="pwr" rot="R90"/>
<pin name="PA7(ADC7/AC0-OUT)" x="-33.02" y="30.48" length="middle"/>
<pin name="PB0(ADC0/AREFB)" x="-33.02" y="22.86" length="middle"/>
<pin name="PC6(MISO/RXD1)" x="-33.02" y="-17.78" length="middle"/>
<pin name="PC7(SCK/TXD1/CLKO/EVO)" x="-33.02" y="-20.32" length="middle"/>
<pin name="PR0(XTAL2)" x="-33.02" y="-38.1" length="middle"/>
<pin name="PDI-CLK/!RESET" x="-33.02" y="-33.02" length="middle" direction="in"/>
<pin name="PD4(OC1A/!SS!)" x="35.56" y="35.56" length="middle" rot="R180"/>
<pin name="PD0(OC0A)" x="35.56" y="45.72" length="middle" rot="R180"/>
<pin name="PDI-DATA" x="-33.02" y="-27.94" length="middle"/>
<pin name="PE2(OC0C/RXD0)" x="35.56" y="15.24" length="middle" rot="R180"/>
<pin name="PE1(OC0B/XCK0/SCL)" x="35.56" y="17.78" length="middle" rot="R180"/>
<pin name="PD7(TXD1/SCK/D+)" x="35.56" y="27.94" length="middle" rot="R180"/>
<pin name="PD5(OC1B/XCK1/MOSI)" x="35.56" y="33.02" length="middle" rot="R180"/>
<pin name="PD1(OC0B/XCK0)" x="35.56" y="43.18" length="middle" rot="R180"/>
<pin name="VCC@2" x="35.56" y="-35.56" length="middle" direction="pwr" rot="R180"/>
<pin name="PE3(OC0D/TXD0)" x="35.56" y="12.7" length="middle" rot="R180"/>
<pin name="VCC@3" x="35.56" y="-38.1" length="middle" direction="pwr" rot="R180"/>
<pin name="PE0(OC0A/SDA)" x="35.56" y="20.32" length="middle" rot="R180"/>
<pin name="PD6(RXD1/MISO/D-)" x="35.56" y="30.48" length="middle" rot="R180"/>
<pin name="PD3(OC0D/TXD0)" x="35.56" y="38.1" length="middle" rot="R180"/>
<pin name="PD2(OC0C/RXD0)" x="35.56" y="40.64" length="middle" rot="R180"/>
<pin name="PB4(ADC4/TMS)" x="-33.02" y="12.7" length="middle"/>
<pin name="PB5(ADC5/TDI)" x="-33.02" y="10.16" length="middle"/>
<pin name="PB6(ADC6/TCK)" x="-33.02" y="7.62" length="middle"/>
<pin name="PB7(ADC7/AC0-OUT/TDO)" x="-33.02" y="5.08" length="middle"/>
<pin name="PE4(OC1A/!SS!)" x="35.56" y="10.16" length="middle" rot="R180"/>
<pin name="PE5(OC1B/SCK1/MOSI)" x="35.56" y="7.62" length="middle" rot="R180"/>
<pin name="PE6(RSC1/MISO/TOSC2)" x="35.56" y="5.08" length="middle" rot="R180"/>
<pin name="PE7(TSD1/SCK/CLKO/EVO/TOSC1)" x="35.56" y="2.54" length="middle" rot="R180"/>
<pin name="PF0(OC0A)" x="35.56" y="-5.08" length="middle" rot="R180"/>
<pin name="PF1(OC0B/XCK0)" x="35.56" y="-7.62" length="middle" rot="R180"/>
<pin name="PF2(OC0C/RXD0)" x="35.56" y="-10.16" length="middle" rot="R180"/>
<pin name="PF3(OC0D/TXD0)" x="35.56" y="-12.7" length="middle" rot="R180"/>
<pin name="PF4" x="35.56" y="-15.24" length="middle" rot="R180"/>
<pin name="PF5" x="35.56" y="-17.78" length="middle" rot="R180"/>
<pin name="PF6" x="35.56" y="-20.32" length="middle" rot="R180"/>
<pin name="PF7" x="35.56" y="-22.86" length="middle" rot="R180"/>
<pin name="GND@5" x="0" y="-55.88" length="middle" direction="pwr" rot="R90"/>
<pin name="GND@6" x="-2.54" y="-55.88" length="middle" direction="pwr" rot="R90"/>
<pin name="VCC@4" x="35.56" y="-40.64" length="middle" direction="pwr" rot="R180"/>
<pin name="VCC@5" x="35.56" y="-43.18" length="middle" direction="pwr" rot="R180"/>
<pin name="GND@PAD" x="-5.08" y="-55.88" length="middle" direction="pwr" rot="R90"/>
</symbol>
<symbol name="MA03-2">
<wire x1="3.81" y1="-5.08" x2="-3.81" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="5.08" x2="-3.81" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-1.27" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-1.27" y2="-2.54" width="0.6096" layer="94"/>
<text x="-3.81" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="4" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="6" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="BAS70-04W" prefix="V">
<gates>
<gate name="G$1" symbol="SCHOTTKY-DUAL-SERIES" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT323">
<connects>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="IO" pad="3"/>
<connect gate="G$1" pin="VCC" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="XMEGA-A3U" prefix="D">
<description>Atmel XMega A3 series processor</description>
<gates>
<gate name="G$1" symbol="XMEGA-A3U" x="2.54" y="-2.54"/>
</gates>
<devices>
<device name="MLF" package="QFN64">
<connects>
<connect gate="G$1" pin="AVCC" pad="61"/>
<connect gate="G$1" pin="GND@1" pad="60"/>
<connect gate="G$1" pin="GND@2" pad="14"/>
<connect gate="G$1" pin="GND@3" pad="24"/>
<connect gate="G$1" pin="GND@4" pad="34"/>
<connect gate="G$1" pin="GND@5" pad="44"/>
<connect gate="G$1" pin="GND@6" pad="52"/>
<connect gate="G$1" pin="GND@PAD" pad="PAD"/>
<connect gate="G$1" pin="PA0(ADC0/AREFA)" pad="62"/>
<connect gate="G$1" pin="PA1(ADC1)" pad="63"/>
<connect gate="G$1" pin="PA2(ADC2)" pad="64"/>
<connect gate="G$1" pin="PA3(ADC3)" pad="1"/>
<connect gate="G$1" pin="PA4(ADC4)" pad="2"/>
<connect gate="G$1" pin="PA5(ADC5)" pad="3"/>
<connect gate="G$1" pin="PA6(ADC6/AC1-OUT)" pad="4"/>
<connect gate="G$1" pin="PA7(ADC7/AC0-OUT)" pad="5"/>
<connect gate="G$1" pin="PB0(ADC0/AREFB)" pad="6"/>
<connect gate="G$1" pin="PB1(ADC1)" pad="7"/>
<connect gate="G$1" pin="PB2(ADC2/DAC0)" pad="8"/>
<connect gate="G$1" pin="PB3(ADC3/DAC1)" pad="9"/>
<connect gate="G$1" pin="PB4(ADC4/TMS)" pad="10"/>
<connect gate="G$1" pin="PB5(ADC5/TDI)" pad="11"/>
<connect gate="G$1" pin="PB6(ADC6/TCK)" pad="12"/>
<connect gate="G$1" pin="PB7(ADC7/AC0-OUT/TDO)" pad="13"/>
<connect gate="G$1" pin="PC0(OC0A/SDA)" pad="16"/>
<connect gate="G$1" pin="PC1(OC0B/XCK0/SCL)" pad="17"/>
<connect gate="G$1" pin="PC2(OC0C/RXD0)" pad="18"/>
<connect gate="G$1" pin="PC3(OC0D/TXD0)" pad="19"/>
<connect gate="G$1" pin="PC4(OC1A/!SS!)" pad="20"/>
<connect gate="G$1" pin="PC5(OC1B/MOSI/XCK1)" pad="21"/>
<connect gate="G$1" pin="PC6(MISO/RXD1)" pad="22"/>
<connect gate="G$1" pin="PC7(SCK/TXD1/CLKO/EVO)" pad="23"/>
<connect gate="G$1" pin="PD0(OC0A)" pad="26"/>
<connect gate="G$1" pin="PD1(OC0B/XCK0)" pad="27"/>
<connect gate="G$1" pin="PD2(OC0C/RXD0)" pad="28"/>
<connect gate="G$1" pin="PD3(OC0D/TXD0)" pad="29"/>
<connect gate="G$1" pin="PD4(OC1A/!SS!)" pad="30"/>
<connect gate="G$1" pin="PD5(OC1B/XCK1/MOSI)" pad="31"/>
<connect gate="G$1" pin="PD6(RXD1/MISO/D-)" pad="32"/>
<connect gate="G$1" pin="PD7(TXD1/SCK/D+)" pad="33"/>
<connect gate="G$1" pin="PDI-CLK/!RESET" pad="57"/>
<connect gate="G$1" pin="PDI-DATA" pad="56"/>
<connect gate="G$1" pin="PE0(OC0A/SDA)" pad="36"/>
<connect gate="G$1" pin="PE1(OC0B/XCK0/SCL)" pad="37"/>
<connect gate="G$1" pin="PE2(OC0C/RXD0)" pad="38"/>
<connect gate="G$1" pin="PE3(OC0D/TXD0)" pad="39"/>
<connect gate="G$1" pin="PE4(OC1A/!SS!)" pad="40"/>
<connect gate="G$1" pin="PE5(OC1B/SCK1/MOSI)" pad="41"/>
<connect gate="G$1" pin="PE6(RSC1/MISO/TOSC2)" pad="42"/>
<connect gate="G$1" pin="PE7(TSD1/SCK/CLKO/EVO/TOSC1)" pad="43"/>
<connect gate="G$1" pin="PF0(OC0A)" pad="46"/>
<connect gate="G$1" pin="PF1(OC0B/XCK0)" pad="47"/>
<connect gate="G$1" pin="PF2(OC0C/RXD0)" pad="48"/>
<connect gate="G$1" pin="PF3(OC0D/TXD0)" pad="49"/>
<connect gate="G$1" pin="PF4" pad="50"/>
<connect gate="G$1" pin="PF5" pad="51"/>
<connect gate="G$1" pin="PF6" pad="54"/>
<connect gate="G$1" pin="PF7" pad="55"/>
<connect gate="G$1" pin="PR0(XTAL2)" pad="58"/>
<connect gate="G$1" pin="PR1(XTAL1)" pad="59"/>
<connect gate="G$1" pin="VCC@1" pad="53"/>
<connect gate="G$1" pin="VCC@2" pad="45"/>
<connect gate="G$1" pin="VCC@3" pad="35"/>
<connect gate="G$1" pin="VCC@4" pad="25"/>
<connect gate="G$1" pin="VCC@5" pad="15"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MA03-2" prefix="X" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="MA03-2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MA03-2">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
<connect gate="1" pin="3" pad="3"/>
<connect gate="1" pin="4" pad="4"/>
<connect gate="1" pin="5" pad="5"/>
<connect gate="1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Display">
<packages>
<package name="SOT23">
<description>&lt;b&gt;SOT-23&lt;/b&gt;</description>
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.4224" y1="-0.6604" x2="-1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.6604" x2="-1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="51"/>
<smd name="3" x="0" y="1.1" dx="1" dy="1.4" layer="1"/>
<smd name="2" x="0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<smd name="1" x="-0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="N-MOS">
<circle x="0" y="-2.794" radius="0.3592" width="0" layer="94"/>
<circle x="0" y="-2.032" radius="0.3592" width="0" layer="94"/>
<circle x="0" y="3.048" radius="0.3592" width="0" layer="94"/>
<rectangle x1="-2.032" y1="1.27" x2="-1.524" y2="2.54" layer="94"/>
<rectangle x1="-2.032" y1="-2.54" x2="-1.524" y2="-1.27" layer="94"/>
<rectangle x1="-2.032" y1="-0.762" x2="-1.524" y2="0.762" layer="94"/>
<wire x1="-1.27" y1="0" x2="-0.254" y2="0.381" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="0.381" x2="-0.254" y2="-0.381" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-0.381" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="0" x2="-0.889" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.032" x2="0" y2="-2.794" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="3.048" x2="1.27" y2="3.048" width="0.1524" layer="94"/>
<wire x1="1.27" y1="3.048" x2="1.27" y2="0.762" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0.762" x2="1.27" y2="0.508" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0.508" x2="1.27" y2="-2.794" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-2.794" x2="0" y2="-2.794" width="0.1524" layer="94"/>
<wire x1="1.778" y1="0" x2="0.762" y2="0" width="0.1524" layer="94"/>
<wire x1="0.762" y1="0" x2="1.27" y2="0.762" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0.762" x2="1.778" y2="0" width="0.1524" layer="94"/>
<wire x1="1.778" y1="0.762" x2="0.762" y2="0.762" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="2.032" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="2.032" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-2.032" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="0" x2="-0.381" y2="-0.254" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-0.254" x2="-0.381" y2="0.254" width="0.254" layer="94"/>
<wire x1="-0.381" y1="0.254" x2="-0.889" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0.508" x2="1.016" y2="0.127" width="0.254" layer="94"/>
<wire x1="1.016" y1="0.127" x2="1.524" y2="0.127" width="0.254" layer="94"/>
<wire x1="1.524" y1="0.127" x2="1.27" y2="0.508" width="0.254" layer="94"/>
<pin name="D" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="G" x="-5.08" y="-2.54" visible="off" length="short" direction="pas"/>
<pin name="S" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<text x="2.54" y="0" size="1.524" layer="95">&gt;NAME</text>
<text x="2.54" y="-1.524" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="P-MOS">
<wire x1="-0.254" y1="0" x2="-1.27" y2="0.381" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0.381" x2="-1.27" y2="-0.381" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-0.381" x2="-0.254" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="0" x2="-0.508" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.508" y1="0" x2="-0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.635" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="2.032" x2="0" y2="2.794" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="0" x2="-0.254" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-3.048" x2="1.27" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-3.048" x2="1.27" y2="0.254" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0.254" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="2.794" width="0.1524" layer="94"/>
<wire x1="1.27" y1="2.794" x2="0" y2="2.794" width="0.1524" layer="94"/>
<wire x1="0.762" y1="-0.508" x2="1.778" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-0.508" x2="1.27" y2="0.254" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0.254" x2="0.762" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0.762" y1="0.254" x2="1.778" y2="0.254" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-2.032" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.032" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="2.032" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="-0.508" y1="0" x2="-1.143" y2="-0.254" width="0.254" layer="94"/>
<wire x1="-1.143" y1="-0.254" x2="-1.143" y2="0.254" width="0.254" layer="94"/>
<wire x1="-1.143" y1="0.254" x2="-0.635" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.524" y2="-0.381" width="0.254" layer="94"/>
<wire x1="1.524" y1="-0.381" x2="1.016" y2="-0.381" width="0.254" layer="94"/>
<wire x1="1.016" y1="-0.381" x2="1.27" y2="0" width="0.254" layer="94"/>
<circle x="0" y="2.794" radius="0.3592" width="0" layer="94"/>
<circle x="0" y="2.032" radius="0.3592" width="0" layer="94"/>
<circle x="0" y="-3.048" radius="0.3592" width="0" layer="94"/>
<text x="-3.048" y="0.254" size="1.524" layer="95" rot="MR0">&gt;NAME</text>
<text x="-3.048" y="-1.524" size="1.27" layer="96" rot="MR0">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-2.54" x2="-1.524" y2="-1.27" layer="94"/>
<rectangle x1="-2.032" y1="1.27" x2="-1.524" y2="2.54" layer="94"/>
<rectangle x1="-2.032" y1="-0.762" x2="-1.524" y2="0.762" layer="94"/>
<pin name="G" x="-5.08" y="2.54" visible="off" length="short" direction="pas"/>
<pin name="D" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="S" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="2N700" prefix="V">
<description>&lt;b&gt;N-CHANNEL MOS FET&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="N-MOS" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
<technology name="0"/>
<technology name="2"/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FDN338P" prefix="V">
<gates>
<gate name="G$1" symbol="P-MOS" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DISTRIBUTOR" value="farnell.com" constant="no"/>
<attribute name="MANUFACTURER" value="FAIRCHILD SEMICONDUCTOR" constant="no"/>
<attribute name="ORDER_NO" value="9846301" constant="no"/>
<attribute name="PART_NO" value="FDN338P" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Androdpod">
<packages>
<package name="TO252">
<description>&lt;b&gt;SMALL OUTLINE TRANSISTOR&lt;/b&gt;&lt;p&gt;
TS-003</description>
<wire x1="3.2766" y1="3.8354" x2="3.277" y2="-2.159" width="0.2032" layer="21"/>
<wire x1="3.277" y1="-2.159" x2="-3.277" y2="-2.159" width="0.2032" layer="21"/>
<wire x1="-3.277" y1="-2.159" x2="-3.2766" y2="3.8354" width="0.2032" layer="21"/>
<wire x1="-3.973" y1="5.983" x2="3.973" y2="5.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-5.983" x2="-3.973" y2="-5.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-5.983" x2="-3.973" y2="5.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="5.983" x2="3.973" y2="-5.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.28" y="-4.8" dx="1" dy="1.6" layer="1"/>
<smd name="2" x="2.28" y="-4.8" dx="1" dy="1.6" layer="1"/>
<smd name="3" x="0" y="2.5" dx="5.4" dy="6.2" layer="1"/>
<text x="-3.81" y="-2.54" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="5.08" y="-2.54" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.7178" y1="-5.1562" x2="-1.8542" y2="-2.2606" layer="51"/>
<rectangle x1="1.8542" y1="-5.1562" x2="2.7178" y2="-2.2606" layer="51"/>
<rectangle x1="-0.4318" y1="-3.0226" x2="0.4318" y2="-2.2606" layer="21"/>
</package>
<package name="SOT223">
<description>&lt;b&gt;Smal Outline Transistor&lt;/b&gt;</description>
<wire x1="-3.124" y1="1.731" x2="-3.124" y2="-1.729" width="0.1524" layer="21"/>
<wire x1="3.124" y1="-1.729" x2="3.124" y2="1.731" width="0.1524" layer="21"/>
<wire x1="-3.124" y1="1.731" x2="3.124" y2="1.731" width="0.1524" layer="21"/>
<wire x1="3.124" y1="-1.729" x2="-3.124" y2="-1.729" width="0.1524" layer="21"/>
<smd name="1" x="-2.2606" y="-3.1496" dx="1.4986" dy="2.0066" layer="1"/>
<smd name="2" x="0.0254" y="-3.1496" dx="1.4986" dy="2.0066" layer="1"/>
<smd name="3" x="2.3114" y="-3.1496" dx="1.4986" dy="2.0066" layer="1"/>
<smd name="4" x="0" y="3.1496" dx="3.81" dy="2.0066" layer="1"/>
<text x="-2.54" y="4.318" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.794" y="-5.842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.524" y1="1.778" x2="1.524" y2="3.302" layer="51"/>
<rectangle x1="-2.667" y1="-3.302" x2="-1.905" y2="-1.778" layer="51"/>
<rectangle x1="1.905" y1="-3.302" x2="2.667" y2="-1.778" layer="51"/>
<rectangle x1="-0.381" y1="-3.302" x2="0.381" y2="-1.778" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="LD1117">
<wire x1="-5.08" y1="-5.08" x2="5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="5.08" y2="2.54" width="0.4064" layer="94"/>
<wire x1="5.08" y1="2.54" x2="-5.08" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<text x="2.54" y="-7.62" size="1.524" layer="95">&gt;NAME</text>
<text x="2.54" y="-10.16" size="1.27" layer="96">&gt;VALUE</text>
<text x="-2.032" y="-4.318" size="1.524" layer="95">GND</text>
<text x="-4.445" y="-0.635" size="1.524" layer="95">IN</text>
<text x="0.635" y="-0.635" size="1.524" layer="95">OUT</text>
<pin name="GND" x="0" y="-7.62" visible="off" length="short" direction="in" rot="R90"/>
<pin name="IN" x="-7.62" y="0" visible="off" length="short" direction="in"/>
<pin name="OUT" x="7.62" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="LD1117" prefix="N" uservalue="yes">
<gates>
<gate name="G$1" symbol="LD1117" x="0" y="0"/>
</gates>
<devices>
<device name="-D" package="TO252">
<connects>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="IN" pad="2"/>
<connect gate="G$1" pin="OUT" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-S" package="SOT223">
<connects>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="IN" pad="3"/>
<connect gate="G$1" pin="OUT" pad="2"/>
</connects>
<technologies>
<technology name="-33CTR">
<attribute name="DISTRIBUTOR" value="farnell.com" constant="no"/>
<attribute name="MANUFACTURER" value="STMICROELECTRONICS" constant="no"/>
<attribute name="ORDER_NO" value="1467780" constant="no"/>
<attribute name="PART_NO" value="LD1117S50CTR" constant="no"/>
</technology>
<technology name="-50CTR">
<attribute name="DISTRIBUTOR" value="farnell.com" constant="no"/>
<attribute name="MANUFACTURER" value="STMICROELECTRONICS" constant="no"/>
<attribute name="ORDER_NO" value="1467781" constant="no"/>
<attribute name="PART_NO" value="LD1117S33CTR" constant="no"/>
</technology>
<technology name="-TR">
<attribute name="DISTRIBUTOR" value="farnell.com" constant="no"/>
<attribute name="MANUFACTURER" value="STMICROELECTRONICS" constant="no"/>
<attribute name="ORDER_NO" value="1202824" constant="no"/>
<attribute name="PART_NO" value="LD1117STR-E" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="fiducial">
<packages>
<package name="PM">
<wire x1="-0.4" y1="-0.4" x2="-2.6" y2="-0.4" width="0.127" layer="21"/>
<wire x1="-2.6" y1="-0.4" x2="-2.6" y2="0.4" width="0.127" layer="21"/>
<wire x1="-2.6" y1="0.4" x2="-0.4" y2="0.4" width="0.127" layer="21"/>
<wire x1="-0.4" y1="0.4" x2="-0.4" y2="2.6" width="0.127" layer="21"/>
<wire x1="-0.4" y1="2.6" x2="0.4" y2="2.6" width="0.127" layer="21"/>
<wire x1="0.4" y1="2.6" x2="0.4" y2="0.4" width="0.127" layer="21"/>
<wire x1="0.4" y1="0.4" x2="2.6" y2="0.4" width="0.127" layer="21"/>
<wire x1="2.6" y1="0.4" x2="2.6" y2="-0.4" width="0.127" layer="21"/>
<wire x1="2.6" y1="-0.4" x2="0.4" y2="-0.4" width="0.127" layer="21"/>
<wire x1="0.4" y1="-0.4" x2="0.4" y2="-2.6" width="0.127" layer="21"/>
<wire x1="0.4" y1="-2.6" x2="-0.4" y2="-2.6" width="0.127" layer="21"/>
<wire x1="-0.4" y1="-2.6" x2="-0.4" y2="-0.4" width="0.127" layer="21"/>
<smd name="P$1" x="0" y="0" dx="5" dy="0.5" layer="1" rot="R180" cream="no"/>
<smd name="P$2" x="0" y="0" dx="5" dy="0.5" layer="1" rot="R270" cream="no"/>
<text x="-0.6" y="-0.8" size="1.016" layer="25" font="vector" rot="R180">&gt;NAME</text>
<text x="-0.6" y="-2" size="1.016" layer="27" font="vector" rot="R180">&gt;VALUE</text>
</package>
<package name="PM-ROUND">
<smd name="P$1" x="0" y="0" dx="1.5" dy="1.5" layer="1" roundness="100" thermals="no" cream="no"/>
<circle x="0" y="0" radius="1.25" width="1" layer="41"/>
<circle x="0" y="0" radius="1" width="1" layer="29"/>
</package>
<package name="PM-SQUARE">
<smd name="P$1" x="-0.25" y="0.25" dx="0.5" dy="0.5" layer="1" cream="no"/>
<smd name="P$2" x="0.25" y="-0.25" dx="0.5" dy="0.5" layer="1" cream="no"/>
<polygon width="0.0508" layer="41">
<vertex x="-1.5" y="1.5"/>
<vertex x="1.5" y="1.5"/>
<vertex x="1.5" y="-1.5"/>
<vertex x="-1.5" y="-1.5"/>
</polygon>
</package>
<package name="PM-SQUARE2">
<smd name="P$1" x="-0.5" y="0.5" dx="1" dy="1" layer="1" cream="no"/>
<smd name="P$2" x="0.5" y="-0.5" dx="1" dy="1" layer="1" cream="no"/>
<polygon width="0.0508" layer="41">
<vertex x="-3" y="3"/>
<vertex x="3" y="3"/>
<vertex x="3" y="-3"/>
<vertex x="-3" y="-3"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="PM">
<wire x1="-5.08" y1="0" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="-5.08" width="0.254" layer="94"/>
<circle x="0" y="0" radius="2.54" width="0.254" layer="94"/>
<circle x="0" y="0" radius="5.08" width="0.254" layer="94"/>
<text x="-0.508" y="-5.588" size="1.524" layer="95" rot="R180">&gt;NAME</text>
<text x="-0.508" y="-7.366" size="1.27" layer="96" rot="R180">&gt;VALUE</text>
<pin name="P$1" x="0" y="-7.62" visible="off" length="middle" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="POSITION-MARK" prefix="J" uservalue="yes">
<gates>
<gate name="G$1" symbol="PM" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PM">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-ROUND" package="PM-ROUND">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SQUARE" package="PM-SQUARE">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SQUARE-LARGE" package="PM-SQUARE2">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1 P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
<class number="2" name="USB" width="0.1905" drill="0.3">
<clearance class="2" value="0.1905"/>
</class>
</classes>
<parts>
<part name="U$2" library="xdevelop-frame" deviceset="XDEVELOP-A3" device="">
<attribute name="CREATE_DATE" value="18.04.14"/>
<attribute name="DRAWING" value=" "/>
<attribute name="EDIT_DATE" value="18.04.14"/>
<attribute name="FILENAME" value=""/>
<attribute name="OBJECT_NO" value=""/>
<attribute name="SUBTITLE" value="Temperature Measurement"/>
<attribute name="TITLE" value="Certoclav EL2"/>
<attribute name="USERNAME" value="Bernhard Wörndl-A."/>
</part>
<part name="C17" library="Basic" deviceset="C-EU" device="C0603" value="100n">
<attribute name="AVAILABLE_QUANTITY" value="3188000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1825492.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2310603"/>
<attribute name="PARTS_PER_PACKAGE" value="4000"/>
<attribute name="PART_NO" value="MC0603B104K500CT"/>
<attribute name="PRICE" value="0.0024"/>
<attribute name="PRICES" value="4000-0:0.0024|20000-0:0.0021|40000--1:0.0019|"/>
<attribute name="QUANTITY_MULTIPLIER" value="4000"/>
</part>
<part name="C15" library="Basic" deviceset="C-EU" device="C0603" value="100n">
<attribute name="AVAILABLE_QUANTITY" value="3188000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1825492.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2310603"/>
<attribute name="PARTS_PER_PACKAGE" value="4000"/>
<attribute name="PART_NO" value="MC0603B104K500CT"/>
<attribute name="PRICE" value="0.0024"/>
<attribute name="PRICES" value="4000-0:0.0024|20000-0:0.0021|40000--1:0.0019|"/>
<attribute name="QUANTITY_MULTIPLIER" value="4000"/>
</part>
<part name="R3" library="Basic" deviceset="R-EU_" device="R0603" value="1k 0.1%">
<attribute name="AVAILABLE_QUANTITY" value="9260"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1581132.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="SUSUMU"/>
<attribute name="MINIMUM_QUANTITY" value="1"/>
<attribute name="ORDER_NO" value="1653252"/>
<attribute name="PARTS_PER_PACKAGE" value="1"/>
<attribute name="PART_NO" value="RR0816P-102-B-T5"/>
<attribute name="PRICE" value="0.0598"/>
<attribute name="PRICES" value="1-0:0.104|25-0:0.095|50-0:0.087|100-0:0.081|500--1:0.075|"/>
<attribute name="QUANTITY_MULTIPLIER" value="1"/>
</part>
<part name="GND32" library="Basic" deviceset="GND" device=""/>
<part name="GND33" library="Basic" deviceset="GND" device=""/>
<part name="GND34" library="Basic" deviceset="GND" device=""/>
<part name="GND39" library="Basic" deviceset="GND" device=""/>
<part name="R9" library="Basic" deviceset="R-EU_" device="R0603" value="100R 0.1%">
<attribute name="AVAILABLE_QUANTITY" value="5000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/485442.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="SUSUMU"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="1653251"/>
<attribute name="PARTS_PER_PACKAGE" value="5000"/>
<attribute name="PART_NO" value="RR0816P-101-B-T5"/>
<attribute name="POPULATE" value=""/>
<attribute name="PRICE" value="0.0924"/>
<attribute name="PRICES" value="5000-0:0.055|15000-0:0.054|25000--1:0.053|"/>
<attribute name="QUANTITY_MULTIPLIER" value="5000"/>
</part>
<part name="R2" library="Basic" deviceset="R-EU_" device="R0603" value="1k 0.1%">
<attribute name="AVAILABLE_QUANTITY" value="9260"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1581132.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="SUSUMU"/>
<attribute name="MINIMUM_QUANTITY" value="1"/>
<attribute name="ORDER_NO" value="1653252"/>
<attribute name="PARTS_PER_PACKAGE" value="1"/>
<attribute name="PART_NO" value="RR0816P-102-B-T5"/>
<attribute name="PRICE" value="0.0598"/>
<attribute name="PRICES" value="1-0:0.104|25-0:0.095|50-0:0.087|100-0:0.081|500--1:0.075|"/>
<attribute name="QUANTITY_MULTIPLIER" value="1"/>
</part>
<part name="V4" library="certoclav" deviceset="FDN337N" device="">
<attribute name="AVAILABLE_QUANTITY" value="1955"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/220571.pdf "/>
<attribute name="MINIMUM_QUANTITY" value="1"/>
<attribute name="PARTS_PER_PACKAGE" value="5"/>
<attribute name="PRICE" value="0.1329"/>
<attribute name="PRICES" value="5-0:0.48|25-0:0.345|100-0:0.176|1000-0:0.151|3000--1:0.0843|"/>
<attribute name="QUANTITY_MULTIPLIER" value="5"/>
</part>
<part name="V3" library="certoclav" deviceset="FDN337N" device="">
<attribute name="AVAILABLE_QUANTITY" value="1955"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/220571.pdf "/>
<attribute name="MINIMUM_QUANTITY" value="1"/>
<attribute name="PARTS_PER_PACKAGE" value="5"/>
<attribute name="PRICE" value="0.1329"/>
<attribute name="PRICES" value="5-0:0.48|25-0:0.345|100-0:0.176|1000-0:0.151|3000--1:0.0843|"/>
<attribute name="QUANTITY_MULTIPLIER" value="5"/>
</part>
<part name="V2" library="certoclav" deviceset="FDN337N" device="">
<attribute name="AVAILABLE_QUANTITY" value="1955"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/220571.pdf "/>
<attribute name="MINIMUM_QUANTITY" value="1"/>
<attribute name="PARTS_PER_PACKAGE" value="5"/>
<attribute name="PRICE" value="0.1329"/>
<attribute name="PRICES" value="5-0:0.48|25-0:0.345|100-0:0.176|1000-0:0.151|3000--1:0.0843|"/>
<attribute name="QUANTITY_MULTIPLIER" value="5"/>
</part>
<part name="V1" library="certoclav" deviceset="FDN337N" device="">
<attribute name="AVAILABLE_QUANTITY" value="1955"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/220571.pdf "/>
<attribute name="MINIMUM_QUANTITY" value="1"/>
<attribute name="PARTS_PER_PACKAGE" value="5"/>
<attribute name="PRICE" value="0.1329"/>
<attribute name="PRICES" value="5-0:0.48|25-0:0.345|100-0:0.176|1000-0:0.151|3000--1:0.0843|"/>
<attribute name="QUANTITY_MULTIPLIER" value="5"/>
</part>
<part name="GND40" library="Basic" deviceset="GND" device=""/>
<part name="GND41" library="Basic" deviceset="GND" device=""/>
<part name="R8" library="Basic" deviceset="R-EU_" device="R0603" value="180R 0.1%">
<attribute name="AVAILABLE_QUANTITY" value=""/>
<attribute name="DATASHEET" value=""/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="TE CONNECTIVITY / AMP"/>
<attribute name="MINIMUM_QUANTITY" value=""/>
<attribute name="ORDER_NO" value="1863392"/>
<attribute name="PARTS_PER_PACKAGE" value=""/>
<attribute name="PART_NO" value="9-1879225-5"/>
<attribute name="POPULATE" value=""/>
<attribute name="PRICE" value="0.162"/>
<attribute name="PRICES" value=""/>
<attribute name="QUANTITY_MULTIPLIER" value=""/>
</part>
<part name="R7" library="Basic" deviceset="R-EU_" device="R0603" value="100R 0.1%">
<attribute name="AVAILABLE_QUANTITY" value="5000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/485442.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="SUSUMU"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="1653251"/>
<attribute name="PARTS_PER_PACKAGE" value="5000"/>
<attribute name="PART_NO" value="RR0816P-101-B-T5"/>
<attribute name="POPULATE" value=""/>
<attribute name="PRICE" value="0.0924"/>
<attribute name="PRICES" value="5000-0:0.055|15000-0:0.054|25000--1:0.053|"/>
<attribute name="QUANTITY_MULTIPLIER" value="5000"/>
</part>
<part name="GND47" library="Basic" deviceset="GND" device=""/>
<part name="GND49" library="Basic" deviceset="GND" device=""/>
<part name="GND50" library="Basic" deviceset="GND" device=""/>
<part name="GND51" library="Basic" deviceset="GND" device=""/>
<part name="C16" library="Basic" deviceset="C-EU" device="C0603" value="100n">
<attribute name="AVAILABLE_QUANTITY" value="3188000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1825492.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2310603"/>
<attribute name="PARTS_PER_PACKAGE" value="4000"/>
<attribute name="PART_NO" value="MC0603B104K500CT"/>
<attribute name="PRICE" value="0.0024"/>
<attribute name="PRICES" value="4000-0:0.0024|20000-0:0.0021|40000--1:0.0019|"/>
<attribute name="QUANTITY_MULTIPLIER" value="4000"/>
</part>
<part name="GND52" library="Basic" deviceset="GND" device=""/>
<part name="R10" library="Basic" deviceset="R-EU_" device="R0603" value="10k">
<attribute name="AVAILABLE_QUANTITY" value="1042000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1697337.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="EXNAME" value="RL"/>
<attribute name="MANUFACTURER" value="VISHAY DRALORIC"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2122493"/>
<attribute name="PARTS_PER_PACKAGE" value="5000"/>
<attribute name="PART_NO" value="CRCW060310K0FKEA"/>
<attribute name="PRICE" value="0.002"/>
<attribute name="PRICES" value="5000-0:0.002|25000-0:0.0018|50000--1:0.0015|"/>
<attribute name="QUANTITY_MULTIPLIER" value="5000"/>
</part>
<part name="R11" library="Basic" deviceset="R-EU_" device="R0603" value="10k">
<attribute name="AVAILABLE_QUANTITY" value="1042000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1697337.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="EXNAME" value="RL"/>
<attribute name="MANUFACTURER" value="VISHAY DRALORIC"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2122493"/>
<attribute name="PARTS_PER_PACKAGE" value="5000"/>
<attribute name="PART_NO" value="CRCW060310K0FKEA"/>
<attribute name="PRICE" value="0.002"/>
<attribute name="PRICES" value="5000-0:0.002|25000-0:0.0018|50000--1:0.0015|"/>
<attribute name="QUANTITY_MULTIPLIER" value="5000"/>
</part>
<part name="R12" library="Basic" deviceset="R-EU_" device="R0603" value="10k">
<attribute name="AVAILABLE_QUANTITY" value="1042000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1697337.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="EXNAME" value="RL"/>
<attribute name="MANUFACTURER" value="VISHAY DRALORIC"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2122493"/>
<attribute name="PARTS_PER_PACKAGE" value="5000"/>
<attribute name="PART_NO" value="CRCW060310K0FKEA"/>
<attribute name="PRICE" value="0.002"/>
<attribute name="PRICES" value="5000-0:0.002|25000-0:0.0018|50000--1:0.0015|"/>
<attribute name="QUANTITY_MULTIPLIER" value="5000"/>
</part>
<part name="R13" library="Basic" deviceset="R-EU_" device="R0603" value="10k">
<attribute name="AVAILABLE_QUANTITY" value="1042000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1697337.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="EXNAME" value="RL"/>
<attribute name="MANUFACTURER" value="VISHAY DRALORIC"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2122493"/>
<attribute name="PARTS_PER_PACKAGE" value="5000"/>
<attribute name="PART_NO" value="CRCW060310K0FKEA"/>
<attribute name="PRICE" value="0.002"/>
<attribute name="PRICES" value="5000-0:0.002|25000-0:0.0018|50000--1:0.0015|"/>
<attribute name="QUANTITY_MULTIPLIER" value="5000"/>
</part>
<part name="R4" library="Basic" deviceset="R-EU_" device="R0603" value="10k">
<attribute name="AVAILABLE_QUANTITY" value="1042000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1697337.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="EXNAME" value="RL"/>
<attribute name="MANUFACTURER" value="VISHAY DRALORIC"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2122493"/>
<attribute name="PARTS_PER_PACKAGE" value="5000"/>
<attribute name="PART_NO" value="CRCW060310K0FKEA"/>
<attribute name="POPULATE" value=""/>
<attribute name="PRICE" value="0.002"/>
<attribute name="PRICES" value="5000-0:0.002|25000-0:0.0018|50000--1:0.0015|"/>
<attribute name="QUANTITY_MULTIPLIER" value="5000"/>
</part>
<part name="R5" library="Basic" deviceset="R-EU_" device="R0603" value="10k">
<attribute name="AVAILABLE_QUANTITY" value="1042000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1697337.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="EXNAME" value="RL"/>
<attribute name="MANUFACTURER" value="VISHAY DRALORIC"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2122493"/>
<attribute name="PARTS_PER_PACKAGE" value="5000"/>
<attribute name="PART_NO" value="CRCW060310K0FKEA"/>
<attribute name="PRICE" value="0.002"/>
<attribute name="PRICES" value="5000-0:0.002|25000-0:0.0018|50000--1:0.0015|"/>
<attribute name="QUANTITY_MULTIPLIER" value="5000"/>
</part>
<part name="R6" library="Basic" deviceset="R-EU_" device="R0603" value="10k">
<attribute name="AVAILABLE_QUANTITY" value="1042000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1697337.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="EXNAME" value="RL"/>
<attribute name="MANUFACTURER" value="VISHAY DRALORIC"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2122493"/>
<attribute name="PARTS_PER_PACKAGE" value="5000"/>
<attribute name="PART_NO" value="CRCW060310K0FKEA"/>
<attribute name="PRICE" value="0.002"/>
<attribute name="PRICES" value="5000-0:0.002|25000-0:0.0018|50000--1:0.0015|"/>
<attribute name="QUANTITY_MULTIPLIER" value="5000"/>
</part>
<part name="V5" library="linemetrics" deviceset="BAS70-04W" device="">
<attribute name="AVAILABLE_QUANTITY" value="5271"/>
<attribute name="DATASHEET" value="http://www.nxp.com/documents/data_sheet/BAS70_1PS7XSB70_SER.pdf"/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="NXP"/>
<attribute name="MINIMUM_QUANTITY" value="10"/>
<attribute name="ORDER_NO" value="8734437"/>
<attribute name="PARTS_PER_PACKAGE" value="10"/>
<attribute name="PART_NO" value="BAS70-04W"/>
<attribute name="PRICE" value="0.08"/>
<attribute name="PRICES" value="10-0:0.08|100-0:0.061|1000-0:0.048|3000--1:0.045|"/>
<attribute name="QUANTITY_MULTIPLIER" value="10"/>
</part>
<part name="GND55" library="Basic" deviceset="GND" device=""/>
<part name="V7" library="linemetrics" deviceset="BAS70-04W" device="">
<attribute name="AVAILABLE_QUANTITY" value="5271"/>
<attribute name="DATASHEET" value="http://www.nxp.com/documents/data_sheet/BAS70_1PS7XSB70_SER.pdf"/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="NXP"/>
<attribute name="MINIMUM_QUANTITY" value="10"/>
<attribute name="ORDER_NO" value="8734437"/>
<attribute name="PARTS_PER_PACKAGE" value="10"/>
<attribute name="PART_NO" value="BAS70-04W"/>
<attribute name="PRICE" value="0.08"/>
<attribute name="PRICES" value="10-0:0.08|100-0:0.061|1000-0:0.048|3000--1:0.045|"/>
<attribute name="QUANTITY_MULTIPLIER" value="10"/>
</part>
<part name="GND56" library="Basic" deviceset="GND" device=""/>
<part name="V6" library="linemetrics" deviceset="BAS70-04W" device="">
<attribute name="AVAILABLE_QUANTITY" value="5271"/>
<attribute name="DATASHEET" value="http://www.nxp.com/documents/data_sheet/BAS70_1PS7XSB70_SER.pdf"/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="NXP"/>
<attribute name="MINIMUM_QUANTITY" value="10"/>
<attribute name="ORDER_NO" value="8734437"/>
<attribute name="PARTS_PER_PACKAGE" value="10"/>
<attribute name="PART_NO" value="BAS70-04W"/>
<attribute name="PRICE" value="0.08"/>
<attribute name="PRICES" value="10-0:0.08|100-0:0.061|1000-0:0.048|3000--1:0.045|"/>
<attribute name="QUANTITY_MULTIPLIER" value="10"/>
</part>
<part name="GND57" library="Basic" deviceset="GND" device=""/>
<part name="GND58" library="Basic" deviceset="GND" device=""/>
<part name="V8" library="Basic" deviceset="ZENER-DIODE-DUAL" device="" value="3V6">
<attribute name="AVAILABLE_QUANTITY" value="783"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/705591.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="NXP"/>
<attribute name="MINIMUM_QUANTITY" value="1"/>
<attribute name="ORDER_NO" value="8735336"/>
<attribute name="PARTS_PER_PACKAGE" value="5"/>
<attribute name="PART_NO" value="BZB984-C3V6"/>
<attribute name="POPULATE" value=""/>
<attribute name="PRICE" value="0.063"/>
<attribute name="PRICES" value="5-0:0.11|100-0:0.085|1000-0:0.063|4000-0:0.06|8000-0:0.038|30000-0:0.0374|80000-0:0.0367|160000--1:0.036|"/>
<attribute name="QUANTITY_MULTIPLIER" value="5"/>
</part>
<part name="U$3" library="xdevelop-frame" deviceset="XDEVELOP-A3" device="">
<attribute name="CREATE_DATE" value="18.04.14"/>
<attribute name="DRAWING" value=" "/>
<attribute name="EDIT_DATE" value="18.04.14"/>
<attribute name="FILENAME" value=""/>
<attribute name="OBJECT_NO" value=""/>
<attribute name="SUBTITLE" value="High Power"/>
<attribute name="TITLE" value="Certoclav EL2"/>
<attribute name="USERNAME" value="Bernhard Wörndl-A."/>
</part>
<part name="GND10" library="Basic" deviceset="GND" device=""/>
<part name="C8" library="Basic" deviceset="C-EU" device="C0603" value="100n">
<attribute name="AVAILABLE_QUANTITY" value="3188000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1825492.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2310603"/>
<attribute name="PARTS_PER_PACKAGE" value="4000"/>
<attribute name="PART_NO" value="MC0603B104K500CT"/>
<attribute name="PRICE" value="0.0024"/>
<attribute name="PRICES" value="4000-0:0.0024|20000-0:0.0021|40000--1:0.0019|"/>
<attribute name="QUANTITY_MULTIPLIER" value="4000"/>
</part>
<part name="C9" library="Basic" deviceset="C-EU" device="C0603" value="100n">
<attribute name="AVAILABLE_QUANTITY" value="3188000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1825492.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2310603"/>
<attribute name="PARTS_PER_PACKAGE" value="4000"/>
<attribute name="PART_NO" value="MC0603B104K500CT"/>
<attribute name="PRICE" value="0.0024"/>
<attribute name="PRICES" value="4000-0:0.0024|20000-0:0.0021|40000--1:0.0019|"/>
<attribute name="QUANTITY_MULTIPLIER" value="4000"/>
</part>
<part name="C10" library="Basic" deviceset="C-EU" device="C0603" value="100n">
<attribute name="AVAILABLE_QUANTITY" value="3188000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1825492.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2310603"/>
<attribute name="PARTS_PER_PACKAGE" value="4000"/>
<attribute name="PART_NO" value="MC0603B104K500CT"/>
<attribute name="POPULATE" value=""/>
<attribute name="PRICE" value="0.0024"/>
<attribute name="PRICES" value="4000-0:0.0024|20000-0:0.0021|40000--1:0.0019|"/>
<attribute name="QUANTITY_MULTIPLIER" value="4000"/>
</part>
<part name="C11" library="Basic" deviceset="C-EU" device="C0603" value="100n">
<attribute name="AVAILABLE_QUANTITY" value="3188000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1825492.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2310603"/>
<attribute name="PARTS_PER_PACKAGE" value="4000"/>
<attribute name="PART_NO" value="MC0603B104K500CT"/>
<attribute name="PRICE" value="0.0024"/>
<attribute name="PRICES" value="4000-0:0.0024|20000-0:0.0021|40000--1:0.0019|"/>
<attribute name="QUANTITY_MULTIPLIER" value="4000"/>
</part>
<part name="C12" library="Basic" deviceset="C-EU" device="C0603" value="100n">
<attribute name="AVAILABLE_QUANTITY" value="3188000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1825492.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2310603"/>
<attribute name="PARTS_PER_PACKAGE" value="4000"/>
<attribute name="PART_NO" value="MC0603B104K500CT"/>
<attribute name="PRICE" value="0.0024"/>
<attribute name="PRICES" value="4000-0:0.0024|20000-0:0.0021|40000--1:0.0019|"/>
<attribute name="QUANTITY_MULTIPLIER" value="4000"/>
</part>
<part name="GND11" library="Basic" deviceset="GND" device=""/>
<part name="GND12" library="Basic" deviceset="GND" device=""/>
<part name="GND13" library="Basic" deviceset="GND" device=""/>
<part name="GND14" library="Basic" deviceset="GND" device=""/>
<part name="GND16" library="Basic" deviceset="GND" device=""/>
<part name="P+5" library="Basic" deviceset="+3.3V" device=""/>
<part name="GND25" library="Basic" deviceset="GND" device=""/>
<part name="C13" library="Basic" deviceset="C-EU" device="C0603" value="100n">
<attribute name="AVAILABLE_QUANTITY" value="3188000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1825492.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2310603"/>
<attribute name="PARTS_PER_PACKAGE" value="4000"/>
<attribute name="PART_NO" value="MC0603B104K500CT"/>
<attribute name="PRICE" value="0.0024"/>
<attribute name="PRICES" value="4000-0:0.0024|20000-0:0.0021|40000--1:0.0019|"/>
<attribute name="QUANTITY_MULTIPLIER" value="4000"/>
</part>
<part name="L4" library="Basic" deviceset="L-EU" device="0603" value="BLM18AG601SN1D">
<attribute name="AVAILABLE_QUANTITY" value="13956"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/356366.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MURATA"/>
<attribute name="MINIMUM_QUANTITY" value="5"/>
<attribute name="ORDER_NO" value="1515679"/>
<attribute name="PARTS_PER_PACKAGE" value="5"/>
<attribute name="PART_NO" value="BLM18AG601SN1D"/>
<attribute name="PRICE" value="0.046"/>
<attribute name="PRICES" value="5-0:0.046|50-0:0.045|100-0:0.042|250-0:0.029|500--1:0.027|"/>
<attribute name="QUANTITY_MULTIPLIER" value="5"/>
</part>
<part name="C14" library="Basic" deviceset="C-EU" device="C0603" value="100n">
<attribute name="AVAILABLE_QUANTITY" value="3188000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1825492.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2310603"/>
<attribute name="PARTS_PER_PACKAGE" value="4000"/>
<attribute name="PART_NO" value="MC0603B104K500CT"/>
<attribute name="PRICE" value="0.0024"/>
<attribute name="PRICES" value="4000-0:0.0024|20000-0:0.0021|40000--1:0.0019|"/>
<attribute name="QUANTITY_MULTIPLIER" value="4000"/>
</part>
<part name="GND35" library="Basic" deviceset="GND" device=""/>
<part name="GND36" library="Basic" deviceset="GND" device=""/>
<part name="P+6" library="Basic" deviceset="+3.3V" device=""/>
<part name="D2" library="linemetrics" deviceset="XMEGA-A3U" device="MLF" value="XMEGA64A3U">
<attribute name="AVAILABLE_QUANTITY" value="20"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1503981.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="ATMEL"/>
<attribute name="MINIMUM_QUANTITY" value="1"/>
<attribute name="ORDER_NO" value="2066304"/>
<attribute name="PARTS_PER_PACKAGE" value="1"/>
<attribute name="PART_NO" value="ATXMEGA192A3U-MH"/>
<attribute name="POPULATE" value=""/>
<attribute name="PRICE" value="3.18"/>
<attribute name="PRICES" value="1-0:10.44|5-0:7.77|10-0:6.64|25-0:6.08|100-0:5.62|500--1:5.22|"/>
<attribute name="QUANTITY_MULTIPLIER" value="1"/>
</part>
<part name="X3" library="linemetrics" deviceset="MA03-2" device="" value="PDI">
<attribute name="AVAILABLE_QUANTITY" value=""/>
<attribute name="DATASHEET" value="http://www.samtec.com/ftppub/pdf/TSW_TH.PDF "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="SAMTEC"/>
<attribute name="MINIMUM_QUANTITY" value="1"/>
<attribute name="ORDER_NO" value="1926075"/>
<attribute name="PARTS_PER_PACKAGE" value="1"/>
<attribute name="PART_NO" value="HTSW-103-05-T-D"/>
<attribute name="POPULATE" value="DNP"/>
<attribute name="PRICE" value="0.427"/>
<attribute name="PRICES" value="10-0:0.427|100-0:0.324|250-0:0.283|500-0:0.258|1000-0:0.237|2500--1:0.198|"/>
<attribute name="QUANTITY_MULTIPLIER" value="1"/>
</part>
<part name="P+7" library="Basic" deviceset="+3.3V" device=""/>
<part name="GND21" library="Basic" deviceset="GND" device=""/>
<part name="V13" library="Display" deviceset="FDN338P" device="">
<attribute name="AVAILABLE_QUANTITY" value="1171"/>
<attribute name="DATASHEET" value="http://www.fairchildsemi.com/ds/FD/FDN338P.pdf"/>
<attribute name="MINIMUM_QUANTITY" value="5"/>
<attribute name="PARTS_PER_PACKAGE" value="1"/>
<attribute name="POPULATE" value="ES"/>
<attribute name="PRICE" value="0.0674"/>
<attribute name="PRICES" value="5-0:0.537|25-0:0.37|100-0:0.242|1000-0:0.122|3000--1:0.108|"/>
<attribute name="QUANTITY_MULTIPLIER" value="5"/>
</part>
<part name="P+12" library="Basic" deviceset="+5V" device=""/>
<part name="R31" library="Basic" deviceset="R-EU_" device="R0603" value="10R">
<attribute name="AVAILABLE_QUANTITY" value="6117"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/34922.pdf"/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="VISHAY DRALORIC"/>
<attribute name="MINIMUM_QUANTITY" value="50"/>
<attribute name="ORDER_NO" value="1738878"/>
<attribute name="PARTS_PER_PACKAGE" value="1"/>
<attribute name="PART_NO" value="CRCW060310R0FKEAHP"/>
<attribute name="POPULATE" value="ES"/>
<attribute name="PRICE" value="0.031"/>
<attribute name="PRICES" value="50-0:0.035|250-0:0.029|1000-0:0.021|5000-0:0.014|15000--1:0.009|"/>
<attribute name="QUANTITY_MULTIPLIER" value="50"/>
</part>
<part name="P+9" library="Basic" deviceset="+5V" device=""/>
<part name="V14" library="Display" deviceset="2N700" device="" technology="0" value="2N7002">
<attribute name="AVAILABLE_QUANTITY" value="16048"/>
<attribute name="DATASHEET" value="http://www.nxp.com/documents/data_sheet/2N7002PW.pdf"/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="NXP"/>
<attribute name="MINIMUM_QUANTITY" value="1"/>
<attribute name="ORDER_NO" value="1829184"/>
<attribute name="PARTS_PER_PACKAGE" value="1"/>
<attribute name="PART_NO" value="2N7002PW"/>
<attribute name="POPULATE" value="ES"/>
<attribute name="PRICE" value="0.039"/>
<attribute name="PRICES" value="1-0:0.039|25-0:0.031|100-0:0.025|250-0:0.021|500--1:0.018|"/>
<attribute name="QUANTITY_MULTIPLIER" value="1"/>
</part>
<part name="GND42" library="Basic" deviceset="GND" device=""/>
<part name="R30" library="Basic" deviceset="R-EU_" device="R0603" value="330R">
<attribute name="AVAILABLE_QUANTITY" value="42126 "/>
<attribute name="DATASHEET" value="http://www.vishay.com/docs/20035/dcrcwe3.pdf"/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="VISHAY DRALORIC"/>
<attribute name="MINIMUM_QUANTITY" value="50"/>
<attribute name="ORDER_NO" value="1469803"/>
<attribute name="PARTS_PER_PACKAGE" value="1"/>
<attribute name="PART_NO" value="CRCW0603330RFKEA"/>
<attribute name="POPULATE" value="ES"/>
<attribute name="PRICE" value="0.018"/>
<attribute name="PRICES" value="50-0:0.02|250-0:0.017|1000-0:0.01|5000-0:0.007|10000--1:0.006|"/>
<attribute name="QUANTITY_MULTIPLIER" value="50"/>
</part>
<part name="R35" library="Basic" deviceset="R-EU_" device="R0603" value="100k">
<attribute name="AVAILABLE_QUANTITY" value="78280"/>
<attribute name="DATASHEET" value="http://www.vishay.com/docs/20035/dcrcwe3.pdf"/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="VISHAY DRALORIC"/>
<attribute name="MINIMUM_QUANTITY" value="50"/>
<attribute name="ORDER_NO" value="1469649"/>
<attribute name="PARTS_PER_PACKAGE" value="1"/>
<attribute name="PART_NO" value="CRCW0603100KFKEA"/>
<attribute name="POPULATE" value="ES"/>
<attribute name="PRICE" value="0.02"/>
<attribute name="PRICES" value="50-0:0.02|250-0:0.017|1000-0:0.01|5000-0:0.008|10000--1:0.007|"/>
<attribute name="QUANTITY_MULTIPLIER" value="50"/>
</part>
<part name="GND43" library="Basic" deviceset="GND" device=""/>
<part name="R32" library="Basic" deviceset="R-EU_" device="R0603" value="10R">
<attribute name="AVAILABLE_QUANTITY" value="6117"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/34922.pdf"/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="VISHAY DRALORIC"/>
<attribute name="MINIMUM_QUANTITY" value="50"/>
<attribute name="ORDER_NO" value="1738878"/>
<attribute name="PARTS_PER_PACKAGE" value="1"/>
<attribute name="PART_NO" value="CRCW060310R0FKEAHP"/>
<attribute name="POPULATE" value="ES"/>
<attribute name="PRICE" value="0.031"/>
<attribute name="PRICES" value="50-0:0.035|250-0:0.029|1000-0:0.021|5000-0:0.014|15000--1:0.009|"/>
<attribute name="QUANTITY_MULTIPLIER" value="50"/>
</part>
<part name="U$4" library="xdevelop-frame" deviceset="XDEVELOP-A3" device="">
<attribute name="CREATE_DATE" value="18.04.14"/>
<attribute name="DRAWING" value=" "/>
<attribute name="EDIT_DATE" value="18.04.14"/>
<attribute name="FILENAME" value=""/>
<attribute name="OBJECT_NO" value=""/>
<attribute name="SUBTITLE" value="High Power"/>
<attribute name="TITLE" value="Certoclav EL2"/>
<attribute name="USERNAME" value="Bernhard Wörndl-A."/>
</part>
<part name="H7" library="certoclav" deviceset="DISPLAY" device="YB2004C" value="YB2004C">
<attribute name="AVAILABLE_QUANTITY" value=""/>
<attribute name="DATASHEET" value=""/>
<attribute name="DISTRIBUTOR" value=""/>
<attribute name="MANUFACTURER" value=""/>
<attribute name="MINIMUM_QUANTITY" value=""/>
<attribute name="ORDER_NO" value=""/>
<attribute name="PARTS_PER_PACKAGE" value=""/>
<attribute name="PART_NO" value=""/>
<attribute name="POPULATE" value="ES"/>
<attribute name="PRICE" value="6.00"/>
<attribute name="PRICES" value=""/>
<attribute name="QUANTITY_MULTIPLIER" value=""/>
</part>
<part name="GND44" library="Basic" deviceset="GND" device=""/>
<part name="GND45" library="Basic" deviceset="GND" device=""/>
<part name="R33" library="Basic" deviceset="R-EU_" device="R0603" value="100k">
<attribute name="AVAILABLE_QUANTITY" value="78280"/>
<attribute name="DATASHEET" value="http://www.vishay.com/docs/20035/dcrcwe3.pdf"/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="VISHAY DRALORIC"/>
<attribute name="MINIMUM_QUANTITY" value="50"/>
<attribute name="ORDER_NO" value="1469649"/>
<attribute name="PARTS_PER_PACKAGE" value="1"/>
<attribute name="PART_NO" value="CRCW0603100KFKEA"/>
<attribute name="POPULATE" value="ES"/>
<attribute name="PRICE" value="0.02"/>
<attribute name="PRICES" value="50-0:0.02|250-0:0.017|1000-0:0.01|5000-0:0.008|10000--1:0.007|"/>
<attribute name="QUANTITY_MULTIPLIER" value="50"/>
</part>
<part name="R36" library="Basic" deviceset="R-EU_" device="R0603" value="1k">
<attribute name="AVAILABLE_QUANTITY" value="15000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1788326.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2130855"/>
<attribute name="PARTS_PER_PACKAGE" value="5000"/>
<attribute name="PART_NO" value="MC0063W060311K"/>
<attribute name="POPULATE" value="ES"/>
<attribute name="PRICE" value="0.0003"/>
<attribute name="PRICES" value="5000--1:0.0003|"/>
<attribute name="QUANTITY_MULTIPLIER" value="5000"/>
</part>
<part name="GND46" library="Basic" deviceset="GND" device=""/>
<part name="P+10" library="Basic" deviceset="+5V" device=""/>
<part name="P+11" library="Basic" deviceset="+5V" device=""/>
<part name="C3" library="Basic" deviceset="C-EU" device="C0603" value="100n">
<attribute name="AVAILABLE_QUANTITY" value="3188000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1825492.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2310603"/>
<attribute name="PARTS_PER_PACKAGE" value="4000"/>
<attribute name="PART_NO" value="MC0603B104K500CT"/>
<attribute name="PRICE" value="0.0024"/>
<attribute name="PRICES" value="4000-0:0.0024|20000-0:0.0021|40000--1:0.0019|"/>
<attribute name="QUANTITY_MULTIPLIER" value="4000"/>
</part>
<part name="C4" library="Basic" deviceset="C-EU" device="C0603" value="100n">
<attribute name="AVAILABLE_QUANTITY" value="3188000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1825492.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2310603"/>
<attribute name="PARTS_PER_PACKAGE" value="4000"/>
<attribute name="PART_NO" value="MC0603B104K500CT"/>
<attribute name="PRICE" value="0.0024"/>
<attribute name="PRICES" value="4000-0:0.0024|20000-0:0.0021|40000--1:0.0019|"/>
<attribute name="QUANTITY_MULTIPLIER" value="4000"/>
</part>
<part name="D1" library="Androdpod" deviceset="LD1117" device="-S" technology="-33CTR" value="LD1117S33CTR">
<attribute name="AVAILABLE_QUANTITY" value="11578"/>
<attribute name="DATASHEET" value="http://www.st.com/stonline/books/pdf/docs/2572.pdf "/>
<attribute name="MINIMUM_QUANTITY" value="1"/>
<attribute name="ORDER_NO" value="1467779"/>
<attribute name="PARTS_PER_PACKAGE" value="1"/>
<attribute name="PART_NO" value="LD1117S33CTR"/>
<attribute name="PRICE" value="0.095"/>
<attribute name="PRICES" value="1--1:0.867|"/>
<attribute name="QUANTITY_MULTIPLIER" value="1"/>
</part>
<part name="C5" library="Basic" deviceset="C-EU" device="C1210" value="47u/6V3">
<attribute name="AVAILABLE_QUANTITY" value="21758"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1723208.pdf"/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="10"/>
<attribute name="ORDER_NO" value="1759367"/>
<attribute name="PARTS_PER_PACKAGE" value="10"/>
<attribute name="PART_NO" value="MCCA000491"/>
<attribute name="POPULATE" value=""/>
<attribute name="PRICE" value="0.188"/>
<attribute name="PRICES" value="10-0:0.317|250-0:0.24|1000-0:0.179|3000--1:0.124|"/>
<attribute name="QUANTITY_MULTIPLIER" value="10"/>
</part>
<part name="P+14" library="Basic" deviceset="+3.3V" device=""/>
<part name="GND48" library="Basic" deviceset="GND" device=""/>
<part name="GND53" library="Basic" deviceset="GND" device=""/>
<part name="GND54" library="Basic" deviceset="GND" device=""/>
<part name="GND59" library="Basic" deviceset="GND" device=""/>
<part name="C1" library="Basic" deviceset="C-EU" device="C1210" value="47u/16V">
<attribute name="AVAILABLE_QUANTITY" value=""/>
<attribute name="DATASHEET" value=""/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MURATA"/>
<attribute name="MINIMUM_QUANTITY" value=""/>
<attribute name="ORDER_NO" value="1907527"/>
<attribute name="PARTS_PER_PACKAGE" value=""/>
<attribute name="PART_NO" value="GRM32EC81C476KE15L"/>
<attribute name="POPULATE" value=""/>
<attribute name="PRICE" value="0.2561"/>
<attribute name="PRICES" value=""/>
<attribute name="QUANTITY_MULTIPLIER" value=""/>
</part>
<part name="GND60" library="Basic" deviceset="GND" device=""/>
<part name="H2" library="Basic" deviceset="LED" device="5MM" value="Green">
<attribute name="AVAILABLE_QUANTITY" value="7979"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1718627.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="KINGBRIGHT"/>
<attribute name="MINIMUM_QUANTITY" value="1"/>
<attribute name="ORDER_NO" value="2335726"/>
<attribute name="PARTS_PER_PACKAGE" value="1"/>
<attribute name="PART_NO" value="L-1503GD"/>
<attribute name="POPULATE" value="Minimal"/>
<attribute name="PRICE" value="0.0508"/>
<attribute name="PRICES" value="1-0:0.093|10-0:0.0837|50-0:0.0775|100-0:0.0698|250-0:0.0628|500-0:0.0565|900-0:0.0508|3000--1:0.0458|"/>
<attribute name="QUANTITY_MULTIPLIER" value="1"/>
</part>
<part name="H3" library="Basic" deviceset="LED" device="5MM" value="Green">
<attribute name="AVAILABLE_QUANTITY" value="7979"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1718627.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="KINGBRIGHT"/>
<attribute name="MINIMUM_QUANTITY" value="1"/>
<attribute name="ORDER_NO" value="2335726"/>
<attribute name="PARTS_PER_PACKAGE" value="1"/>
<attribute name="PART_NO" value="L-1503GD"/>
<attribute name="POPULATE" value="Minimal"/>
<attribute name="PRICE" value="0.0508"/>
<attribute name="PRICES" value="1-0:0.093|10-0:0.0837|50-0:0.0775|100-0:0.0698|250-0:0.0628|500-0:0.0565|900-0:0.0508|3000--1:0.0458|"/>
<attribute name="QUANTITY_MULTIPLIER" value="1"/>
</part>
<part name="H4" library="Basic" deviceset="LED" device="5MM" value="Green">
<attribute name="AVAILABLE_QUANTITY" value="7979"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1718627.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="KINGBRIGHT"/>
<attribute name="MINIMUM_QUANTITY" value="1"/>
<attribute name="ORDER_NO" value="2335726"/>
<attribute name="PARTS_PER_PACKAGE" value="1"/>
<attribute name="PART_NO" value="L-1503GD"/>
<attribute name="POPULATE" value="Minimal"/>
<attribute name="PRICE" value="0.0508"/>
<attribute name="PRICES" value="1-0:0.093|10-0:0.0837|50-0:0.0775|100-0:0.0698|250-0:0.0628|500-0:0.0565|900-0:0.0508|3000--1:0.0458|"/>
<attribute name="QUANTITY_MULTIPLIER" value="1"/>
</part>
<part name="H5" library="Basic" deviceset="LED" device="5MM" value="Green">
<attribute name="AVAILABLE_QUANTITY" value="7979"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1718627.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="KINGBRIGHT"/>
<attribute name="MINIMUM_QUANTITY" value="1"/>
<attribute name="ORDER_NO" value="2335726"/>
<attribute name="PARTS_PER_PACKAGE" value="1"/>
<attribute name="PART_NO" value="L-1503GD"/>
<attribute name="POPULATE" value="Minimal"/>
<attribute name="PRICE" value="0.0508"/>
<attribute name="PRICES" value="1-0:0.093|10-0:0.0837|50-0:0.0775|100-0:0.0698|250-0:0.0628|500-0:0.0565|900-0:0.0508|3000--1:0.0458|"/>
<attribute name="QUANTITY_MULTIPLIER" value="1"/>
</part>
<part name="R18" library="Basic" deviceset="R-EU_" device="R0603" value="100R">
<attribute name="AVAILABLE_QUANTITY" value="590000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1788326.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2130776"/>
<attribute name="PARTS_PER_PACKAGE" value="5000"/>
<attribute name="PART_NO" value="MC0063W06031100R"/>
<attribute name="POPULATE" value="Minimal"/>
<attribute name="PRICE" value="0.0009"/>
<attribute name="PRICES" value="5000-0:0.0009|25000-0:0.0008|50000--1:0.0006|"/>
<attribute name="QUANTITY_MULTIPLIER" value="5000"/>
</part>
<part name="GND61" library="Basic" deviceset="GND" device=""/>
<part name="R19" library="Basic" deviceset="R-EU_" device="R0603" value="100R">
<attribute name="AVAILABLE_QUANTITY" value="590000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1788326.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2130776"/>
<attribute name="PARTS_PER_PACKAGE" value="5000"/>
<attribute name="PART_NO" value="MC0063W06031100R"/>
<attribute name="POPULATE" value="Minimal"/>
<attribute name="PRICE" value="0.0009"/>
<attribute name="PRICES" value="5000-0:0.0009|25000-0:0.0008|50000--1:0.0006|"/>
<attribute name="QUANTITY_MULTIPLIER" value="5000"/>
</part>
<part name="R20" library="Basic" deviceset="R-EU_" device="R0603" value="100R">
<attribute name="AVAILABLE_QUANTITY" value="590000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1788326.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2130776"/>
<attribute name="PARTS_PER_PACKAGE" value="5000"/>
<attribute name="PART_NO" value="MC0063W06031100R"/>
<attribute name="POPULATE" value="Minimal"/>
<attribute name="PRICE" value="0.0009"/>
<attribute name="PRICES" value="5000-0:0.0009|25000-0:0.0008|50000--1:0.0006|"/>
<attribute name="QUANTITY_MULTIPLIER" value="5000"/>
</part>
<part name="R21" library="Basic" deviceset="R-EU_" device="R0603" value="100R">
<attribute name="AVAILABLE_QUANTITY" value="590000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1788326.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2130776"/>
<attribute name="PARTS_PER_PACKAGE" value="5000"/>
<attribute name="PART_NO" value="MC0063W06031100R"/>
<attribute name="POPULATE" value="Minimal"/>
<attribute name="PRICE" value="0.0009"/>
<attribute name="PRICES" value="5000-0:0.0009|25000-0:0.0008|50000--1:0.0006|"/>
<attribute name="QUANTITY_MULTIPLIER" value="5000"/>
</part>
<part name="GND62" library="Basic" deviceset="GND" device=""/>
<part name="GND63" library="Basic" deviceset="GND" device=""/>
<part name="GND64" library="Basic" deviceset="GND" device=""/>
<part name="H6" library="Basic" deviceset="LED" device="5MM" value="Green">
<attribute name="AVAILABLE_QUANTITY" value="7979"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1718627.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="KINGBRIGHT"/>
<attribute name="MINIMUM_QUANTITY" value="1"/>
<attribute name="ORDER_NO" value="2335726"/>
<attribute name="PARTS_PER_PACKAGE" value="1"/>
<attribute name="PART_NO" value="L-1503GD"/>
<attribute name="POPULATE" value="Minimal"/>
<attribute name="PRICE" value="0.0508"/>
<attribute name="PRICES" value="1-0:0.093|10-0:0.0837|50-0:0.0775|100-0:0.0698|250-0:0.0628|500-0:0.0565|900-0:0.0508|3000--1:0.0458|"/>
<attribute name="QUANTITY_MULTIPLIER" value="1"/>
</part>
<part name="R22" library="Basic" deviceset="R-EU_" device="R0603" value="100R">
<attribute name="AVAILABLE_QUANTITY" value="590000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1788326.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2130776"/>
<attribute name="PARTS_PER_PACKAGE" value="5000"/>
<attribute name="PART_NO" value="MC0063W06031100R"/>
<attribute name="POPULATE" value="Minimal"/>
<attribute name="PRICE" value="0.0009"/>
<attribute name="PRICES" value="5000-0:0.0009|25000-0:0.0008|50000--1:0.0006|"/>
<attribute name="QUANTITY_MULTIPLIER" value="5000"/>
</part>
<part name="GND65" library="Basic" deviceset="GND" device=""/>
<part name="X1" library="certoclav" deviceset="MA05-2" device="-LOCKED" value="Power">
<attribute name="AVAILABLE_QUANTITY" value="1"/>
<attribute name="DATASHEET" value=""/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="WURTH ELEKTRONIK"/>
<attribute name="MINIMUM_QUANTITY" value="1"/>
<attribute name="ORDER_NO" value="1642019"/>
<attribute name="PARTS_PER_PACKAGE" value="1"/>
<attribute name="PART_NO" value="61201021621"/>
<attribute name="POPULATE" value=""/>
<attribute name="PRICE" value="0.1"/>
<attribute name="PRICES" value="0.0818"/>
<attribute name="QUANTITY_MULTIPLIER" value="http://www.farnell.com/datasheets/1520732.pdf "/>
</part>
<part name="GND67" library="Basic" deviceset="GND" device=""/>
<part name="GND68" library="Basic" deviceset="GND" device=""/>
<part name="L2" library="Basic" deviceset="L-EU" device="0603" value="BLM18AG601SN1D">
<attribute name="AVAILABLE_QUANTITY" value="13956"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/356366.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MURATA"/>
<attribute name="MINIMUM_QUANTITY" value="5"/>
<attribute name="ORDER_NO" value="1515679"/>
<attribute name="PARTS_PER_PACKAGE" value="5"/>
<attribute name="PART_NO" value="BLM18AG601SN1D"/>
<attribute name="PRICE" value="0.046"/>
<attribute name="PRICES" value="5-0:0.046|50-0:0.045|100-0:0.042|250-0:0.029|500--1:0.027|"/>
<attribute name="QUANTITY_MULTIPLIER" value="5"/>
</part>
<part name="C6" library="Basic" deviceset="C-EU" device="C0603" value="100n">
<attribute name="AVAILABLE_QUANTITY" value="3188000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1825492.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2310603"/>
<attribute name="PARTS_PER_PACKAGE" value="4000"/>
<attribute name="PART_NO" value="MC0603B104K500CT"/>
<attribute name="PRICE" value="0.0024"/>
<attribute name="PRICES" value="4000-0:0.0024|20000-0:0.0021|40000--1:0.0019|"/>
<attribute name="QUANTITY_MULTIPLIER" value="4000"/>
</part>
<part name="GND69" library="Basic" deviceset="GND" device=""/>
<part name="L3" library="Basic" deviceset="L-EU" device="0603" value="BLM18AG601SN1D">
<attribute name="AVAILABLE_QUANTITY" value="13956"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/356366.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MURATA"/>
<attribute name="MINIMUM_QUANTITY" value="5"/>
<attribute name="ORDER_NO" value="1515679"/>
<attribute name="PARTS_PER_PACKAGE" value="5"/>
<attribute name="PART_NO" value="BLM18AG601SN1D"/>
<attribute name="PRICE" value="0.046"/>
<attribute name="PRICES" value="5-0:0.046|50-0:0.045|100-0:0.042|250-0:0.029|500--1:0.027|"/>
<attribute name="QUANTITY_MULTIPLIER" value="5"/>
</part>
<part name="C7" library="Basic" deviceset="C-EU" device="C0603" value="100n">
<attribute name="AVAILABLE_QUANTITY" value="3188000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1825492.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2310603"/>
<attribute name="PARTS_PER_PACKAGE" value="4000"/>
<attribute name="PART_NO" value="MC0603B104K500CT"/>
<attribute name="PRICE" value="0.0024"/>
<attribute name="PRICES" value="4000-0:0.0024|20000-0:0.0021|40000--1:0.0019|"/>
<attribute name="QUANTITY_MULTIPLIER" value="4000"/>
</part>
<part name="J4" library="fiducial" deviceset="POSITION-MARK" device="-ROUND"/>
<part name="J3" library="fiducial" deviceset="POSITION-MARK" device="-ROUND"/>
<part name="J2" library="fiducial" deviceset="POSITION-MARK" device="-ROUND"/>
<part name="J1" library="fiducial" deviceset="POSITION-MARK" device="-ROUND"/>
<part name="S1" library="certoclav" deviceset="SWITCH-MOMENTARY-2" device="PTH" value="NORTH">
<attribute name="AVAILABLE_QUANTITY" value=""/>
<attribute name="DATASHEET" value=""/>
<attribute name="DISTRIBUTOR" value="we-online.de"/>
<attribute name="MANUFACTURER" value="WUERTH ELECTRONIK"/>
<attribute name="MINIMUM_QUANTITY" value=""/>
<attribute name="ORDER_NO" value="430186130716"/>
<attribute name="PARTS_PER_PACKAGE" value=""/>
<attribute name="PART_NO" value="430186130716"/>
<attribute name="POPULATE" value="ES"/>
<attribute name="PRICE" value="0.125"/>
<attribute name="PRICES" value=""/>
<attribute name="QUANTITY_MULTIPLIER" value=""/>
</part>
<part name="S2" library="certoclav" deviceset="SWITCH-MOMENTARY-2" device="PTH" value="SOUTH">
<attribute name="AVAILABLE_QUANTITY" value=""/>
<attribute name="DATASHEET" value=""/>
<attribute name="DISTRIBUTOR" value="we-online.de"/>
<attribute name="MANUFACTURER" value="WUERTH ELECTRONIK"/>
<attribute name="MINIMUM_QUANTITY" value=""/>
<attribute name="ORDER_NO" value="430186130716"/>
<attribute name="PARTS_PER_PACKAGE" value=""/>
<attribute name="PART_NO" value="430186130716"/>
<attribute name="POPULATE" value="ES"/>
<attribute name="PRICE" value="0.125"/>
<attribute name="PRICES" value=""/>
<attribute name="QUANTITY_MULTIPLIER" value=""/>
</part>
<part name="S3" library="certoclav" deviceset="SWITCH-MOMENTARY-2" device="PTH" value="EAST">
<attribute name="AVAILABLE_QUANTITY" value=""/>
<attribute name="DATASHEET" value=""/>
<attribute name="DISTRIBUTOR" value="we-online.de"/>
<attribute name="MANUFACTURER" value="WUERTH ELECTRONIK"/>
<attribute name="MINIMUM_QUANTITY" value=""/>
<attribute name="ORDER_NO" value="430186130716"/>
<attribute name="PARTS_PER_PACKAGE" value=""/>
<attribute name="PART_NO" value="430186130716"/>
<attribute name="POPULATE" value="DNP"/>
<attribute name="PRICE" value="0.125"/>
<attribute name="PRICES" value=""/>
<attribute name="QUANTITY_MULTIPLIER" value=""/>
</part>
<part name="S4" library="certoclav" deviceset="SWITCH-MOMENTARY-2" device="PTH" value="WEST">
<attribute name="AVAILABLE_QUANTITY" value=""/>
<attribute name="DATASHEET" value=""/>
<attribute name="DISTRIBUTOR" value="we-online.de"/>
<attribute name="MANUFACTURER" value="WUERTH ELECTRONIK"/>
<attribute name="MINIMUM_QUANTITY" value=""/>
<attribute name="ORDER_NO" value="430186130716"/>
<attribute name="PARTS_PER_PACKAGE" value=""/>
<attribute name="PART_NO" value="430186130716"/>
<attribute name="POPULATE" value="DNP"/>
<attribute name="PRICE" value="0.125"/>
<attribute name="PRICES" value=""/>
<attribute name="QUANTITY_MULTIPLIER" value=""/>
</part>
<part name="S5" library="certoclav" deviceset="SWITCH-MOMENTARY-2" device="PTH" value="CENTER">
<attribute name="AVAILABLE_QUANTITY" value=""/>
<attribute name="DATASHEET" value=""/>
<attribute name="DISTRIBUTOR" value="we-online.de"/>
<attribute name="MANUFACTURER" value="WUERTH ELECTRONIK"/>
<attribute name="MINIMUM_QUANTITY" value=""/>
<attribute name="ORDER_NO" value="430186130716"/>
<attribute name="PARTS_PER_PACKAGE" value=""/>
<attribute name="PART_NO" value="430186130716"/>
<attribute name="POPULATE" value="ES"/>
<attribute name="PRICE" value="0.125"/>
<attribute name="PRICES" value=""/>
<attribute name="QUANTITY_MULTIPLIER" value=""/>
</part>
<part name="S6" library="certoclav" deviceset="SWITCH-MOMENTARY-2" device="PTH" value="START">
<attribute name="AVAILABLE_QUANTITY" value=""/>
<attribute name="DATASHEET" value=""/>
<attribute name="DISTRIBUTOR" value="we-online.de"/>
<attribute name="MANUFACTURER" value="WUERTH ELECTRONIK"/>
<attribute name="MINIMUM_QUANTITY" value=""/>
<attribute name="ORDER_NO" value="430186130716"/>
<attribute name="PARTS_PER_PACKAGE" value=""/>
<attribute name="PART_NO" value="430186130716"/>
<attribute name="POPULATE" value="Minimal"/>
<attribute name="PRICE" value="0.125"/>
<attribute name="PRICES" value=""/>
<attribute name="QUANTITY_MULTIPLIER" value=""/>
</part>
<part name="GND70" library="Basic" deviceset="GND" device=""/>
<part name="GND71" library="Basic" deviceset="GND" device=""/>
<part name="GND72" library="Basic" deviceset="GND" device=""/>
<part name="GND73" library="Basic" deviceset="GND" device=""/>
<part name="GND74" library="Basic" deviceset="GND" device=""/>
<part name="GND75" library="Basic" deviceset="GND" device=""/>
<part name="X2" library="certoclav" deviceset="FTDI-TTL232R" device="" value="DEBUG">
<attribute name="AVAILABLE_QUANTITY" value=""/>
<attribute name="DATASHEET" value=""/>
<attribute name="DISTRIBUTOR" value=""/>
<attribute name="MANUFACTURER" value=""/>
<attribute name="MINIMUM_QUANTITY" value=""/>
<attribute name="ORDER_NO" value=""/>
<attribute name="PARTS_PER_PACKAGE" value=""/>
<attribute name="PART_NO" value=""/>
<attribute name="POPULATE" value="DNP"/>
<attribute name="PRICE" value=""/>
<attribute name="PRICES" value=""/>
<attribute name="QUANTITY_MULTIPLIER" value=""/>
</part>
<part name="GND76" library="Basic" deviceset="GND" device=""/>
<part name="D3" library="certoclav" deviceset="MAX232ECWE" device="" value="MAX232DG4">
<attribute name="AVAILABLE_QUANTITY" value=""/>
<attribute name="DATASHEET" value=""/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="TEXAS INSTRUMENTS"/>
<attribute name="MINIMUM_QUANTITY" value=""/>
<attribute name="ORDER_NO" value="9589775"/>
<attribute name="PARTS_PER_PACKAGE" value=""/>
<attribute name="PART_NO" value="MAX232DG4"/>
<attribute name="POPULATE" value=""/>
<attribute name="PRICE" value="0.383"/>
<attribute name="PRICES" value=""/>
<attribute name="QUANTITY_MULTIPLIER" value=""/>
</part>
<part name="C25" library="Basic" deviceset="C-EU" device="C0603" value="1u">
<attribute name="AVAILABLE_QUANTITY" value="28000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1825491.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2310567"/>
<attribute name="PARTS_PER_PACKAGE" value="4000"/>
<attribute name="PART_NO" value="MC0603X105K250CT"/>
<attribute name="POPULATE" value=""/>
<attribute name="PRICE" value="0.0067"/>
<attribute name="PRICES" value="4000-0:0.0076|20000-0:0.0066|40000--1:0.0058|"/>
<attribute name="QUANTITY_MULTIPLIER" value="4000"/>
</part>
<part name="C24" library="Basic" deviceset="C-EU" device="C0603" value="1u">
<attribute name="AVAILABLE_QUANTITY" value="28000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1825491.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2310567"/>
<attribute name="PARTS_PER_PACKAGE" value="4000"/>
<attribute name="PART_NO" value="MC0603X105K250CT"/>
<attribute name="POPULATE" value=""/>
<attribute name="PRICE" value="0.0067"/>
<attribute name="PRICES" value="4000-0:0.0076|20000-0:0.0066|40000--1:0.0058|"/>
<attribute name="QUANTITY_MULTIPLIER" value="4000"/>
</part>
<part name="C28" library="Basic" deviceset="C-EU" device="C0603" value="1u">
<attribute name="AVAILABLE_QUANTITY" value="28000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1825491.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2310567"/>
<attribute name="PARTS_PER_PACKAGE" value="4000"/>
<attribute name="PART_NO" value="MC0603X105K250CT"/>
<attribute name="POPULATE" value=""/>
<attribute name="PRICE" value="0.0067"/>
<attribute name="PRICES" value="4000-0:0.0076|20000-0:0.0066|40000--1:0.0058|"/>
<attribute name="QUANTITY_MULTIPLIER" value="4000"/>
</part>
<part name="C29" library="Basic" deviceset="C-EU" device="C0603" value="1u">
<attribute name="AVAILABLE_QUANTITY" value="28000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1825491.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2310567"/>
<attribute name="PARTS_PER_PACKAGE" value="4000"/>
<attribute name="PART_NO" value="MC0603X105K250CT"/>
<attribute name="POPULATE" value=""/>
<attribute name="PRICE" value="0.0067"/>
<attribute name="PRICES" value="4000-0:0.0076|20000-0:0.0066|40000--1:0.0058|"/>
<attribute name="QUANTITY_MULTIPLIER" value="4000"/>
</part>
<part name="GND77" library="Basic" deviceset="GND" device=""/>
<part name="GND78" library="Basic" deviceset="GND" device=""/>
<part name="C26" library="Basic" deviceset="C-EU" device="C0603" value="100n">
<attribute name="AVAILABLE_QUANTITY" value="3188000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1825492.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2310603"/>
<attribute name="PARTS_PER_PACKAGE" value="4000"/>
<attribute name="PART_NO" value="MC0603B104K500CT"/>
<attribute name="PRICE" value="0.0024"/>
<attribute name="PRICES" value="4000-0:0.0024|20000-0:0.0021|40000--1:0.0019|"/>
<attribute name="QUANTITY_MULTIPLIER" value="4000"/>
</part>
<part name="C27" library="Basic" deviceset="C-EU" device="C0603" value="1u">
<attribute name="AVAILABLE_QUANTITY" value="28000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1825491.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2310567"/>
<attribute name="PARTS_PER_PACKAGE" value="4000"/>
<attribute name="PART_NO" value="MC0603X105K250CT"/>
<attribute name="POPULATE" value=""/>
<attribute name="PRICE" value="0.0067"/>
<attribute name="PRICES" value="4000-0:0.0076|20000-0:0.0066|40000--1:0.0058|"/>
<attribute name="QUANTITY_MULTIPLIER" value="4000"/>
</part>
<part name="P+15" library="Basic" deviceset="+5V" device=""/>
<part name="GND79" library="Basic" deviceset="GND" device=""/>
<part name="X6" library="certoclav" deviceset="M09" device="" value="Printer">
<attribute name="AVAILABLE_QUANTITY" value=""/>
<attribute name="DATASHEET" value=""/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="WURTH ELEKTRONIK"/>
<attribute name="MINIMUM_QUANTITY" value=""/>
<attribute name="ORDER_NO" value="1841196"/>
<attribute name="PARTS_PER_PACKAGE" value=""/>
<attribute name="PART_NO" value="618009231221"/>
<attribute name="POPULATE" value=""/>
<attribute name="PRICE" value="0.375"/>
<attribute name="PRICES" value=""/>
<attribute name="QUANTITY_MULTIPLIER" value=""/>
</part>
<part name="GND80" library="Basic" deviceset="GND" device=""/>
<part name="R17" library="Basic" deviceset="R-EU_" device="R0603" value="0R">
<attribute name="AVAILABLE_QUANTITY" value="224003"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1788326.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MC0603WG00000T5E-TR"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="1692453"/>
<attribute name="PARTS_PER_PACKAGE" value="5000"/>
<attribute name="PART_NO" value="MULTICOMP"/>
<attribute name="PRICE" value="0.001"/>
<attribute name="PRICES" value="5000--1:0.001|"/>
<attribute name="QUANTITY_MULTIPLIER" value="5000"/>
</part>
<part name="R15" library="Basic" deviceset="R-EU_" device="R0603" value="0R">
<attribute name="AVAILABLE_QUANTITY" value="224003"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1788326.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MC0603WG00000T5E-TR"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="1692453"/>
<attribute name="PARTS_PER_PACKAGE" value="5000"/>
<attribute name="PART_NO" value="MULTICOMP"/>
<attribute name="POPULATE" value=""/>
<attribute name="PRICE" value="0.001"/>
<attribute name="PRICES" value="5000--1:0.001|"/>
<attribute name="QUANTITY_MULTIPLIER" value="5000"/>
</part>
<part name="X5" library="certoclav" deviceset="MA03-1" device="" value="Printer">
<attribute name="AVAILABLE_QUANTITY" value=""/>
<attribute name="DATASHEET" value=""/>
<attribute name="DISTRIBUTOR" value=""/>
<attribute name="MANUFACTURER" value=""/>
<attribute name="MINIMUM_QUANTITY" value=""/>
<attribute name="ORDER_NO" value=""/>
<attribute name="PARTS_PER_PACKAGE" value=""/>
<attribute name="PART_NO" value=""/>
<attribute name="POPULATE" value="DNP"/>
<attribute name="PRICE" value=""/>
<attribute name="PRICES" value=""/>
<attribute name="QUANTITY_MULTIPLIER" value=""/>
</part>
<part name="GND81" library="Basic" deviceset="GND" device=""/>
<part name="R24" library="Basic" deviceset="R-EU_" device="R0603" value="10k">
<attribute name="AVAILABLE_QUANTITY" value="1042000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1697337.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="VISHAY DRALORIC"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2122493"/>
<attribute name="PARTS_PER_PACKAGE" value="5000"/>
<attribute name="PART_NO" value="CRCW060310K0FKEA"/>
<attribute name="PRICE" value="0.002"/>
<attribute name="PRICES" value="5000-0:0.002|25000-0:0.0018|50000--1:0.0015|"/>
<attribute name="QUANTITY_MULTIPLIER" value="5000"/>
</part>
<part name="R16" library="Basic" deviceset="R-EU_" device="R0603" value="10k">
<attribute name="AVAILABLE_QUANTITY" value="1042000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1697337.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="VISHAY DRALORIC"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2122493"/>
<attribute name="PARTS_PER_PACKAGE" value="5000"/>
<attribute name="PART_NO" value="CRCW060310K0FKEA"/>
<attribute name="POPULATE" value=""/>
<attribute name="PRICE" value="0.002"/>
<attribute name="PRICES" value="5000-0:0.002|25000-0:0.0018|50000--1:0.0015|"/>
<attribute name="QUANTITY_MULTIPLIER" value="5000"/>
</part>
<part name="R26" library="Basic" deviceset="R-EU_" device="R0603" value="18k">
<attribute name="AVAILABLE_QUANTITY" value="25871"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1788326.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="1"/>
<attribute name="ORDER_NO" value="9330720"/>
<attribute name="PARTS_PER_PACKAGE" value="50"/>
<attribute name="PART_NO" value="MC0063W0603118K"/>
<attribute name="PRICE" value="0.005"/>
<attribute name="PRICES" value="50-0:0.005|250-0:0.004|1000--1:0.003|"/>
<attribute name="QUANTITY_MULTIPLIER" value="50"/>
</part>
<part name="R25" library="Basic" deviceset="R-EU_" device="R0603" value="18k">
<attribute name="AVAILABLE_QUANTITY" value="25871"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1788326.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="1"/>
<attribute name="ORDER_NO" value="9330720"/>
<attribute name="PARTS_PER_PACKAGE" value="50"/>
<attribute name="PART_NO" value="MC0063W0603118K"/>
<attribute name="PRICE" value="0.005"/>
<attribute name="PRICES" value="50-0:0.005|250-0:0.004|1000--1:0.003|"/>
<attribute name="QUANTITY_MULTIPLIER" value="50"/>
</part>
<part name="GND82" library="Basic" deviceset="GND" device=""/>
<part name="GND83" library="Basic" deviceset="GND" device=""/>
<part name="GND17" library="Basic" deviceset="GND" device=""/>
<part name="C30" library="Basic" deviceset="C-EU" device="C0603" value="100n">
<attribute name="AVAILABLE_QUANTITY" value="3188000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1825492.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2310603"/>
<attribute name="PARTS_PER_PACKAGE" value="4000"/>
<attribute name="PART_NO" value="MC0603B104K500CT"/>
<attribute name="POPULATE" value="ES"/>
<attribute name="PRICE" value="0.0024"/>
<attribute name="PRICES" value="4000-0:0.0024|20000-0:0.0021|40000--1:0.0019|"/>
<attribute name="QUANTITY_MULTIPLIER" value="4000"/>
</part>
<part name="GND18" library="Basic" deviceset="GND" device=""/>
<part name="GND19" library="Basic" deviceset="GND" device=""/>
<part name="R29" library="Basic" deviceset="R-EU_" device="R0603" value="1M">
<attribute name="AVAILABLE_QUANTITY" value="1555000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1788326.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2128826"/>
<attribute name="PARTS_PER_PACKAGE" value="5000"/>
<attribute name="PART_NO" value="MC0063W060311M"/>
<attribute name="POPULATE" value="ES"/>
<attribute name="PRICE" value="0.0005"/>
<attribute name="PRICES" value="5000--1:0.0005|"/>
<attribute name="QUANTITY_MULTIPLIER" value="5000"/>
</part>
<part name="V11" library="certoclav" deviceset="TVS-DIODE" device="" value="PESD5V0X1BCAL">
<attribute name="AVAILABLE_QUANTITY" value="5894"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1600347.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="NXP"/>
<attribute name="MINIMUM_QUANTITY" value="1"/>
<attribute name="ORDER_NO" value="2114788"/>
<attribute name="PARTS_PER_PACKAGE" value="1"/>
<attribute name="PART_NO" value="PESD5V0X1BCAL"/>
<attribute name="POPULATE" value="ES"/>
<attribute name="PRICE" value="0.11"/>
<attribute name="PRICES" value="1-0:0.208|25-0:0.156|100-0:0.138|250-0:0.125|500-0:0.11|2000-0:0.099|4000-0:0.0891|20000--1:0.0802|"/>
<attribute name="QUANTITY_MULTIPLIER" value="1"/>
</part>
<part name="V10" library="certoclav" deviceset="TVS-DIODE" device="" value="PESD5V0X1BCAL">
<attribute name="AVAILABLE_QUANTITY" value="5894"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1600347.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="NXP"/>
<attribute name="MINIMUM_QUANTITY" value="1"/>
<attribute name="ORDER_NO" value="2114788"/>
<attribute name="PARTS_PER_PACKAGE" value="1"/>
<attribute name="PART_NO" value="PESD5V0X1BCAL"/>
<attribute name="POPULATE" value="ES"/>
<attribute name="PRICE" value="0.11"/>
<attribute name="PRICES" value="1-0:0.208|25-0:0.156|100-0:0.138|250-0:0.125|500-0:0.11|2000-0:0.099|4000-0:0.0891|20000--1:0.0802|"/>
<attribute name="QUANTITY_MULTIPLIER" value="1"/>
</part>
<part name="GND27" library="Basic" deviceset="GND" device=""/>
<part name="GND28" library="Basic" deviceset="GND" device=""/>
<part name="V12" library="certoclav" deviceset="TVS-DIODE" device="" value="PESD5V0X1BCAL">
<attribute name="AVAILABLE_QUANTITY" value="5894"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1600347.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="NXP"/>
<attribute name="MINIMUM_QUANTITY" value="1"/>
<attribute name="ORDER_NO" value="2114788"/>
<attribute name="PARTS_PER_PACKAGE" value="1"/>
<attribute name="PART_NO" value="PESD5V0X1BCAL"/>
<attribute name="POPULATE" value="ES"/>
<attribute name="PRICE" value="0.11"/>
<attribute name="PRICES" value="1-0:0.208|25-0:0.156|100-0:0.138|250-0:0.125|500-0:0.11|2000-0:0.099|4000-0:0.0891|20000--1:0.0802|"/>
<attribute name="QUANTITY_MULTIPLIER" value="1"/>
</part>
<part name="GND29" library="Basic" deviceset="GND" device=""/>
<part name="R27" library="Basic" deviceset="R-EU_" device="R0603" value="10k">
<attribute name="AVAILABLE_QUANTITY" value="1042000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1697337.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="VISHAY DRALORIC"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2122493"/>
<attribute name="PARTS_PER_PACKAGE" value="5000"/>
<attribute name="PART_NO" value="CRCW060310K0FKEA"/>
<attribute name="POPULATE" value="ES"/>
<attribute name="PRICE" value="0.002"/>
<attribute name="PRICES" value="5000-0:0.002|25000-0:0.0018|50000--1:0.0015|"/>
<attribute name="QUANTITY_MULTIPLIER" value="5000"/>
</part>
<part name="GND30" library="Basic" deviceset="GND" device=""/>
<part name="R28" library="Basic" deviceset="R-EU_" device="R0603" value="18k">
<attribute name="AVAILABLE_QUANTITY" value="25871"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1788326.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="1"/>
<attribute name="ORDER_NO" value="9330720"/>
<attribute name="PARTS_PER_PACKAGE" value="50"/>
<attribute name="PART_NO" value="MC0063W0603118K"/>
<attribute name="POPULATE" value="ES"/>
<attribute name="PRICE" value="0.005"/>
<attribute name="PRICES" value="50-0:0.005|250-0:0.004|1000--1:0.003|"/>
<attribute name="QUANTITY_MULTIPLIER" value="50"/>
</part>
<part name="X7" library="certoclav" deviceset="USB-B" device="WE61400416121" value="USB">
<attribute name="AVAILABLE_QUANTITY" value=""/>
<attribute name="DATASHEET" value=""/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="WURTH ELEKTRONIK"/>
<attribute name="MINIMUM_QUANTITY" value=""/>
<attribute name="ORDER_NO" value="1642035"/>
<attribute name="PARTS_PER_PACKAGE" value=""/>
<attribute name="PART_NO" value="61400416121"/>
<attribute name="POPULATE" value="ES"/>
<attribute name="PRICE" value="0.195"/>
<attribute name="PRICES" value=""/>
<attribute name="QUANTITY_MULTIPLIER" value=""/>
</part>
<part name="X8" library="certoclav" deviceset="MA05-1" device="" value="USB">
<attribute name="AVAILABLE_QUANTITY" value=""/>
<attribute name="DATASHEET" value=""/>
<attribute name="DISTRIBUTOR" value=""/>
<attribute name="MANUFACTURER" value=""/>
<attribute name="MINIMUM_QUANTITY" value=""/>
<attribute name="ORDER_NO" value=""/>
<attribute name="PARTS_PER_PACKAGE" value=""/>
<attribute name="PART_NO" value=""/>
<attribute name="POPULATE" value="DNP"/>
<attribute name="PRICE" value=""/>
<attribute name="PRICES" value=""/>
<attribute name="QUANTITY_MULTIPLIER" value=""/>
</part>
<part name="L1" library="Basic" deviceset="L-EU" device="0603" value="BLM18AG601SN1D">
<attribute name="AVAILABLE_QUANTITY" value="13956"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/356366.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MURATA"/>
<attribute name="MINIMUM_QUANTITY" value="5"/>
<attribute name="ORDER_NO" value="1515679"/>
<attribute name="PARTS_PER_PACKAGE" value="5"/>
<attribute name="PART_NO" value="BLM18AG601SN1D"/>
<attribute name="PRICE" value="0.046"/>
<attribute name="PRICES" value="5-0:0.046|50-0:0.045|100-0:0.042|250-0:0.029|500--1:0.027|"/>
<attribute name="QUANTITY_MULTIPLIER" value="5"/>
</part>
<part name="R48" library="Basic" deviceset="R-EU_" device="R0603" value="1k8">
<attribute name="AVAILABLE_QUANTITY" value="25871"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1788326.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="1"/>
<attribute name="ORDER_NO" value="2130878"/>
<attribute name="PARTS_PER_PACKAGE" value="50"/>
<attribute name="PART_NO" value="MC0063W060311K8"/>
<attribute name="POPULATE" value="ES"/>
<attribute name="PRICE" value="0.005"/>
<attribute name="PRICES" value="50-0:0.005|250-0:0.004|1000--1:0.003|"/>
<attribute name="QUANTITY_MULTIPLIER" value="50"/>
</part>
<part name="R40" library="Basic" deviceset="R-EU_" device="R0603" value="1k">
<attribute name="AVAILABLE_QUANTITY" value="15000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1788326.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2130855"/>
<attribute name="PARTS_PER_PACKAGE" value="5000"/>
<attribute name="PART_NO" value="MC0063W060311K"/>
<attribute name="POPULATE" value="ES"/>
<attribute name="PRICE" value="0.0003"/>
<attribute name="PRICES" value="5000--1:0.0003|"/>
<attribute name="QUANTITY_MULTIPLIER" value="5000"/>
</part>
<part name="R39" library="Basic" deviceset="R-EU_" device="R0603" value="1k">
<attribute name="AVAILABLE_QUANTITY" value="15000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1788326.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2130855"/>
<attribute name="PARTS_PER_PACKAGE" value="5000"/>
<attribute name="PART_NO" value="MC0063W060311K"/>
<attribute name="POPULATE" value="ES"/>
<attribute name="PRICE" value="0.0003"/>
<attribute name="PRICES" value="5000--1:0.0003|"/>
<attribute name="QUANTITY_MULTIPLIER" value="5000"/>
</part>
<part name="R38" library="Basic" deviceset="R-EU_" device="R0603" value="1k">
<attribute name="AVAILABLE_QUANTITY" value="15000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1788326.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2130855"/>
<attribute name="PARTS_PER_PACKAGE" value="5000"/>
<attribute name="PART_NO" value="MC0063W060311K"/>
<attribute name="POPULATE" value="ES"/>
<attribute name="PRICE" value="0.0003"/>
<attribute name="PRICES" value="5000--1:0.0003|"/>
<attribute name="QUANTITY_MULTIPLIER" value="5000"/>
</part>
<part name="R37" library="Basic" deviceset="R-EU_" device="R0603" value="1k">
<attribute name="AVAILABLE_QUANTITY" value="15000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1788326.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2130855"/>
<attribute name="PARTS_PER_PACKAGE" value="5000"/>
<attribute name="PART_NO" value="MC0063W060311K"/>
<attribute name="POPULATE" value="ES"/>
<attribute name="PRICE" value="0.0003"/>
<attribute name="PRICES" value="5000--1:0.0003|"/>
<attribute name="QUANTITY_MULTIPLIER" value="5000"/>
</part>
<part name="R47" library="Basic" deviceset="R-EU_" device="R0603" value="1k8">
<attribute name="AVAILABLE_QUANTITY" value="25871"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1788326.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="1"/>
<attribute name="ORDER_NO" value="2130878"/>
<attribute name="PARTS_PER_PACKAGE" value="50"/>
<attribute name="PART_NO" value="MC0063W060311K8"/>
<attribute name="POPULATE" value="ES"/>
<attribute name="PRICE" value="0.005"/>
<attribute name="PRICES" value="50-0:0.005|250-0:0.004|1000--1:0.003|"/>
<attribute name="QUANTITY_MULTIPLIER" value="50"/>
</part>
<part name="R46" library="Basic" deviceset="R-EU_" device="R0603" value="1k8">
<attribute name="AVAILABLE_QUANTITY" value="25871"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1788326.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="1"/>
<attribute name="ORDER_NO" value="2130878"/>
<attribute name="PARTS_PER_PACKAGE" value="50"/>
<attribute name="PART_NO" value="MC0063W060311K8"/>
<attribute name="POPULATE" value="ES"/>
<attribute name="PRICE" value="0.005"/>
<attribute name="PRICES" value="50-0:0.005|250-0:0.004|1000--1:0.003|"/>
<attribute name="QUANTITY_MULTIPLIER" value="50"/>
</part>
<part name="R45" library="Basic" deviceset="R-EU_" device="R0603" value="1k8">
<attribute name="AVAILABLE_QUANTITY" value="25871"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1788326.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="1"/>
<attribute name="ORDER_NO" value="2130878"/>
<attribute name="PARTS_PER_PACKAGE" value="50"/>
<attribute name="PART_NO" value="MC0063W060311K8"/>
<attribute name="POPULATE" value="ES"/>
<attribute name="PRICE" value="0.005"/>
<attribute name="PRICES" value="50-0:0.005|250-0:0.004|1000--1:0.003|"/>
<attribute name="QUANTITY_MULTIPLIER" value="50"/>
</part>
<part name="R44" library="Basic" deviceset="R-EU_" device="R0603" value="1k8">
<attribute name="AVAILABLE_QUANTITY" value="25871"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1788326.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="1"/>
<attribute name="ORDER_NO" value="2130878"/>
<attribute name="PARTS_PER_PACKAGE" value="50"/>
<attribute name="PART_NO" value="MC0063W060311K8"/>
<attribute name="POPULATE" value="DNP"/>
<attribute name="PRICE" value="0.005"/>
<attribute name="PRICES" value="50-0:0.005|250-0:0.004|1000--1:0.003|"/>
<attribute name="QUANTITY_MULTIPLIER" value="50"/>
</part>
<part name="R43" library="Basic" deviceset="R-EU_" device="R0603" value="1k8">
<attribute name="AVAILABLE_QUANTITY" value="25871"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1788326.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="1"/>
<attribute name="ORDER_NO" value="2130878"/>
<attribute name="PARTS_PER_PACKAGE" value="50"/>
<attribute name="PART_NO" value="MC0063W060311K8"/>
<attribute name="POPULATE" value="DNP"/>
<attribute name="PRICE" value="0.005"/>
<attribute name="PRICES" value="50-0:0.005|250-0:0.004|1000--1:0.003|"/>
<attribute name="QUANTITY_MULTIPLIER" value="50"/>
</part>
<part name="R42" library="Basic" deviceset="R-EU_" device="R0603" value="1k8">
<attribute name="AVAILABLE_QUANTITY" value="25871"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1788326.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="1"/>
<attribute name="ORDER_NO" value="2130878"/>
<attribute name="PARTS_PER_PACKAGE" value="50"/>
<attribute name="PART_NO" value="MC0063W060311K8"/>
<attribute name="POPULATE" value="DNP"/>
<attribute name="PRICE" value="0.005"/>
<attribute name="PRICES" value="50-0:0.005|250-0:0.004|1000--1:0.003|"/>
<attribute name="QUANTITY_MULTIPLIER" value="50"/>
</part>
<part name="R41" library="Basic" deviceset="R-EU_" device="R0603" value="1k8">
<attribute name="AVAILABLE_QUANTITY" value="25871"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1788326.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="1"/>
<attribute name="ORDER_NO" value="2130878"/>
<attribute name="PARTS_PER_PACKAGE" value="50"/>
<attribute name="PART_NO" value="MC0063W060311K8"/>
<attribute name="POPULATE" value="DNP"/>
<attribute name="PRICE" value="0.005"/>
<attribute name="PRICES" value="50-0:0.005|250-0:0.004|1000--1:0.003|"/>
<attribute name="QUANTITY_MULTIPLIER" value="50"/>
</part>
<part name="GND1" library="Basic" deviceset="GND" device=""/>
<part name="GND2" library="Basic" deviceset="GND" device=""/>
<part name="X9" library="certoclav" deviceset="USD-SOCKET" device="SMD" value="USD">
<attribute name="AVAILABLE_QUANTITY" value=""/>
<attribute name="DATASHEET" value=" "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="WURTH ELEKTRONIK"/>
<attribute name="MINIMUM_QUANTITY" value=""/>
<attribute name="ORDER_NO" value="2081363"/>
<attribute name="PARTS_PER_PACKAGE" value=""/>
<attribute name="PART_NO" value="693071010811"/>
<attribute name="POPULATE" value="DNP"/>
<attribute name="PRICE" value="1.3"/>
<attribute name="PRICES" value=""/>
<attribute name="QUANTITY_MULTIPLIER" value=""/>
</part>
<part name="GND3" library="Basic" deviceset="GND" device=""/>
<part name="P+1" library="Basic" deviceset="+3.3V" device=""/>
<part name="X10" library="certoclav" deviceset="MA07-1" device="" value="SD">
<attribute name="AVAILABLE_QUANTITY" value=""/>
<attribute name="DATASHEET" value=""/>
<attribute name="DISTRIBUTOR" value=""/>
<attribute name="MANUFACTURER" value=""/>
<attribute name="MINIMUM_QUANTITY" value=""/>
<attribute name="ORDER_NO" value=""/>
<attribute name="PARTS_PER_PACKAGE" value=""/>
<attribute name="PART_NO" value=""/>
<attribute name="POPULATE" value="DNP"/>
<attribute name="PRICE" value=""/>
<attribute name="PRICES" value=""/>
<attribute name="QUANTITY_MULTIPLIER" value=""/>
</part>
<part name="GND4" library="Basic" deviceset="GND" device=""/>
<part name="P+2" library="Basic" deviceset="+3.3V" device=""/>
<part name="V15" library="certoclav" deviceset="TVS-DIODE" device="" value="PESD5V0X1BCAL">
<attribute name="AVAILABLE_QUANTITY" value="5894"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1600347.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="NXP"/>
<attribute name="MINIMUM_QUANTITY" value="1"/>
<attribute name="ORDER_NO" value="2114788"/>
<attribute name="PARTS_PER_PACKAGE" value="1"/>
<attribute name="PART_NO" value="PESD5V0X1BCAL"/>
<attribute name="POPULATE" value="DNP"/>
<attribute name="PRICE" value="0.11"/>
<attribute name="PRICES" value="1-0:0.208|25-0:0.156|100-0:0.138|250-0:0.125|500-0:0.11|2000-0:0.099|4000-0:0.0891|20000--1:0.0802|"/>
<attribute name="QUANTITY_MULTIPLIER" value="1"/>
</part>
<part name="V16" library="certoclav" deviceset="TVS-DIODE" device="" value="PESD5V0X1BCAL">
<attribute name="AVAILABLE_QUANTITY" value="5894"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1600347.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="NXP"/>
<attribute name="MINIMUM_QUANTITY" value="1"/>
<attribute name="ORDER_NO" value="2114788"/>
<attribute name="PARTS_PER_PACKAGE" value="1"/>
<attribute name="PART_NO" value="PESD5V0X1BCAL"/>
<attribute name="POPULATE" value="DNP"/>
<attribute name="PRICE" value="0.11"/>
<attribute name="PRICES" value="1-0:0.208|25-0:0.156|100-0:0.138|250-0:0.125|500-0:0.11|2000-0:0.099|4000-0:0.0891|20000--1:0.0802|"/>
<attribute name="QUANTITY_MULTIPLIER" value="1"/>
</part>
<part name="V17" library="certoclav" deviceset="TVS-DIODE" device="" value="PESD5V0X1BCAL">
<attribute name="AVAILABLE_QUANTITY" value="5894"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1600347.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="NXP"/>
<attribute name="MINIMUM_QUANTITY" value="1"/>
<attribute name="ORDER_NO" value="2114788"/>
<attribute name="PARTS_PER_PACKAGE" value="1"/>
<attribute name="PART_NO" value="PESD5V0X1BCAL"/>
<attribute name="POPULATE" value="DNP"/>
<attribute name="PRICE" value="0.11"/>
<attribute name="PRICES" value="1-0:0.208|25-0:0.156|100-0:0.138|250-0:0.125|500-0:0.11|2000-0:0.099|4000-0:0.0891|20000--1:0.0802|"/>
<attribute name="QUANTITY_MULTIPLIER" value="1"/>
</part>
<part name="V18" library="certoclav" deviceset="TVS-DIODE" device="" value="PESD5V0X1BCAL">
<attribute name="AVAILABLE_QUANTITY" value="5894"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1600347.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="NXP"/>
<attribute name="MINIMUM_QUANTITY" value="1"/>
<attribute name="ORDER_NO" value="2114788"/>
<attribute name="PARTS_PER_PACKAGE" value="1"/>
<attribute name="PART_NO" value="PESD5V0X1BCAL"/>
<attribute name="POPULATE" value="DNP"/>
<attribute name="PRICE" value="0.11"/>
<attribute name="PRICES" value="1-0:0.208|25-0:0.156|100-0:0.138|250-0:0.125|500-0:0.11|2000-0:0.099|4000-0:0.0891|20000--1:0.0802|"/>
<attribute name="QUANTITY_MULTIPLIER" value="1"/>
</part>
<part name="GND5" library="Basic" deviceset="GND" device=""/>
<part name="GND6" library="Basic" deviceset="GND" device=""/>
<part name="GND7" library="Basic" deviceset="GND" device=""/>
<part name="GND8" library="Basic" deviceset="GND" device=""/>
<part name="V19" library="certoclav" deviceset="TVS-DIODE" device="" value="PESD5V0X1BCAL">
<attribute name="AVAILABLE_QUANTITY" value="5894"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1600347.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="NXP"/>
<attribute name="MINIMUM_QUANTITY" value="1"/>
<attribute name="ORDER_NO" value="2114788"/>
<attribute name="PARTS_PER_PACKAGE" value="1"/>
<attribute name="PART_NO" value="PESD5V0X1BCAL"/>
<attribute name="POPULATE" value="DNP"/>
<attribute name="PRICE" value="0.11"/>
<attribute name="PRICES" value="1-0:0.208|25-0:0.156|100-0:0.138|250-0:0.125|500-0:0.11|2000-0:0.099|4000-0:0.0891|20000--1:0.0802|"/>
<attribute name="QUANTITY_MULTIPLIER" value="1"/>
</part>
<part name="GND9" library="Basic" deviceset="GND" device=""/>
<part name="V20" library="certoclav" deviceset="TVS-DIODE" device="" value="PESD5V0X1BCAL">
<attribute name="AVAILABLE_QUANTITY" value="5894"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1600347.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="NXP"/>
<attribute name="MINIMUM_QUANTITY" value="1"/>
<attribute name="ORDER_NO" value="2114788"/>
<attribute name="PARTS_PER_PACKAGE" value="1"/>
<attribute name="PART_NO" value="PESD5V0X1BCAL"/>
<attribute name="POPULATE" value="DNP"/>
<attribute name="PRICE" value="0.11"/>
<attribute name="PRICES" value="1-0:0.208|25-0:0.156|100-0:0.138|250-0:0.125|500-0:0.11|2000-0:0.099|4000-0:0.0891|20000--1:0.0802|"/>
<attribute name="QUANTITY_MULTIPLIER" value="1"/>
</part>
<part name="GND15" library="Basic" deviceset="GND" device=""/>
<part name="A6" library="certoclav" deviceset="ADDTIONAL" device="" value="Pinheader for display">
<attribute name="AVAILABLE_QUANTITY" value=""/>
<attribute name="DATASHEET" value=""/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="WURTH ELEKTRONIK"/>
<attribute name="MINIMUM_QUANTITY" value=""/>
<attribute name="ORDER_NO" value="2356166"/>
<attribute name="PARTS_PER_PACKAGE" value=""/>
<attribute name="PART_NO" value="61301611121"/>
<attribute name="POPULATE" value="ES"/>
<attribute name="PRICE" value="0.41"/>
<attribute name="PRICES" value=""/>
<attribute name="QUANTITY_MULTIPLIER" value=""/>
</part>
<part name="H1" library="certoclav" deviceset="BUZZER" device="-17-P10">
<attribute name="AVAILABLE_QUANTITY" value="2211"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/84905.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="TDK"/>
<attribute name="MINIMUM_QUANTITY" value="1"/>
<attribute name="ORDER_NO" value="1669968"/>
<attribute name="PARTS_PER_PACKAGE" value="1"/>
<attribute name="PART_NO" value="PS1720P02"/>
<attribute name="POPULATE" value=""/>
<attribute name="PRICE" value="0.31"/>
<attribute name="PRICES" value="1-0:0.71|10-0:0.659|50-0:0.607|100-0:0.379|250-0:0.369|400--1:0.353|"/>
<attribute name="QUANTITY_MULTIPLIER" value="1"/>
</part>
<part name="GND20" library="Basic" deviceset="GND" device=""/>
<part name="V9" library="Display" deviceset="2N700" device="" technology="0" value="2N7002">
<attribute name="AVAILABLE_QUANTITY" value="16048"/>
<attribute name="DATASHEET" value="http://www.nxp.com/documents/data_sheet/2N7002PW.pdf"/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="NXP"/>
<attribute name="MINIMUM_QUANTITY" value="1"/>
<attribute name="ORDER_NO" value="1829184"/>
<attribute name="PARTS_PER_PACKAGE" value="1"/>
<attribute name="PART_NO" value="2N7002PW"/>
<attribute name="POPULATE" value=""/>
<attribute name="PRICE" value="0.039"/>
<attribute name="PRICES" value="1-0:0.039|25-0:0.031|100-0:0.025|250-0:0.021|500--1:0.018|"/>
<attribute name="QUANTITY_MULTIPLIER" value="1"/>
</part>
<part name="R23" library="Basic" deviceset="R-EU_" device="R0603" value="10k">
<attribute name="AVAILABLE_QUANTITY" value="1042000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1697337.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="VISHAY DRALORIC"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2122493"/>
<attribute name="PARTS_PER_PACKAGE" value="5000"/>
<attribute name="PART_NO" value="CRCW060310K0FKEA"/>
<attribute name="POPULATE" value=""/>
<attribute name="PRICE" value="0.002"/>
<attribute name="PRICES" value="5000-0:0.002|25000-0:0.0018|50000--1:0.0015|"/>
<attribute name="QUANTITY_MULTIPLIER" value="5000"/>
</part>
<part name="GND22" library="Basic" deviceset="GND" device=""/>
<part name="P+3" library="Basic" deviceset="+5V" device=""/>
<part name="R14" library="Basic" deviceset="R-EU_" device="R0603" value="1k">
<attribute name="AVAILABLE_QUANTITY" value="15000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1788326.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2130855"/>
<attribute name="PARTS_PER_PACKAGE" value="5000"/>
<attribute name="PART_NO" value="MC0063W060311K"/>
<attribute name="POPULATE" value=""/>
<attribute name="PRICE" value="0.0003"/>
<attribute name="PRICES" value="5000--1:0.0003|"/>
<attribute name="QUANTITY_MULTIPLIER" value="5000"/>
</part>
<part name="A1" library="certoclav" deviceset="ADDTIONAL" device="" value="LED spacer">
<attribute name="AVAILABLE_QUANTITY" value="7513"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1655835.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="KINGBRIGHT"/>
<attribute name="MINIMUM_QUANTITY" value="1"/>
<attribute name="ORDER_NO" value="2217991"/>
<attribute name="PARTS_PER_PACKAGE" value="1"/>
<attribute name="PART_NO" value="BR-5.08"/>
<attribute name="POPULATE" value="Minimal"/>
<attribute name="PRICE" value="0.0332"/>
<attribute name="PRICES" value="1-0:0.093|10-0:0.0775|50-0:0.0698|100-0:0.062|250-0:0.0558|500-0:0.0465|2000-0:0.0419|3000--1:0.0377|"/>
<attribute name="QUANTITY_MULTIPLIER" value="1"/>
</part>
<part name="A2" library="certoclav" deviceset="ADDTIONAL" device="" value="LED spacer">
<attribute name="AVAILABLE_QUANTITY" value="7513"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1655835.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="KINGBRIGHT"/>
<attribute name="MINIMUM_QUANTITY" value="1"/>
<attribute name="ORDER_NO" value="2217991"/>
<attribute name="PARTS_PER_PACKAGE" value="1"/>
<attribute name="PART_NO" value="BR-5.08"/>
<attribute name="POPULATE" value="Minimal"/>
<attribute name="PRICE" value="0.0332"/>
<attribute name="PRICES" value="1-0:0.093|10-0:0.0775|50-0:0.0698|100-0:0.062|250-0:0.0558|500-0:0.0465|2000-0:0.0419|3000--1:0.0377|"/>
<attribute name="QUANTITY_MULTIPLIER" value="1"/>
</part>
<part name="A3" library="certoclav" deviceset="ADDTIONAL" device="" value="LED spacer">
<attribute name="AVAILABLE_QUANTITY" value="7513"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1655835.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="KINGBRIGHT"/>
<attribute name="MINIMUM_QUANTITY" value="1"/>
<attribute name="ORDER_NO" value="2217991"/>
<attribute name="PARTS_PER_PACKAGE" value="1"/>
<attribute name="PART_NO" value="BR-5.08"/>
<attribute name="POPULATE" value="Minimal"/>
<attribute name="PRICE" value="0.0332"/>
<attribute name="PRICES" value="1-0:0.093|10-0:0.0775|50-0:0.0698|100-0:0.062|250-0:0.0558|500-0:0.0465|2000-0:0.0419|3000--1:0.0377|"/>
<attribute name="QUANTITY_MULTIPLIER" value="1"/>
</part>
<part name="A4" library="certoclav" deviceset="ADDTIONAL" device="" value="LED spacer">
<attribute name="AVAILABLE_QUANTITY" value="7513"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1655835.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="KINGBRIGHT"/>
<attribute name="MINIMUM_QUANTITY" value="1"/>
<attribute name="ORDER_NO" value="2217991"/>
<attribute name="PARTS_PER_PACKAGE" value="1"/>
<attribute name="PART_NO" value="BR-5.08"/>
<attribute name="POPULATE" value="Minimal"/>
<attribute name="PRICE" value="0.0332"/>
<attribute name="PRICES" value="1-0:0.093|10-0:0.0775|50-0:0.0698|100-0:0.062|250-0:0.0558|500-0:0.0465|2000-0:0.0419|3000--1:0.0377|"/>
<attribute name="QUANTITY_MULTIPLIER" value="1"/>
</part>
<part name="A5" library="certoclav" deviceset="ADDTIONAL" device="" value="LED spacer">
<attribute name="AVAILABLE_QUANTITY" value="7513"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1655835.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="KINGBRIGHT"/>
<attribute name="MINIMUM_QUANTITY" value="1"/>
<attribute name="ORDER_NO" value="2217991"/>
<attribute name="PARTS_PER_PACKAGE" value="1"/>
<attribute name="PART_NO" value="BR-5.08"/>
<attribute name="POPULATE" value="Minimal"/>
<attribute name="PRICE" value="0.0332"/>
<attribute name="PRICES" value="1-0:0.093|10-0:0.0775|50-0:0.0698|100-0:0.062|250-0:0.0558|500-0:0.0465|2000-0:0.0419|3000--1:0.0377|"/>
<attribute name="QUANTITY_MULTIPLIER" value="1"/>
</part>
<part name="C21" library="Basic" deviceset="C-EU" device="C0603" value="10n">
<attribute name="AVAILABLE_QUANTITY" value="1808000 "/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1825492.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2310601"/>
<attribute name="PARTS_PER_PACKAGE" value="4000"/>
<attribute name="PART_NO" value="MC0603B103K500CT"/>
<attribute name="PRICE" value="0.0012"/>
<attribute name="PRICES" value="4000-0:0.0024|20000-0:0.0021|40000--1:0.0019|"/>
<attribute name="QUANTITY_MULTIPLIER" value="4000"/>
</part>
<part name="GND23" library="Basic" deviceset="GND" device=""/>
<part name="C18" library="Basic" deviceset="C-EU" device="C0603" value="10n">
<attribute name="AVAILABLE_QUANTITY" value="1808000 "/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1825492.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2310601"/>
<attribute name="PARTS_PER_PACKAGE" value="4000"/>
<attribute name="PART_NO" value="MC0603B103K500CT"/>
<attribute name="POPULATE" value=""/>
<attribute name="PRICE" value="0.0012"/>
<attribute name="PRICES" value="4000-0:0.0024|20000-0:0.0021|40000--1:0.0019|"/>
<attribute name="QUANTITY_MULTIPLIER" value="4000"/>
</part>
<part name="C19" library="Basic" deviceset="C-EU" device="C0603" value="10n">
<attribute name="AVAILABLE_QUANTITY" value="1808000 "/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1825492.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2310601"/>
<attribute name="PARTS_PER_PACKAGE" value="4000"/>
<attribute name="PART_NO" value="MC0603B103K500CT"/>
<attribute name="PRICE" value="0.0012"/>
<attribute name="PRICES" value="4000-0:0.0024|20000-0:0.0021|40000--1:0.0019|"/>
<attribute name="QUANTITY_MULTIPLIER" value="4000"/>
</part>
<part name="C22" library="Basic" deviceset="C-EU" device="C0603" value="10n">
<attribute name="AVAILABLE_QUANTITY" value="1808000 "/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1825492.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2310601"/>
<attribute name="PARTS_PER_PACKAGE" value="4000"/>
<attribute name="PART_NO" value="MC0603B103K500CT"/>
<attribute name="PRICE" value="0.0012"/>
<attribute name="PRICES" value="4000-0:0.0024|20000-0:0.0021|40000--1:0.0019|"/>
<attribute name="QUANTITY_MULTIPLIER" value="4000"/>
</part>
<part name="GND24" library="Basic" deviceset="GND" device=""/>
<part name="C20" library="Basic" deviceset="C-EU" device="C0603" value="10n">
<attribute name="AVAILABLE_QUANTITY" value="1808000 "/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1825492.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2310601"/>
<attribute name="PARTS_PER_PACKAGE" value="4000"/>
<attribute name="PART_NO" value="MC0603B103K500CT"/>
<attribute name="PRICE" value="0.0012"/>
<attribute name="PRICES" value="4000-0:0.0024|20000-0:0.0021|40000--1:0.0019|"/>
<attribute name="QUANTITY_MULTIPLIER" value="4000"/>
</part>
<part name="C23" library="Basic" deviceset="C-EU" device="C0603" value="10n">
<attribute name="AVAILABLE_QUANTITY" value="1808000 "/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1825492.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2310601"/>
<attribute name="PARTS_PER_PACKAGE" value="4000"/>
<attribute name="PART_NO" value="MC0603B103K500CT"/>
<attribute name="PRICE" value="0.0012"/>
<attribute name="PRICES" value="4000-0:0.0024|20000-0:0.0021|40000--1:0.0019|"/>
<attribute name="QUANTITY_MULTIPLIER" value="4000"/>
</part>
<part name="GND26" library="Basic" deviceset="GND" device=""/>
<part name="R34" library="Basic" deviceset="R-EU_" device="R0603" value="1k">
<attribute name="AVAILABLE_QUANTITY" value="15000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1788326.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2130855"/>
<attribute name="PARTS_PER_PACKAGE" value="5000"/>
<attribute name="PART_NO" value="MC0063W060311K"/>
<attribute name="POPULATE" value="ES"/>
<attribute name="PRICE" value="0.0003"/>
<attribute name="PRICES" value="5000--1:0.0003|"/>
<attribute name="QUANTITY_MULTIPLIER" value="5000"/>
</part>
<part name="GND31" library="Basic" deviceset="GND" device=""/>
<part name="R1" library="Basic" deviceset="R-EU_" device="R0603" value="100R">
<attribute name="AVAILABLE_QUANTITY" value="590000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1788326.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2130776"/>
<attribute name="PARTS_PER_PACKAGE" value="5000"/>
<attribute name="PART_NO" value="MC0063W06031100R"/>
<attribute name="PRICE" value="0.0009"/>
<attribute name="PRICES" value="5000-0:0.0009|25000-0:0.0008|50000--1:0.0006|"/>
<attribute name="QUANTITY_MULTIPLIER" value="5000"/>
</part>
<part name="X4" library="certoclav" deviceset="61900411121" device="" value="PT100">
<attribute name="AVAILABLE_QUANTITY" value=""/>
<attribute name="DATASHEET" value=""/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="WURTH ELEKTRONIK"/>
<attribute name="MINIMUM_QUANTITY" value=""/>
<attribute name="ORDER_NO" value="1841378"/>
<attribute name="PARTS_PER_PACKAGE" value=""/>
<attribute name="PART_NO" value="61900411121"/>
<attribute name="POPULATE" value=""/>
<attribute name="PRICE" value="0.36"/>
<attribute name="PRICES" value=""/>
<attribute name="QUANTITY_MULTIPLIER" value=""/>
</part>
<part name="P+4" library="Basic" deviceset="+5V" device=""/>
<part name="C2" library="Basic" deviceset="C-EU" device="C1210" value="47u/16V">
<attribute name="AVAILABLE_QUANTITY" value=""/>
<attribute name="DATASHEET" value=""/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MURATA"/>
<attribute name="MINIMUM_QUANTITY" value=""/>
<attribute name="ORDER_NO" value="1907527"/>
<attribute name="PARTS_PER_PACKAGE" value=""/>
<attribute name="PART_NO" value="GRM32EC81C476KE15L"/>
<attribute name="POPULATE" value=""/>
<attribute name="PRICE" value="0.2561"/>
<attribute name="PRICES" value=""/>
<attribute name="QUANTITY_MULTIPLIER" value=""/>
</part>
<part name="GND38" library="Basic" deviceset="GND" device=""/>
<part name="C31" library="Basic" deviceset="C-EU" device="C0603" value="100n">
<attribute name="AVAILABLE_QUANTITY" value="3188000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1825492.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2310603"/>
<attribute name="PARTS_PER_PACKAGE" value="4000"/>
<attribute name="PART_NO" value="MC0603B104K500CT"/>
<attribute name="POPULATE" value="ES"/>
<attribute name="PRICE" value="0.0024"/>
<attribute name="PRICES" value="4000-0:0.0024|20000-0:0.0021|40000--1:0.0019|"/>
<attribute name="QUANTITY_MULTIPLIER" value="4000"/>
</part>
<part name="GND66" library="Basic" deviceset="GND" device=""/>
<part name="P+8" library="Basic" deviceset="+5V" device=""/>
<part name="C33" library="Basic" deviceset="C-EU" device="C0603" value="1u">
<attribute name="AVAILABLE_QUANTITY" value="28000"/>
<attribute name="DATASHEET" value="http://www.farnell.com/datasheets/1825491.pdf "/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MULTICOMP"/>
<attribute name="MINIMUM_QUANTITY" value="0"/>
<attribute name="ORDER_NO" value="2310567"/>
<attribute name="PARTS_PER_PACKAGE" value="4000"/>
<attribute name="PART_NO" value="MC0603X105K250CT"/>
<attribute name="POPULATE" value="ES"/>
<attribute name="PRICE" value="0.0067"/>
<attribute name="PRICES" value="4000-0:0.0076|20000-0:0.0066|40000--1:0.0058|"/>
<attribute name="QUANTITY_MULTIPLIER" value="4000"/>
</part>
<part name="C32" library="Basic" deviceset="C-EU" device="C1210" value="47u/16V">
<attribute name="AVAILABLE_QUANTITY" value=""/>
<attribute name="DATASHEET" value=""/>
<attribute name="DISTRIBUTOR" value="farnell.com"/>
<attribute name="MANUFACTURER" value="MURATA"/>
<attribute name="MINIMUM_QUANTITY" value=""/>
<attribute name="ORDER_NO" value="1907527"/>
<attribute name="PARTS_PER_PACKAGE" value=""/>
<attribute name="PART_NO" value="GRM32EC81C476KE15L"/>
<attribute name="POPULATE" value="ES"/>
<attribute name="PRICE" value="0.2561"/>
<attribute name="PRICES" value=""/>
<attribute name="QUANTITY_MULTIPLIER" value=""/>
</part>
<part name="GND37" library="Basic" deviceset="GND" device=""/>
<part name="P+13" library="Basic" deviceset="+5V" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="4.572" y="13.716" size="1.778" layer="97">1.0</text>
<text x="13.716" y="13.716" size="1.778" layer="97">Inital Revision</text>
<text x="38.354" y="13.716" size="1.778" layer="97">18.04.14</text>
<wire x1="254" y1="71.12" x2="294.64" y2="71.12" width="0.1524" layer="97" style="longdash"/>
<wire x1="294.64" y1="71.12" x2="294.64" y2="116.84" width="0.1524" layer="97" style="longdash"/>
<wire x1="294.64" y1="116.84" x2="254" y2="116.84" width="0.1524" layer="97" style="longdash"/>
<wire x1="254" y1="116.84" x2="254" y2="71.12" width="0.1524" layer="97" style="longdash"/>
<text x="256.54" y="112.776" size="2.54" layer="97">Analog Voltage Filter</text>
<text x="256.54" y="109.22" size="1.778" layer="97">to filter out most high frequency</text>
<text x="256.54" y="106.68" size="1.778" layer="97">distortion on the power line</text>
<wire x1="81.28" y1="53.34" x2="81.28" y2="25.4" width="0.1524" layer="97" style="longdash"/>
<wire x1="81.28" y1="25.4" x2="10.16" y2="25.4" width="0.1524" layer="97" style="longdash"/>
<wire x1="10.16" y1="25.4" x2="10.16" y2="53.34" width="0.1524" layer="97" style="longdash"/>
<wire x1="10.16" y1="53.34" x2="81.28" y2="53.34" width="0.1524" layer="97" style="longdash"/>
<text x="78.74" y="48.26" size="2.54" layer="97" rot="MR0">PDI Interface</text>
<text x="76.2" y="254" size="2.54" layer="97" font="vector">3.3 V</text>
<text x="76.2" y="251.714" size="1.778" layer="97" font="vector">Power Management</text>
<text x="76.2" y="249.428" size="1.778" layer="97" font="vector">Output Voltage 3.3V, Imax=0.8A, Icont &lt;0,1 A</text>
<wire x1="73.66" y1="259.08" x2="170.18" y2="259.08" width="0.1524" layer="97" style="longdash"/>
<wire x1="73.66" y1="210.82" x2="170.18" y2="210.82" width="0.1524" layer="97" style="longdash"/>
<wire x1="170.18" y1="259.08" x2="170.18" y2="210.82" width="0.1524" layer="97" style="longdash"/>
<wire x1="73.66" y1="259.08" x2="73.66" y2="210.82" width="0.1524" layer="97" style="longdash"/>
<wire x1="63.5" y1="259.08" x2="12.7" y2="259.08" width="0.1524" layer="97" style="longdash"/>
<wire x1="12.7" y1="213.36" x2="12.7" y2="259.08" width="0.1524" layer="97" style="longdash"/>
<wire x1="63.5" y1="213.36" x2="63.5" y2="259.08" width="0.1524" layer="97" style="longdash"/>
<wire x1="63.5" y1="213.36" x2="12.7" y2="213.36" width="0.1524" layer="97" style="longdash"/>
<text x="55.88" y="254" size="2.54" layer="97" font="vector" rot="MR0">Board interconnect</text>
<wire x1="269.24" y1="167.64" x2="317.5" y2="167.64" width="0.1524" layer="97" style="longdash"/>
<text x="271.78" y="162.56" size="2.54" layer="97">Internal Debug UART</text>
<wire x1="269.24" y1="167.64" x2="269.24" y2="134.62" width="0.1524" layer="97" style="longdash"/>
<wire x1="269.24" y1="134.62" x2="317.5" y2="134.62" width="0.1524" layer="97" style="longdash"/>
<wire x1="317.5" y1="167.64" x2="317.5" y2="134.62" width="0.1524" layer="97" style="longdash"/>
</plain>
<instances>
<instance part="U$3" gate="G$1" x="0" y="0">
<attribute name="CREATE_DATE" x="0" y="0" size="1.778" layer="96" display="off"/>
<attribute name="EDIT_DATE" x="0" y="0" size="1.778" layer="96" display="off"/>
<attribute name="USERNAME" x="0" y="0" size="1.778" layer="96" display="off"/>
<attribute name="FILENAME" x="0" y="0" size="1.778" layer="96" display="off"/>
<attribute name="SUBTITLE" x="0" y="0" size="1.778" layer="96" display="off"/>
<attribute name="TITLE" x="0" y="0" size="1.778" layer="96" display="off"/>
<attribute name="OBJECT_NO" x="0" y="0" size="1.778" layer="96" display="off"/>
<attribute name="DRAWING" x="0" y="0" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND10" gate="1" x="170.18" y="73.66"/>
<instance part="C8" gate="G$1" x="200.66" y="91.44">
<attribute name="MANUFACTURER" x="200.66" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="200.66" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="200.66" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="200.66" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="200.66" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="200.66" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="200.66" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="200.66" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="200.66" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="200.66" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="200.66" y="91.44" size="1.778" layer="96" display="off"/>
</instance>
<instance part="C9" gate="G$1" x="210.82" y="91.44">
<attribute name="MANUFACTURER" x="210.82" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="210.82" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="210.82" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="210.82" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="210.82" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="210.82" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="210.82" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="210.82" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="210.82" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="210.82" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="210.82" y="91.44" size="1.778" layer="96" display="off"/>
</instance>
<instance part="C10" gate="G$1" x="220.98" y="91.44">
<attribute name="MANUFACTURER" x="220.98" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="220.98" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="220.98" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="220.98" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="220.98" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="220.98" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="220.98" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="220.98" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="220.98" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="220.98" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="220.98" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="POPULATE" x="220.98" y="91.44" size="1.778" layer="96" rot="R180" display="off"/>
</instance>
<instance part="C11" gate="G$1" x="231.14" y="91.44">
<attribute name="MANUFACTURER" x="231.14" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="231.14" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="231.14" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="231.14" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="231.14" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="231.14" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="231.14" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="231.14" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="231.14" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="231.14" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="231.14" y="91.44" size="1.778" layer="96" display="off"/>
</instance>
<instance part="C12" gate="G$1" x="241.3" y="91.44">
<attribute name="MANUFACTURER" x="241.3" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="241.3" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="241.3" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="241.3" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="241.3" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="241.3" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="241.3" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="241.3" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="241.3" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="241.3" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="241.3" y="91.44" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND11" gate="1" x="200.66" y="76.2"/>
<instance part="GND12" gate="1" x="210.82" y="76.2"/>
<instance part="GND13" gate="1" x="220.98" y="76.2"/>
<instance part="GND14" gate="1" x="231.14" y="76.2"/>
<instance part="GND16" gate="1" x="241.3" y="76.2"/>
<instance part="P+5" gate="1" x="241.3" y="111.76"/>
<instance part="GND25" gate="1" x="162.56" y="73.66"/>
<instance part="C13" gate="G$1" x="261.62" y="91.44">
<attribute name="DISTRIBUTOR" x="261.62" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="261.62" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="261.62" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="261.62" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="261.62" y="91.44" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="PRICE" x="261.62" y="91.44" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="261.62" y="91.44" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="261.62" y="91.44" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="261.62" y="91.44" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="261.62" y="91.44" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="DATASHEET" x="261.62" y="91.44" size="1.778" layer="96" font="vector" display="off"/>
</instance>
<instance part="L4" gate="G$1" x="271.78" y="99.06" smashed="yes" rot="R90">
<attribute name="NAME" x="270.764" y="95.9866" size="1.524" layer="95"/>
<attribute name="VALUE" x="264.414" y="94.234" size="1.27" layer="96"/>
<attribute name="DISTRIBUTOR" x="271.78" y="99.06" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="MANUFACTURER" x="271.78" y="99.06" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="ORDER_NO" x="271.78" y="99.06" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="PART_NO" x="271.78" y="99.06" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="PRICES" x="271.78" y="99.06" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="PRICE" x="271.78" y="99.06" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="271.78" y="99.06" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="271.78" y="99.06" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="271.78" y="99.06" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="DATASHEET" x="271.78" y="99.06" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="271.78" y="99.06" size="1.778" layer="96" display="off"/>
</instance>
<instance part="C14" gate="G$1" x="281.94" y="91.44">
<attribute name="DISTRIBUTOR" x="281.94" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="281.94" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="281.94" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="281.94" y="91.44" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="281.94" y="91.44" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="PRICE" x="281.94" y="91.44" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="281.94" y="91.44" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="281.94" y="91.44" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="281.94" y="91.44" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="281.94" y="91.44" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="DATASHEET" x="281.94" y="91.44" size="1.778" layer="96" font="vector" display="off"/>
</instance>
<instance part="GND35" gate="1" x="261.62" y="76.2"/>
<instance part="GND36" gate="1" x="281.94" y="76.2"/>
<instance part="P+6" gate="1" x="281.94" y="104.14"/>
<instance part="D2" gate="G$1" x="162.56" y="139.7" smashed="yes">
<attribute name="NAME" x="151.892" y="87.884" size="1.524" layer="95" ratio="5" rot="R180"/>
<attribute name="VALUE" x="152.146" y="85.852" size="1.27" layer="96" ratio="5" rot="R180"/>
<attribute name="PRICE" x="162.56" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="162.56" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="162.56" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="162.56" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="162.56" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="162.56" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="162.56" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="162.56" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="162.56" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="162.56" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="162.56" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="POPULATE" x="162.56" y="139.7" size="1.778" layer="96" display="off"/>
</instance>
<instance part="X3" gate="1" x="27.94" y="38.1" smashed="yes" rot="MR180">
<attribute name="VALUE" x="29.718" y="29.21" size="1.27" layer="96" rot="MR0"/>
<attribute name="NAME" x="29.21" y="30.988" size="1.524" layer="95" rot="MR0"/>
<attribute name="MANUFACTURER" x="27.94" y="38.1" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="PART_NO" x="27.94" y="38.1" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="DISTRIBUTOR" x="27.94" y="38.1" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="ORDER_NO" x="27.94" y="38.1" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="PRICE" x="27.94" y="38.1" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="PRICES" x="27.94" y="38.1" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="27.94" y="38.1" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="27.94" y="38.1" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="DATASHEET" x="27.94" y="38.1" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="27.94" y="38.1" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="27.94" y="38.1" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="POPULATE" x="27.94" y="38.1" size="1.778" layer="96" display="off"/>
</instance>
<instance part="P+7" gate="1" x="17.78" y="45.72"/>
<instance part="GND21" gate="1" x="17.78" y="30.48"/>
<instance part="C3" gate="G$1" x="119.38" y="231.14">
<attribute name="DISTRIBUTOR" x="119.38" y="231.14" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="119.38" y="231.14" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="119.38" y="231.14" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="119.38" y="231.14" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="119.38" y="231.14" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="PRICE" x="119.38" y="231.14" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="119.38" y="231.14" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="119.38" y="231.14" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="119.38" y="231.14" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="119.38" y="231.14" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="DATASHEET" x="119.38" y="231.14" size="1.778" layer="96" font="vector" display="off"/>
</instance>
<instance part="C4" gate="G$1" x="149.86" y="231.14">
<attribute name="DISTRIBUTOR" x="149.86" y="231.14" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="149.86" y="231.14" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="149.86" y="231.14" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="149.86" y="231.14" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="149.86" y="231.14" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="PRICE" x="149.86" y="231.14" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="149.86" y="231.14" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="149.86" y="231.14" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="149.86" y="231.14" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="149.86" y="231.14" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="DATASHEET" x="149.86" y="231.14" size="1.778" layer="96" font="vector" display="off"/>
</instance>
<instance part="D1" gate="G$1" x="134.62" y="238.76" smashed="yes">
<attribute name="NAME" x="134.366" y="233.172" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="134.366" y="231.394" size="1.27" layer="96" rot="R180"/>
<attribute name="PRICES" x="134.62" y="238.76" size="1.778" layer="96" font="vector" rot="MR0" display="off"/>
<attribute name="PRICE" x="134.62" y="238.76" size="1.778" layer="96" font="vector" rot="MR0" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="134.62" y="238.76" size="1.778" layer="96" font="vector" rot="MR0" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="134.62" y="238.76" size="1.778" layer="96" font="vector" rot="MR0" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="134.62" y="238.76" size="1.778" layer="96" font="vector" rot="MR0" display="off"/>
<attribute name="DATASHEET" x="134.62" y="238.76" size="1.778" layer="96" font="vector" rot="MR0" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="134.62" y="238.76" size="1.778" layer="96" rot="MR0" display="off"/>
</instance>
<instance part="C5" gate="G$1" x="162.56" y="231.14">
<attribute name="DISTRIBUTOR" x="162.56" y="231.14" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="162.56" y="231.14" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="162.56" y="231.14" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="162.56" y="231.14" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="162.56" y="231.14" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="PRICE" x="162.56" y="231.14" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="162.56" y="231.14" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="162.56" y="231.14" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="162.56" y="231.14" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="162.56" y="231.14" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="DATASHEET" x="162.56" y="231.14" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="POPULATE" x="162.56" y="231.14" size="1.778" layer="96" display="off"/>
</instance>
<instance part="P+14" gate="1" x="162.56" y="243.84"/>
<instance part="GND48" gate="1" x="119.38" y="218.44"/>
<instance part="GND53" gate="1" x="134.62" y="218.44"/>
<instance part="GND54" gate="1" x="149.86" y="218.44"/>
<instance part="GND59" gate="1" x="162.56" y="218.44"/>
<instance part="C1" gate="G$1" x="96.52" y="231.14">
<attribute name="POPULATE" x="96.52" y="231.14" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="96.52" y="231.14" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="96.52" y="231.14" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="96.52" y="231.14" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="96.52" y="231.14" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="96.52" y="231.14" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="96.52" y="231.14" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="96.52" y="231.14" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="96.52" y="231.14" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="96.52" y="231.14" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="96.52" y="231.14" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="96.52" y="231.14" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND60" gate="1" x="96.52" y="218.44"/>
<instance part="X1" gate="G$1" x="40.64" y="233.68" smashed="yes" rot="MR0">
<attribute name="VALUE" x="43.18" y="222.25" size="1.27" layer="96" rot="MR0"/>
<attribute name="NAME" x="42.418" y="224.028" size="1.524" layer="95" rot="MR0"/>
<attribute name="POPULATE" x="40.64" y="233.68" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="40.64" y="233.68" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="40.64" y="233.68" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="40.64" y="233.68" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="40.64" y="233.68" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="40.64" y="233.68" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="40.64" y="233.68" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="40.64" y="233.68" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="40.64" y="233.68" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="40.64" y="233.68" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="40.64" y="233.68" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="40.64" y="233.68" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND67" gate="1" x="30.48" y="220.98" rot="MR0"/>
<instance part="GND68" gate="1" x="96.52" y="101.6"/>
<instance part="L2" gate="G$1" x="88.9" y="121.92" smashed="yes" rot="R90">
<attribute name="NAME" x="85.09" y="121.6914" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="93.218" y="120.396" size="1.27" layer="96"/>
<attribute name="DISTRIBUTOR" x="88.9" y="121.92" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="MANUFACTURER" x="88.9" y="121.92" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="ORDER_NO" x="88.9" y="121.92" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="PART_NO" x="88.9" y="121.92" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="PRICES" x="88.9" y="121.92" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="PRICE" x="88.9" y="121.92" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="88.9" y="121.92" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="88.9" y="121.92" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="88.9" y="121.92" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="DATASHEET" x="88.9" y="121.92" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="88.9" y="121.92" size="1.778" layer="96" display="off"/>
</instance>
<instance part="C6" gate="G$1" x="78.74" y="111.76">
<attribute name="MANUFACTURER" x="78.74" y="111.76" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="78.74" y="111.76" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="78.74" y="111.76" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="78.74" y="111.76" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="78.74" y="111.76" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="78.74" y="111.76" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="78.74" y="111.76" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="78.74" y="111.76" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="78.74" y="111.76" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="78.74" y="111.76" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="78.74" y="111.76" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND69" gate="1" x="78.74" y="101.6"/>
<instance part="L3" gate="G$1" x="88.9" y="119.38" smashed="yes" rot="R90">
<attribute name="NAME" x="85.09" y="119.1514" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="80.264" y="115.824" size="1.27" layer="96"/>
<attribute name="DISTRIBUTOR" x="88.9" y="119.38" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="MANUFACTURER" x="88.9" y="119.38" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="ORDER_NO" x="88.9" y="119.38" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="PART_NO" x="88.9" y="119.38" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="PRICES" x="88.9" y="119.38" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="PRICE" x="88.9" y="119.38" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="88.9" y="119.38" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="88.9" y="119.38" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="88.9" y="119.38" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="DATASHEET" x="88.9" y="119.38" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="88.9" y="119.38" size="1.778" layer="96" display="off"/>
</instance>
<instance part="C7" gate="G$1" x="96.52" y="111.76">
<attribute name="MANUFACTURER" x="96.52" y="111.76" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="96.52" y="111.76" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="96.52" y="111.76" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="96.52" y="111.76" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="96.52" y="111.76" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="96.52" y="111.76" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="96.52" y="111.76" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="96.52" y="111.76" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="96.52" y="111.76" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="96.52" y="111.76" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="96.52" y="111.76" size="1.778" layer="96" display="off"/>
</instance>
<instance part="J4" gate="G$1" x="370.84" y="33.02"/>
<instance part="J3" gate="G$1" x="355.6" y="33.02"/>
<instance part="J2" gate="G$1" x="340.36" y="33.02"/>
<instance part="J1" gate="G$1" x="325.12" y="33.02"/>
<instance part="X2" gate="1" x="312.42" y="152.4" smashed="yes" rot="MR0">
<attribute name="VALUE" x="314.452" y="138.176" size="1.27" layer="96" rot="MR0"/>
<attribute name="NAME" x="313.182" y="139.954" size="1.524" layer="95" rot="MR0"/>
<attribute name="POPULATE" x="312.42" y="152.4" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="312.42" y="152.4" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="312.42" y="152.4" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="312.42" y="152.4" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="312.42" y="152.4" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="312.42" y="152.4" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="312.42" y="152.4" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="312.42" y="152.4" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="312.42" y="152.4" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="312.42" y="152.4" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="312.42" y="152.4" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="312.42" y="152.4" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND76" gate="1" x="302.26" y="137.16"/>
<instance part="L1" gate="G$1" x="83.82" y="238.76" smashed="yes" rot="R90">
<attribute name="NAME" x="85.09" y="237.2614" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="75.438" y="234.188" size="1.27" layer="96"/>
<attribute name="DISTRIBUTOR" x="83.82" y="238.76" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="MANUFACTURER" x="83.82" y="238.76" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="ORDER_NO" x="83.82" y="238.76" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="PART_NO" x="83.82" y="238.76" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="PRICES" x="83.82" y="238.76" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="PRICE" x="83.82" y="238.76" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="83.82" y="238.76" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="83.82" y="238.76" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="83.82" y="238.76" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="DATASHEET" x="83.82" y="238.76" size="1.778" layer="96" font="vector" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="83.82" y="238.76" size="1.778" layer="96" display="off"/>
</instance>
<instance part="R1" gate="G$1" x="68.58" y="121.92" smashed="yes">
<attribute name="NAME" x="65.786" y="121.6914" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="71.374" y="120.396" size="1.27" layer="96"/>
<attribute name="MANUFACTURER" x="68.58" y="121.92" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="PART_NO" x="68.58" y="121.92" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="DISTRIBUTOR" x="68.58" y="121.92" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="ORDER_NO" x="68.58" y="121.92" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="PRICE" x="68.58" y="121.92" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="PRICES" x="68.58" y="121.92" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="68.58" y="121.92" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="68.58" y="121.92" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="DATASHEET" x="68.58" y="121.92" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="68.58" y="121.92" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="68.58" y="121.92" size="1.778" layer="96" rot="R90" display="off"/>
</instance>
<instance part="P+4" gate="1" x="96.52" y="243.84"/>
<instance part="C2" gate="G$1" x="109.22" y="231.14">
<attribute name="POPULATE" x="109.22" y="231.14" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="109.22" y="231.14" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="109.22" y="231.14" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="109.22" y="231.14" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="109.22" y="231.14" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="109.22" y="231.14" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="109.22" y="231.14" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="109.22" y="231.14" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="109.22" y="231.14" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="109.22" y="231.14" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="109.22" y="231.14" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="109.22" y="231.14" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND38" gate="1" x="109.22" y="218.44"/>
</instances>
<busses>
<bus name="TEMPERATURE:TEMP_REFERENCE,TEMP_POS,TEMP_POS2,TEMP_NEG,TEMP_NEG_EN,TEMP_180_EN,TEMP_100_EN,TEMP_SENSE_EN">
<segment>
<wire x1="58.42" y1="152.4" x2="91.44" y2="152.4" width="0.762" layer="92"/>
<wire x1="91.44" y1="152.4" x2="93.98" y2="154.94" width="0.762" layer="92"/>
<wire x1="93.98" y1="154.94" x2="93.98" y2="185.42" width="0.762" layer="92"/>
<label x="58.42" y="153.162" size="1.778" layer="95"/>
</segment>
</bus>
<bus name="DISPLAY:RS,RW,E,DB[4..7],BL-PWM,V0-PWM">
<segment>
<wire x1="81.28" y1="147.32" x2="96.52" y2="147.32" width="0.762" layer="92"/>
<wire x1="96.52" y1="147.32" x2="99.06" y2="147.32" width="0.762" layer="92"/>
<wire x1="99.06" y1="147.32" x2="99.06" y2="127" width="0.762" layer="92"/>
<label x="81.788" y="149.606" size="1.778" layer="95" rot="MR180"/>
<wire x1="99.06" y1="147.32" x2="99.06" y2="162.56" width="0.762" layer="92"/>
<wire x1="99.06" y1="162.56" x2="99.06" y2="195.58" width="0.762" layer="92"/>
<wire x1="99.06" y1="195.58" x2="101.6" y2="198.12" width="0.762" layer="92"/>
<wire x1="101.6" y1="198.12" x2="198.12" y2="198.12" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="LED[0..4]">
<segment>
<wire x1="93.98" y1="127" x2="93.98" y2="139.7" width="0.762" layer="92"/>
<wire x1="93.98" y1="139.7" x2="91.44" y2="142.24" width="0.762" layer="92"/>
<wire x1="91.44" y1="142.24" x2="58.42" y2="142.24" width="0.762" layer="92"/>
<label x="58.42" y="144.526" size="1.778" layer="95" rot="MR180"/>
</segment>
</bus>
<bus name="BTN[0..4]">
<segment>
<wire x1="226.06" y1="119.38" x2="213.36" y2="119.38" width="0.762" layer="92"/>
<wire x1="213.36" y1="119.38" x2="210.82" y2="121.92" width="0.762" layer="92"/>
<wire x1="210.82" y1="121.92" x2="210.82" y2="132.08" width="0.762" layer="92"/>
<label x="226.06" y="119.888" size="1.778" layer="95" rot="MR0"/>
</segment>
</bus>
<bus name="USB:D_N,D_P,VBUS_DET">
<segment>
<wire x1="213.36" y1="170.18" x2="213.36" y2="165.1" width="0.762" layer="92"/>
<wire x1="213.36" y1="165.1" x2="215.9" y2="162.56" width="0.762" layer="92"/>
<wire x1="215.9" y1="162.56" x2="241.3" y2="162.56" width="0.762" layer="92"/>
<label x="241.3" y="164.846" size="1.778" layer="95" rot="R180"/>
</segment>
</bus>
<bus name="PRINTER:RXD,TXD,CTS,RTS">
<segment>
<wire x1="213.36" y1="177.8" x2="213.36" y2="185.42" width="0.762" layer="92"/>
<wire x1="213.36" y1="185.42" x2="215.9" y2="187.96" width="0.762" layer="92"/>
<wire x1="215.9" y1="187.96" x2="241.3" y2="187.96" width="0.762" layer="92"/>
<label x="241.3" y="190.5" size="1.778" layer="95" rot="R180"/>
</segment>
</bus>
<bus name="SD:SD_SDI,SD_SDO,SD_SCK,SD_CS,SD_CD">
<segment>
<wire x1="238.76" y1="157.48" x2="238.76" y2="137.16" width="0.762" layer="92"/>
<wire x1="238.76" y1="137.16" x2="241.3" y2="134.62" width="0.762" layer="92"/>
<wire x1="241.3" y1="134.62" x2="251.46" y2="134.62" width="0.762" layer="92"/>
<label x="251.46" y="136.906" size="1.778" layer="95" rot="R180"/>
</segment>
</bus>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="170.18" y1="76.2" x2="170.18" y2="78.74" width="0.1524" layer="91"/>
<wire x1="170.18" y1="78.74" x2="170.18" y2="83.82" width="0.1524" layer="91"/>
<wire x1="172.72" y1="83.82" x2="172.72" y2="78.74" width="0.1524" layer="91"/>
<wire x1="172.72" y1="78.74" x2="170.18" y2="78.74" width="0.1524" layer="91"/>
<junction x="170.18" y="78.74"/>
<wire x1="170.18" y1="78.74" x2="167.64" y2="78.74" width="0.1524" layer="91"/>
<wire x1="167.64" y1="78.74" x2="167.64" y2="83.82" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="GND@1"/>
<pinref part="D2" gate="G$1" pin="GND@2"/>
<pinref part="D2" gate="G$1" pin="GND@3"/>
</segment>
<segment>
<pinref part="C8" gate="G$1" pin="2"/>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="200.66" y1="86.36" x2="200.66" y2="78.74" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C9" gate="G$1" pin="2"/>
<pinref part="GND12" gate="1" pin="GND"/>
<wire x1="210.82" y1="86.36" x2="210.82" y2="78.74" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C10" gate="G$1" pin="2"/>
<pinref part="GND13" gate="1" pin="GND"/>
<wire x1="220.98" y1="86.36" x2="220.98" y2="78.74" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C11" gate="G$1" pin="2"/>
<pinref part="GND14" gate="1" pin="GND"/>
<wire x1="231.14" y1="86.36" x2="231.14" y2="78.74" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C12" gate="G$1" pin="2"/>
<pinref part="GND16" gate="1" pin="GND"/>
<wire x1="241.3" y1="86.36" x2="241.3" y2="78.74" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND35" gate="1" pin="GND"/>
<pinref part="C13" gate="G$1" pin="2"/>
<wire x1="261.62" y1="78.74" x2="261.62" y2="86.36" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND36" gate="1" pin="GND"/>
<pinref part="C14" gate="G$1" pin="2"/>
<wire x1="281.94" y1="78.74" x2="281.94" y2="86.36" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND25" gate="1" pin="GND"/>
<wire x1="162.56" y1="76.2" x2="162.56" y2="78.74" width="0.1524" layer="91"/>
<wire x1="165.1" y1="83.82" x2="165.1" y2="78.74" width="0.1524" layer="91"/>
<wire x1="162.56" y1="78.74" x2="165.1" y2="78.74" width="0.1524" layer="91"/>
<wire x1="162.56" y1="78.74" x2="162.56" y2="83.82" width="0.1524" layer="91"/>
<junction x="162.56" y="78.74"/>
<wire x1="162.56" y1="78.74" x2="160.02" y2="78.74" width="0.1524" layer="91"/>
<wire x1="160.02" y1="78.74" x2="160.02" y2="83.82" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="GND@4"/>
<pinref part="D2" gate="G$1" pin="GND@5"/>
<pinref part="D2" gate="G$1" pin="GND@6"/>
<wire x1="157.48" y1="83.82" x2="157.48" y2="78.74" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="GND@PAD"/>
<wire x1="160.02" y1="78.74" x2="157.48" y2="78.74" width="0.1524" layer="91"/>
<junction x="160.02" y="78.74"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="6"/>
<wire x1="20.32" y1="35.56" x2="17.78" y2="35.56" width="0.1524" layer="91"/>
<pinref part="GND21" gate="1" pin="GND"/>
<wire x1="17.78" y1="35.56" x2="17.78" y2="33.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C3" gate="G$1" pin="2"/>
<pinref part="GND48" gate="1" pin="GND"/>
<wire x1="119.38" y1="226.06" x2="119.38" y2="220.98" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="134.62" y1="220.98" x2="134.62" y2="231.14" width="0.1524" layer="91"/>
<pinref part="D1" gate="G$1" pin="GND"/>
<pinref part="GND53" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="2"/>
<pinref part="GND54" gate="1" pin="GND"/>
<wire x1="149.86" y1="226.06" x2="149.86" y2="220.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C5" gate="G$1" pin="2"/>
<pinref part="GND59" gate="1" pin="GND"/>
<wire x1="162.56" y1="226.06" x2="162.56" y2="220.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND60" gate="1" pin="GND"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="96.52" y1="220.98" x2="96.52" y2="226.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND67" gate="1" pin="GND"/>
<wire x1="30.48" y1="223.52" x2="30.48" y2="228.6" width="0.1524" layer="91"/>
<pinref part="X1" gate="G$1" pin="1"/>
<wire x1="30.48" y1="228.6" x2="33.02" y2="228.6" width="0.1524" layer="91"/>
<pinref part="X1" gate="G$1" pin="3"/>
<wire x1="33.02" y1="231.14" x2="30.48" y2="231.14" width="0.1524" layer="91"/>
<wire x1="30.48" y1="231.14" x2="30.48" y2="228.6" width="0.1524" layer="91"/>
<junction x="30.48" y="228.6"/>
<pinref part="X1" gate="G$1" pin="5"/>
<wire x1="33.02" y1="233.68" x2="30.48" y2="233.68" width="0.1524" layer="91"/>
<wire x1="30.48" y1="233.68" x2="30.48" y2="231.14" width="0.1524" layer="91"/>
<junction x="30.48" y="231.14"/>
<pinref part="X1" gate="G$1" pin="7"/>
<wire x1="33.02" y1="236.22" x2="30.48" y2="236.22" width="0.1524" layer="91"/>
<wire x1="30.48" y1="236.22" x2="30.48" y2="233.68" width="0.1524" layer="91"/>
<junction x="30.48" y="233.68"/>
<pinref part="X1" gate="G$1" pin="9"/>
<wire x1="33.02" y1="238.76" x2="30.48" y2="238.76" width="0.1524" layer="91"/>
<wire x1="30.48" y1="238.76" x2="30.48" y2="236.22" width="0.1524" layer="91"/>
<junction x="30.48" y="236.22"/>
</segment>
<segment>
<pinref part="GND68" gate="1" pin="GND"/>
<wire x1="96.52" y1="106.68" x2="96.52" y2="104.14" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="GND69" gate="1" pin="GND"/>
<pinref part="C6" gate="G$1" pin="2"/>
<wire x1="78.74" y1="104.14" x2="78.74" y2="106.68" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="GND"/>
<wire x1="304.8" y1="144.78" x2="302.26" y2="144.78" width="0.1524" layer="91"/>
<pinref part="GND76" gate="1" pin="GND"/>
<wire x1="302.26" y1="144.78" x2="302.26" y2="139.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND38" gate="1" pin="GND"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="109.22" y1="220.98" x2="109.22" y2="226.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+3.3V" class="0">
<segment>
<wire x1="198.12" y1="96.52" x2="200.66" y2="96.52" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="200.66" y1="96.52" x2="200.66" y2="93.98" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="198.12" y1="99.06" x2="210.82" y2="99.06" width="0.1524" layer="91"/>
<wire x1="210.82" y1="99.06" x2="210.82" y2="96.52" width="0.1524" layer="91"/>
<wire x1="210.82" y1="96.52" x2="210.82" y2="93.98" width="0.1524" layer="91"/>
<wire x1="200.66" y1="96.52" x2="210.82" y2="96.52" width="0.1524" layer="91"/>
<junction x="200.66" y="96.52"/>
<junction x="210.82" y="96.52"/>
<wire x1="198.12" y1="101.6" x2="220.98" y2="101.6" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="220.98" y1="101.6" x2="220.98" y2="96.52" width="0.1524" layer="91"/>
<wire x1="220.98" y1="96.52" x2="220.98" y2="93.98" width="0.1524" layer="91"/>
<wire x1="210.82" y1="96.52" x2="220.98" y2="96.52" width="0.1524" layer="91"/>
<junction x="220.98" y="96.52"/>
<wire x1="198.12" y1="104.14" x2="231.14" y2="104.14" width="0.1524" layer="91"/>
<wire x1="231.14" y1="104.14" x2="231.14" y2="96.52" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="231.14" y1="96.52" x2="231.14" y2="93.98" width="0.1524" layer="91"/>
<wire x1="220.98" y1="96.52" x2="231.14" y2="96.52" width="0.1524" layer="91"/>
<junction x="231.14" y="96.52"/>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="198.12" y1="106.68" x2="241.3" y2="106.68" width="0.1524" layer="91"/>
<wire x1="241.3" y1="106.68" x2="241.3" y2="96.52" width="0.1524" layer="91"/>
<wire x1="241.3" y1="96.52" x2="241.3" y2="93.98" width="0.1524" layer="91"/>
<wire x1="231.14" y1="96.52" x2="241.3" y2="96.52" width="0.1524" layer="91"/>
<junction x="241.3" y="96.52"/>
<pinref part="P+5" gate="1" pin="+3.3V"/>
<wire x1="241.3" y1="106.68" x2="241.3" y2="109.22" width="0.1524" layer="91"/>
<junction x="241.3" y="106.68"/>
<pinref part="D2" gate="G$1" pin="VCC@1"/>
<pinref part="D2" gate="G$1" pin="VCC@2"/>
<pinref part="D2" gate="G$1" pin="VCC@3"/>
<pinref part="D2" gate="G$1" pin="VCC@4"/>
<pinref part="D2" gate="G$1" pin="VCC@5"/>
</segment>
<segment>
<pinref part="C14" gate="G$1" pin="1"/>
<wire x1="281.94" y1="93.98" x2="281.94" y2="99.06" width="0.1524" layer="91"/>
<pinref part="L4" gate="G$1" pin="2"/>
<wire x1="281.94" y1="99.06" x2="276.86" y2="99.06" width="0.1524" layer="91"/>
<pinref part="P+6" gate="1" pin="+3.3V"/>
<wire x1="281.94" y1="99.06" x2="281.94" y2="101.6" width="0.1524" layer="91"/>
<junction x="281.94" y="99.06"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="2"/>
<wire x1="20.32" y1="40.64" x2="17.78" y2="40.64" width="0.1524" layer="91"/>
<pinref part="P+7" gate="1" pin="+3.3V"/>
<wire x1="17.78" y1="40.64" x2="17.78" y2="43.18" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="162.56" y1="238.76" x2="149.86" y2="238.76" width="0.1524" layer="91"/>
<wire x1="149.86" y1="238.76" x2="142.24" y2="238.76" width="0.1524" layer="91"/>
<wire x1="149.86" y1="238.76" x2="149.86" y2="233.68" width="0.1524" layer="91"/>
<junction x="149.86" y="238.76"/>
<pinref part="C4" gate="G$1" pin="1"/>
<pinref part="D1" gate="G$1" pin="OUT"/>
<wire x1="162.56" y1="241.3" x2="162.56" y2="238.76" width="0.1524" layer="91"/>
<wire x1="162.56" y1="233.68" x2="162.56" y2="238.76" width="0.1524" layer="91"/>
<junction x="162.56" y="238.76"/>
<pinref part="C5" gate="G$1" pin="1"/>
<pinref part="P+14" gate="1" pin="+3.3V"/>
</segment>
</net>
<net name="PDI-CLOCK" class="0">
<segment>
<wire x1="129.54" y1="106.68" x2="104.14" y2="106.68" width="0.1524" layer="91"/>
<label x="104.14" y="106.934" size="1.524" layer="95"/>
<pinref part="D2" gate="G$1" pin="PDI-CLK/!RESET"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="5"/>
<wire x1="50.8" y1="35.56" x2="35.56" y2="35.56" width="0.1524" layer="91"/>
<label x="50.8" y="35.814" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="PDI-DATA" class="0">
<segment>
<wire x1="129.54" y1="111.76" x2="104.14" y2="111.76" width="0.1524" layer="91"/>
<label x="104.14" y="112.014" size="1.524" layer="95"/>
<pinref part="D2" gate="G$1" pin="PDI-DATA"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="1"/>
<wire x1="50.8" y1="40.64" x2="35.56" y2="40.64" width="0.1524" layer="91"/>
<label x="50.8" y="40.894" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="L4" gate="G$1" pin="1"/>
<wire x1="261.62" y1="99.06" x2="266.7" y2="99.06" width="0.1524" layer="91"/>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="261.62" y1="93.98" x2="261.62" y2="99.06" width="0.1524" layer="91"/>
<junction x="261.62" y="99.06"/>
<wire x1="246.38" y1="99.06" x2="261.62" y2="99.06" width="0.1524" layer="91"/>
<wire x1="246.38" y1="114.3" x2="246.38" y2="99.06" width="0.1524" layer="91"/>
<wire x1="236.22" y1="114.3" x2="246.38" y2="114.3" width="0.1524" layer="91"/>
<wire x1="236.22" y1="111.76" x2="236.22" y2="114.3" width="0.1524" layer="91"/>
<wire x1="198.12" y1="111.76" x2="236.22" y2="111.76" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="AVCC"/>
</segment>
</net>
<net name="D_P" class="2">
<segment>
<pinref part="D2" gate="G$1" pin="PD7(TXD1/SCK/D+)"/>
<wire x1="198.12" y1="167.64" x2="210.82" y2="167.64" width="0.1524" layer="91"/>
<label x="198.374" y="167.894" size="1.778" layer="95"/>
<wire x1="210.82" y1="167.64" x2="213.36" y2="165.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VBUS_DET" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="PD5(OC1B/XCK1/MOSI)"/>
<wire x1="210.82" y1="172.72" x2="198.12" y2="172.72" width="0.1524" layer="91"/>
<label x="198.374" y="172.974" size="1.778" layer="95"/>
<wire x1="210.82" y1="172.72" x2="213.36" y2="170.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TEMP_REFERENCE" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="PB2(ADC2/DAC0)"/>
<wire x1="129.54" y1="157.48" x2="96.52" y2="157.48" width="0.1524" layer="91"/>
<wire x1="96.52" y1="157.48" x2="93.98" y2="154.94" width="0.1524" layer="91"/>
<label x="129.54" y="159.766" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="D2" gate="G$1" pin="PA0(ADC0/AREFA)"/>
<wire x1="129.54" y1="187.96" x2="96.52" y2="187.96" width="0.1524" layer="91"/>
<wire x1="96.52" y1="187.96" x2="93.98" y2="185.42" width="0.1524" layer="91"/>
<label x="129.54" y="190.246" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="TEMP_NEG" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="PA4(ADC4)"/>
<wire x1="129.54" y1="177.8" x2="96.52" y2="177.8" width="0.1524" layer="91"/>
<wire x1="96.52" y1="177.8" x2="93.98" y2="175.26" width="0.1524" layer="91"/>
<label x="129.54" y="180.086" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="TEMP_POS2" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="PA3(ADC3)"/>
<wire x1="129.54" y1="180.34" x2="96.52" y2="180.34" width="0.1524" layer="91"/>
<wire x1="96.52" y1="180.34" x2="93.98" y2="177.8" width="0.1524" layer="91"/>
<label x="129.54" y="182.626" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="TEMP_POS" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="PA2(ADC2)"/>
<wire x1="129.54" y1="182.88" x2="96.52" y2="182.88" width="0.1524" layer="91"/>
<wire x1="96.52" y1="182.88" x2="93.98" y2="180.34" width="0.1524" layer="91"/>
<label x="129.54" y="185.166" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="TEMP_NEG_EN" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="PA1(ADC1)"/>
<wire x1="129.54" y1="185.42" x2="96.52" y2="185.42" width="0.1524" layer="91"/>
<wire x1="96.52" y1="185.42" x2="93.98" y2="182.88" width="0.1524" layer="91"/>
<label x="129.54" y="187.706" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="TEMP_SENSE_EN" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="PA5(ADC5)"/>
<wire x1="129.54" y1="175.26" x2="96.52" y2="175.26" width="0.1524" layer="91"/>
<wire x1="96.52" y1="175.26" x2="93.98" y2="172.72" width="0.1524" layer="91"/>
<label x="129.54" y="177.546" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="TEMP_100_EN" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="PA6(ADC6/AC1-OUT)"/>
<wire x1="129.54" y1="172.72" x2="96.52" y2="172.72" width="0.1524" layer="91"/>
<wire x1="96.52" y1="172.72" x2="93.98" y2="170.18" width="0.1524" layer="91"/>
<label x="129.54" y="175.006" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="TEMP_180_EN" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="PA7(ADC7/AC0-OUT)"/>
<wire x1="129.54" y1="170.18" x2="96.52" y2="170.18" width="0.1524" layer="91"/>
<wire x1="96.52" y1="170.18" x2="93.98" y2="167.64" width="0.1524" layer="91"/>
<label x="129.54" y="172.466" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="SSR" class="0">
<segment>
<wire x1="55.88" y1="228.6" x2="55.88" y2="190.5" width="0.1524" layer="91"/>
<label x="48.26" y="230.632" size="1.778" layer="95" rot="MR180"/>
<wire x1="55.88" y1="190.5" x2="55.88" y2="124.46" width="0.1524" layer="91"/>
<wire x1="63.5" y1="121.92" x2="55.88" y2="121.92" width="0.1524" layer="91"/>
<wire x1="55.88" y1="121.92" x2="55.88" y2="124.46" width="0.1524" layer="91"/>
<pinref part="X1" gate="G$1" pin="2"/>
<wire x1="48.26" y1="228.6" x2="55.88" y2="228.6" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="ZC" class="0">
<segment>
<wire x1="53.34" y1="142.24" x2="53.34" y2="231.14" width="0.1524" layer="91"/>
<label x="48.514" y="233.172" size="1.778" layer="95" rot="MR180"/>
<wire x1="83.82" y1="119.38" x2="53.34" y2="119.38" width="0.1524" layer="91"/>
<wire x1="53.34" y1="119.38" x2="53.34" y2="142.24" width="0.1524" layer="91"/>
<pinref part="L3" gate="G$1" pin="1"/>
<pinref part="X1" gate="G$1" pin="4"/>
<wire x1="48.26" y1="231.14" x2="53.34" y2="231.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TXD" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="PD3(OC0D/TXD0)"/>
<wire x1="198.12" y1="177.8" x2="210.82" y2="177.8" width="0.1524" layer="91"/>
<label x="198.374" y="178.054" size="1.778" layer="95"/>
<wire x1="210.82" y1="177.8" x2="213.36" y2="180.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RXD" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="PD2(OC0C/RXD0)"/>
<wire x1="198.12" y1="180.34" x2="210.82" y2="180.34" width="0.1524" layer="91"/>
<label x="198.374" y="180.594" size="1.778" layer="95"/>
<wire x1="210.82" y1="180.34" x2="213.36" y2="182.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CTS" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="PD1(OC0B/XCK0)"/>
<wire x1="198.12" y1="182.88" x2="210.82" y2="182.88" width="0.1524" layer="91"/>
<label x="198.374" y="183.134" size="1.778" layer="95"/>
<wire x1="210.82" y1="182.88" x2="213.36" y2="185.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="96.52" y1="233.68" x2="96.52" y2="238.76" width="0.1524" layer="91"/>
<pinref part="D1" gate="G$1" pin="IN"/>
<wire x1="127" y1="238.76" x2="119.38" y2="238.76" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="119.38" y1="238.76" x2="119.38" y2="233.68" width="0.1524" layer="91"/>
<wire x1="119.38" y1="238.76" x2="109.22" y2="238.76" width="0.1524" layer="91"/>
<junction x="119.38" y="238.76"/>
<junction x="96.52" y="238.76"/>
<wire x1="109.22" y1="238.76" x2="96.52" y2="238.76" width="0.1524" layer="91"/>
<wire x1="88.9" y1="238.76" x2="96.52" y2="238.76" width="0.1524" layer="91"/>
<pinref part="L1" gate="G$1" pin="2"/>
<junction x="96.52" y="238.76"/>
<pinref part="P+4" gate="1" pin="+5V"/>
<wire x1="96.52" y1="241.3" x2="96.52" y2="238.76" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="109.22" y1="233.68" x2="109.22" y2="238.76" width="0.1524" layer="91"/>
<junction x="109.22" y="238.76"/>
</segment>
</net>
<net name="LED0" class="0">
<segment>
<wire x1="96.52" y1="137.16" x2="93.98" y2="139.7" width="0.1524" layer="91"/>
<label x="129.286" y="137.414" size="1.778" layer="95" rot="MR0"/>
<pinref part="D2" gate="G$1" pin="PC0(OC0A/SDA)"/>
<wire x1="96.52" y1="137.16" x2="129.54" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LED1" class="0">
<segment>
<wire x1="96.52" y1="134.62" x2="93.98" y2="137.16" width="0.1524" layer="91"/>
<label x="129.286" y="134.874" size="1.778" layer="95" rot="MR0"/>
<pinref part="D2" gate="G$1" pin="PC1(OC0B/XCK0/SCL)"/>
<wire x1="96.52" y1="134.62" x2="129.54" y2="134.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LED2" class="0">
<segment>
<wire x1="96.52" y1="132.08" x2="93.98" y2="134.62" width="0.1524" layer="91"/>
<label x="129.286" y="132.334" size="1.778" layer="95" rot="MR0"/>
<pinref part="D2" gate="G$1" pin="PC2(OC0C/RXD0)"/>
<wire x1="96.52" y1="132.08" x2="129.54" y2="132.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LED3" class="0">
<segment>
<wire x1="96.52" y1="129.54" x2="93.98" y2="132.08" width="0.1524" layer="91"/>
<label x="129.286" y="129.794" size="1.778" layer="95" rot="MR0"/>
<pinref part="D2" gate="G$1" pin="PC3(OC0D/TXD0)"/>
<wire x1="96.52" y1="129.54" x2="129.54" y2="129.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LED4" class="0">
<segment>
<wire x1="96.52" y1="124.46" x2="93.98" y2="127" width="0.1524" layer="91"/>
<label x="129.286" y="126.492" size="1.778" layer="95" rot="R180"/>
<pinref part="D2" gate="G$1" pin="PC5(OC1B/MOSI/XCK1)"/>
<wire x1="96.52" y1="124.46" x2="129.54" y2="124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ZC-FILT" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="PC7(SCK/TXD1/CLKO/EVO)"/>
<wire x1="129.54" y1="119.38" x2="96.52" y2="119.38" width="0.1524" layer="91"/>
<label x="129.286" y="121.412" size="1.778" layer="95" rot="R180"/>
<wire x1="96.52" y1="119.38" x2="93.98" y2="119.38" width="0.1524" layer="91"/>
<wire x1="96.52" y1="116.84" x2="96.52" y2="119.38" width="0.1524" layer="91"/>
<junction x="96.52" y="119.38"/>
<pinref part="L3" gate="G$1" pin="2"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="96.52" y1="114.3" x2="96.52" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SSR-FILT" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="PC6(MISO/RXD1)"/>
<wire x1="129.54" y1="121.92" x2="93.98" y2="121.92" width="0.1524" layer="91"/>
<pinref part="L2" gate="G$1" pin="2"/>
<label x="129.286" y="123.952" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="BTN0" class="0">
<segment>
<wire x1="208.28" y1="134.62" x2="210.82" y2="132.08" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="PF0(OC0A)"/>
<wire x1="198.12" y1="134.62" x2="208.28" y2="134.62" width="0.1524" layer="91"/>
<label x="198.374" y="134.874" size="1.778" layer="95"/>
</segment>
</net>
<net name="BTN1" class="0">
<segment>
<wire x1="208.28" y1="132.08" x2="210.82" y2="129.54" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="PF1(OC0B/XCK0)"/>
<wire x1="198.12" y1="132.08" x2="208.28" y2="132.08" width="0.1524" layer="91"/>
<label x="198.374" y="132.334" size="1.778" layer="95"/>
</segment>
</net>
<net name="BTN2" class="0">
<segment>
<wire x1="208.28" y1="129.54" x2="210.82" y2="127" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="PF2(OC0C/RXD0)"/>
<wire x1="198.12" y1="129.54" x2="208.28" y2="129.54" width="0.1524" layer="91"/>
<label x="198.374" y="129.794" size="1.778" layer="95"/>
</segment>
</net>
<net name="BTN3" class="0">
<segment>
<wire x1="208.28" y1="127" x2="210.82" y2="124.46" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="PF3(OC0D/TXD0)"/>
<wire x1="198.12" y1="127" x2="208.28" y2="127" width="0.1524" layer="91"/>
<label x="198.374" y="127.254" size="1.778" layer="95"/>
</segment>
</net>
<net name="BTN4" class="0">
<segment>
<wire x1="208.28" y1="124.46" x2="210.82" y2="121.92" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="PF4"/>
<wire x1="198.12" y1="124.46" x2="208.28" y2="124.46" width="0.1524" layer="91"/>
<label x="198.374" y="124.714" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="PE3(OC0D/TXD0)"/>
<wire x1="198.12" y1="152.4" x2="241.3" y2="152.4" width="0.1524" layer="91"/>
<wire x1="241.3" y1="152.4" x2="259.08" y2="152.4" width="0.1524" layer="91"/>
<wire x1="259.08" y1="152.4" x2="292.1" y2="152.4" width="0.1524" layer="91"/>
<wire x1="292.1" y1="152.4" x2="294.64" y2="154.94" width="0.1524" layer="91"/>
<pinref part="X2" gate="1" pin="RXD"/>
<wire x1="294.64" y1="154.94" x2="304.8" y2="154.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="X2" gate="1" pin="TXD"/>
<wire x1="304.8" y1="152.4" x2="294.64" y2="152.4" width="0.1524" layer="91"/>
<wire x1="294.64" y1="152.4" x2="292.1" y2="154.94" width="0.1524" layer="91"/>
<wire x1="292.1" y1="154.94" x2="261.62" y2="154.94" width="0.1524" layer="91"/>
<wire x1="261.62" y1="154.94" x2="243.84" y2="154.94" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="PE2(OC0C/RXD0)"/>
<wire x1="243.84" y1="154.94" x2="198.12" y2="154.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="X2" gate="1" pin="!CTS"/>
<wire x1="304.8" y1="147.32" x2="302.26" y2="147.32" width="0.1524" layer="91"/>
<wire x1="302.26" y1="147.32" x2="302.26" y2="149.86" width="0.1524" layer="91"/>
<wire x1="302.26" y1="149.86" x2="302.26" y2="157.48" width="0.1524" layer="91"/>
<pinref part="X2" gate="1" pin="!RTS"/>
<wire x1="302.26" y1="157.48" x2="304.8" y2="157.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RTS" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="PD4(OC1A/!SS!)"/>
<wire x1="198.12" y1="175.26" x2="210.82" y2="175.26" width="0.1524" layer="91"/>
<label x="198.374" y="175.514" size="1.778" layer="95"/>
<wire x1="210.82" y1="175.26" x2="213.36" y2="177.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="D_N" class="2">
<segment>
<pinref part="D2" gate="G$1" pin="PD6(RXD1/MISO/D-)"/>
<wire x1="198.12" y1="170.18" x2="210.82" y2="170.18" width="0.1524" layer="91"/>
<label x="198.374" y="170.434" size="1.778" layer="95"/>
<wire x1="210.82" y1="170.18" x2="213.36" y2="167.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="5V_IN" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="8"/>
<pinref part="L1" gate="G$1" pin="1"/>
<wire x1="78.74" y1="238.76" x2="55.88" y2="238.76" width="0.1524" layer="91"/>
<pinref part="X1" gate="G$1" pin="10"/>
<wire x1="48.26" y1="238.76" x2="55.88" y2="238.76" width="0.1524" layer="91"/>
<junction x="55.88" y="238.76"/>
<wire x1="55.88" y1="238.76" x2="55.88" y2="236.22" width="0.1524" layer="91"/>
<wire x1="55.88" y1="236.22" x2="48.26" y2="236.22" width="0.1524" layer="91"/>
<junction x="55.88" y="236.22"/>
<wire x1="55.88" y1="233.68" x2="55.88" y2="236.22" width="0.1524" layer="91"/>
<pinref part="X1" gate="G$1" pin="6"/>
<wire x1="48.26" y1="233.68" x2="55.88" y2="233.68" width="0.1524" layer="91"/>
<label x="48.514" y="240.792" size="1.778" layer="95" rot="MR180"/>
<label x="48.514" y="238.252" size="1.778" layer="95" rot="MR180"/>
<label x="48.514" y="235.712" size="1.778" layer="95" rot="MR180"/>
</segment>
</net>
<net name="SD_CS" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="PE4(OC1A/!SS!)"/>
<wire x1="198.12" y1="149.86" x2="236.22" y2="149.86" width="0.1524" layer="91"/>
<wire x1="236.22" y1="149.86" x2="238.76" y2="147.32" width="0.1524" layer="91"/>
<label x="236.22" y="151.892" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="SD_SDI" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="PE5(OC1B/SCK1/MOSI)"/>
<wire x1="198.12" y1="147.32" x2="236.22" y2="147.32" width="0.1524" layer="91"/>
<wire x1="236.22" y1="147.32" x2="238.76" y2="144.78" width="0.1524" layer="91"/>
<label x="236.22" y="149.352" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="SD_SDO" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="PE6(RSC1/MISO/TOSC2)"/>
<wire x1="198.12" y1="144.78" x2="236.22" y2="144.78" width="0.1524" layer="91"/>
<wire x1="236.22" y1="144.78" x2="238.76" y2="142.24" width="0.1524" layer="91"/>
<label x="236.22" y="146.812" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="SD_SCK" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="PE7(TSD1/SCK/CLKO/EVO/TOSC1)"/>
<wire x1="198.12" y1="142.24" x2="236.22" y2="142.24" width="0.1524" layer="91"/>
<wire x1="236.22" y1="142.24" x2="238.76" y2="139.7" width="0.1524" layer="91"/>
<label x="236.22" y="144.272" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="SD_CD" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="PE0(OC0A/SDA)"/>
<wire x1="198.12" y1="160.02" x2="236.22" y2="160.02" width="0.1524" layer="91"/>
<wire x1="236.22" y1="160.02" x2="238.76" y2="157.48" width="0.1524" layer="91"/>
<label x="236.22" y="162.052" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="BL-PWM" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="PC4(OC1A/!SS!)"/>
<wire x1="129.54" y1="127" x2="99.06" y2="127" width="0.1524" layer="91"/>
<label x="129.286" y="129.032" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DB7" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="PB7(ADC7/AC0-OUT/TDO)"/>
<wire x1="129.54" y1="144.78" x2="101.6" y2="144.78" width="0.1524" layer="91"/>
<wire x1="101.6" y1="144.78" x2="99.06" y2="142.24" width="0.1524" layer="91"/>
<label x="129.54" y="146.812" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DB6" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="PB6(ADC6/TCK)"/>
<wire x1="129.54" y1="147.32" x2="101.6" y2="147.32" width="0.1524" layer="91"/>
<wire x1="101.6" y1="147.32" x2="99.06" y2="144.78" width="0.1524" layer="91"/>
<label x="129.54" y="149.352" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DB5" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="PB5(ADC5/TDI)"/>
<wire x1="129.54" y1="149.86" x2="101.6" y2="149.86" width="0.1524" layer="91"/>
<wire x1="101.6" y1="149.86" x2="99.06" y2="147.32" width="0.1524" layer="91"/>
<label x="129.54" y="151.892" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="DB4" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="PB4(ADC4/TMS)"/>
<wire x1="129.54" y1="152.4" x2="101.6" y2="152.4" width="0.1524" layer="91"/>
<wire x1="101.6" y1="152.4" x2="99.06" y2="149.86" width="0.1524" layer="91"/>
<label x="129.54" y="154.432" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="E" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="PB3(ADC3/DAC1)"/>
<wire x1="129.54" y1="154.94" x2="101.6" y2="154.94" width="0.1524" layer="91"/>
<wire x1="101.6" y1="154.94" x2="99.06" y2="152.4" width="0.1524" layer="91"/>
<label x="129.54" y="156.972" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="RW" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="PB1(ADC1)"/>
<wire x1="129.54" y1="160.02" x2="99.06" y2="160.02" width="0.1524" layer="91"/>
<label x="129.54" y="162.052" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="RS" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="PB0(ADC0/AREFB)"/>
<wire x1="129.54" y1="162.56" x2="99.06" y2="162.56" width="0.1524" layer="91"/>
<label x="129.54" y="164.592" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="BUZZER" class="0">
<segment>
<pinref part="D2" gate="G$1" pin="PE1(OC0B/XCK0/SCL)"/>
<wire x1="198.12" y1="157.48" x2="213.36" y2="157.48" width="0.1524" layer="91"/>
<label x="213.36" y="159.512" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="V0-PWM" class="0">
<segment>
<wire x1="198.12" y1="198.12" x2="200.66" y2="195.58" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="PD0(OC0A)"/>
<wire x1="198.12" y1="185.42" x2="200.66" y2="185.42" width="0.1524" layer="91"/>
<wire x1="200.66" y1="195.58" x2="200.66" y2="185.42" width="0.1524" layer="91"/>
<label x="200.406" y="185.674" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<pinref part="L2" gate="G$1" pin="1"/>
<wire x1="83.82" y1="121.92" x2="78.74" y2="121.92" width="0.1524" layer="91"/>
<wire x1="78.74" y1="121.92" x2="73.66" y2="121.92" width="0.1524" layer="91"/>
<junction x="78.74" y="121.92"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="78.74" y1="114.3" x2="78.74" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="4.572" y="13.716" size="1.778" layer="97">1.0</text>
<text x="13.716" y="13.716" size="1.778" layer="97">Inital Revision</text>
<text x="38.354" y="13.716" size="1.778" layer="97">18.04.14</text>
<wire x1="73.66" y1="185.42" x2="162.56" y2="185.42" width="0.1524" layer="97" style="longdash"/>
<wire x1="162.56" y1="185.42" x2="182.88" y2="185.42" width="0.1524" layer="97" style="longdash"/>
<wire x1="182.88" y1="185.42" x2="228.6" y2="185.42" width="0.1524" layer="97" style="longdash"/>
<wire x1="228.6" y1="185.42" x2="256.54" y2="185.42" width="0.1524" layer="97" style="longdash"/>
<wire x1="256.54" y1="185.42" x2="292.1" y2="185.42" width="0.1524" layer="97" style="longdash"/>
<wire x1="73.66" y1="101.6" x2="162.56" y2="101.6" width="0.1524" layer="97" style="longdash"/>
<wire x1="162.56" y1="101.6" x2="182.88" y2="101.6" width="0.1524" layer="97" style="longdash"/>
<wire x1="182.88" y1="101.6" x2="228.6" y2="101.6" width="0.1524" layer="97" style="longdash"/>
<wire x1="228.6" y1="101.6" x2="256.54" y2="101.6" width="0.1524" layer="97" style="longdash"/>
<wire x1="256.54" y1="101.6" x2="292.1" y2="101.6" width="0.1524" layer="97" style="longdash"/>
<wire x1="73.66" y1="185.42" x2="73.66" y2="101.6" width="0.1524" layer="97" style="longdash"/>
<wire x1="292.1" y1="185.42" x2="292.1" y2="101.6" width="0.1524" layer="97" style="longdash"/>
<wire x1="40.64" y1="185.42" x2="73.66" y2="185.42" width="0.1524" layer="97" style="longdash"/>
<wire x1="40.64" y1="185.42" x2="40.64" y2="101.6" width="0.1524" layer="97" style="longdash"/>
<wire x1="40.64" y1="101.6" x2="73.66" y2="101.6" width="0.1524" layer="97" style="longdash"/>
<text x="43.18" y="182.88" size="1.778" layer="97" align="top-left">Input Connector</text>
<wire x1="256.54" y1="185.42" x2="256.54" y2="101.6" width="0.1524" layer="97" style="longdash"/>
<text x="261.62" y="182.88" size="1.778" layer="97" align="top-left">Lowpass filtering</text>
<wire x1="228.6" y1="185.42" x2="228.6" y2="101.6" width="0.1524" layer="97" style="longdash"/>
<text x="233.68" y="182.88" size="1.778" layer="97" align="top-left">Reference Bridge</text>
<wire x1="182.88" y1="185.42" x2="182.88" y2="101.6" width="0.1524" layer="97" style="longdash"/>
<text x="195.58" y="182.88" size="1.778" layer="97" align="top-left">Calibration Resistors</text>
<text x="167.64" y="182.88" size="1.778" layer="97" align="top-left">Sensing</text>
<wire x1="162.56" y1="185.42" x2="162.56" y2="101.6" width="0.1524" layer="97" style="longdash"/>
<text x="101.6" y="182.88" size="1.778" layer="97" align="top-left">ESD Protection (with HF suppression)</text>
<text x="25.4" y="144.78" size="3.81" layer="97" rot="R180" align="center">PT100</text>
<text x="43.18" y="180.34" size="1.27" layer="97" align="top-left">We use the same 
connector as used 
for mains wiring 
as they are cheap</text>
</plain>
<instances>
<instance part="U$2" gate="G$1" x="0" y="0">
<attribute name="CREATE_DATE" x="0" y="0" size="1.778" layer="96" display="off"/>
<attribute name="EDIT_DATE" x="0" y="0" size="1.778" layer="96" display="off"/>
<attribute name="USERNAME" x="0" y="0" size="1.778" layer="96" display="off"/>
<attribute name="FILENAME" x="0" y="0" size="1.778" layer="96" display="off"/>
<attribute name="SUBTITLE" x="0" y="0" size="1.778" layer="96" display="off"/>
<attribute name="TITLE" x="0" y="0" size="1.778" layer="96" display="off"/>
<attribute name="OBJECT_NO" x="0" y="0" size="1.778" layer="96" display="off"/>
<attribute name="DRAWING" x="0" y="0" size="1.778" layer="96" display="off"/>
</instance>
<instance part="C17" gate="G$1" x="287.02" y="139.7" smashed="yes">
<attribute name="NAME" x="286.512" y="137.287" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="286.512" y="135.509" size="1.27" layer="96" rot="R180"/>
<attribute name="MANUFACTURER" x="287.02" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="287.02" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="287.02" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="287.02" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="287.02" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="287.02" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="287.02" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="287.02" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="287.02" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="287.02" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="287.02" y="139.7" size="1.778" layer="96" display="off"/>
</instance>
<instance part="C15" gate="G$1" x="276.86" y="139.7" smashed="yes">
<attribute name="NAME" x="276.352" y="137.287" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="276.352" y="135.509" size="1.27" layer="96" rot="R180"/>
<attribute name="MANUFACTURER" x="276.86" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="276.86" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="276.86" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="276.86" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="276.86" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="276.86" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="276.86" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="276.86" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="276.86" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="276.86" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="276.86" y="139.7" size="1.778" layer="96" display="off"/>
</instance>
<instance part="R3" gate="G$1" x="241.3" y="167.64" smashed="yes" rot="R270">
<attribute name="NAME" x="240.03" y="169.4434" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="240.03" y="167.386" size="1.27" layer="96" rot="R180"/>
<attribute name="MANUFACTURER" x="241.3" y="167.64" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="PART_NO" x="241.3" y="167.64" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="DISTRIBUTOR" x="241.3" y="167.64" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="ORDER_NO" x="241.3" y="167.64" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="PRICE" x="241.3" y="167.64" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="PRICES" x="241.3" y="167.64" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="241.3" y="167.64" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="241.3" y="167.64" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="DATASHEET" x="241.3" y="167.64" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="241.3" y="167.64" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="241.3" y="167.64" size="1.778" layer="96" rot="R180" display="off"/>
</instance>
<instance part="GND32" gate="1" x="167.64" y="114.3"/>
<instance part="GND33" gate="1" x="241.3" y="114.3"/>
<instance part="GND34" gate="1" x="276.86" y="114.3"/>
<instance part="GND39" gate="1" x="287.02" y="114.3"/>
<instance part="R9" gate="G$1" x="241.3" y="147.32" smashed="yes" rot="R270">
<attribute name="NAME" x="240.03" y="148.8694" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="240.03" y="146.812" size="1.27" layer="96" rot="R180"/>
<attribute name="POPULATE" x="241.3" y="147.32" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="241.3" y="147.32" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="241.3" y="147.32" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="241.3" y="147.32" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="241.3" y="147.32" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="241.3" y="147.32" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="241.3" y="147.32" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="241.3" y="147.32" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="241.3" y="147.32" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="241.3" y="147.32" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="241.3" y="147.32" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="241.3" y="147.32" size="1.778" layer="96" display="off"/>
</instance>
<instance part="R2" gate="G$1" x="177.8" y="167.64" smashed="yes" rot="R270">
<attribute name="NAME" x="176.53" y="169.4434" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="176.53" y="167.386" size="1.27" layer="96" rot="R180"/>
<attribute name="MANUFACTURER" x="177.8" y="167.64" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="PART_NO" x="177.8" y="167.64" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="DISTRIBUTOR" x="177.8" y="167.64" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="ORDER_NO" x="177.8" y="167.64" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="PRICE" x="177.8" y="167.64" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="PRICES" x="177.8" y="167.64" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="177.8" y="167.64" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="177.8" y="167.64" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="DATASHEET" x="177.8" y="167.64" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="177.8" y="167.64" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="177.8" y="167.64" size="1.778" layer="96" rot="R180" display="off"/>
</instance>
<instance part="V4" gate="G$1" x="241.3" y="134.62" rot="MR0">
<attribute name="PRICE" x="241.3" y="134.62" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="241.3" y="134.62" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="241.3" y="134.62" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="241.3" y="134.62" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="241.3" y="134.62" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="241.3" y="134.62" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="241.3" y="134.62" size="1.778" layer="96" display="off"/>
</instance>
<instance part="V3" gate="G$1" x="213.36" y="134.62" rot="MR0">
<attribute name="PRICE" x="213.36" y="134.62" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="213.36" y="134.62" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="213.36" y="134.62" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="213.36" y="134.62" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="213.36" y="134.62" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="213.36" y="134.62" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="213.36" y="134.62" size="1.778" layer="96" display="off"/>
</instance>
<instance part="V2" gate="G$1" x="193.04" y="134.62" rot="MR0">
<attribute name="PRICE" x="193.04" y="134.62" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="193.04" y="134.62" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="193.04" y="134.62" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="193.04" y="134.62" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="193.04" y="134.62" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="193.04" y="134.62" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="193.04" y="134.62" size="1.778" layer="96" display="off"/>
</instance>
<instance part="V1" gate="G$1" x="167.64" y="134.62" rot="MR0">
<attribute name="PRICE" x="167.64" y="134.62" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="167.64" y="134.62" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="167.64" y="134.62" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="167.64" y="134.62" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="167.64" y="134.62" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="167.64" y="134.62" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="167.64" y="134.62" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND40" gate="1" x="193.04" y="114.3"/>
<instance part="GND41" gate="1" x="213.36" y="114.3"/>
<instance part="R8" gate="G$1" x="213.36" y="147.32" smashed="yes" rot="R270">
<attribute name="NAME" x="212.09" y="148.8694" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="212.09" y="146.812" size="1.27" layer="96" rot="R180"/>
<attribute name="POPULATE" x="213.36" y="147.32" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="213.36" y="147.32" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="213.36" y="147.32" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="213.36" y="147.32" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="213.36" y="147.32" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="213.36" y="147.32" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="213.36" y="147.32" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="213.36" y="147.32" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="213.36" y="147.32" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="213.36" y="147.32" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="213.36" y="147.32" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="213.36" y="147.32" size="1.778" layer="96" display="off"/>
</instance>
<instance part="R7" gate="G$1" x="193.04" y="147.32" smashed="yes" rot="R270">
<attribute name="NAME" x="191.77" y="148.8694" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="191.77" y="146.812" size="1.27" layer="96" rot="R180"/>
<attribute name="POPULATE" x="193.04" y="147.32" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="193.04" y="147.32" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="193.04" y="147.32" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="193.04" y="147.32" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="193.04" y="147.32" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="193.04" y="147.32" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="193.04" y="147.32" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="193.04" y="147.32" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="193.04" y="147.32" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="193.04" y="147.32" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="193.04" y="147.32" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="193.04" y="147.32" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND47" gate="1" x="175.26" y="114.3"/>
<instance part="GND49" gate="1" x="200.66" y="114.3"/>
<instance part="GND50" gate="1" x="220.98" y="114.3"/>
<instance part="GND51" gate="1" x="248.92" y="114.3"/>
<instance part="C16" gate="G$1" x="281.94" y="139.7" smashed="yes">
<attribute name="NAME" x="281.432" y="137.287" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="281.432" y="135.509" size="1.27" layer="96" rot="R180"/>
<attribute name="MANUFACTURER" x="281.94" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="281.94" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="281.94" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="281.94" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="281.94" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="281.94" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="281.94" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="281.94" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="281.94" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="281.94" y="139.7" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="281.94" y="139.7" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND52" gate="1" x="281.94" y="114.3"/>
<instance part="R10" gate="G$1" x="175.26" y="124.46" smashed="yes" rot="R270">
<attribute name="NAME" x="173.99" y="126.0094" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="173.99" y="124.206" size="1.27" layer="96" rot="R180"/>
<attribute name="EXNAME" x="176.53" y="126.492" size="2.54" layer="96" rot="R270" display="off"/>
<attribute name="MANUFACTURER" x="175.26" y="124.46" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="PART_NO" x="175.26" y="124.46" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="DISTRIBUTOR" x="175.26" y="124.46" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="ORDER_NO" x="175.26" y="124.46" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="PRICE" x="175.26" y="124.46" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="PRICES" x="175.26" y="124.46" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="175.26" y="124.46" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="175.26" y="124.46" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="DATASHEET" x="175.26" y="124.46" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="175.26" y="124.46" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="175.26" y="124.46" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="R11" gate="G$1" x="200.66" y="124.46" smashed="yes" rot="R270">
<attribute name="NAME" x="199.39" y="126.0094" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="199.39" y="124.206" size="1.27" layer="96" rot="R180"/>
<attribute name="EXNAME" x="201.93" y="126.492" size="2.54" layer="96" rot="R270" display="off"/>
<attribute name="MANUFACTURER" x="200.66" y="124.46" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="PART_NO" x="200.66" y="124.46" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="DISTRIBUTOR" x="200.66" y="124.46" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="ORDER_NO" x="200.66" y="124.46" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="PRICE" x="200.66" y="124.46" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="PRICES" x="200.66" y="124.46" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="200.66" y="124.46" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="200.66" y="124.46" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="DATASHEET" x="200.66" y="124.46" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="200.66" y="124.46" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="200.66" y="124.46" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="R12" gate="G$1" x="220.98" y="124.46" smashed="yes" rot="R270">
<attribute name="NAME" x="219.71" y="126.0094" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="219.71" y="124.206" size="1.27" layer="96" rot="R180"/>
<attribute name="EXNAME" x="222.25" y="126.492" size="2.54" layer="96" rot="R270" display="off"/>
<attribute name="MANUFACTURER" x="220.98" y="124.46" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="PART_NO" x="220.98" y="124.46" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="DISTRIBUTOR" x="220.98" y="124.46" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="ORDER_NO" x="220.98" y="124.46" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="PRICE" x="220.98" y="124.46" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="PRICES" x="220.98" y="124.46" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="220.98" y="124.46" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="220.98" y="124.46" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="DATASHEET" x="220.98" y="124.46" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="220.98" y="124.46" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="220.98" y="124.46" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="R13" gate="G$1" x="248.92" y="124.46" smashed="yes" rot="R270">
<attribute name="NAME" x="247.65" y="126.0094" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="247.65" y="124.206" size="1.27" layer="96" rot="R180"/>
<attribute name="EXNAME" x="250.19" y="126.492" size="2.54" layer="96" rot="R270" display="off"/>
<attribute name="MANUFACTURER" x="248.92" y="124.46" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="PART_NO" x="248.92" y="124.46" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="DISTRIBUTOR" x="248.92" y="124.46" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="ORDER_NO" x="248.92" y="124.46" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="PRICE" x="248.92" y="124.46" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="PRICES" x="248.92" y="124.46" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="248.92" y="124.46" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="248.92" y="124.46" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="DATASHEET" x="248.92" y="124.46" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="248.92" y="124.46" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="248.92" y="124.46" size="1.778" layer="96" rot="R270" display="off"/>
</instance>
<instance part="R4" gate="G$1" x="266.7" y="160.02" smashed="yes">
<attribute name="NAME" x="263.906" y="159.7914" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="269.494" y="158.496" size="1.27" layer="96"/>
<attribute name="EXNAME" x="264.668" y="161.29" size="2.54" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="266.7" y="160.02" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="266.7" y="160.02" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="266.7" y="160.02" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="266.7" y="160.02" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="266.7" y="160.02" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="266.7" y="160.02" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="266.7" y="160.02" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="266.7" y="160.02" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="266.7" y="160.02" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="266.7" y="160.02" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="266.7" y="160.02" size="1.778" layer="96" display="off"/>
<attribute name="POPULATE" x="266.7" y="160.02" size="1.778" layer="96" display="off"/>
</instance>
<instance part="R5" gate="G$1" x="266.7" y="157.48" smashed="yes">
<attribute name="NAME" x="264.16" y="156.9974" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="269.494" y="155.702" size="1.27" layer="96"/>
<attribute name="EXNAME" x="264.668" y="158.75" size="2.54" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="266.7" y="157.48" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="266.7" y="157.48" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="266.7" y="157.48" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="266.7" y="157.48" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="266.7" y="157.48" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="266.7" y="157.48" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="266.7" y="157.48" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="266.7" y="157.48" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="266.7" y="157.48" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="266.7" y="157.48" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="266.7" y="157.48" size="1.778" layer="96" display="off"/>
</instance>
<instance part="R6" gate="G$1" x="266.7" y="154.94" smashed="yes">
<attribute name="NAME" x="264.16" y="154.4574" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="269.494" y="153.162" size="1.27" layer="96"/>
<attribute name="EXNAME" x="264.668" y="156.21" size="2.54" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="266.7" y="154.94" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="266.7" y="154.94" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="266.7" y="154.94" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="266.7" y="154.94" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="266.7" y="154.94" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="266.7" y="154.94" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="266.7" y="154.94" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="266.7" y="154.94" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="266.7" y="154.94" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="266.7" y="154.94" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="266.7" y="154.94" size="1.778" layer="96" display="off"/>
</instance>
<instance part="V5" gate="G$1" x="88.9" y="129.54" smashed="yes" rot="MR0">
<attribute name="NAME" x="87.122" y="129.5146" size="1.524" layer="95" rot="MR0"/>
<attribute name="VALUE" x="87.122" y="127.9906" size="1.27" layer="96" rot="MR0"/>
<attribute name="MANUFACTURER" x="88.9" y="129.54" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="PART_NO" x="88.9" y="129.54" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="DISTRIBUTOR" x="88.9" y="129.54" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="ORDER_NO" x="88.9" y="129.54" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="PRICE" x="88.9" y="129.54" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="PRICES" x="88.9" y="129.54" size="1.524" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="88.9" y="129.54" size="1.524" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="88.9" y="129.54" size="1.524" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="88.9" y="129.54" size="1.524" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="88.9" y="129.54" size="1.524" layer="96" display="off"/>
<attribute name="DATASHEET" x="88.9" y="129.54" size="1.524" layer="96" display="off"/>
</instance>
<instance part="GND55" gate="1" x="88.9" y="114.3"/>
<instance part="V7" gate="G$1" x="134.62" y="129.54" smashed="yes" rot="MR0">
<attribute name="NAME" x="132.842" y="129.7686" size="1.524" layer="95" rot="MR0"/>
<attribute name="VALUE" x="132.842" y="127.9906" size="1.27" layer="96" rot="MR0"/>
<attribute name="MANUFACTURER" x="134.62" y="129.54" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="PART_NO" x="134.62" y="129.54" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="DISTRIBUTOR" x="134.62" y="129.54" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="ORDER_NO" x="134.62" y="129.54" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="PRICE" x="134.62" y="129.54" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="PRICES" x="134.62" y="129.54" size="1.524" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="134.62" y="129.54" size="1.524" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="134.62" y="129.54" size="1.524" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="134.62" y="129.54" size="1.524" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="134.62" y="129.54" size="1.524" layer="96" display="off"/>
<attribute name="DATASHEET" x="134.62" y="129.54" size="1.524" layer="96" display="off"/>
</instance>
<instance part="GND56" gate="1" x="134.62" y="114.3"/>
<instance part="V6" gate="G$1" x="111.76" y="129.54" smashed="yes" rot="MR0">
<attribute name="NAME" x="109.982" y="129.5146" size="1.524" layer="95" rot="MR0"/>
<attribute name="VALUE" x="109.982" y="127.9906" size="1.27" layer="96" rot="MR0"/>
<attribute name="MANUFACTURER" x="111.76" y="129.54" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="PART_NO" x="111.76" y="129.54" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="DISTRIBUTOR" x="111.76" y="129.54" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="ORDER_NO" x="111.76" y="129.54" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="PRICE" x="111.76" y="129.54" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="PRICES" x="111.76" y="129.54" size="1.524" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="111.76" y="129.54" size="1.524" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="111.76" y="129.54" size="1.524" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="111.76" y="129.54" size="1.524" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="111.76" y="129.54" size="1.524" layer="96" display="off"/>
<attribute name="DATASHEET" x="111.76" y="129.54" size="1.524" layer="96" display="off"/>
</instance>
<instance part="GND57" gate="1" x="111.76" y="114.3"/>
<instance part="GND58" gate="1" x="157.48" y="114.3"/>
<instance part="V8" gate="G$1" x="157.48" y="129.54" smashed="yes" rot="MR0">
<attribute name="MANUFACTURER" x="157.48" y="129.54" size="2.54" layer="96" rot="MR0" display="off"/>
<attribute name="PART_NO" x="157.48" y="129.54" size="2.54" layer="96" rot="MR0" display="off"/>
<attribute name="DISTRIBUTOR" x="157.48" y="129.54" size="2.54" layer="96" rot="MR0" display="off"/>
<attribute name="ORDER_NO" x="157.48" y="129.54" size="2.54" layer="96" rot="MR0" display="off"/>
<attribute name="PRICE" x="157.48" y="129.54" size="2.54" layer="96" rot="MR0" display="off"/>
<attribute name="NAME" x="152.908" y="130.0226" size="1.524" layer="95" rot="MR0"/>
<attribute name="VALUE" x="152.908" y="128.4986" size="1.27" layer="96" rot="MR0"/>
<attribute name="POPULATE" x="157.48" y="129.54" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="157.48" y="129.54" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="157.48" y="129.54" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="157.48" y="129.54" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="157.48" y="129.54" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="157.48" y="129.54" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="157.48" y="129.54" size="1.778" layer="96" display="off"/>
</instance>
<instance part="C21" gate="G$1" x="96.52" y="124.46" smashed="yes">
<attribute name="NAME" x="96.012" y="122.047" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="96.012" y="120.269" size="1.27" layer="96" rot="R180"/>
<attribute name="MANUFACTURER" x="96.52" y="124.46" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="96.52" y="124.46" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="96.52" y="124.46" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="96.52" y="124.46" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="96.52" y="124.46" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="96.52" y="124.46" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="96.52" y="124.46" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="96.52" y="124.46" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="96.52" y="124.46" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="96.52" y="124.46" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="96.52" y="124.46" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND23" gate="1" x="96.52" y="114.3"/>
<instance part="C18" gate="G$1" x="96.52" y="137.16" smashed="yes">
<attribute name="NAME" x="96.012" y="134.747" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="96.012" y="132.969" size="1.27" layer="96" rot="R180"/>
<attribute name="MANUFACTURER" x="96.52" y="137.16" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="96.52" y="137.16" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="96.52" y="137.16" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="96.52" y="137.16" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="96.52" y="137.16" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="96.52" y="137.16" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="96.52" y="137.16" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="96.52" y="137.16" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="96.52" y="137.16" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="96.52" y="137.16" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="96.52" y="137.16" size="1.778" layer="96" display="off"/>
<attribute name="POPULATE" x="96.52" y="137.16" size="1.778" layer="96" display="off"/>
</instance>
<instance part="C19" gate="G$1" x="119.38" y="137.16" smashed="yes">
<attribute name="NAME" x="118.872" y="134.747" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="118.872" y="132.969" size="1.27" layer="96" rot="R180"/>
<attribute name="MANUFACTURER" x="119.38" y="137.16" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="119.38" y="137.16" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="119.38" y="137.16" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="119.38" y="137.16" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="119.38" y="137.16" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="119.38" y="137.16" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="119.38" y="137.16" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="119.38" y="137.16" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="119.38" y="137.16" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="119.38" y="137.16" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="119.38" y="137.16" size="1.778" layer="96" display="off"/>
</instance>
<instance part="C22" gate="G$1" x="119.38" y="124.46" smashed="yes">
<attribute name="NAME" x="118.872" y="122.047" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="118.872" y="120.269" size="1.27" layer="96" rot="R180"/>
<attribute name="MANUFACTURER" x="119.38" y="124.46" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="119.38" y="124.46" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="119.38" y="124.46" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="119.38" y="124.46" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="119.38" y="124.46" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="119.38" y="124.46" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="119.38" y="124.46" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="119.38" y="124.46" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="119.38" y="124.46" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="119.38" y="124.46" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="119.38" y="124.46" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND24" gate="1" x="119.38" y="114.3"/>
<instance part="C20" gate="G$1" x="142.24" y="137.16" smashed="yes">
<attribute name="NAME" x="141.732" y="134.747" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="141.732" y="132.969" size="1.27" layer="96" rot="R180"/>
<attribute name="MANUFACTURER" x="142.24" y="137.16" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="142.24" y="137.16" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="142.24" y="137.16" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="142.24" y="137.16" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="142.24" y="137.16" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="142.24" y="137.16" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="142.24" y="137.16" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="142.24" y="137.16" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="142.24" y="137.16" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="142.24" y="137.16" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="142.24" y="137.16" size="1.778" layer="96" display="off"/>
</instance>
<instance part="C23" gate="G$1" x="142.24" y="124.46" smashed="yes">
<attribute name="NAME" x="141.732" y="122.047" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="141.732" y="120.269" size="1.27" layer="96" rot="R180"/>
<attribute name="MANUFACTURER" x="142.24" y="124.46" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="142.24" y="124.46" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="142.24" y="124.46" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="142.24" y="124.46" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="142.24" y="124.46" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="142.24" y="124.46" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="142.24" y="124.46" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="142.24" y="124.46" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="142.24" y="124.46" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="142.24" y="124.46" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="142.24" y="124.46" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND26" gate="1" x="142.24" y="114.3"/>
<instance part="X4" gate="G$1" x="55.88" y="160.02" smashed="yes" rot="R90">
<attribute name="NAME" x="57.15" y="144.272" size="1.27" layer="95" rot="R180"/>
<attribute name="VALUE" x="58.928" y="142.494" size="1.27" layer="96" rot="R180"/>
<attribute name="POPULATE" x="55.88" y="160.02" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="MANUFACTURER" x="55.88" y="160.02" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="PART_NO" x="55.88" y="160.02" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="DISTRIBUTOR" x="55.88" y="160.02" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="ORDER_NO" x="55.88" y="160.02" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="PRICE" x="55.88" y="160.02" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="PRICES" x="55.88" y="160.02" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="55.88" y="160.02" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="55.88" y="160.02" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="DATASHEET" x="55.88" y="160.02" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="55.88" y="160.02" size="1.778" layer="96" rot="R180" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="55.88" y="160.02" size="1.778" layer="96" rot="R180" display="off"/>
</instance>
</instances>
<busses>
<bus name="TEMPERATURE:TEMP_REFERENCE,TEMP_POS,TEMP_POS2,TEMP_NEG,TEMP_NEG_EN,TEMP_180_EN,TEMP_100_EN,TEMP_SENSE_EN">
<segment>
<wire x1="325.12" y1="172.72" x2="325.12" y2="96.52" width="0.762" layer="92"/>
<wire x1="325.12" y1="96.52" x2="327.66" y2="93.98" width="0.762" layer="92"/>
<wire x1="327.66" y1="93.98" x2="370.84" y2="93.98" width="0.762" layer="92"/>
<label x="370.84" y="96.774" size="1.778" layer="95" rot="R180"/>
</segment>
</bus>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="GND39" gate="1" pin="GND"/>
<pinref part="C17" gate="G$1" pin="2"/>
<wire x1="287.02" y1="116.84" x2="287.02" y2="134.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C15" gate="G$1" pin="2"/>
<pinref part="GND34" gate="1" pin="GND"/>
<wire x1="276.86" y1="116.84" x2="276.86" y2="134.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="V1" gate="G$1" pin="S"/>
<pinref part="GND32" gate="1" pin="GND"/>
<wire x1="167.64" y1="129.54" x2="167.64" y2="116.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="V2" gate="G$1" pin="S"/>
<pinref part="GND40" gate="1" pin="GND"/>
<wire x1="193.04" y1="129.54" x2="193.04" y2="116.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="V3" gate="G$1" pin="S"/>
<pinref part="GND41" gate="1" pin="GND"/>
<wire x1="213.36" y1="129.54" x2="213.36" y2="116.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="V4" gate="G$1" pin="S"/>
<pinref part="GND33" gate="1" pin="GND"/>
<wire x1="241.3" y1="129.54" x2="241.3" y2="116.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND47" gate="1" pin="GND"/>
<wire x1="175.26" y1="119.38" x2="175.26" y2="116.84" width="0.1524" layer="91"/>
<pinref part="R10" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="GND49" gate="1" pin="GND"/>
<wire x1="200.66" y1="119.38" x2="200.66" y2="116.84" width="0.1524" layer="91"/>
<pinref part="R11" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="GND50" gate="1" pin="GND"/>
<wire x1="220.98" y1="116.84" x2="220.98" y2="119.38" width="0.1524" layer="91"/>
<pinref part="R12" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="GND51" gate="1" pin="GND"/>
<wire x1="248.92" y1="116.84" x2="248.92" y2="119.38" width="0.1524" layer="91"/>
<pinref part="R13" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="GND52" gate="1" pin="GND"/>
<pinref part="C16" gate="G$1" pin="2"/>
<wire x1="281.94" y1="116.84" x2="281.94" y2="134.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="V5" gate="G$1" pin="GND"/>
<pinref part="GND55" gate="1" pin="GND"/>
<wire x1="88.9" y1="124.46" x2="88.9" y2="116.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="V7" gate="G$1" pin="GND"/>
<pinref part="GND56" gate="1" pin="GND"/>
<wire x1="134.62" y1="124.46" x2="134.62" y2="116.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="V6" gate="G$1" pin="GND"/>
<pinref part="GND57" gate="1" pin="GND"/>
<wire x1="111.76" y1="124.46" x2="111.76" y2="116.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND58" gate="1" pin="GND"/>
<pinref part="V8" gate="G$1" pin="A"/>
<wire x1="157.48" y1="116.84" x2="157.48" y2="124.46" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C21" gate="G$1" pin="2"/>
<pinref part="GND23" gate="1" pin="GND"/>
<wire x1="96.52" y1="119.38" x2="96.52" y2="116.84" width="0.1524" layer="91" style="longdash"/>
</segment>
<segment>
<pinref part="C22" gate="G$1" pin="2"/>
<pinref part="GND24" gate="1" pin="GND"/>
<wire x1="119.38" y1="119.38" x2="119.38" y2="116.84" width="0.1524" layer="91" style="longdash"/>
</segment>
<segment>
<pinref part="GND26" gate="1" pin="GND"/>
<pinref part="C23" gate="G$1" pin="2"/>
<wire x1="142.24" y1="116.84" x2="142.24" y2="119.38" width="0.1524" layer="91" style="longdash"/>
</segment>
</net>
<net name="TEMP_NEG" class="0">
<segment>
<wire x1="299.72" y1="154.94" x2="320.04" y2="154.94" width="0.1524" layer="91"/>
<wire x1="276.86" y1="154.94" x2="276.86" y2="142.24" width="0.1524" layer="91"/>
<junction x="276.86" y="154.94"/>
<pinref part="C15" gate="G$1" pin="1"/>
<wire x1="276.86" y1="154.94" x2="274.32" y2="154.94" width="0.1524" layer="91"/>
<wire x1="274.32" y1="154.94" x2="276.86" y2="154.94" width="0.1524" layer="91"/>
<wire x1="276.86" y1="154.94" x2="299.72" y2="154.94" width="0.1524" layer="91"/>
<wire x1="271.78" y1="154.94" x2="276.86" y2="154.94" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="2"/>
<label x="320.04" y="156.972" size="1.778" layer="95" rot="R180"/>
<wire x1="320.04" y1="154.94" x2="322.58" y2="154.94" width="0.1524" layer="91"/>
<wire x1="322.58" y1="154.94" x2="325.12" y2="152.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TEMP_POS" class="0">
<segment>
<wire x1="287.02" y1="160.02" x2="274.32" y2="160.02" width="0.1524" layer="91"/>
<pinref part="C17" gate="G$1" pin="1"/>
<wire x1="287.02" y1="142.24" x2="287.02" y2="160.02" width="0.1524" layer="91"/>
<junction x="287.02" y="160.02"/>
<wire x1="302.26" y1="160.02" x2="287.02" y2="160.02" width="0.1524" layer="91"/>
<wire x1="271.78" y1="160.02" x2="274.32" y2="160.02" width="0.1524" layer="91"/>
<wire x1="302.26" y1="160.02" x2="320.04" y2="160.02" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="2"/>
<label x="320.04" y="162.052" size="1.778" layer="95" rot="R180"/>
<wire x1="320.04" y1="160.02" x2="322.58" y2="160.02" width="0.1524" layer="91"/>
<wire x1="322.58" y1="160.02" x2="325.12" y2="157.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TEMP3+" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="177.8" y1="160.02" x2="193.04" y2="160.02" width="0.1524" layer="91"/>
<wire x1="193.04" y1="160.02" x2="213.36" y2="160.02" width="0.1524" layer="91"/>
<wire x1="213.36" y1="160.02" x2="261.62" y2="160.02" width="0.1524" layer="91"/>
<wire x1="177.8" y1="162.56" x2="177.8" y2="160.02" width="0.1524" layer="91"/>
<junction x="177.8" y="160.02"/>
<wire x1="66.04" y1="160.02" x2="101.6" y2="160.02" width="0.1524" layer="91"/>
<wire x1="101.6" y1="160.02" x2="177.8" y2="160.02" width="0.1524" layer="91"/>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="213.36" y1="152.4" x2="213.36" y2="160.02" width="0.1524" layer="91"/>
<junction x="213.36" y="160.02"/>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="193.04" y1="152.4" x2="193.04" y2="160.02" width="0.1524" layer="91"/>
<junction x="193.04" y="160.02"/>
<pinref part="R4" gate="G$1" pin="1"/>
<pinref part="V5" gate="G$1" pin="IO"/>
<wire x1="91.44" y1="129.54" x2="96.52" y2="129.54" width="0.1524" layer="91"/>
<wire x1="96.52" y1="129.54" x2="101.6" y2="129.54" width="0.1524" layer="91"/>
<wire x1="101.6" y1="129.54" x2="101.6" y2="160.02" width="0.1524" layer="91"/>
<junction x="101.6" y="160.02"/>
<wire x1="66.04" y1="160.02" x2="66.04" y2="162.56" width="0.1524" layer="91" style="dashdot"/>
<wire x1="66.04" y1="162.56" x2="63.5" y2="162.56" width="0.1524" layer="91"/>
<pinref part="C18" gate="G$1" pin="2"/>
<wire x1="96.52" y1="129.54" x2="96.52" y2="132.08" width="0.1524" layer="91" style="longdash"/>
<junction x="96.52" y="129.54"/>
<pinref part="C21" gate="G$1" pin="1"/>
<wire x1="96.52" y1="129.54" x2="96.52" y2="127" width="0.1524" layer="91" style="longdash"/>
<pinref part="X4" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="241.3" y1="162.56" x2="241.3" y2="154.94" width="0.1524" layer="91"/>
<wire x1="261.62" y1="154.94" x2="241.3" y2="154.94" width="0.1524" layer="91"/>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="241.3" y1="152.4" x2="241.3" y2="154.94" width="0.1524" layer="91"/>
<junction x="241.3" y="154.94"/>
<pinref part="R6" gate="G$1" pin="1"/>
</segment>
</net>
<net name="TEMP3-" class="0">
<segment>
<wire x1="167.64" y1="139.7" x2="167.64" y2="154.94" width="0.1524" layer="91"/>
<wire x1="167.64" y1="154.94" x2="147.32" y2="154.94" width="0.1524" layer="91"/>
<wire x1="147.32" y1="154.94" x2="71.12" y2="154.94" width="0.1524" layer="91"/>
<wire x1="71.12" y1="154.94" x2="66.04" y2="154.94" width="0.1524" layer="91"/>
<pinref part="V1" gate="G$1" pin="D"/>
<pinref part="V7" gate="G$1" pin="IO"/>
<wire x1="137.16" y1="129.54" x2="142.24" y2="129.54" width="0.1524" layer="91"/>
<wire x1="142.24" y1="129.54" x2="147.32" y2="129.54" width="0.1524" layer="91"/>
<wire x1="147.32" y1="129.54" x2="147.32" y2="154.94" width="0.1524" layer="91"/>
<junction x="147.32" y="154.94"/>
<wire x1="66.04" y1="154.94" x2="66.04" y2="152.4" width="0.1524" layer="91"/>
<wire x1="66.04" y1="152.4" x2="63.5" y2="152.4" width="0.1524" layer="91"/>
<pinref part="C23" gate="G$1" pin="1"/>
<wire x1="142.24" y1="127" x2="142.24" y2="129.54" width="0.1524" layer="91" style="longdash"/>
<junction x="142.24" y="129.54"/>
<pinref part="C20" gate="G$1" pin="2"/>
<wire x1="142.24" y1="132.08" x2="142.24" y2="129.54" width="0.1524" layer="91" style="longdash"/>
<pinref part="X4" gate="G$1" pin="3"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="241.3" y1="139.7" x2="241.3" y2="142.24" width="0.1524" layer="91"/>
<pinref part="V4" gate="G$1" pin="D"/>
</segment>
</net>
<net name="TEMP_REFERENCE" class="0">
<segment>
<wire x1="320.04" y1="175.26" x2="241.3" y2="175.26" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="241.3" y1="175.26" x2="177.8" y2="175.26" width="0.1524" layer="91"/>
<wire x1="177.8" y1="175.26" x2="177.8" y2="172.72" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="241.3" y1="172.72" x2="241.3" y2="175.26" width="0.1524" layer="91"/>
<junction x="241.3" y="175.26"/>
<label x="320.04" y="177.292" size="1.778" layer="95" rot="R180"/>
<pinref part="V7" gate="G$1" pin="VCC"/>
<wire x1="177.8" y1="175.26" x2="160.02" y2="175.26" width="0.1524" layer="91"/>
<wire x1="160.02" y1="175.26" x2="154.94" y2="175.26" width="0.1524" layer="91"/>
<wire x1="154.94" y1="175.26" x2="134.62" y2="175.26" width="0.1524" layer="91"/>
<wire x1="134.62" y1="175.26" x2="134.62" y2="142.24" width="0.1524" layer="91"/>
<junction x="177.8" y="175.26"/>
<pinref part="V6" gate="G$1" pin="VCC"/>
<wire x1="134.62" y1="142.24" x2="134.62" y2="134.62" width="0.1524" layer="91"/>
<wire x1="134.62" y1="175.26" x2="111.76" y2="175.26" width="0.1524" layer="91"/>
<wire x1="111.76" y1="175.26" x2="111.76" y2="142.24" width="0.1524" layer="91"/>
<junction x="134.62" y="175.26"/>
<pinref part="V5" gate="G$1" pin="VCC"/>
<wire x1="111.76" y1="142.24" x2="111.76" y2="134.62" width="0.1524" layer="91"/>
<wire x1="111.76" y1="175.26" x2="88.9" y2="175.26" width="0.1524" layer="91"/>
<wire x1="88.9" y1="175.26" x2="88.9" y2="142.24" width="0.1524" layer="91"/>
<junction x="111.76" y="175.26"/>
<pinref part="V8" gate="G$1" pin="C1"/>
<wire x1="88.9" y1="142.24" x2="88.9" y2="134.62" width="0.1524" layer="91"/>
<wire x1="154.94" y1="132.08" x2="154.94" y2="175.26" width="0.1524" layer="91"/>
<junction x="154.94" y="175.26"/>
<pinref part="V8" gate="G$1" pin="C"/>
<wire x1="160.02" y1="132.08" x2="160.02" y2="175.26" width="0.1524" layer="91"/>
<junction x="160.02" y="175.26"/>
<wire x1="320.04" y1="175.26" x2="322.58" y2="175.26" width="0.1524" layer="91"/>
<wire x1="322.58" y1="175.26" x2="325.12" y2="172.72" width="0.1524" layer="91"/>
<pinref part="C18" gate="G$1" pin="1"/>
<wire x1="96.52" y1="139.7" x2="96.52" y2="142.24" width="0.1524" layer="91" style="longdash"/>
<wire x1="96.52" y1="142.24" x2="88.9" y2="142.24" width="0.1524" layer="91" style="longdash"/>
<junction x="88.9" y="142.24"/>
<pinref part="C19" gate="G$1" pin="1"/>
<wire x1="119.38" y1="139.7" x2="119.38" y2="142.24" width="0.1524" layer="91" style="longdash"/>
<wire x1="119.38" y1="142.24" x2="111.76" y2="142.24" width="0.1524" layer="91" style="longdash"/>
<junction x="111.76" y="142.24"/>
<pinref part="C20" gate="G$1" pin="1"/>
<wire x1="142.24" y1="139.7" x2="142.24" y2="142.24" width="0.1524" layer="91" style="longdash"/>
<wire x1="142.24" y1="142.24" x2="134.62" y2="142.24" width="0.1524" layer="91" style="longdash"/>
<junction x="134.62" y="142.24"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="V2" gate="G$1" pin="D"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="193.04" y1="139.7" x2="193.04" y2="142.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="V3" gate="G$1" pin="D"/>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="213.36" y1="139.7" x2="213.36" y2="142.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<wire x1="66.04" y1="157.48" x2="124.46" y2="157.48" width="0.1524" layer="91"/>
<wire x1="124.46" y1="157.48" x2="170.18" y2="157.48" width="0.1524" layer="91"/>
<wire x1="170.18" y1="157.48" x2="261.62" y2="157.48" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="1"/>
<pinref part="V6" gate="G$1" pin="IO"/>
<wire x1="114.3" y1="129.54" x2="119.38" y2="129.54" width="0.1524" layer="91"/>
<wire x1="119.38" y1="129.54" x2="124.46" y2="129.54" width="0.1524" layer="91"/>
<wire x1="124.46" y1="129.54" x2="124.46" y2="157.48" width="0.1524" layer="91"/>
<junction x="124.46" y="157.48"/>
<wire x1="66.04" y1="157.48" x2="63.5" y2="157.48" width="0.1524" layer="91"/>
<pinref part="C19" gate="G$1" pin="2"/>
<wire x1="119.38" y1="132.08" x2="119.38" y2="129.54" width="0.1524" layer="91" style="longdash"/>
<junction x="119.38" y="129.54"/>
<pinref part="C22" gate="G$1" pin="1"/>
<wire x1="119.38" y1="129.54" x2="119.38" y2="127" width="0.1524" layer="91" style="longdash"/>
<pinref part="X4" gate="G$1" pin="2"/>
</segment>
</net>
<net name="TEMP_POS2" class="0">
<segment>
<wire x1="271.78" y1="157.48" x2="274.32" y2="157.48" width="0.1524" layer="91"/>
<wire x1="274.32" y1="157.48" x2="281.94" y2="157.48" width="0.1524" layer="91"/>
<wire x1="281.94" y1="157.48" x2="320.04" y2="157.48" width="0.1524" layer="91"/>
<pinref part="C16" gate="G$1" pin="1"/>
<wire x1="281.94" y1="142.24" x2="281.94" y2="157.48" width="0.1524" layer="91"/>
<junction x="281.94" y="157.48"/>
<pinref part="R5" gate="G$1" pin="2"/>
<label x="320.04" y="159.512" size="1.778" layer="95" rot="R180"/>
<wire x1="320.04" y1="157.48" x2="322.58" y2="157.48" width="0.1524" layer="91"/>
<wire x1="322.58" y1="157.48" x2="325.12" y2="154.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TEMP_SENSE_EN" class="0">
<segment>
<pinref part="V1" gate="G$1" pin="G"/>
<wire x1="172.72" y1="132.08" x2="175.26" y2="132.08" width="0.1524" layer="91"/>
<wire x1="175.26" y1="132.08" x2="175.26" y2="129.54" width="0.1524" layer="91"/>
<wire x1="175.26" y1="132.08" x2="180.34" y2="132.08" width="0.1524" layer="91"/>
<junction x="175.26" y="132.08"/>
<wire x1="180.34" y1="132.08" x2="180.34" y2="104.14" width="0.1524" layer="91"/>
<wire x1="180.34" y1="104.14" x2="320.04" y2="104.14" width="0.1524" layer="91"/>
<pinref part="R10" gate="G$1" pin="1"/>
<label x="320.04" y="106.172" size="1.778" layer="95" rot="R180"/>
<wire x1="320.04" y1="104.14" x2="322.58" y2="104.14" width="0.1524" layer="91"/>
<wire x1="322.58" y1="104.14" x2="325.12" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TEMP_100_EN" class="0">
<segment>
<pinref part="V2" gate="G$1" pin="G"/>
<wire x1="198.12" y1="132.08" x2="200.66" y2="132.08" width="0.1524" layer="91"/>
<wire x1="200.66" y1="132.08" x2="200.66" y2="129.54" width="0.1524" layer="91"/>
<wire x1="200.66" y1="132.08" x2="203.2" y2="132.08" width="0.1524" layer="91"/>
<junction x="200.66" y="132.08"/>
<wire x1="203.2" y1="132.08" x2="205.74" y2="132.08" width="0.1524" layer="91"/>
<wire x1="205.74" y1="132.08" x2="205.74" y2="106.68" width="0.1524" layer="91"/>
<wire x1="205.74" y1="106.68" x2="320.04" y2="106.68" width="0.1524" layer="91"/>
<pinref part="R11" gate="G$1" pin="1"/>
<label x="320.04" y="108.712" size="1.778" layer="95" rot="R180"/>
<wire x1="320.04" y1="106.68" x2="322.58" y2="106.68" width="0.1524" layer="91"/>
<wire x1="322.58" y1="106.68" x2="325.12" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TEMP_180_EN" class="0">
<segment>
<pinref part="V3" gate="G$1" pin="G"/>
<wire x1="218.44" y1="132.08" x2="220.98" y2="132.08" width="0.1524" layer="91"/>
<wire x1="220.98" y1="132.08" x2="220.98" y2="129.54" width="0.1524" layer="91"/>
<wire x1="220.98" y1="132.08" x2="226.06" y2="132.08" width="0.1524" layer="91"/>
<junction x="220.98" y="132.08"/>
<wire x1="226.06" y1="132.08" x2="226.06" y2="109.22" width="0.1524" layer="91"/>
<wire x1="226.06" y1="109.22" x2="320.04" y2="109.22" width="0.1524" layer="91"/>
<pinref part="R12" gate="G$1" pin="1"/>
<label x="320.04" y="111.252" size="1.778" layer="95" rot="R180"/>
<wire x1="320.04" y1="109.22" x2="322.58" y2="109.22" width="0.1524" layer="91"/>
<wire x1="322.58" y1="109.22" x2="325.12" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TEMP_NEG_EN" class="0">
<segment>
<pinref part="V4" gate="G$1" pin="G"/>
<wire x1="246.38" y1="132.08" x2="248.92" y2="132.08" width="0.1524" layer="91"/>
<wire x1="248.92" y1="132.08" x2="248.92" y2="129.54" width="0.1524" layer="91"/>
<wire x1="248.92" y1="132.08" x2="254" y2="132.08" width="0.1524" layer="91"/>
<junction x="248.92" y="132.08"/>
<wire x1="254" y1="132.08" x2="254" y2="111.76" width="0.1524" layer="91"/>
<wire x1="254" y1="111.76" x2="314.96" y2="111.76" width="0.1524" layer="91"/>
<wire x1="314.96" y1="111.76" x2="320.04" y2="111.76" width="0.1524" layer="91"/>
<pinref part="R13" gate="G$1" pin="1"/>
<label x="320.04" y="113.792" size="1.778" layer="95" rot="R180"/>
<wire x1="320.04" y1="111.76" x2="322.58" y2="111.76" width="0.1524" layer="91"/>
<wire x1="322.58" y1="111.76" x2="325.12" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="4.572" y="13.716" size="1.778" layer="97">1.0</text>
<text x="13.716" y="13.716" size="1.778" layer="97">Inital Revision</text>
<text x="38.354" y="13.716" size="1.778" layer="97">18.04.14</text>
<wire x1="177.8" y1="139.7" x2="177.8" y2="20.32" width="0.1524" layer="97" style="longdash"/>
<wire x1="20.32" y1="139.7" x2="53.34" y2="139.7" width="0.1524" layer="97" style="longdash"/>
<wire x1="53.34" y1="139.7" x2="119.38" y2="139.7" width="0.1524" layer="97" style="longdash"/>
<wire x1="119.38" y1="139.7" x2="177.8" y2="139.7" width="0.1524" layer="97" style="longdash"/>
<wire x1="20.32" y1="139.7" x2="20.32" y2="20.32" width="0.1524" layer="97" style="longdash"/>
<wire x1="20.32" y1="20.32" x2="53.34" y2="20.32" width="0.1524" layer="97" style="longdash"/>
<text x="55.88" y="137.16" size="2.54" layer="97" align="top-left">Display</text>
<wire x1="53.34" y1="20.32" x2="119.38" y2="20.32" width="0.1524" layer="97" style="longdash"/>
<wire x1="119.38" y1="20.32" x2="177.8" y2="20.32" width="0.1524" layer="97" style="longdash"/>
<wire x1="53.34" y1="139.7" x2="53.34" y2="20.32" width="0.1524" layer="97" style="longdash"/>
<text x="22.86" y="137.16" size="2.54" layer="97" align="top-left">Contrast</text>
<text x="22.86" y="132.08" size="1.778" layer="97" align="top-left">may be adjusted
using a PWM</text>
<wire x1="119.38" y1="139.7" x2="119.38" y2="106.68" width="0.1524" layer="97" style="longdash"/>
<wire x1="119.38" y1="78.74" x2="119.38" y2="20.32" width="0.1524" layer="97" style="longdash"/>
<text x="175.26" y="134.62" size="2.54" layer="97" rot="R180" align="top-left">PWM adjustable backlight</text>
<text x="7.62" y="149.86" size="5.08" layer="97" align="top-left">Multicontrol only!</text>
<wire x1="40.64" y1="259.08" x2="124.46" y2="259.08" width="0.1524" layer="97" style="longdash"/>
<wire x1="40.64" y1="259.08" x2="40.64" y2="198.12" width="0.1524" layer="97" style="longdash"/>
<wire x1="124.46" y1="259.08" x2="124.46" y2="198.12" width="0.1524" layer="97" style="longdash"/>
<wire x1="40.64" y1="198.12" x2="124.46" y2="198.12" width="0.1524" layer="97" style="longdash"/>
<text x="43.18" y="256.54" size="2.54" layer="97" align="top-left">Status LEDs</text>
<wire x1="35.56" y1="195.58" x2="35.56" y2="154.94" width="0.1524" layer="97" style="longdash"/>
<wire x1="35.56" y1="195.58" x2="124.46" y2="195.58" width="0.1524" layer="97" style="longdash"/>
<wire x1="35.56" y1="154.94" x2="124.46" y2="154.94" width="0.1524" layer="97" style="longdash"/>
<wire x1="124.46" y1="195.58" x2="124.46" y2="154.94" width="0.1524" layer="97" style="longdash"/>
<text x="38.1" y="193.04" size="2.54" layer="97" align="top-left">Buttons</text>
<wire x1="378.46" y1="193.04" x2="330.2" y2="193.04" width="0.1524" layer="97" style="longdash"/>
<wire x1="378.46" y1="231.14" x2="378.46" y2="193.04" width="0.1524" layer="97" style="longdash"/>
<wire x1="378.46" y1="231.14" x2="330.2" y2="231.14" width="0.1524" layer="97" style="longdash"/>
<wire x1="330.2" y1="231.14" x2="330.2" y2="193.04" width="0.1524" layer="97" style="longdash"/>
<text x="332.74" y="195.58" size="2.54" layer="97">PCB-mount RS232</text>
<wire x1="330.2" y1="266.7" x2="330.2" y2="231.14" width="0.1524" layer="97" style="longdash"/>
<wire x1="378.46" y1="266.7" x2="378.46" y2="231.14" width="0.1524" layer="97" style="longdash"/>
<wire x1="378.46" y1="266.7" x2="330.2" y2="266.7" width="0.1524" layer="97" style="longdash"/>
<text x="332.74" y="261.62" size="2.54" layer="97">Wall-mount RS232</text>
<wire x1="330.2" y1="193.04" x2="213.36" y2="193.04" width="0.1524" layer="97" style="longdash"/>
<wire x1="213.36" y1="266.7" x2="213.36" y2="193.04" width="0.1524" layer="97" style="longdash"/>
<wire x1="330.2" y1="266.7" x2="213.36" y2="266.7" width="0.1524" layer="97" style="longdash"/>
<text x="218.44" y="261.62" size="2.54" layer="97">Level translator</text>
<text x="248.92" y="261.62" size="1.778" layer="97">Could be switched to MAX202 if 15kV ESD protection is needed</text>
<wire x1="271.78" y1="187.96" x2="271.78" y2="137.16" width="0.1524" layer="97" style="longdash"/>
<wire x1="271.78" y1="137.16" x2="330.2" y2="137.16" width="0.1524" layer="97" style="longdash"/>
<wire x1="330.2" y1="137.16" x2="378.46" y2="137.16" width="0.1524" layer="97" style="longdash"/>
<wire x1="378.46" y1="187.96" x2="330.2" y2="187.96" width="0.1524" layer="97" style="longdash"/>
<text x="274.32" y="139.7" size="2.54" layer="97">ESD Protection</text>
<wire x1="330.2" y1="187.96" x2="271.78" y2="187.96" width="0.1524" layer="97" style="longdash"/>
<wire x1="378.46" y1="187.96" x2="378.46" y2="137.16" width="0.1524" layer="97" style="longdash"/>
<wire x1="248.92" y1="187.96" x2="248.92" y2="137.16" width="0.1524" layer="97" style="longdash"/>
<wire x1="271.78" y1="137.16" x2="248.92" y2="137.16" width="0.1524" layer="97" style="longdash"/>
<wire x1="271.78" y1="187.96" x2="248.92" y2="187.96" width="0.1524" layer="97" style="longdash"/>
<wire x1="330.2" y1="187.96" x2="330.2" y2="137.16" width="0.1524" layer="97" style="longdash"/>
<text x="332.74" y="111.76" size="2.54" layer="97">Wall-mount USB</text>
<text x="332.74" y="182.88" size="2.54" layer="97">PCB-mount USB</text>
<wire x1="330.2" y1="137.16" x2="330.2" y2="109.22" width="0.1524" layer="97" style="longdash"/>
<wire x1="378.46" y1="137.16" x2="378.46" y2="109.22" width="0.1524" layer="97" style="longdash"/>
<wire x1="330.2" y1="109.22" x2="378.46" y2="109.22" width="0.1524" layer="97" style="longdash"/>
<wire x1="129.54" y1="271.78" x2="129.54" y2="190.5" width="0.1524" layer="97" style="longdash"/>
<text x="10.16" y="266.7" size="5.08" layer="97" align="top-left">EL+Multicontrol only!</text>
<wire x1="129.54" y1="190.5" x2="129.54" y2="152.4" width="0.1524" layer="97" style="longdash"/>
<wire x1="5.08" y1="152.4" x2="129.54" y2="152.4" width="0.1524" layer="97" style="longdash"/>
<wire x1="185.42" y1="106.68" x2="378.46" y2="106.68" width="0.1524" layer="97" style="longdash"/>
<wire x1="185.42" y1="106.68" x2="185.42" y2="53.34" width="0.1524" layer="97" style="longdash"/>
<wire x1="185.42" y1="53.34" x2="185.42" y2="20.32" width="0.1524" layer="97" style="longdash"/>
<wire x1="185.42" y1="20.32" x2="378.46" y2="20.32" width="0.1524" layer="97" style="longdash"/>
<wire x1="378.46" y1="106.68" x2="378.46" y2="53.34" width="0.1524" layer="97" style="longdash"/>
<wire x1="378.46" y1="53.34" x2="378.46" y2="20.32" width="0.1524" layer="97" style="longdash"/>
<wire x1="185.42" y1="53.34" x2="378.46" y2="53.34" width="0.1524" layer="97" style="longdash"/>
<text x="187.96" y="22.86" size="2.54" layer="97">Wall-mount SD</text>
<text x="187.96" y="55.88" size="2.54" layer="97">Internal µSD</text>
<text x="134.62" y="259.08" size="5.08" layer="97" align="top-left">Both</text>
<wire x1="129.54" y1="190.5" x2="383.54" y2="190.5" width="0.1524" layer="97" style="longdash"/>
<wire x1="205.74" y1="266.7" x2="157.48" y2="266.7" width="0.1524" layer="97" style="longdash"/>
<wire x1="157.48" y1="266.7" x2="157.48" y2="193.04" width="0.1524" layer="97" style="longdash"/>
<wire x1="157.48" y1="193.04" x2="205.74" y2="193.04" width="0.1524" layer="97" style="longdash"/>
<wire x1="205.74" y1="193.04" x2="205.74" y2="266.7" width="0.1524" layer="97" style="longdash"/>
<text x="162.56" y="261.62" size="2.54" layer="97">Buzzer</text>
</plain>
<instances>
<instance part="V13" gate="G$1" x="134.62" y="121.92" smashed="yes" rot="MR0">
<attribute name="NAME" x="132.588" y="123.698" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="132.588" y="121.666" size="1.27" layer="96" rot="R180"/>
<attribute name="PRICE" x="134.62" y="121.92" size="3.81" layer="96" rot="MR180" display="off"/>
<attribute name="PRICES" x="134.62" y="121.92" size="3.81" layer="96" rot="MR180" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="134.62" y="121.92" size="3.81" layer="96" rot="MR180" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="134.62" y="121.92" size="3.81" layer="96" rot="MR180" display="off"/>
<attribute name="DATASHEET" x="134.62" y="121.92" size="3.81" layer="96" rot="MR180" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="134.62" y="121.92" size="3.81" layer="96" rot="MR180" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="134.62" y="121.92" size="3.81" layer="96" rot="MR180" display="off"/>
<attribute name="POPULATE" x="134.62" y="121.92" size="1.778" layer="96" display="off"/>
</instance>
<instance part="P+12" gate="1" x="134.62" y="132.08" rot="MR0"/>
<instance part="R31" gate="G$1" x="134.62" y="106.68" smashed="yes" rot="MR90">
<attribute name="NAME" x="133.35" y="107.1626" size="1.524" layer="95" rot="MR0"/>
<attribute name="VALUE" x="133.35" y="105.156" size="1.27" layer="96" rot="MR0"/>
<attribute name="PRICE" x="134.62" y="106.68" size="3.81" layer="96" rot="MR0" display="off"/>
<attribute name="PRICES" x="134.62" y="106.68" size="3.81" layer="96" rot="MR0" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="134.62" y="106.68" size="3.81" layer="96" rot="MR0" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="134.62" y="106.68" size="3.81" layer="96" rot="MR0" display="off"/>
<attribute name="DATASHEET" x="134.62" y="106.68" size="3.81" layer="96" rot="MR0" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="134.62" y="106.68" size="3.81" layer="96" rot="MR0" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="134.62" y="106.68" size="3.81" layer="96" rot="MR0" display="off"/>
<attribute name="MANUFACTURER" x="134.62" y="106.68" size="3.81" layer="96" rot="MR0" display="off"/>
<attribute name="PART_NO" x="134.62" y="106.68" size="3.81" layer="96" rot="MR0" display="off"/>
<attribute name="DISTRIBUTOR" x="134.62" y="106.68" size="3.81" layer="96" rot="MR0" display="off"/>
<attribute name="ORDER_NO" x="134.62" y="106.68" size="3.81" layer="96" rot="MR0" display="off"/>
<attribute name="POPULATE" x="134.62" y="106.68" size="1.778" layer="96" display="off"/>
</instance>
<instance part="P+9" gate="1" x="152.4" y="132.08" rot="MR0"/>
<instance part="V14" gate="G$1" x="152.4" y="93.98" smashed="yes" rot="MR0">
<attribute name="NAME" x="150.114" y="94.488" size="1.524" layer="95" rot="MR0"/>
<attribute name="VALUE" x="150.114" y="92.71" size="1.27" layer="96" rot="MR0"/>
<attribute name="MANUFACTURER" x="152.4" y="93.98" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="PART_NO" x="152.4" y="93.98" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="DISTRIBUTOR" x="152.4" y="93.98" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="ORDER_NO" x="152.4" y="93.98" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="PRICE" x="152.4" y="93.98" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="PRICES" x="152.4" y="93.98" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="152.4" y="93.98" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="152.4" y="93.98" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="DATASHEET" x="152.4" y="93.98" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="152.4" y="93.98" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="152.4" y="93.98" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="POPULATE" x="152.4" y="93.98" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND42" gate="1" x="152.4" y="83.82" rot="MR0"/>
<instance part="R30" gate="G$1" x="152.4" y="121.92" smashed="yes" rot="MR90">
<attribute name="NAME" x="151.13" y="122.4026" size="1.524" layer="95" rot="MR0"/>
<attribute name="VALUE" x="151.13" y="120.65" size="1.27" layer="96" rot="MR0"/>
<attribute name="DISTRIBUTOR" x="152.4" y="121.92" size="1.778" layer="96" rot="MR90" display="off"/>
<attribute name="MANUFACTURER" x="152.4" y="121.92" size="1.778" layer="96" rot="MR90" display="off"/>
<attribute name="ORDER_NO" x="152.4" y="121.92" size="1.778" layer="96" rot="MR90" display="off"/>
<attribute name="PART_NO" x="152.4" y="121.92" size="1.778" layer="96" rot="MR90" display="off"/>
<attribute name="PRICES" x="152.4" y="121.92" size="1.778" layer="96" font="vector" rot="MR0" display="off"/>
<attribute name="PRICE" x="152.4" y="121.92" size="1.778" layer="96" font="vector" rot="MR0" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="152.4" y="121.92" size="1.778" layer="96" font="vector" rot="MR0" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="152.4" y="121.92" size="1.778" layer="96" font="vector" rot="MR0" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="152.4" y="121.92" size="1.778" layer="96" font="vector" rot="MR0" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="152.4" y="121.92" size="1.778" layer="96" font="vector" rot="MR0" display="off"/>
<attribute name="DATASHEET" x="152.4" y="121.92" size="1.778" layer="96" font="vector" rot="MR0" display="off"/>
<attribute name="POPULATE" x="152.4" y="121.92" size="1.778" layer="96" display="off"/>
</instance>
<instance part="R35" gate="G$1" x="165.1" y="93.98" smashed="yes" rot="MR90">
<attribute name="NAME" x="164.084" y="94.4626" size="1.524" layer="95" rot="MR0"/>
<attribute name="VALUE" x="163.83" y="92.71" size="1.27" layer="96" rot="MR0"/>
<attribute name="DISTRIBUTOR" x="165.1" y="93.98" size="1.778" layer="96" rot="MR90" display="off"/>
<attribute name="MANUFACTURER" x="165.1" y="93.98" size="1.778" layer="96" rot="MR90" display="off"/>
<attribute name="ORDER_NO" x="165.1" y="93.98" size="1.778" layer="96" rot="MR90" display="off"/>
<attribute name="PART_NO" x="165.1" y="93.98" size="1.778" layer="96" rot="MR90" display="off"/>
<attribute name="PRICES" x="165.1" y="93.98" size="1.778" layer="96" font="vector" rot="MR0" display="off"/>
<attribute name="PRICE" x="165.1" y="93.98" size="1.778" layer="96" font="vector" rot="MR0" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="165.1" y="93.98" size="1.778" layer="96" font="vector" rot="MR0" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="165.1" y="93.98" size="1.778" layer="96" font="vector" rot="MR0" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="165.1" y="93.98" size="1.778" layer="96" font="vector" rot="MR0" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="165.1" y="93.98" size="1.778" layer="96" font="vector" rot="MR0" display="off"/>
<attribute name="DATASHEET" x="165.1" y="93.98" size="1.778" layer="96" font="vector" rot="MR0" display="off"/>
<attribute name="POPULATE" x="165.1" y="93.98" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND43" gate="1" x="165.1" y="83.82" rot="MR0"/>
<instance part="R32" gate="G$1" x="142.24" y="106.68" smashed="yes" rot="MR90">
<attribute name="NAME" x="141.224" y="107.1626" size="1.524" layer="95" rot="MR0"/>
<attribute name="VALUE" x="140.97" y="105.156" size="1.27" layer="96" rot="MR0"/>
<attribute name="PRICE" x="142.24" y="106.68" size="3.81" layer="96" rot="MR0" display="off"/>
<attribute name="PRICES" x="142.24" y="106.68" size="3.81" layer="96" rot="MR0" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="142.24" y="106.68" size="3.81" layer="96" rot="MR0" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="142.24" y="106.68" size="3.81" layer="96" rot="MR0" display="off"/>
<attribute name="DATASHEET" x="142.24" y="106.68" size="3.81" layer="96" rot="MR0" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="142.24" y="106.68" size="3.81" layer="96" rot="MR0" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="142.24" y="106.68" size="3.81" layer="96" rot="MR0" display="off"/>
<attribute name="MANUFACTURER" x="142.24" y="106.68" size="3.81" layer="96" rot="MR0" display="off"/>
<attribute name="PART_NO" x="142.24" y="106.68" size="3.81" layer="96" rot="MR0" display="off"/>
<attribute name="DISTRIBUTOR" x="142.24" y="106.68" size="3.81" layer="96" rot="MR0" display="off"/>
<attribute name="ORDER_NO" x="142.24" y="106.68" size="3.81" layer="96" rot="MR0" display="off"/>
<attribute name="POPULATE" x="142.24" y="106.68" size="1.778" layer="96" display="off"/>
</instance>
<instance part="U$4" gate="G$1" x="0" y="0">
<attribute name="CREATE_DATE" x="0" y="0" size="1.778" layer="96" display="off"/>
<attribute name="EDIT_DATE" x="0" y="0" size="1.778" layer="96" display="off"/>
<attribute name="USERNAME" x="0" y="0" size="1.778" layer="96" display="off"/>
<attribute name="FILENAME" x="0" y="0" size="1.778" layer="96" display="off"/>
<attribute name="SUBTITLE" x="0" y="0" size="1.778" layer="96" display="off"/>
<attribute name="TITLE" x="0" y="0" size="1.778" layer="96" display="off"/>
<attribute name="OBJECT_NO" x="0" y="0" size="1.778" layer="96" display="off"/>
<attribute name="DRAWING" x="0" y="0" size="1.778" layer="96" display="off"/>
</instance>
<instance part="H7" gate="G$1" x="96.52" y="88.9">
<attribute name="POPULATE" x="96.52" y="88.9" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="96.52" y="88.9" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="96.52" y="88.9" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="96.52" y="88.9" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="96.52" y="88.9" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="96.52" y="88.9" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="96.52" y="88.9" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="96.52" y="88.9" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="96.52" y="88.9" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="96.52" y="88.9" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="96.52" y="88.9" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="96.52" y="88.9" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND44" gate="1" x="134.62" y="83.82"/>
<instance part="GND45" gate="1" x="58.42" y="76.2"/>
<instance part="R33" gate="G$1" x="48.26" y="101.6" smashed="yes" rot="R270">
<attribute name="NAME" x="46.736" y="102.8954" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="46.736" y="100.838" size="1.27" layer="96" rot="R180"/>
<attribute name="MANUFACTURER" x="48.26" y="101.6" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="PART_NO" x="48.26" y="101.6" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="DISTRIBUTOR" x="48.26" y="101.6" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="ORDER_NO" x="48.26" y="101.6" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="PRICE" x="48.26" y="101.6" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="PRICES" x="48.26" y="101.6" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="48.26" y="101.6" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="48.26" y="101.6" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="DATASHEET" x="48.26" y="101.6" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="48.26" y="101.6" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="48.26" y="101.6" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="POPULATE" x="48.26" y="101.6" size="1.778" layer="96" display="off"/>
</instance>
<instance part="R36" gate="G$1" x="48.26" y="86.36" smashed="yes" rot="R270">
<attribute name="NAME" x="46.736" y="87.6554" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="46.736" y="85.598" size="1.27" layer="96" rot="R180"/>
<attribute name="POPULATE" x="48.26" y="86.36" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="48.26" y="86.36" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="48.26" y="86.36" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="48.26" y="86.36" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="48.26" y="86.36" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="48.26" y="86.36" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="48.26" y="86.36" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="48.26" y="86.36" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="48.26" y="86.36" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="48.26" y="86.36" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="48.26" y="86.36" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="48.26" y="86.36" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND46" gate="1" x="48.26" y="76.2"/>
<instance part="P+10" gate="1" x="48.26" y="111.76"/>
<instance part="P+11" gate="1" x="58.42" y="111.76"/>
<instance part="H2" gate="G$1" x="58.42" y="226.06">
<attribute name="POPULATE" x="58.42" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="58.42" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="58.42" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="58.42" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="58.42" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="58.42" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="58.42" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="58.42" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="58.42" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="58.42" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="58.42" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="58.42" y="226.06" size="1.778" layer="96" display="off"/>
</instance>
<instance part="H3" gate="G$1" x="71.12" y="226.06">
<attribute name="MANUFACTURER" x="71.12" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="71.12" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="71.12" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="71.12" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="71.12" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="71.12" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="71.12" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="71.12" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="71.12" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="71.12" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="71.12" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="POPULATE" x="71.12" y="226.06" size="1.778" layer="96" display="off"/>
</instance>
<instance part="H4" gate="G$1" x="83.82" y="226.06">
<attribute name="MANUFACTURER" x="83.82" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="83.82" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="83.82" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="83.82" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="83.82" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="83.82" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="83.82" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="83.82" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="83.82" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="83.82" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="83.82" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="POPULATE" x="83.82" y="226.06" size="1.778" layer="96" display="off"/>
</instance>
<instance part="H5" gate="G$1" x="96.52" y="226.06">
<attribute name="MANUFACTURER" x="96.52" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="96.52" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="96.52" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="96.52" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="96.52" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="96.52" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="96.52" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="96.52" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="96.52" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="96.52" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="96.52" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="POPULATE" x="96.52" y="226.06" size="1.778" layer="96" display="off"/>
</instance>
<instance part="R18" gate="G$1" x="58.42" y="213.36" smashed="yes" rot="R270">
<attribute name="NAME" x="56.896" y="214.6554" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="56.896" y="212.598" size="1.27" layer="96" rot="R180"/>
<attribute name="MANUFACTURER" x="58.42" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="58.42" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="58.42" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="58.42" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="58.42" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="58.42" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="58.42" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="58.42" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="58.42" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="58.42" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="58.42" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="POPULATE" x="58.42" y="213.36" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND61" gate="1" x="58.42" y="203.2"/>
<instance part="R19" gate="G$1" x="71.12" y="213.36" smashed="yes" rot="R270">
<attribute name="NAME" x="69.596" y="214.6554" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="69.596" y="212.598" size="1.27" layer="96" rot="R180"/>
<attribute name="POPULATE" x="71.12" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="71.12" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="71.12" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="71.12" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="71.12" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="71.12" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="71.12" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="71.12" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="71.12" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="71.12" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="71.12" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="71.12" y="213.36" size="1.778" layer="96" display="off"/>
</instance>
<instance part="R20" gate="G$1" x="83.82" y="213.36" smashed="yes" rot="R270">
<attribute name="NAME" x="82.296" y="214.6554" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="82.296" y="212.598" size="1.27" layer="96" rot="R180"/>
<attribute name="MANUFACTURER" x="83.82" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="83.82" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="83.82" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="83.82" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="83.82" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="83.82" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="83.82" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="83.82" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="83.82" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="83.82" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="83.82" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="POPULATE" x="83.82" y="213.36" size="1.778" layer="96" display="off"/>
</instance>
<instance part="R21" gate="G$1" x="96.52" y="213.36" smashed="yes" rot="R270">
<attribute name="NAME" x="94.996" y="214.6554" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="94.996" y="212.598" size="1.27" layer="96" rot="R180"/>
<attribute name="MANUFACTURER" x="96.52" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="96.52" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="96.52" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="96.52" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="96.52" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="96.52" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="96.52" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="96.52" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="96.52" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="96.52" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="96.52" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="POPULATE" x="96.52" y="213.36" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND62" gate="1" x="71.12" y="203.2"/>
<instance part="GND63" gate="1" x="83.82" y="203.2"/>
<instance part="GND64" gate="1" x="96.52" y="203.2"/>
<instance part="H6" gate="G$1" x="109.22" y="226.06">
<attribute name="MANUFACTURER" x="109.22" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="109.22" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="109.22" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="109.22" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="109.22" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="109.22" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="109.22" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="109.22" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="109.22" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="109.22" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="109.22" y="226.06" size="1.778" layer="96" display="off"/>
<attribute name="POPULATE" x="109.22" y="226.06" size="1.778" layer="96" display="off"/>
</instance>
<instance part="R22" gate="G$1" x="109.22" y="213.36" smashed="yes" rot="R270">
<attribute name="NAME" x="107.696" y="214.6554" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="107.696" y="212.598" size="1.27" layer="96" rot="R180"/>
<attribute name="MANUFACTURER" x="109.22" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="109.22" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="109.22" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="109.22" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="109.22" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="109.22" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="109.22" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="109.22" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="109.22" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="109.22" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="109.22" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="POPULATE" x="109.22" y="213.36" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND65" gate="1" x="109.22" y="203.2"/>
<instance part="S1" gate="G$1" x="53.34" y="170.18" smashed="yes" rot="R270">
<attribute name="NAME" x="52.832" y="171.958" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="52.832" y="169.926" size="1.27" layer="96" rot="R180"/>
<attribute name="POPULATE" x="53.34" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="53.34" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="53.34" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="53.34" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="53.34" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="53.34" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="53.34" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="53.34" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="53.34" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="53.34" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="53.34" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="53.34" y="170.18" size="1.778" layer="96" display="off"/>
</instance>
<instance part="S2" gate="G$1" x="66.04" y="170.18" smashed="yes" rot="R270">
<attribute name="NAME" x="65.532" y="171.958" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="65.278" y="169.926" size="1.27" layer="96" rot="R180"/>
<attribute name="POPULATE" x="66.04" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="66.04" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="66.04" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="66.04" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="66.04" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="66.04" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="66.04" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="66.04" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="66.04" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="66.04" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="66.04" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="66.04" y="170.18" size="1.778" layer="96" display="off"/>
</instance>
<instance part="S3" gate="G$1" x="78.74" y="170.18" smashed="yes" rot="R270">
<attribute name="NAME" x="77.978" y="171.958" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="77.978" y="169.926" size="1.27" layer="96" rot="R180"/>
<attribute name="POPULATE" x="78.74" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="78.74" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="78.74" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="78.74" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="78.74" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="78.74" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="78.74" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="78.74" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="78.74" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="78.74" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="78.74" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="78.74" y="170.18" size="1.778" layer="96" display="off"/>
</instance>
<instance part="S4" gate="G$1" x="91.44" y="170.18" smashed="yes" rot="R270">
<attribute name="NAME" x="90.678" y="171.958" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="90.678" y="169.926" size="1.27" layer="96" rot="R180"/>
<attribute name="POPULATE" x="91.44" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="91.44" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="91.44" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="91.44" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="91.44" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="91.44" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="91.44" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="91.44" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="91.44" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="91.44" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="91.44" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="91.44" y="170.18" size="1.778" layer="96" display="off"/>
</instance>
<instance part="S5" gate="G$1" x="104.14" y="170.18" smashed="yes" rot="R270">
<attribute name="NAME" x="103.378" y="171.958" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="103.378" y="169.926" size="1.27" layer="96" rot="R180"/>
<attribute name="POPULATE" x="104.14" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="104.14" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="104.14" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="104.14" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="104.14" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="104.14" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="104.14" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="104.14" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="104.14" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="104.14" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="104.14" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="104.14" y="170.18" size="1.778" layer="96" display="off"/>
</instance>
<instance part="S6" gate="G$1" x="116.84" y="170.18" smashed="yes" rot="R270">
<attribute name="NAME" x="116.078" y="171.958" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="116.078" y="169.926" size="1.27" layer="96" rot="R180"/>
<attribute name="POPULATE" x="116.84" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="116.84" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="116.84" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="116.84" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="116.84" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="116.84" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="116.84" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="116.84" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="116.84" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="116.84" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="116.84" y="170.18" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="116.84" y="170.18" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND70" gate="1" x="53.34" y="160.02"/>
<instance part="GND71" gate="1" x="66.04" y="160.02"/>
<instance part="GND72" gate="1" x="78.74" y="160.02"/>
<instance part="GND73" gate="1" x="91.44" y="160.02"/>
<instance part="GND74" gate="1" x="104.14" y="160.02"/>
<instance part="GND75" gate="1" x="116.84" y="160.02"/>
<instance part="D3" gate="G$1" x="284.48" y="228.6" smashed="yes">
<attribute name="NAME" x="283.464" y="208.661" size="1.524" layer="95"/>
<attribute name="VALUE" x="280.162" y="206.756" size="1.27" layer="96"/>
<attribute name="POPULATE" x="284.48" y="228.6" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="284.48" y="228.6" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="284.48" y="228.6" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="284.48" y="228.6" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="284.48" y="228.6" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="284.48" y="228.6" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="284.48" y="228.6" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="284.48" y="228.6" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="284.48" y="228.6" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="284.48" y="228.6" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="284.48" y="228.6" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="284.48" y="228.6" size="1.778" layer="96" display="off"/>
</instance>
<instance part="C25" gate="G$1" x="259.08" y="243.84" smashed="yes">
<attribute name="NAME" x="258.826" y="241.427" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="258.826" y="239.649" size="1.27" layer="96" rot="R180"/>
<attribute name="POPULATE" x="259.08" y="243.84" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="259.08" y="243.84" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="259.08" y="243.84" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="259.08" y="243.84" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="259.08" y="243.84" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="259.08" y="243.84" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="259.08" y="243.84" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="259.08" y="243.84" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="259.08" y="243.84" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="259.08" y="243.84" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="259.08" y="243.84" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="259.08" y="243.84" size="1.778" layer="96" display="off"/>
</instance>
<instance part="C24" gate="G$1" x="248.92" y="243.84" smashed="yes">
<attribute name="NAME" x="248.666" y="241.427" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="248.666" y="239.649" size="1.27" layer="96" rot="R180"/>
<attribute name="MANUFACTURER" x="248.92" y="243.84" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="248.92" y="243.84" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="248.92" y="243.84" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="248.92" y="243.84" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="248.92" y="243.84" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="248.92" y="243.84" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="248.92" y="243.84" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="248.92" y="243.84" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="248.92" y="243.84" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="248.92" y="243.84" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="248.92" y="243.84" size="1.778" layer="96" display="off"/>
<attribute name="POPULATE" x="248.92" y="243.84" size="1.778" layer="96" display="off"/>
</instance>
<instance part="C28" gate="G$1" x="309.88" y="238.76" smashed="yes">
<attribute name="NAME" x="309.626" y="236.347" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="309.626" y="234.569" size="1.27" layer="96" rot="R180"/>
<attribute name="MANUFACTURER" x="309.88" y="238.76" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="309.88" y="238.76" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="309.88" y="238.76" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="309.88" y="238.76" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="309.88" y="238.76" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="309.88" y="238.76" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="309.88" y="238.76" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="309.88" y="238.76" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="309.88" y="238.76" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="309.88" y="238.76" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="309.88" y="238.76" size="1.778" layer="96" display="off"/>
<attribute name="POPULATE" x="309.88" y="238.76" size="1.778" layer="96" display="off"/>
</instance>
<instance part="C29" gate="G$1" x="314.96" y="238.76" smashed="yes">
<attribute name="NAME" x="314.706" y="236.347" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="314.706" y="234.569" size="1.27" layer="96" rot="R180"/>
<attribute name="MANUFACTURER" x="314.96" y="238.76" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="314.96" y="238.76" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="314.96" y="238.76" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="314.96" y="238.76" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="314.96" y="238.76" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="314.96" y="238.76" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="314.96" y="238.76" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="314.96" y="238.76" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="314.96" y="238.76" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="314.96" y="238.76" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="314.96" y="238.76" size="1.778" layer="96" display="off"/>
<attribute name="POPULATE" x="314.96" y="238.76" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND77" gate="1" x="309.88" y="226.06"/>
<instance part="GND78" gate="1" x="314.96" y="226.06"/>
<instance part="D3" gate="P" x="223.52" y="241.3"/>
<instance part="C26" gate="G$1" x="231.14" y="241.3" smashed="yes">
<attribute name="MANUFACTURER" x="231.14" y="241.3" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="231.14" y="241.3" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="231.14" y="241.3" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="231.14" y="241.3" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="231.14" y="241.3" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="231.14" y="241.3" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="231.14" y="241.3" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="231.14" y="241.3" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="231.14" y="241.3" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="231.14" y="241.3" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="231.14" y="241.3" size="1.778" layer="96" display="off"/>
<attribute name="NAME" x="230.886" y="239.141" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="230.886" y="237.363" size="1.27" layer="96" rot="R180"/>
</instance>
<instance part="C27" gate="G$1" x="236.22" y="241.3" smashed="yes">
<attribute name="NAME" x="235.966" y="238.887" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="235.966" y="237.109" size="1.27" layer="96" rot="R180"/>
<attribute name="MANUFACTURER" x="236.22" y="241.3" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="236.22" y="241.3" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="236.22" y="241.3" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="236.22" y="241.3" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="236.22" y="241.3" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="236.22" y="241.3" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="236.22" y="241.3" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="236.22" y="241.3" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="236.22" y="241.3" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="236.22" y="241.3" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="236.22" y="241.3" size="1.778" layer="96" display="off"/>
<attribute name="POPULATE" x="236.22" y="241.3" size="1.778" layer="96" display="off"/>
</instance>
<instance part="P+15" gate="1" x="231.14" y="256.54"/>
<instance part="GND79" gate="1" x="231.14" y="226.06"/>
<instance part="X6" gate="-1" x="363.22" y="215.9" smashed="yes" rot="MR0">
<attribute name="VALUE" x="360.172" y="204.851" size="1.27" layer="96" rot="MR180"/>
<attribute name="NAME" x="361.188" y="206.756" size="1.524" layer="95" rot="MR180"/>
<attribute name="POPULATE" x="363.22" y="215.9" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="363.22" y="215.9" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="363.22" y="215.9" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="363.22" y="215.9" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="363.22" y="215.9" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="363.22" y="215.9" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="363.22" y="215.9" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="363.22" y="215.9" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="363.22" y="215.9" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="363.22" y="215.9" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="363.22" y="215.9" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="363.22" y="215.9" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND80" gate="1" x="353.06" y="205.74"/>
<instance part="R17" gate="G$1" x="322.58" y="215.9" smashed="yes">
<attribute name="NAME" x="319.786" y="215.6714" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="325.374" y="214.376" size="1.27" layer="96"/>
<attribute name="MANUFACTURER" x="322.58" y="215.9" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="322.58" y="215.9" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="322.58" y="215.9" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="322.58" y="215.9" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="322.58" y="215.9" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="322.58" y="215.9" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="322.58" y="215.9" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="322.58" y="215.9" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="322.58" y="215.9" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="322.58" y="215.9" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="322.58" y="215.9" size="1.778" layer="96" display="off"/>
</instance>
<instance part="R15" gate="G$1" x="322.58" y="218.44" smashed="yes">
<attribute name="NAME" x="319.786" y="218.2114" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="325.374" y="216.916" size="1.27" layer="96"/>
<attribute name="POPULATE" x="322.58" y="218.44" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="322.58" y="218.44" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="322.58" y="218.44" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="322.58" y="218.44" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="322.58" y="218.44" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="322.58" y="218.44" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="322.58" y="218.44" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="322.58" y="218.44" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="322.58" y="218.44" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="322.58" y="218.44" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="322.58" y="218.44" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="322.58" y="218.44" size="1.778" layer="96" display="off"/>
</instance>
<instance part="X5" gate="G$1" x="365.76" y="248.92" smashed="yes" rot="MR0">
<attribute name="VALUE" x="361.95" y="241.046" size="1.27" layer="96" rot="MR180"/>
<attribute name="NAME" x="363.22" y="243.332" size="1.524" layer="95" rot="MR180"/>
<attribute name="POPULATE" x="365.76" y="248.92" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="365.76" y="248.92" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="365.76" y="248.92" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="365.76" y="248.92" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="365.76" y="248.92" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="365.76" y="248.92" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="365.76" y="248.92" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="365.76" y="248.92" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="365.76" y="248.92" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="365.76" y="248.92" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="365.76" y="248.92" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="365.76" y="248.92" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND81" gate="1" x="355.6" y="238.76"/>
<instance part="R24" gate="G$1" x="256.54" y="213.36" smashed="yes">
<attribute name="NAME" x="253.492" y="212.8774" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="259.334" y="211.582" size="1.27" layer="96"/>
<attribute name="MANUFACTURER" x="256.54" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="256.54" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="256.54" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="256.54" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="256.54" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="256.54" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="256.54" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="256.54" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="256.54" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="256.54" y="213.36" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="256.54" y="213.36" size="1.778" layer="96" display="off"/>
</instance>
<instance part="R16" gate="G$1" x="256.54" y="215.9" smashed="yes">
<attribute name="NAME" x="253.492" y="215.4174" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="259.334" y="214.122" size="1.27" layer="96"/>
<attribute name="MANUFACTURER" x="256.54" y="215.9" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="256.54" y="215.9" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="256.54" y="215.9" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="256.54" y="215.9" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="256.54" y="215.9" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="256.54" y="215.9" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="256.54" y="215.9" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="256.54" y="215.9" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="256.54" y="215.9" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="256.54" y="215.9" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="256.54" y="215.9" size="1.778" layer="96" display="off"/>
<attribute name="POPULATE" x="256.54" y="215.9" size="1.778" layer="96" display="off"/>
</instance>
<instance part="R26" gate="G$1" x="246.38" y="205.74" smashed="yes" rot="R270">
<attribute name="NAME" x="245.11" y="207.2894" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="245.11" y="205.486" size="1.27" layer="96" rot="R180"/>
<attribute name="MANUFACTURER" x="246.38" y="205.74" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="246.38" y="205.74" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="246.38" y="205.74" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="246.38" y="205.74" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="246.38" y="205.74" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="246.38" y="205.74" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="246.38" y="205.74" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="246.38" y="205.74" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="246.38" y="205.74" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="246.38" y="205.74" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="246.38" y="205.74" size="1.778" layer="96" display="off"/>
</instance>
<instance part="R25" gate="G$1" x="238.76" y="205.74" smashed="yes" rot="R270">
<attribute name="NAME" x="237.49" y="207.2894" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="237.49" y="205.486" size="1.27" layer="96" rot="R180"/>
<attribute name="MANUFACTURER" x="238.76" y="205.74" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="238.76" y="205.74" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="238.76" y="205.74" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="238.76" y="205.74" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="238.76" y="205.74" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="238.76" y="205.74" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="238.76" y="205.74" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="238.76" y="205.74" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="238.76" y="205.74" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="238.76" y="205.74" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="238.76" y="205.74" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND82" gate="1" x="246.38" y="195.58"/>
<instance part="GND83" gate="1" x="238.76" y="195.58"/>
<instance part="GND17" gate="1" x="335.28" y="147.32"/>
<instance part="C30" gate="G$1" x="347.98" y="160.02" smashed="yes">
<attribute name="MANUFACTURER" x="347.98" y="160.02" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="347.98" y="160.02" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="347.98" y="160.02" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="347.98" y="160.02" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="347.98" y="160.02" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="347.98" y="160.02" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="347.98" y="160.02" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="347.98" y="160.02" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="347.98" y="160.02" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="347.98" y="160.02" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="347.98" y="160.02" size="1.778" layer="96" display="off"/>
<attribute name="NAME" x="347.726" y="157.607" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="347.726" y="155.829" size="1.27" layer="96" rot="R180"/>
<attribute name="POPULATE" x="347.98" y="160.02" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND18" gate="1" x="347.98" y="147.32"/>
<instance part="GND19" gate="1" x="340.36" y="147.32"/>
<instance part="R29" gate="G$1" x="340.36" y="157.48" smashed="yes" rot="R270">
<attribute name="NAME" x="339.09" y="159.2834" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="339.09" y="157.226" size="1.27" layer="96" rot="R180"/>
<attribute name="MANUFACTURER" x="340.36" y="157.48" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="PART_NO" x="340.36" y="157.48" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="DISTRIBUTOR" x="340.36" y="157.48" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="ORDER_NO" x="340.36" y="157.48" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="PRICE" x="340.36" y="157.48" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="PRICES" x="340.36" y="157.48" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="340.36" y="157.48" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="340.36" y="157.48" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="DATASHEET" x="340.36" y="157.48" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="340.36" y="157.48" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="340.36" y="157.48" size="1.778" layer="96" rot="MR0" display="off"/>
<attribute name="POPULATE" x="340.36" y="157.48" size="1.778" layer="96" display="off"/>
</instance>
<instance part="V11" gate="G$1" x="307.34" y="167.64">
<attribute name="PRICE" x="307.34" y="167.64" size="2.54" layer="96" display="off"/>
<attribute name="PRICES" x="307.34" y="167.64" size="2.54" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="307.34" y="167.64" size="2.54" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="307.34" y="167.64" size="2.54" layer="96" display="off"/>
<attribute name="DATASHEET" x="307.34" y="167.64" size="2.54" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="307.34" y="167.64" size="2.54" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="307.34" y="167.64" size="2.54" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="307.34" y="167.64" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="307.34" y="167.64" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="307.34" y="167.64" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="307.34" y="167.64" size="1.778" layer="96" display="off"/>
<attribute name="POPULATE" x="307.34" y="167.64" size="1.778" layer="96" display="off"/>
</instance>
<instance part="V10" gate="G$1" x="289.56" y="167.64">
<attribute name="PRICE" x="289.56" y="167.64" size="2.54" layer="96" display="off"/>
<attribute name="PRICES" x="289.56" y="167.64" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="289.56" y="167.64" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="289.56" y="167.64" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="289.56" y="167.64" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="289.56" y="167.64" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="289.56" y="167.64" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="289.56" y="167.64" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="289.56" y="167.64" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="289.56" y="167.64" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="289.56" y="167.64" size="1.778" layer="96" display="off"/>
<attribute name="POPULATE" x="289.56" y="167.64" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND27" gate="1" x="289.56" y="157.48"/>
<instance part="GND28" gate="1" x="307.34" y="157.48"/>
<instance part="V12" gate="G$1" x="325.12" y="167.64">
<attribute name="PRICE" x="325.12" y="167.64" size="2.54" layer="96" display="off"/>
<attribute name="PRICES" x="325.12" y="167.64" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="325.12" y="167.64" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="325.12" y="167.64" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="325.12" y="167.64" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="325.12" y="167.64" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="325.12" y="167.64" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="325.12" y="167.64" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="325.12" y="167.64" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="325.12" y="167.64" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="325.12" y="167.64" size="1.778" layer="96" display="off"/>
<attribute name="POPULATE" x="325.12" y="167.64" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND29" gate="1" x="325.12" y="157.48"/>
<instance part="R27" gate="G$1" x="264.16" y="180.34" smashed="yes">
<attribute name="NAME" x="261.112" y="179.8574" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="266.954" y="178.562" size="1.27" layer="96"/>
<attribute name="MANUFACTURER" x="264.16" y="180.34" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="264.16" y="180.34" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="264.16" y="180.34" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="264.16" y="180.34" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="264.16" y="180.34" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="264.16" y="180.34" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="264.16" y="180.34" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="264.16" y="180.34" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="264.16" y="180.34" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="264.16" y="180.34" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="264.16" y="180.34" size="1.778" layer="96" display="off"/>
<attribute name="POPULATE" x="264.16" y="180.34" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND30" gate="1" x="256.54" y="147.32"/>
<instance part="R28" gate="G$1" x="256.54" y="162.56" smashed="yes" rot="R270">
<attribute name="NAME" x="255.27" y="164.1094" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="255.27" y="162.306" size="1.27" layer="96" rot="R180"/>
<attribute name="POPULATE" x="256.54" y="162.56" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="256.54" y="162.56" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="256.54" y="162.56" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="256.54" y="162.56" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="256.54" y="162.56" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="256.54" y="162.56" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="256.54" y="162.56" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="256.54" y="162.56" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="256.54" y="162.56" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="256.54" y="162.56" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="256.54" y="162.56" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="256.54" y="162.56" size="1.778" layer="96" display="off"/>
</instance>
<instance part="X7" gate="G$1" x="368.3" y="175.26">
<attribute name="MANUFACTURER" x="368.3" y="175.26" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="368.3" y="175.26" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="368.3" y="175.26" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="368.3" y="175.26" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="368.3" y="175.26" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="368.3" y="175.26" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="368.3" y="175.26" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="368.3" y="175.26" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="368.3" y="175.26" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="368.3" y="175.26" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="368.3" y="175.26" size="1.778" layer="96" display="off"/>
<attribute name="POPULATE" x="368.3" y="175.26" size="1.778" layer="96" display="off"/>
</instance>
<instance part="X8" gate="G$1" x="373.38" y="124.46" smashed="yes" rot="R180">
<attribute name="VALUE" x="373.38" y="114.3" size="1.27" layer="96" rot="R180"/>
<attribute name="NAME" x="373.38" y="116.078" size="1.524" layer="95" rot="R180"/>
<attribute name="POPULATE" x="373.38" y="124.46" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="373.38" y="124.46" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="373.38" y="124.46" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="373.38" y="124.46" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="373.38" y="124.46" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="373.38" y="124.46" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="373.38" y="124.46" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="373.38" y="124.46" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="373.38" y="124.46" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="373.38" y="124.46" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="373.38" y="124.46" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="373.38" y="124.46" size="1.778" layer="96" display="off"/>
</instance>
<instance part="R48" gate="G$1" x="111.76" y="38.1" smashed="yes" rot="R270">
<attribute name="NAME" x="112.2426" y="35.306" size="1.524" layer="95" rot="R270"/>
<attribute name="VALUE" x="112.268" y="43.942" size="1.27" layer="96" rot="R270"/>
<attribute name="MANUFACTURER" x="111.76" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="111.76" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="111.76" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="111.76" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="111.76" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="111.76" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="111.76" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="111.76" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="111.76" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="111.76" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="111.76" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="POPULATE" x="111.76" y="38.1" size="1.778" layer="96" display="off"/>
</instance>
<instance part="R40" gate="G$1" x="111.76" y="60.96" smashed="yes" rot="R90">
<attribute name="NAME" x="112.2426" y="57.912" size="1.524" layer="95" rot="R270"/>
<attribute name="VALUE" x="113.538" y="63.754" size="1.27" layer="96" rot="R90"/>
<attribute name="MANUFACTURER" x="111.76" y="60.96" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="PART_NO" x="111.76" y="60.96" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="DISTRIBUTOR" x="111.76" y="60.96" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="ORDER_NO" x="111.76" y="60.96" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="PRICE" x="111.76" y="60.96" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="PRICES" x="111.76" y="60.96" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="111.76" y="60.96" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="111.76" y="60.96" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="DATASHEET" x="111.76" y="60.96" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="111.76" y="60.96" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="111.76" y="60.96" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="POPULATE" x="111.76" y="60.96" size="1.778" layer="96" display="off"/>
</instance>
<instance part="R39" gate="G$1" x="109.22" y="60.96" smashed="yes" rot="R90">
<attribute name="NAME" x="109.7026" y="57.912" size="1.524" layer="95" rot="R270"/>
<attribute name="VALUE" x="110.998" y="63.754" size="1.27" layer="96" rot="R90"/>
<attribute name="MANUFACTURER" x="109.22" y="60.96" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="PART_NO" x="109.22" y="60.96" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="DISTRIBUTOR" x="109.22" y="60.96" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="ORDER_NO" x="109.22" y="60.96" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="PRICE" x="109.22" y="60.96" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="PRICES" x="109.22" y="60.96" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="109.22" y="60.96" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="109.22" y="60.96" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="DATASHEET" x="109.22" y="60.96" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="109.22" y="60.96" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="109.22" y="60.96" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="POPULATE" x="109.22" y="60.96" size="1.778" layer="96" display="off"/>
</instance>
<instance part="R38" gate="G$1" x="106.68" y="60.96" smashed="yes" rot="R90">
<attribute name="NAME" x="107.1626" y="57.912" size="1.524" layer="95" rot="R270"/>
<attribute name="VALUE" x="108.458" y="63.754" size="1.27" layer="96" rot="R90"/>
<attribute name="MANUFACTURER" x="106.68" y="60.96" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="PART_NO" x="106.68" y="60.96" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="DISTRIBUTOR" x="106.68" y="60.96" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="ORDER_NO" x="106.68" y="60.96" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="PRICE" x="106.68" y="60.96" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="PRICES" x="106.68" y="60.96" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="106.68" y="60.96" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="106.68" y="60.96" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="DATASHEET" x="106.68" y="60.96" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="106.68" y="60.96" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="106.68" y="60.96" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="POPULATE" x="106.68" y="60.96" size="1.778" layer="96" display="off"/>
</instance>
<instance part="R37" gate="G$1" x="104.14" y="60.96" smashed="yes" rot="R90">
<attribute name="NAME" x="104.6226" y="57.912" size="1.524" layer="95" rot="R270"/>
<attribute name="VALUE" x="105.918" y="63.754" size="1.27" layer="96" rot="R90"/>
<attribute name="MANUFACTURER" x="104.14" y="60.96" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="PART_NO" x="104.14" y="60.96" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="DISTRIBUTOR" x="104.14" y="60.96" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="ORDER_NO" x="104.14" y="60.96" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="PRICE" x="104.14" y="60.96" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="PRICES" x="104.14" y="60.96" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="104.14" y="60.96" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="104.14" y="60.96" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="DATASHEET" x="104.14" y="60.96" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="104.14" y="60.96" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="104.14" y="60.96" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="POPULATE" x="104.14" y="60.96" size="1.778" layer="96" display="off"/>
</instance>
<instance part="R47" gate="G$1" x="109.22" y="38.1" smashed="yes" rot="R270">
<attribute name="NAME" x="109.7026" y="35.306" size="1.524" layer="95" rot="R270"/>
<attribute name="VALUE" x="109.728" y="43.942" size="1.27" layer="96" rot="R270"/>
<attribute name="MANUFACTURER" x="109.22" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="109.22" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="109.22" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="109.22" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="109.22" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="109.22" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="109.22" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="109.22" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="109.22" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="109.22" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="109.22" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="POPULATE" x="109.22" y="38.1" size="1.778" layer="96" display="off"/>
</instance>
<instance part="R46" gate="G$1" x="106.68" y="38.1" smashed="yes" rot="R270">
<attribute name="NAME" x="107.1626" y="35.306" size="1.524" layer="95" rot="R270"/>
<attribute name="VALUE" x="107.188" y="43.942" size="1.27" layer="96" rot="R270"/>
<attribute name="MANUFACTURER" x="106.68" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="106.68" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="106.68" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="106.68" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="106.68" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="106.68" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="106.68" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="106.68" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="106.68" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="106.68" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="106.68" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="POPULATE" x="106.68" y="38.1" size="1.778" layer="96" display="off"/>
</instance>
<instance part="R45" gate="G$1" x="104.14" y="38.1" smashed="yes" rot="R270">
<attribute name="NAME" x="104.6226" y="35.306" size="1.524" layer="95" rot="R270"/>
<attribute name="VALUE" x="104.648" y="43.942" size="1.27" layer="96" rot="R270"/>
<attribute name="MANUFACTURER" x="104.14" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="104.14" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="104.14" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="104.14" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="104.14" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="104.14" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="104.14" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="104.14" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="104.14" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="104.14" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="104.14" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="POPULATE" x="104.14" y="38.1" size="1.778" layer="96" display="off"/>
</instance>
<instance part="R44" gate="G$1" x="101.6" y="38.1" smashed="yes" rot="R270">
<attribute name="NAME" x="102.0826" y="35.306" size="1.524" layer="95" rot="R270"/>
<attribute name="VALUE" x="102.108" y="43.942" size="1.27" layer="96" rot="R270"/>
<attribute name="MANUFACTURER" x="101.6" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="101.6" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="101.6" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="101.6" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="101.6" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="101.6" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="101.6" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="101.6" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="101.6" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="101.6" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="101.6" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="POPULATE" x="101.6" y="38.1" size="1.778" layer="96" display="off"/>
</instance>
<instance part="R43" gate="G$1" x="99.06" y="38.1" smashed="yes" rot="R270">
<attribute name="NAME" x="99.5426" y="35.306" size="1.524" layer="95" rot="R270"/>
<attribute name="VALUE" x="99.568" y="43.942" size="1.27" layer="96" rot="R270"/>
<attribute name="MANUFACTURER" x="99.06" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="99.06" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="99.06" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="99.06" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="99.06" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="99.06" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="99.06" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="99.06" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="99.06" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="99.06" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="99.06" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="POPULATE" x="99.06" y="38.1" size="1.778" layer="96" display="off"/>
</instance>
<instance part="R42" gate="G$1" x="96.52" y="38.1" smashed="yes" rot="R270">
<attribute name="NAME" x="97.0026" y="35.306" size="1.524" layer="95" rot="R270"/>
<attribute name="VALUE" x="97.028" y="43.942" size="1.27" layer="96" rot="R270"/>
<attribute name="MANUFACTURER" x="96.52" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="96.52" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="96.52" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="96.52" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="96.52" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="96.52" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="96.52" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="96.52" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="96.52" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="96.52" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="96.52" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="POPULATE" x="96.52" y="38.1" size="1.778" layer="96" display="off"/>
</instance>
<instance part="R41" gate="G$1" x="93.98" y="38.1" smashed="yes" rot="R270">
<attribute name="NAME" x="94.4626" y="35.306" size="1.524" layer="95" rot="R270"/>
<attribute name="VALUE" x="94.488" y="43.942" size="1.27" layer="96" rot="R270"/>
<attribute name="MANUFACTURER" x="93.98" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="93.98" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="93.98" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="93.98" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="93.98" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="93.98" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="93.98" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="93.98" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="93.98" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="93.98" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="93.98" y="38.1" size="1.778" layer="96" display="off"/>
<attribute name="POPULATE" x="93.98" y="38.1" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND1" gate="1" x="93.98" y="25.4"/>
<instance part="GND2" gate="1" x="111.76" y="25.4"/>
<instance part="X9" gate="G$1" x="355.6" y="86.36" smashed="yes">
<attribute name="NAME" x="358.394" y="64.008" size="1.524" layer="95"/>
<attribute name="VALUE" x="357.886" y="62.23" size="1.27" layer="96"/>
<attribute name="POPULATE" x="355.6" y="86.36" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="355.6" y="86.36" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="355.6" y="86.36" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="355.6" y="86.36" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="355.6" y="86.36" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="355.6" y="86.36" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="355.6" y="86.36" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="355.6" y="86.36" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="355.6" y="86.36" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="355.6" y="86.36" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="355.6" y="86.36" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="355.6" y="86.36" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND3" gate="1" x="340.36" y="63.5"/>
<instance part="P+1" gate="1" x="330.2" y="101.6"/>
<instance part="X10" gate="1" x="363.22" y="35.56" smashed="yes" rot="R180">
<attribute name="VALUE" x="363.474" y="22.606" size="1.27" layer="96" rot="R180"/>
<attribute name="NAME" x="363.474" y="24.638" size="1.524" layer="95" rot="R180"/>
<attribute name="POPULATE" x="363.22" y="35.56" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="363.22" y="35.56" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="363.22" y="35.56" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="363.22" y="35.56" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="363.22" y="35.56" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="363.22" y="35.56" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="363.22" y="35.56" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="363.22" y="35.56" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="363.22" y="35.56" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="363.22" y="35.56" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="363.22" y="35.56" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="363.22" y="35.56" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND4" gate="1" x="350.52" y="22.86"/>
<instance part="P+2" gate="1" x="350.52" y="48.26"/>
<instance part="V15" gate="G$1" x="228.6" y="68.58">
<attribute name="PRICE" x="228.6" y="68.58" size="2.54" layer="96" display="off"/>
<attribute name="PRICES" x="228.6" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="228.6" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="228.6" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="228.6" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="228.6" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="228.6" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="228.6" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="228.6" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="228.6" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="228.6" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="POPULATE" x="228.6" y="68.58" size="1.778" layer="96" display="off"/>
</instance>
<instance part="V16" gate="G$1" x="248.92" y="68.58">
<attribute name="PRICE" x="248.92" y="68.58" size="2.54" layer="96" display="off"/>
<attribute name="PRICES" x="248.92" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="248.92" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="248.92" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="248.92" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="248.92" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="248.92" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="POPULATE" x="248.92" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="248.92" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="248.92" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="248.92" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="248.92" y="68.58" size="1.778" layer="96" display="off"/>
</instance>
<instance part="V17" gate="G$1" x="269.24" y="68.58">
<attribute name="PRICE" x="269.24" y="68.58" size="2.54" layer="96" display="off"/>
<attribute name="PRICES" x="269.24" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="269.24" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="269.24" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="269.24" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="269.24" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="269.24" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="269.24" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="269.24" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="269.24" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="269.24" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="POPULATE" x="269.24" y="68.58" size="1.778" layer="96" display="off"/>
</instance>
<instance part="V18" gate="G$1" x="289.56" y="68.58">
<attribute name="PRICE" x="289.56" y="68.58" size="2.54" layer="96" display="off"/>
<attribute name="PRICES" x="289.56" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="289.56" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="289.56" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="289.56" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="289.56" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="289.56" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="289.56" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="289.56" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="289.56" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="289.56" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="POPULATE" x="289.56" y="68.58" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND5" gate="1" x="289.56" y="58.42"/>
<instance part="GND6" gate="1" x="269.24" y="58.42"/>
<instance part="GND7" gate="1" x="248.92" y="58.42"/>
<instance part="GND8" gate="1" x="228.6" y="58.42"/>
<instance part="V19" gate="G$1" x="309.88" y="68.58">
<attribute name="PRICE" x="309.88" y="68.58" size="2.54" layer="96" display="off"/>
<attribute name="PRICES" x="309.88" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="309.88" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="309.88" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="309.88" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="309.88" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="309.88" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="309.88" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="309.88" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="309.88" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="309.88" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="POPULATE" x="309.88" y="68.58" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND9" gate="1" x="309.88" y="58.42"/>
<instance part="V20" gate="G$1" x="330.2" y="68.58">
<attribute name="PRICE" x="330.2" y="68.58" size="2.54" layer="96" display="off"/>
<attribute name="PRICES" x="330.2" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="330.2" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="330.2" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="330.2" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="330.2" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="330.2" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="330.2" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="330.2" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="330.2" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="330.2" y="68.58" size="1.778" layer="96" display="off"/>
<attribute name="POPULATE" x="330.2" y="68.58" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND15" gate="1" x="330.2" y="58.42"/>
<instance part="A6" gate="G$1" x="96.52" y="114.3" smashed="yes">
<attribute name="NAME" x="95.25" y="109.474" size="1.524" layer="95"/>
<attribute name="VALUE" x="88.9" y="107.442" size="1.27" layer="96"/>
<attribute name="POPULATE" x="96.52" y="114.3" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="96.52" y="114.3" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="96.52" y="114.3" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="96.52" y="114.3" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="96.52" y="114.3" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="96.52" y="114.3" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="96.52" y="114.3" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="96.52" y="114.3" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="96.52" y="114.3" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="96.52" y="114.3" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="96.52" y="114.3" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="96.52" y="114.3" size="1.778" layer="96" display="off"/>
</instance>
<instance part="H1" gate="G$1" x="190.5" y="238.76" smashed="yes" rot="R270">
<attribute name="NAME" x="191.262" y="230.886" size="1.524" layer="95"/>
<attribute name="VALUE" x="186.436" y="228.727" size="1.27" layer="96"/>
<attribute name="POPULATE" x="190.5" y="238.76" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="190.5" y="238.76" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="190.5" y="238.76" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="190.5" y="238.76" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="190.5" y="238.76" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="190.5" y="238.76" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="190.5" y="238.76" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="190.5" y="238.76" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="190.5" y="238.76" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="190.5" y="238.76" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="190.5" y="238.76" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="190.5" y="238.76" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND20" gate="1" x="185.42" y="203.2"/>
<instance part="V9" gate="G$1" x="185.42" y="223.52" smashed="yes">
<attribute name="NAME" x="182.372" y="225.044" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="182.372" y="222.758" size="1.27" layer="96" rot="R180"/>
<attribute name="MANUFACTURER" x="185.42" y="223.52" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="185.42" y="223.52" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="185.42" y="223.52" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="185.42" y="223.52" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="185.42" y="223.52" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="185.42" y="223.52" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="185.42" y="223.52" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="185.42" y="223.52" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="185.42" y="223.52" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="185.42" y="223.52" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="185.42" y="223.52" size="1.778" layer="96" display="off"/>
<attribute name="POPULATE" x="185.42" y="223.52" size="1.778" layer="96" rot="MR0" display="off"/>
</instance>
<instance part="R23" gate="G$1" x="177.8" y="213.36" smashed="yes" rot="R270">
<attribute name="NAME" x="176.276" y="214.6554" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="176.276" y="212.852" size="1.27" layer="96" rot="R180"/>
<attribute name="MANUFACTURER" x="177.8" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="PART_NO" x="177.8" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="DISTRIBUTOR" x="177.8" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="ORDER_NO" x="177.8" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="PRICE" x="177.8" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="PRICES" x="177.8" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="177.8" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="177.8" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="DATASHEET" x="177.8" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="177.8" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="177.8" y="213.36" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="POPULATE" x="177.8" y="213.36" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND22" gate="1" x="177.8" y="203.2"/>
<instance part="P+3" gate="1" x="185.42" y="251.46"/>
<instance part="R14" gate="G$1" x="180.34" y="238.76" smashed="yes" rot="R270">
<attribute name="NAME" x="178.816" y="240.0554" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="178.816" y="238.252" size="1.27" layer="96" rot="R180"/>
<attribute name="MANUFACTURER" x="180.34" y="238.76" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="PART_NO" x="180.34" y="238.76" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="DISTRIBUTOR" x="180.34" y="238.76" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="ORDER_NO" x="180.34" y="238.76" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="PRICE" x="180.34" y="238.76" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="PRICES" x="180.34" y="238.76" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="180.34" y="238.76" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="180.34" y="238.76" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="DATASHEET" x="180.34" y="238.76" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="180.34" y="238.76" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="180.34" y="238.76" size="1.778" layer="96" rot="R270" display="off"/>
<attribute name="POPULATE" x="180.34" y="238.76" size="1.778" layer="96" display="off"/>
</instance>
<instance part="A1" gate="G$1" x="55.88" y="246.38" smashed="yes">
<attribute name="NAME" x="54.61" y="241.808" size="1.524" layer="95"/>
<attribute name="VALUE" x="51.562" y="240.03" size="1.27" layer="96"/>
<attribute name="POPULATE" x="55.88" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="55.88" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="55.88" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="55.88" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="55.88" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="55.88" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="55.88" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="55.88" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="55.88" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="55.88" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="55.88" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="55.88" y="246.38" size="1.778" layer="96" display="off"/>
</instance>
<instance part="A2" gate="G$1" x="68.58" y="246.38" smashed="yes">
<attribute name="NAME" x="67.31" y="241.808" size="1.524" layer="95"/>
<attribute name="VALUE" x="64.262" y="240.03" size="1.27" layer="96"/>
<attribute name="MANUFACTURER" x="68.58" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="68.58" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="68.58" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="68.58" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="68.58" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="68.58" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="68.58" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="68.58" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="68.58" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="68.58" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="68.58" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="POPULATE" x="68.58" y="246.38" size="1.778" layer="96" display="off"/>
</instance>
<instance part="A3" gate="G$1" x="81.28" y="246.38" smashed="yes">
<attribute name="NAME" x="80.01" y="241.808" size="1.524" layer="95"/>
<attribute name="VALUE" x="76.962" y="240.03" size="1.27" layer="96"/>
<attribute name="MANUFACTURER" x="81.28" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="81.28" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="81.28" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="81.28" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="81.28" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="81.28" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="81.28" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="81.28" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="81.28" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="81.28" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="81.28" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="POPULATE" x="81.28" y="246.38" size="1.778" layer="96" display="off"/>
</instance>
<instance part="A4" gate="G$1" x="93.98" y="246.38" smashed="yes">
<attribute name="NAME" x="92.71" y="241.808" size="1.524" layer="95"/>
<attribute name="VALUE" x="89.662" y="240.03" size="1.27" layer="96"/>
<attribute name="MANUFACTURER" x="93.98" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="93.98" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="93.98" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="93.98" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="93.98" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="93.98" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="93.98" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="93.98" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="93.98" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="93.98" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="93.98" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="POPULATE" x="93.98" y="246.38" size="1.778" layer="96" display="off"/>
</instance>
<instance part="A5" gate="G$1" x="106.68" y="246.38" smashed="yes">
<attribute name="NAME" x="105.41" y="241.808" size="1.524" layer="95"/>
<attribute name="VALUE" x="102.362" y="240.03" size="1.27" layer="96"/>
<attribute name="MANUFACTURER" x="106.68" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="106.68" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="106.68" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="106.68" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="106.68" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="106.68" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="106.68" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="106.68" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="106.68" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="106.68" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="106.68" y="246.38" size="1.778" layer="96" display="off"/>
<attribute name="POPULATE" x="106.68" y="246.38" size="1.778" layer="96" display="off"/>
</instance>
<instance part="R34" gate="G$1" x="33.02" y="93.98" smashed="yes">
<attribute name="NAME" x="30.988" y="91.1606" size="1.524" layer="95"/>
<attribute name="VALUE" x="31.75" y="89.662" size="1.27" layer="96"/>
<attribute name="POPULATE" x="33.02" y="93.98" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="PRICE" x="33.02" y="93.98" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="PRICES" x="33.02" y="93.98" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="33.02" y="93.98" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="33.02" y="93.98" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="DATASHEET" x="33.02" y="93.98" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="33.02" y="93.98" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="33.02" y="93.98" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="MANUFACTURER" x="33.02" y="93.98" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="PART_NO" x="33.02" y="93.98" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="DISTRIBUTOR" x="33.02" y="93.98" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="ORDER_NO" x="33.02" y="93.98" size="1.778" layer="96" rot="R90" display="off"/>
</instance>
<instance part="GND31" gate="1" x="40.64" y="76.2"/>
<instance part="C31" gate="G$1" x="71.12" y="121.92" smashed="yes">
<attribute name="MANUFACTURER" x="71.12" y="121.92" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="71.12" y="121.92" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="71.12" y="121.92" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="71.12" y="121.92" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="71.12" y="121.92" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="71.12" y="121.92" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="71.12" y="121.92" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="71.12" y="121.92" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="71.12" y="121.92" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="71.12" y="121.92" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="71.12" y="121.92" size="1.778" layer="96" display="off"/>
<attribute name="NAME" x="70.866" y="119.507" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="70.866" y="117.729" size="1.27" layer="96" rot="R180"/>
<attribute name="POPULATE" x="71.12" y="121.92" size="1.778" layer="96" display="off"/>
</instance>
<instance part="GND66" gate="1" x="71.12" y="111.76"/>
<instance part="P+8" gate="1" x="71.12" y="129.54"/>
<instance part="C33" gate="G$1" x="40.64" y="86.36" smashed="yes">
<attribute name="NAME" x="40.386" y="83.947" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="40.386" y="82.169" size="1.27" layer="96" rot="R180"/>
<attribute name="MANUFACTURER" x="40.64" y="86.36" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="40.64" y="86.36" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="40.64" y="86.36" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="40.64" y="86.36" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="40.64" y="86.36" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="40.64" y="86.36" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="40.64" y="86.36" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="40.64" y="86.36" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="40.64" y="86.36" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="40.64" y="86.36" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="40.64" y="86.36" size="1.778" layer="96" display="off"/>
<attribute name="POPULATE" x="40.64" y="86.36" size="1.778" layer="96" display="off"/>
</instance>
<instance part="C32" gate="G$1" x="81.28" y="121.92" smashed="yes">
<attribute name="POPULATE" x="81.28" y="121.92" size="1.778" layer="96" display="off"/>
<attribute name="PRICE" x="81.28" y="121.92" size="1.778" layer="96" display="off"/>
<attribute name="PRICES" x="81.28" y="121.92" size="1.778" layer="96" display="off"/>
<attribute name="MINIMUM_QUANTITY" x="81.28" y="121.92" size="1.778" layer="96" display="off"/>
<attribute name="QUANTITY_MULTIPLIER" x="81.28" y="121.92" size="1.778" layer="96" display="off"/>
<attribute name="DATASHEET" x="81.28" y="121.92" size="1.778" layer="96" display="off"/>
<attribute name="PARTS_PER_PACKAGE" x="81.28" y="121.92" size="1.778" layer="96" display="off"/>
<attribute name="AVAILABLE_QUANTITY" x="81.28" y="121.92" size="1.778" layer="96" display="off"/>
<attribute name="MANUFACTURER" x="81.28" y="121.92" size="1.778" layer="96" display="off"/>
<attribute name="PART_NO" x="81.28" y="121.92" size="1.778" layer="96" display="off"/>
<attribute name="DISTRIBUTOR" x="81.28" y="121.92" size="1.778" layer="96" display="off"/>
<attribute name="ORDER_NO" x="81.28" y="121.92" size="1.778" layer="96" display="off"/>
<attribute name="NAME" x="81.026" y="119.761" size="1.524" layer="95" rot="R180"/>
<attribute name="VALUE" x="80.772" y="117.729" size="1.27" layer="96" rot="R180"/>
</instance>
<instance part="GND37" gate="1" x="81.28" y="111.76"/>
<instance part="P+13" gate="1" x="81.28" y="129.54"/>
</instances>
<busses>
<bus name="DISPLAY:RS,RW,E,DB[4..7],BL-PWM,V0-PWM">
<segment>
<wire x1="7.62" y1="68.58" x2="66.04" y2="68.58" width="0.762" layer="92"/>
<label x="7.62" y="69.342" size="1.778" layer="95"/>
<wire x1="66.04" y1="68.58" x2="66.04" y2="55.88" width="0.762" layer="92"/>
<wire x1="66.04" y1="55.88" x2="66.04" y2="48.26" width="0.762" layer="92"/>
<wire x1="66.04" y1="68.58" x2="83.82" y2="68.58" width="0.762" layer="92"/>
<wire x1="83.82" y1="68.58" x2="170.18" y2="68.58" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="LED[0..4]">
<segment>
<wire x1="15.24" y1="238.76" x2="106.68" y2="238.76" width="0.762" layer="92"/>
<label x="15.24" y="239.268" size="1.778" layer="95"/>
</segment>
</bus>
<bus name="BTN[0..4]">
<segment>
<wire x1="15.24" y1="185.42" x2="114.3" y2="185.42" width="0.762" layer="92"/>
<label x="15.24" y="185.928" size="1.778" layer="95"/>
</segment>
</bus>
<bus name="SD:SD_SDI,SD_SDO,SD_SCK,SD_CS,SD_CD">
<segment>
<wire x1="190.5" y1="101.6" x2="205.74" y2="101.6" width="0.762" layer="92"/>
<wire x1="205.74" y1="101.6" x2="208.28" y2="99.06" width="0.762" layer="92"/>
<wire x1="208.28" y1="99.06" x2="208.28" y2="30.48" width="0.762" layer="92"/>
<label x="190.246" y="102.362" size="1.778" layer="95"/>
</segment>
</bus>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<wire x1="152.4" y1="86.36" x2="152.4" y2="88.9" width="0.1524" layer="91"/>
<pinref part="GND42" gate="1" pin="GND"/>
<pinref part="V14" gate="G$1" pin="S"/>
</segment>
<segment>
<wire x1="165.1" y1="86.36" x2="165.1" y2="88.9" width="0.1524" layer="91"/>
<pinref part="GND43" gate="1" pin="GND"/>
<pinref part="R35" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="H7" gate="G$1" pin="K-@16"/>
<wire x1="132.08" y1="88.9" x2="134.62" y2="88.9" width="0.1524" layer="91"/>
<pinref part="GND44" gate="1" pin="GND"/>
<wire x1="134.62" y1="88.9" x2="134.62" y2="86.36" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND45" gate="1" pin="GND"/>
<wire x1="58.42" y1="78.74" x2="58.42" y2="86.36" width="0.1524" layer="91"/>
<pinref part="H7" gate="G$1" pin="DRL"/>
<wire x1="58.42" y1="86.36" x2="60.96" y2="86.36" width="0.1524" layer="91"/>
<wire x1="58.42" y1="86.36" x2="58.42" y2="88.9" width="0.1524" layer="91"/>
<junction x="58.42" y="86.36"/>
<pinref part="H7" gate="G$1" pin="VSS@1"/>
<wire x1="58.42" y1="88.9" x2="60.96" y2="88.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND46" gate="1" pin="GND"/>
<pinref part="R36" gate="G$1" pin="2"/>
<wire x1="48.26" y1="78.74" x2="48.26" y2="81.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND61" gate="1" pin="GND"/>
<pinref part="R18" gate="G$1" pin="2"/>
<wire x1="58.42" y1="205.74" x2="58.42" y2="208.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND62" gate="1" pin="GND"/>
<pinref part="R19" gate="G$1" pin="2"/>
<wire x1="71.12" y1="205.74" x2="71.12" y2="208.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND63" gate="1" pin="GND"/>
<pinref part="R20" gate="G$1" pin="2"/>
<wire x1="83.82" y1="205.74" x2="83.82" y2="208.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND64" gate="1" pin="GND"/>
<pinref part="R21" gate="G$1" pin="2"/>
<wire x1="96.52" y1="205.74" x2="96.52" y2="208.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R22" gate="G$1" pin="2"/>
<pinref part="GND65" gate="1" pin="GND"/>
<wire x1="109.22" y1="208.28" x2="109.22" y2="205.74" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND70" gate="1" pin="GND"/>
<pinref part="S1" gate="G$1" pin="2"/>
<wire x1="53.34" y1="162.56" x2="53.34" y2="165.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND71" gate="1" pin="GND"/>
<pinref part="S2" gate="G$1" pin="2"/>
<wire x1="66.04" y1="162.56" x2="66.04" y2="165.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND72" gate="1" pin="GND"/>
<pinref part="S3" gate="G$1" pin="2"/>
<wire x1="78.74" y1="162.56" x2="78.74" y2="165.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND73" gate="1" pin="GND"/>
<pinref part="S4" gate="G$1" pin="2"/>
<wire x1="91.44" y1="162.56" x2="91.44" y2="165.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND74" gate="1" pin="GND"/>
<pinref part="S5" gate="G$1" pin="2"/>
<wire x1="104.14" y1="162.56" x2="104.14" y2="165.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND75" gate="1" pin="GND"/>
<pinref part="S6" gate="G$1" pin="2"/>
<wire x1="116.84" y1="162.56" x2="116.84" y2="165.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C28" gate="G$1" pin="2"/>
<pinref part="GND77" gate="1" pin="GND"/>
<wire x1="309.88" y1="233.68" x2="309.88" y2="228.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C29" gate="G$1" pin="2"/>
<pinref part="GND78" gate="1" pin="GND"/>
<wire x1="314.96" y1="233.68" x2="314.96" y2="228.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C26" gate="G$1" pin="2"/>
<pinref part="GND79" gate="1" pin="GND"/>
<wire x1="231.14" y1="236.22" x2="231.14" y2="231.14" width="0.1524" layer="91"/>
<pinref part="D3" gate="P" pin="GND"/>
<wire x1="231.14" y1="231.14" x2="231.14" y2="228.6" width="0.1524" layer="91"/>
<wire x1="223.52" y1="233.68" x2="223.52" y2="231.14" width="0.1524" layer="91"/>
<wire x1="223.52" y1="231.14" x2="231.14" y2="231.14" width="0.1524" layer="91"/>
<junction x="231.14" y="231.14"/>
<pinref part="C27" gate="G$1" pin="2"/>
<wire x1="231.14" y1="231.14" x2="236.22" y2="231.14" width="0.1524" layer="91"/>
<wire x1="236.22" y1="231.14" x2="236.22" y2="236.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND80" gate="1" pin="GND"/>
<wire x1="353.06" y1="208.28" x2="353.06" y2="210.82" width="0.1524" layer="91"/>
<pinref part="X6" gate="-1" pin="5"/>
<wire x1="353.06" y1="210.82" x2="355.6" y2="210.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND81" gate="1" pin="GND"/>
<wire x1="355.6" y1="241.3" x2="355.6" y2="248.92" width="0.1524" layer="91"/>
<pinref part="X5" gate="G$1" pin="2"/>
<wire x1="355.6" y1="248.92" x2="358.14" y2="248.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R26" gate="G$1" pin="2"/>
<pinref part="GND82" gate="1" pin="GND"/>
<wire x1="246.38" y1="200.66" x2="246.38" y2="198.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R25" gate="G$1" pin="2"/>
<pinref part="GND83" gate="1" pin="GND"/>
<wire x1="238.76" y1="200.66" x2="238.76" y2="198.12" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="365.76" y1="172.72" x2="360.68" y2="172.72" width="0.1524" layer="91"/>
<pinref part="GND17" gate="1" pin="GND"/>
<wire x1="360.68" y1="172.72" x2="335.28" y2="172.72" width="0.1524" layer="91"/>
<wire x1="335.28" y1="172.72" x2="335.28" y2="149.86" width="0.1524" layer="91"/>
<pinref part="X7" gate="G$1" pin="4"/>
<wire x1="360.68" y1="172.72" x2="360.68" y2="121.92" width="0.1524" layer="91"/>
<junction x="360.68" y="172.72"/>
<pinref part="X8" gate="G$1" pin="4"/>
<wire x1="360.68" y1="121.92" x2="365.76" y2="121.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND19" gate="1" pin="GND"/>
<pinref part="R29" gate="G$1" pin="2"/>
<wire x1="340.36" y1="149.86" x2="340.36" y2="152.4" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND18" gate="1" pin="GND"/>
<pinref part="C30" gate="G$1" pin="2"/>
<wire x1="347.98" y1="149.86" x2="347.98" y2="154.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND28" gate="1" pin="GND"/>
<pinref part="V11" gate="G$1" pin="A"/>
<wire x1="307.34" y1="160.02" x2="307.34" y2="162.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND27" gate="1" pin="GND"/>
<pinref part="V10" gate="G$1" pin="A"/>
<wire x1="289.56" y1="160.02" x2="289.56" y2="162.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND29" gate="1" pin="GND"/>
<pinref part="V12" gate="G$1" pin="A"/>
<wire x1="325.12" y1="160.02" x2="325.12" y2="162.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND30" gate="1" pin="GND"/>
<pinref part="R28" gate="G$1" pin="2"/>
<wire x1="256.54" y1="157.48" x2="256.54" y2="149.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R41" gate="G$1" pin="2"/>
<wire x1="93.98" y1="33.02" x2="93.98" y2="30.48" width="0.1524" layer="91"/>
<wire x1="93.98" y1="30.48" x2="96.52" y2="30.48" width="0.1524" layer="91"/>
<pinref part="R48" gate="G$1" pin="2"/>
<wire x1="96.52" y1="30.48" x2="99.06" y2="30.48" width="0.1524" layer="91"/>
<wire x1="99.06" y1="30.48" x2="101.6" y2="30.48" width="0.1524" layer="91"/>
<wire x1="101.6" y1="30.48" x2="104.14" y2="30.48" width="0.1524" layer="91"/>
<wire x1="104.14" y1="30.48" x2="106.68" y2="30.48" width="0.1524" layer="91"/>
<wire x1="106.68" y1="30.48" x2="109.22" y2="30.48" width="0.1524" layer="91"/>
<wire x1="109.22" y1="30.48" x2="111.76" y2="30.48" width="0.1524" layer="91"/>
<wire x1="111.76" y1="30.48" x2="111.76" y2="33.02" width="0.1524" layer="91"/>
<pinref part="R47" gate="G$1" pin="2"/>
<wire x1="109.22" y1="33.02" x2="109.22" y2="30.48" width="0.1524" layer="91"/>
<junction x="109.22" y="30.48"/>
<pinref part="R46" gate="G$1" pin="2"/>
<wire x1="106.68" y1="33.02" x2="106.68" y2="30.48" width="0.1524" layer="91"/>
<junction x="106.68" y="30.48"/>
<pinref part="R45" gate="G$1" pin="2"/>
<wire x1="104.14" y1="33.02" x2="104.14" y2="30.48" width="0.1524" layer="91"/>
<junction x="104.14" y="30.48"/>
<pinref part="R44" gate="G$1" pin="2"/>
<wire x1="101.6" y1="33.02" x2="101.6" y2="30.48" width="0.1524" layer="91"/>
<junction x="101.6" y="30.48"/>
<pinref part="R43" gate="G$1" pin="2"/>
<wire x1="99.06" y1="33.02" x2="99.06" y2="30.48" width="0.1524" layer="91"/>
<junction x="99.06" y="30.48"/>
<pinref part="R42" gate="G$1" pin="2"/>
<wire x1="96.52" y1="33.02" x2="96.52" y2="30.48" width="0.1524" layer="91"/>
<junction x="96.52" y="30.48"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="93.98" y1="30.48" x2="93.98" y2="27.94" width="0.1524" layer="91"/>
<junction x="93.98" y="30.48"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="111.76" y1="30.48" x2="111.76" y2="27.94" width="0.1524" layer="91"/>
<junction x="111.76" y="30.48"/>
</segment>
<segment>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="340.36" y1="66.04" x2="340.36" y2="68.58" width="0.1524" layer="91"/>
<pinref part="X9" gate="G$1" pin="SHIELD2"/>
<wire x1="340.36" y1="68.58" x2="347.98" y2="68.58" width="0.1524" layer="91"/>
<pinref part="X9" gate="G$1" pin="SHIELD1"/>
<wire x1="347.98" y1="71.12" x2="340.36" y2="71.12" width="0.1524" layer="91"/>
<wire x1="340.36" y1="71.12" x2="340.36" y2="68.58" width="0.1524" layer="91"/>
<junction x="340.36" y="68.58"/>
<wire x1="340.36" y1="71.12" x2="340.36" y2="86.36" width="0.1524" layer="91"/>
<junction x="340.36" y="71.12"/>
<pinref part="X9" gate="G$1" pin="GND"/>
<wire x1="340.36" y1="86.36" x2="347.98" y2="86.36" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="350.52" y1="25.4" x2="350.52" y2="33.02" width="0.1524" layer="91"/>
<pinref part="X10" gate="1" pin="5"/>
<wire x1="350.52" y1="33.02" x2="355.6" y2="33.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND8" gate="1" pin="GND"/>
<pinref part="V15" gate="G$1" pin="A"/>
<wire x1="228.6" y1="60.96" x2="228.6" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND7" gate="1" pin="GND"/>
<pinref part="V16" gate="G$1" pin="A"/>
<wire x1="248.92" y1="60.96" x2="248.92" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND6" gate="1" pin="GND"/>
<pinref part="V17" gate="G$1" pin="A"/>
<wire x1="269.24" y1="60.96" x2="269.24" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND5" gate="1" pin="GND"/>
<pinref part="V18" gate="G$1" pin="A"/>
<wire x1="289.56" y1="60.96" x2="289.56" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND9" gate="1" pin="GND"/>
<pinref part="V19" gate="G$1" pin="A"/>
<wire x1="309.88" y1="60.96" x2="309.88" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND15" gate="1" pin="GND"/>
<pinref part="V20" gate="G$1" pin="A"/>
<wire x1="330.2" y1="60.96" x2="330.2" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND20" gate="1" pin="GND"/>
<pinref part="V9" gate="G$1" pin="S"/>
<wire x1="185.42" y1="205.74" x2="185.42" y2="218.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND22" gate="1" pin="GND"/>
<pinref part="R23" gate="G$1" pin="2"/>
<wire x1="177.8" y1="205.74" x2="177.8" y2="208.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND31" gate="1" pin="GND"/>
<wire x1="40.64" y1="78.74" x2="40.64" y2="81.28" width="0.1524" layer="91" style="longdash"/>
<pinref part="C33" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="C31" gate="G$1" pin="2"/>
<pinref part="GND66" gate="1" pin="GND"/>
<wire x1="71.12" y1="116.84" x2="71.12" y2="114.3" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C32" gate="G$1" pin="2"/>
<pinref part="GND37" gate="1" pin="GND"/>
<wire x1="81.28" y1="116.84" x2="81.28" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<wire x1="152.4" y1="127" x2="152.4" y2="129.54" width="0.1524" layer="91"/>
<pinref part="R30" gate="G$1" pin="2"/>
<pinref part="P+9" gate="1" pin="+5V"/>
</segment>
<segment>
<wire x1="134.62" y1="127" x2="134.62" y2="129.54" width="0.1524" layer="91"/>
<pinref part="V13" gate="G$1" pin="S"/>
<pinref part="P+12" gate="1" pin="+5V"/>
</segment>
<segment>
<pinref part="P+10" gate="1" pin="+5V"/>
<pinref part="R33" gate="G$1" pin="1"/>
<wire x1="48.26" y1="109.22" x2="48.26" y2="106.68" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="H7" gate="G$1" pin="VDD@2"/>
<wire x1="60.96" y1="99.06" x2="58.42" y2="99.06" width="0.1524" layer="91"/>
<wire x1="58.42" y1="99.06" x2="58.42" y2="109.22" width="0.1524" layer="91"/>
<pinref part="P+11" gate="1" pin="+5V"/>
</segment>
<segment>
<pinref part="D3" gate="P" pin="VCC"/>
<wire x1="223.52" y1="248.92" x2="223.52" y2="251.46" width="0.1524" layer="91"/>
<wire x1="223.52" y1="251.46" x2="231.14" y2="251.46" width="0.1524" layer="91"/>
<pinref part="P+15" gate="1" pin="+5V"/>
<wire x1="231.14" y1="251.46" x2="231.14" y2="254" width="0.1524" layer="91"/>
<pinref part="C26" gate="G$1" pin="1"/>
<wire x1="231.14" y1="251.46" x2="231.14" y2="243.84" width="0.1524" layer="91"/>
<junction x="231.14" y="251.46"/>
<wire x1="231.14" y1="251.46" x2="236.22" y2="251.46" width="0.1524" layer="91"/>
<pinref part="C27" gate="G$1" pin="1"/>
<wire x1="236.22" y1="251.46" x2="236.22" y2="243.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="H1" gate="G$1" pin="1S"/>
<pinref part="P+3" gate="1" pin="+5V"/>
<wire x1="185.42" y1="243.84" x2="185.42" y2="246.38" width="0.1524" layer="91"/>
<pinref part="H1" gate="G$1" pin="1"/>
<wire x1="185.42" y1="246.38" x2="185.42" y2="248.92" width="0.1524" layer="91"/>
<wire x1="187.96" y1="243.84" x2="187.96" y2="246.38" width="0.1524" layer="91"/>
<wire x1="187.96" y1="246.38" x2="185.42" y2="246.38" width="0.1524" layer="91"/>
<junction x="185.42" y="246.38"/>
<wire x1="185.42" y1="246.38" x2="180.34" y2="246.38" width="0.1524" layer="91"/>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="180.34" y1="243.84" x2="180.34" y2="246.38" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C31" gate="G$1" pin="1"/>
<pinref part="P+8" gate="1" pin="+5V"/>
<wire x1="71.12" y1="124.46" x2="71.12" y2="127" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+13" gate="1" pin="+5V"/>
<pinref part="C32" gate="G$1" pin="1"/>
<wire x1="81.28" y1="127" x2="81.28" y2="124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<wire x1="134.62" y1="116.84" x2="134.62" y2="114.3" width="0.1524" layer="91"/>
<pinref part="V13" gate="G$1" pin="D"/>
<pinref part="R31" gate="G$1" pin="2"/>
<wire x1="134.62" y1="114.3" x2="134.62" y2="111.76" width="0.1524" layer="91"/>
<wire x1="134.62" y1="114.3" x2="142.24" y2="114.3" width="0.1524" layer="91"/>
<junction x="134.62" y="114.3"/>
<pinref part="R32" gate="G$1" pin="2"/>
<wire x1="142.24" y1="114.3" x2="142.24" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<wire x1="152.4" y1="116.84" x2="152.4" y2="114.3" width="0.1524" layer="91"/>
<wire x1="152.4" y1="99.06" x2="152.4" y2="114.3" width="0.1524" layer="91"/>
<wire x1="139.7" y1="124.46" x2="144.78" y2="124.46" width="0.1524" layer="91"/>
<wire x1="144.78" y1="124.46" x2="144.78" y2="114.3" width="0.1524" layer="91"/>
<wire x1="144.78" y1="114.3" x2="152.4" y2="114.3" width="0.1524" layer="91"/>
<junction x="152.4" y="114.3"/>
<pinref part="R30" gate="G$1" pin="1"/>
<pinref part="V14" gate="G$1" pin="D"/>
<pinref part="V13" gate="G$1" pin="G"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="R31" gate="G$1" pin="1"/>
<pinref part="R32" gate="G$1" pin="1"/>
<wire x1="134.62" y1="99.06" x2="134.62" y2="101.6" width="0.1524" layer="91"/>
<wire x1="142.24" y1="101.6" x2="142.24" y2="99.06" width="0.1524" layer="91"/>
<wire x1="142.24" y1="99.06" x2="134.62" y2="99.06" width="0.1524" layer="91"/>
<junction x="134.62" y="99.06"/>
<pinref part="H7" gate="G$1" pin="A+@15"/>
<wire x1="134.62" y1="99.06" x2="132.08" y2="99.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="R36" gate="G$1" pin="1"/>
<wire x1="48.26" y1="91.44" x2="48.26" y2="93.98" width="0.1524" layer="91"/>
<pinref part="H7" gate="G$1" pin="VO@3"/>
<wire x1="48.26" y1="93.98" x2="60.96" y2="93.98" width="0.1524" layer="91"/>
<pinref part="R33" gate="G$1" pin="2"/>
<wire x1="48.26" y1="93.98" x2="48.26" y2="96.52" width="0.1524" layer="91"/>
<junction x="48.26" y="93.98"/>
<wire x1="40.64" y1="88.9" x2="40.64" y2="93.98" width="0.1524" layer="91" style="longdash"/>
<wire x1="40.64" y1="93.98" x2="48.26" y2="93.98" width="0.1524" layer="91" style="longdash"/>
<pinref part="R34" gate="G$1" pin="2"/>
<wire x1="38.1" y1="93.98" x2="40.64" y2="93.98" width="0.1524" layer="91" style="longdash"/>
<junction x="40.64" y="93.98"/>
<pinref part="C33" gate="G$1" pin="1"/>
</segment>
</net>
<net name="BL-PWM" class="0">
<segment>
<wire x1="170.18" y1="68.58" x2="172.72" y2="71.12" width="0.1524" layer="91"/>
<wire x1="165.1" y1="99.06" x2="165.1" y2="101.6" width="0.1524" layer="91"/>
<wire x1="160.02" y1="101.6" x2="165.1" y2="101.6" width="0.1524" layer="91"/>
<wire x1="157.48" y1="91.44" x2="160.02" y2="91.44" width="0.1524" layer="91"/>
<wire x1="160.02" y1="91.44" x2="160.02" y2="101.6" width="0.1524" layer="91"/>
<wire x1="172.72" y1="101.6" x2="165.1" y2="101.6" width="0.1524" layer="91"/>
<junction x="165.1" y="101.6"/>
<pinref part="R35" gate="G$1" pin="2"/>
<pinref part="V14" gate="G$1" pin="G"/>
<wire x1="172.72" y1="71.12" x2="172.72" y2="101.6" width="0.1524" layer="91"/>
<label x="172.466" y="71.374" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="R18" gate="G$1" pin="1"/>
<pinref part="H2" gate="G$1" pin="C"/>
<wire x1="58.42" y1="218.44" x2="58.42" y2="220.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LED0" class="0">
<segment>
<pinref part="H2" gate="G$1" pin="A"/>
<wire x1="58.42" y1="228.6" x2="58.42" y2="236.22" width="0.1524" layer="91"/>
<wire x1="58.42" y1="236.22" x2="55.88" y2="238.76" width="0.1524" layer="91"/>
<label x="58.166" y="230.124" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="LED1" class="0">
<segment>
<pinref part="H3" gate="G$1" pin="A"/>
<wire x1="71.12" y1="228.6" x2="71.12" y2="236.22" width="0.1524" layer="91"/>
<wire x1="71.12" y1="236.22" x2="68.58" y2="238.76" width="0.1524" layer="91"/>
<label x="70.866" y="230.124" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="LED2" class="0">
<segment>
<pinref part="H4" gate="G$1" pin="A"/>
<wire x1="83.82" y1="228.6" x2="83.82" y2="236.22" width="0.1524" layer="91"/>
<wire x1="83.82" y1="236.22" x2="81.28" y2="238.76" width="0.1524" layer="91"/>
<label x="83.566" y="230.124" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="LED3" class="0">
<segment>
<pinref part="H5" gate="G$1" pin="A"/>
<wire x1="96.52" y1="228.6" x2="96.52" y2="236.22" width="0.1524" layer="91"/>
<wire x1="96.52" y1="236.22" x2="93.98" y2="238.76" width="0.1524" layer="91"/>
<label x="96.266" y="230.124" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="H3" gate="G$1" pin="C"/>
<pinref part="R19" gate="G$1" pin="1"/>
<wire x1="71.12" y1="220.98" x2="71.12" y2="218.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="H4" gate="G$1" pin="C"/>
<pinref part="R20" gate="G$1" pin="1"/>
<wire x1="83.82" y1="220.98" x2="83.82" y2="218.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="H5" gate="G$1" pin="C"/>
<pinref part="R21" gate="G$1" pin="1"/>
<wire x1="96.52" y1="220.98" x2="96.52" y2="218.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="R22" gate="G$1" pin="1"/>
<pinref part="H6" gate="G$1" pin="C"/>
<wire x1="109.22" y1="218.44" x2="109.22" y2="220.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LED4" class="0">
<segment>
<pinref part="H6" gate="G$1" pin="A"/>
<wire x1="109.22" y1="228.6" x2="109.22" y2="236.22" width="0.1524" layer="91"/>
<wire x1="109.22" y1="236.22" x2="106.68" y2="238.76" width="0.1524" layer="91"/>
<label x="108.966" y="230.124" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="BTN3" class="0">
<segment>
<pinref part="S3" gate="G$1" pin="1"/>
<wire x1="78.74" y1="175.26" x2="78.74" y2="182.88" width="0.1524" layer="91"/>
<wire x1="78.74" y1="182.88" x2="76.2" y2="185.42" width="0.1524" layer="91"/>
<label x="76.708" y="182.88" size="1.778" layer="95" rot="R270"/>
</segment>
</net>
<net name="BTN4" class="0">
<segment>
<pinref part="S2" gate="G$1" pin="1"/>
<wire x1="66.04" y1="175.26" x2="66.04" y2="182.88" width="0.1524" layer="91"/>
<wire x1="66.04" y1="182.88" x2="63.5" y2="185.42" width="0.1524" layer="91"/>
<label x="64.008" y="182.88" size="1.778" layer="95" rot="R270"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="D3" gate="G$1" pin="C1-"/>
<pinref part="C25" gate="G$1" pin="2"/>
<wire x1="269.24" y1="236.22" x2="259.08" y2="236.22" width="0.1524" layer="91"/>
<wire x1="259.08" y1="236.22" x2="259.08" y2="238.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="D3" gate="G$1" pin="C1+"/>
<wire x1="269.24" y1="241.3" x2="264.16" y2="241.3" width="0.1524" layer="91"/>
<wire x1="264.16" y1="241.3" x2="264.16" y2="248.92" width="0.1524" layer="91"/>
<pinref part="C25" gate="G$1" pin="1"/>
<wire x1="264.16" y1="248.92" x2="259.08" y2="248.92" width="0.1524" layer="91"/>
<wire x1="259.08" y1="248.92" x2="259.08" y2="246.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="D3" gate="G$1" pin="C2-"/>
<pinref part="C24" gate="G$1" pin="2"/>
<wire x1="269.24" y1="226.06" x2="248.92" y2="226.06" width="0.1524" layer="91"/>
<wire x1="248.92" y1="226.06" x2="248.92" y2="238.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="D3" gate="G$1" pin="C2+"/>
<wire x1="269.24" y1="231.14" x2="254" y2="231.14" width="0.1524" layer="91"/>
<wire x1="254" y1="231.14" x2="254" y2="248.92" width="0.1524" layer="91"/>
<pinref part="C24" gate="G$1" pin="1"/>
<wire x1="254" y1="248.92" x2="248.92" y2="248.92" width="0.1524" layer="91"/>
<wire x1="248.92" y1="248.92" x2="248.92" y2="246.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TXD" class="0">
<segment>
<pinref part="D3" gate="G$1" pin="T2IN"/>
<wire x1="226.06" y1="218.44" x2="269.24" y2="218.44" width="0.1524" layer="91"/>
<label x="226.06" y="218.694" size="1.778" layer="95"/>
</segment>
</net>
<net name="RXD" class="0">
<segment>
<wire x1="226.06" y1="215.9" x2="238.76" y2="215.9" width="0.1524" layer="91"/>
<pinref part="R16" gate="G$1" pin="1"/>
<pinref part="R25" gate="G$1" pin="1"/>
<wire x1="238.76" y1="215.9" x2="251.46" y2="215.9" width="0.1524" layer="91"/>
<wire x1="238.76" y1="210.82" x2="238.76" y2="215.9" width="0.1524" layer="91"/>
<junction x="238.76" y="215.9"/>
<label x="226.06" y="216.154" size="1.778" layer="95"/>
</segment>
</net>
<net name="CTS" class="0">
<segment>
<wire x1="226.06" y1="213.36" x2="246.38" y2="213.36" width="0.1524" layer="91"/>
<pinref part="R24" gate="G$1" pin="1"/>
<pinref part="R26" gate="G$1" pin="1"/>
<wire x1="246.38" y1="213.36" x2="251.46" y2="213.36" width="0.1524" layer="91"/>
<wire x1="246.38" y1="210.82" x2="246.38" y2="213.36" width="0.1524" layer="91"/>
<junction x="246.38" y="213.36"/>
<label x="226.06" y="213.614" size="1.778" layer="95"/>
</segment>
</net>
<net name="RTS" class="0">
<segment>
<pinref part="D3" gate="G$1" pin="T1IN"/>
<wire x1="226.06" y1="220.98" x2="269.24" y2="220.98" width="0.1524" layer="91"/>
<label x="226.06" y="221.234" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="D3" gate="G$1" pin="V-"/>
<wire x1="299.72" y1="233.68" x2="304.8" y2="233.68" width="0.1524" layer="91"/>
<wire x1="304.8" y1="233.68" x2="304.8" y2="236.22" width="0.1524" layer="91"/>
<wire x1="304.8" y1="236.22" x2="304.8" y2="243.84" width="0.1524" layer="91"/>
<wire x1="304.8" y1="243.84" x2="309.88" y2="243.84" width="0.1524" layer="91"/>
<pinref part="C28" gate="G$1" pin="1"/>
<wire x1="309.88" y1="243.84" x2="309.88" y2="241.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="D3" gate="G$1" pin="V+"/>
<wire x1="299.72" y1="238.76" x2="302.26" y2="238.76" width="0.1524" layer="91"/>
<wire x1="302.26" y1="238.76" x2="302.26" y2="246.38" width="0.1524" layer="91"/>
<wire x1="302.26" y1="246.38" x2="314.96" y2="246.38" width="0.1524" layer="91"/>
<wire x1="314.96" y1="246.38" x2="314.96" y2="241.3" width="0.1524" layer="91"/>
<pinref part="C29" gate="G$1" pin="1"/>
</segment>
</net>
<net name="RXD-232" class="0">
<segment>
<pinref part="D3" gate="G$1" pin="R1IN"/>
<wire x1="299.72" y1="215.9" x2="317.5" y2="215.9" width="0.1524" layer="91"/>
<label x="304.8" y="216.154" size="1.778" layer="95"/>
<pinref part="R17" gate="G$1" pin="1"/>
</segment>
</net>
<net name="TXD-232" class="0">
<segment>
<pinref part="D3" gate="G$1" pin="T2OUT"/>
<wire x1="299.72" y1="218.44" x2="317.5" y2="218.44" width="0.1524" layer="91"/>
<label x="304.8" y="218.694" size="1.778" layer="95"/>
<pinref part="R15" gate="G$1" pin="1"/>
</segment>
</net>
<net name="RTS-232" class="0">
<segment>
<pinref part="D3" gate="G$1" pin="T1OUT"/>
<wire x1="299.72" y1="220.98" x2="312.42" y2="220.98" width="0.1524" layer="91"/>
<label x="304.8" y="221.234" size="1.778" layer="95"/>
<wire x1="312.42" y1="220.98" x2="335.28" y2="220.98" width="0.1524" layer="91"/>
<wire x1="335.28" y1="220.98" x2="335.28" y2="228.6" width="0.1524" layer="91"/>
<wire x1="335.28" y1="228.6" x2="373.38" y2="228.6" width="0.1524" layer="91"/>
<wire x1="373.38" y1="228.6" x2="373.38" y2="218.44" width="0.1524" layer="91"/>
<pinref part="X6" gate="-1" pin="7"/>
<wire x1="370.84" y1="218.44" x2="373.38" y2="218.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CTS-232" class="0">
<segment>
<pinref part="D3" gate="G$1" pin="R2IN"/>
<wire x1="299.72" y1="213.36" x2="309.88" y2="213.36" width="0.1524" layer="91"/>
<label x="304.8" y="213.614" size="1.778" layer="95"/>
<wire x1="309.88" y1="213.36" x2="335.28" y2="213.36" width="0.1524" layer="91"/>
<wire x1="335.28" y1="213.36" x2="335.28" y2="200.66" width="0.1524" layer="91"/>
<wire x1="335.28" y1="200.66" x2="373.38" y2="200.66" width="0.1524" layer="91"/>
<wire x1="373.38" y1="200.66" x2="373.38" y2="215.9" width="0.1524" layer="91"/>
<pinref part="X6" gate="-1" pin="8"/>
<wire x1="373.38" y1="215.9" x2="370.84" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="X6" gate="-1" pin="2"/>
<wire x1="337.82" y1="218.44" x2="350.52" y2="218.44" width="0.1524" layer="91"/>
<wire x1="350.52" y1="218.44" x2="355.6" y2="218.44" width="0.1524" layer="91"/>
<wire x1="335.28" y1="215.9" x2="337.82" y2="218.44" width="0.1524" layer="91"/>
<wire x1="327.66" y1="215.9" x2="335.28" y2="215.9" width="0.1524" layer="91"/>
<pinref part="R17" gate="G$1" pin="2"/>
<pinref part="X5" gate="G$1" pin="1"/>
<wire x1="358.14" y1="246.38" x2="350.52" y2="246.38" width="0.1524" layer="91"/>
<wire x1="350.52" y1="246.38" x2="350.52" y2="218.44" width="0.1524" layer="91"/>
<junction x="350.52" y="218.44"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="X6" gate="-1" pin="3"/>
<wire x1="337.82" y1="215.9" x2="347.98" y2="215.9" width="0.1524" layer="91"/>
<wire x1="347.98" y1="215.9" x2="355.6" y2="215.9" width="0.1524" layer="91"/>
<wire x1="335.28" y1="218.44" x2="337.82" y2="215.9" width="0.1524" layer="91"/>
<wire x1="327.66" y1="218.44" x2="335.28" y2="218.44" width="0.1524" layer="91"/>
<pinref part="R15" gate="G$1" pin="2"/>
<pinref part="X5" gate="G$1" pin="3"/>
<wire x1="358.14" y1="251.46" x2="347.98" y2="251.46" width="0.1524" layer="91"/>
<wire x1="347.98" y1="251.46" x2="347.98" y2="215.9" width="0.1524" layer="91"/>
<junction x="347.98" y="215.9"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="D3" gate="G$1" pin="R1OUT"/>
<pinref part="R16" gate="G$1" pin="2"/>
<wire x1="261.62" y1="215.9" x2="269.24" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="D3" gate="G$1" pin="R2OUT"/>
<pinref part="R24" gate="G$1" pin="2"/>
<wire x1="261.62" y1="213.36" x2="269.24" y2="213.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="D_N" class="2">
<segment>
<wire x1="307.34" y1="177.8" x2="355.6" y2="177.8" width="0.1524" layer="91"/>
<pinref part="V11" gate="G$1" pin="A1"/>
<wire x1="355.6" y1="177.8" x2="365.76" y2="177.8" width="0.1524" layer="91"/>
<wire x1="307.34" y1="172.72" x2="307.34" y2="177.8" width="0.1524" layer="91"/>
<wire x1="233.68" y1="177.8" x2="307.34" y2="177.8" width="0.1524" layer="91"/>
<junction x="307.34" y="177.8"/>
<pinref part="X7" gate="G$1" pin="2"/>
<wire x1="355.6" y1="177.8" x2="355.6" y2="127" width="0.1524" layer="91"/>
<junction x="355.6" y="177.8"/>
<pinref part="X8" gate="G$1" pin="2"/>
<wire x1="355.6" y1="127" x2="365.76" y2="127" width="0.1524" layer="91"/>
<label x="234.188" y="178.054" size="1.778" layer="95"/>
</segment>
</net>
<net name="D_P" class="2">
<segment>
<pinref part="V10" gate="G$1" pin="A1"/>
<wire x1="358.14" y1="175.26" x2="365.76" y2="175.26" width="0.1524" layer="91"/>
<pinref part="X7" gate="G$1" pin="3"/>
<wire x1="358.14" y1="175.26" x2="358.14" y2="124.46" width="0.1524" layer="91"/>
<junction x="358.14" y="175.26"/>
<pinref part="X8" gate="G$1" pin="3"/>
<wire x1="358.14" y1="124.46" x2="365.76" y2="124.46" width="0.1524" layer="91"/>
<label x="234.188" y="175.514" size="1.778" layer="95"/>
<wire x1="233.68" y1="175.26" x2="289.56" y2="175.26" width="0.1524" layer="91"/>
<wire x1="289.56" y1="175.26" x2="358.14" y2="175.26" width="0.1524" layer="91"/>
<wire x1="289.56" y1="172.72" x2="289.56" y2="175.26" width="0.1524" layer="91"/>
<junction x="289.56" y="175.26"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="V12" gate="G$1" pin="A1"/>
<wire x1="325.12" y1="180.34" x2="353.06" y2="180.34" width="0.1524" layer="91"/>
<wire x1="353.06" y1="180.34" x2="365.76" y2="180.34" width="0.1524" layer="91"/>
<wire x1="325.12" y1="172.72" x2="325.12" y2="180.34" width="0.1524" layer="91"/>
<pinref part="R27" gate="G$1" pin="2"/>
<wire x1="269.24" y1="180.34" x2="325.12" y2="180.34" width="0.1524" layer="91"/>
<junction x="325.12" y="180.34"/>
<pinref part="X7" gate="G$1" pin="1"/>
<wire x1="353.06" y1="180.34" x2="353.06" y2="129.54" width="0.1524" layer="91"/>
<junction x="353.06" y="180.34"/>
<pinref part="X8" gate="G$1" pin="1"/>
<wire x1="353.06" y1="129.54" x2="365.76" y2="129.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VBUS_DET" class="0">
<segment>
<wire x1="256.54" y1="172.72" x2="256.54" y2="180.34" width="0.1524" layer="91"/>
<pinref part="R27" gate="G$1" pin="1"/>
<wire x1="256.54" y1="180.34" x2="259.08" y2="180.34" width="0.1524" layer="91"/>
<wire x1="256.54" y1="180.34" x2="233.68" y2="180.34" width="0.1524" layer="91"/>
<junction x="256.54" y="180.34"/>
<pinref part="R28" gate="G$1" pin="1"/>
<wire x1="256.54" y1="172.72" x2="256.54" y2="167.64" width="0.1524" layer="91"/>
<label x="234.188" y="180.848" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="C30" gate="G$1" pin="1"/>
<pinref part="R29" gate="G$1" pin="1"/>
<wire x1="340.36" y1="162.56" x2="340.36" y2="165.1" width="0.1524" layer="91"/>
<pinref part="X7" gate="G$1" pin="S1"/>
<wire x1="340.36" y1="165.1" x2="340.36" y2="167.64" width="0.1524" layer="91"/>
<wire x1="340.36" y1="167.64" x2="363.22" y2="167.64" width="0.1524" layer="91"/>
<pinref part="X7" gate="G$1" pin="S2"/>
<wire x1="363.22" y1="167.64" x2="368.3" y2="167.64" width="0.1524" layer="91"/>
<wire x1="368.3" y1="165.1" x2="347.98" y2="165.1" width="0.1524" layer="91"/>
<junction x="340.36" y="165.1"/>
<wire x1="347.98" y1="165.1" x2="340.36" y2="165.1" width="0.1524" layer="91"/>
<wire x1="363.22" y1="167.64" x2="363.22" y2="119.38" width="0.1524" layer="91"/>
<junction x="363.22" y="167.64"/>
<pinref part="X8" gate="G$1" pin="5"/>
<wire x1="363.22" y1="119.38" x2="365.76" y2="119.38" width="0.1524" layer="91"/>
<wire x1="347.98" y1="162.56" x2="347.98" y2="165.1" width="0.1524" layer="91"/>
<junction x="347.98" y="165.1"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="R40" gate="G$1" pin="2"/>
<pinref part="H7" gate="G$1" pin="DB7@14"/>
<wire x1="111.76" y1="66.04" x2="111.76" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="R39" gate="G$1" pin="2"/>
<pinref part="H7" gate="G$1" pin="DB6@13"/>
<wire x1="109.22" y1="66.04" x2="109.22" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="R38" gate="G$1" pin="2"/>
<pinref part="H7" gate="G$1" pin="DB5@12"/>
<wire x1="106.68" y1="66.04" x2="106.68" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="R37" gate="G$1" pin="2"/>
<pinref part="H7" gate="G$1" pin="DB4@11"/>
<wire x1="104.14" y1="66.04" x2="104.14" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="H7" gate="G$1" pin="DB3@10"/>
<wire x1="101.6" y1="66.04" x2="101.6" y2="76.2" width="0.1524" layer="91"/>
<pinref part="R44" gate="G$1" pin="1"/>
<wire x1="101.6" y1="55.88" x2="101.6" y2="43.18" width="0.1524" layer="91"/>
<wire x1="101.6" y1="66.04" x2="101.6" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$48" class="0">
<segment>
<pinref part="H7" gate="G$1" pin="DB0@7"/>
<wire x1="93.98" y1="66.04" x2="93.98" y2="76.2" width="0.1524" layer="91"/>
<pinref part="R41" gate="G$1" pin="1"/>
<wire x1="93.98" y1="55.88" x2="93.98" y2="53.34" width="0.1524" layer="91"/>
<wire x1="93.98" y1="53.34" x2="93.98" y2="43.18" width="0.1524" layer="91"/>
<wire x1="93.98" y1="66.04" x2="93.98" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DB1" class="0">
<segment>
<pinref part="R42" gate="G$1" pin="1"/>
<wire x1="96.52" y1="60.96" x2="96.52" y2="43.18" width="0.1524" layer="91"/>
<pinref part="H7" gate="G$1" pin="DB1@8"/>
<wire x1="96.52" y1="66.04" x2="96.52" y2="76.2" width="0.1524" layer="91"/>
<wire x1="96.52" y1="66.04" x2="96.52" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DB2" class="0">
<segment>
<pinref part="R43" gate="G$1" pin="1"/>
<wire x1="99.06" y1="55.88" x2="99.06" y2="58.42" width="0.1524" layer="91"/>
<wire x1="99.06" y1="58.42" x2="99.06" y2="43.18" width="0.1524" layer="91"/>
<pinref part="H7" gate="G$1" pin="DB2@9"/>
<wire x1="99.06" y1="66.04" x2="99.06" y2="76.2" width="0.1524" layer="91"/>
<wire x1="99.06" y1="66.04" x2="99.06" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DB4" class="0">
<segment>
<wire x1="66.04" y1="55.88" x2="68.58" y2="53.34" width="0.1524" layer="91"/>
<pinref part="R37" gate="G$1" pin="1"/>
<pinref part="R45" gate="G$1" pin="1"/>
<wire x1="104.14" y1="55.88" x2="104.14" y2="53.34" width="0.1524" layer="91"/>
<wire x1="104.14" y1="53.34" x2="104.14" y2="43.18" width="0.1524" layer="91"/>
<wire x1="68.58" y1="53.34" x2="104.14" y2="53.34" width="0.1524" layer="91"/>
<junction x="104.14" y="53.34"/>
<label x="68.58" y="53.594" size="1.778" layer="95"/>
</segment>
</net>
<net name="DB5" class="0">
<segment>
<wire x1="66.04" y1="53.34" x2="68.58" y2="50.8" width="0.1524" layer="91"/>
<pinref part="R38" gate="G$1" pin="1"/>
<pinref part="R46" gate="G$1" pin="1"/>
<wire x1="106.68" y1="55.88" x2="106.68" y2="50.8" width="0.1524" layer="91"/>
<wire x1="106.68" y1="50.8" x2="106.68" y2="43.18" width="0.1524" layer="91"/>
<wire x1="68.58" y1="50.8" x2="106.68" y2="50.8" width="0.1524" layer="91"/>
<junction x="106.68" y="50.8"/>
<label x="68.58" y="51.054" size="1.778" layer="95"/>
</segment>
</net>
<net name="DB6" class="0">
<segment>
<wire x1="66.04" y1="50.8" x2="68.58" y2="48.26" width="0.1524" layer="91"/>
<pinref part="R39" gate="G$1" pin="1"/>
<pinref part="R47" gate="G$1" pin="1"/>
<wire x1="109.22" y1="55.88" x2="109.22" y2="48.26" width="0.1524" layer="91"/>
<wire x1="109.22" y1="48.26" x2="109.22" y2="43.18" width="0.1524" layer="91"/>
<wire x1="68.58" y1="48.26" x2="109.22" y2="48.26" width="0.1524" layer="91"/>
<junction x="109.22" y="48.26"/>
<label x="68.58" y="48.514" size="1.778" layer="95"/>
</segment>
</net>
<net name="DB7" class="0">
<segment>
<wire x1="66.04" y1="48.26" x2="68.58" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R40" gate="G$1" pin="1"/>
<pinref part="R48" gate="G$1" pin="1"/>
<wire x1="111.76" y1="55.88" x2="111.76" y2="45.72" width="0.1524" layer="91"/>
<wire x1="111.76" y1="45.72" x2="111.76" y2="43.18" width="0.1524" layer="91"/>
<wire x1="68.58" y1="45.72" x2="111.76" y2="45.72" width="0.1524" layer="91"/>
<junction x="111.76" y="45.72"/>
<label x="68.58" y="45.974" size="1.778" layer="95"/>
</segment>
</net>
<net name="E" class="0">
<segment>
<pinref part="H7" gate="G$1" pin="E@6"/>
<wire x1="86.36" y1="76.2" x2="86.36" y2="71.12" width="0.1524" layer="91"/>
<wire x1="86.36" y1="71.12" x2="83.82" y2="68.58" width="0.1524" layer="91"/>
<label x="86.106" y="71.374" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="RW" class="0">
<segment>
<pinref part="H7" gate="G$1" pin="R/W@5"/>
<wire x1="83.82" y1="76.2" x2="83.82" y2="71.12" width="0.1524" layer="91"/>
<wire x1="83.82" y1="71.12" x2="81.28" y2="68.58" width="0.1524" layer="91"/>
<label x="83.566" y="71.374" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="RS" class="0">
<segment>
<pinref part="H7" gate="G$1" pin="RS@4"/>
<wire x1="81.28" y1="76.2" x2="81.28" y2="71.12" width="0.1524" layer="91"/>
<wire x1="81.28" y1="71.12" x2="78.74" y2="68.58" width="0.1524" layer="91"/>
<label x="81.026" y="71.374" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="SD_CS" class="0">
<segment>
<wire x1="347.98" y1="96.52" x2="228.6" y2="96.52" width="0.1524" layer="91"/>
<wire x1="228.6" y1="96.52" x2="210.82" y2="96.52" width="0.1524" layer="91"/>
<wire x1="210.82" y1="96.52" x2="208.28" y2="99.06" width="0.1524" layer="91"/>
<pinref part="X9" gate="G$1" pin="CS"/>
<label x="211.074" y="96.774" size="1.778" layer="95"/>
<pinref part="V15" gate="G$1" pin="A1"/>
<wire x1="228.6" y1="73.66" x2="228.6" y2="96.52" width="0.1524" layer="91"/>
<junction x="228.6" y="96.52"/>
</segment>
<segment>
<pinref part="X10" gate="1" pin="1"/>
<wire x1="355.6" y1="43.18" x2="210.82" y2="43.18" width="0.1524" layer="91"/>
<wire x1="210.82" y1="43.18" x2="208.28" y2="45.72" width="0.1524" layer="91"/>
<label x="211.074" y="43.434" size="1.778" layer="95"/>
</segment>
</net>
<net name="SD_SDI" class="0">
<segment>
<wire x1="347.98" y1="93.98" x2="248.92" y2="93.98" width="0.1524" layer="91"/>
<wire x1="248.92" y1="93.98" x2="210.82" y2="93.98" width="0.1524" layer="91"/>
<wire x1="210.82" y1="93.98" x2="208.28" y2="96.52" width="0.1524" layer="91"/>
<pinref part="X9" gate="G$1" pin="DI"/>
<label x="211.074" y="94.234" size="1.778" layer="95"/>
<pinref part="V16" gate="G$1" pin="A1"/>
<wire x1="248.92" y1="73.66" x2="248.92" y2="93.98" width="0.1524" layer="91"/>
<junction x="248.92" y="93.98"/>
</segment>
<segment>
<pinref part="X10" gate="1" pin="2"/>
<wire x1="355.6" y1="40.64" x2="210.82" y2="40.64" width="0.1524" layer="91"/>
<wire x1="210.82" y1="40.64" x2="208.28" y2="43.18" width="0.1524" layer="91"/>
<label x="211.074" y="40.894" size="1.778" layer="95"/>
</segment>
</net>
<net name="SD_SCK" class="0">
<segment>
<wire x1="347.98" y1="88.9" x2="269.24" y2="88.9" width="0.1524" layer="91"/>
<wire x1="269.24" y1="88.9" x2="210.82" y2="88.9" width="0.1524" layer="91"/>
<wire x1="210.82" y1="88.9" x2="208.28" y2="91.44" width="0.1524" layer="91"/>
<pinref part="X9" gate="G$1" pin="SCK"/>
<label x="211.074" y="89.154" size="1.778" layer="95"/>
<pinref part="V17" gate="G$1" pin="A1"/>
<wire x1="269.24" y1="73.66" x2="269.24" y2="88.9" width="0.1524" layer="91"/>
<junction x="269.24" y="88.9"/>
</segment>
<segment>
<pinref part="X10" gate="1" pin="4"/>
<wire x1="355.6" y1="35.56" x2="210.82" y2="35.56" width="0.1524" layer="91"/>
<wire x1="210.82" y1="35.56" x2="208.28" y2="38.1" width="0.1524" layer="91"/>
<label x="211.074" y="35.814" size="1.778" layer="95"/>
</segment>
</net>
<net name="SD_SDO" class="0">
<segment>
<wire x1="347.98" y1="83.82" x2="289.56" y2="83.82" width="0.1524" layer="91"/>
<wire x1="289.56" y1="83.82" x2="210.82" y2="83.82" width="0.1524" layer="91"/>
<wire x1="210.82" y1="83.82" x2="208.28" y2="86.36" width="0.1524" layer="91"/>
<pinref part="X9" gate="G$1" pin="DO"/>
<label x="211.074" y="84.074" size="1.778" layer="95"/>
<pinref part="V18" gate="G$1" pin="A1"/>
<wire x1="289.56" y1="73.66" x2="289.56" y2="83.82" width="0.1524" layer="91"/>
<junction x="289.56" y="83.82"/>
</segment>
<segment>
<pinref part="X10" gate="1" pin="6"/>
<wire x1="355.6" y1="30.48" x2="210.82" y2="30.48" width="0.1524" layer="91"/>
<wire x1="210.82" y1="30.48" x2="208.28" y2="33.02" width="0.1524" layer="91"/>
<label x="211.074" y="30.734" size="1.778" layer="95"/>
</segment>
</net>
<net name="SD_CD" class="0">
<segment>
<wire x1="347.98" y1="76.2" x2="309.88" y2="76.2" width="0.1524" layer="91"/>
<wire x1="309.88" y1="76.2" x2="210.82" y2="76.2" width="0.1524" layer="91"/>
<wire x1="210.82" y1="76.2" x2="208.28" y2="78.74" width="0.1524" layer="91"/>
<pinref part="X9" gate="G$1" pin="CD1"/>
<label x="211.074" y="76.454" size="1.778" layer="95"/>
<pinref part="V19" gate="G$1" pin="A1"/>
<wire x1="309.88" y1="73.66" x2="309.88" y2="76.2" width="0.1524" layer="91"/>
<junction x="309.88" y="76.2"/>
</segment>
<segment>
<pinref part="X10" gate="1" pin="7"/>
<wire x1="355.6" y1="27.94" x2="210.82" y2="27.94" width="0.1524" layer="91"/>
<wire x1="210.82" y1="27.94" x2="208.28" y2="30.48" width="0.1524" layer="91"/>
<label x="211.074" y="28.194" size="1.778" layer="95"/>
</segment>
</net>
<net name="+3.3V" class="0">
<segment>
<pinref part="X9" gate="G$1" pin="VCC"/>
<wire x1="347.98" y1="91.44" x2="330.2" y2="91.44" width="0.1524" layer="91"/>
<pinref part="P+1" gate="1" pin="+3.3V"/>
<wire x1="330.2" y1="91.44" x2="330.2" y2="99.06" width="0.1524" layer="91"/>
<pinref part="V20" gate="G$1" pin="A1"/>
<wire x1="330.2" y1="73.66" x2="330.2" y2="91.44" width="0.1524" layer="91"/>
<junction x="330.2" y="91.44"/>
</segment>
<segment>
<pinref part="X10" gate="1" pin="3"/>
<wire x1="355.6" y1="38.1" x2="350.52" y2="38.1" width="0.1524" layer="91"/>
<pinref part="P+2" gate="1" pin="+3.3V"/>
<wire x1="350.52" y1="38.1" x2="350.52" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BTN2" class="0">
<segment>
<pinref part="S5" gate="G$1" pin="1"/>
<wire x1="104.14" y1="175.26" x2="104.14" y2="182.88" width="0.1524" layer="91"/>
<wire x1="104.14" y1="182.88" x2="101.6" y2="185.42" width="0.1524" layer="91"/>
<label x="102.108" y="182.88" size="1.778" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="S6" gate="G$1" pin="1"/>
<wire x1="116.84" y1="175.26" x2="116.84" y2="182.88" width="0.1524" layer="91"/>
<wire x1="116.84" y1="182.88" x2="114.3" y2="185.42" width="0.1524" layer="91"/>
<label x="114.808" y="182.88" size="1.778" layer="95" rot="R270"/>
</segment>
</net>
<net name="BTN1" class="0">
<segment>
<pinref part="S1" gate="G$1" pin="1"/>
<wire x1="53.34" y1="175.26" x2="53.34" y2="182.88" width="0.1524" layer="91"/>
<wire x1="53.34" y1="182.88" x2="50.8" y2="185.42" width="0.1524" layer="91"/>
<label x="51.308" y="182.88" size="1.778" layer="95" rot="R270"/>
</segment>
</net>
<net name="BTN0" class="0">
<segment>
<pinref part="S4" gate="G$1" pin="1"/>
<wire x1="91.44" y1="175.26" x2="91.44" y2="182.88" width="0.1524" layer="91"/>
<wire x1="91.44" y1="182.88" x2="88.9" y2="185.42" width="0.1524" layer="91"/>
<label x="89.408" y="182.88" size="1.778" layer="95" rot="R270"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="V9" gate="G$1" pin="D"/>
<pinref part="H1" gate="G$1" pin="2S"/>
<wire x1="185.42" y1="228.6" x2="185.42" y2="231.14" width="0.1524" layer="91"/>
<pinref part="H1" gate="G$1" pin="2"/>
<wire x1="185.42" y1="231.14" x2="185.42" y2="233.68" width="0.1524" layer="91"/>
<wire x1="187.96" y1="233.68" x2="187.96" y2="231.14" width="0.1524" layer="91"/>
<wire x1="187.96" y1="231.14" x2="185.42" y2="231.14" width="0.1524" layer="91"/>
<junction x="185.42" y="231.14"/>
<wire x1="185.42" y1="231.14" x2="180.34" y2="231.14" width="0.1524" layer="91"/>
<pinref part="R14" gate="G$1" pin="2"/>
<wire x1="180.34" y1="231.14" x2="180.34" y2="233.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BUZZER" class="0">
<segment>
<pinref part="R23" gate="G$1" pin="1"/>
<wire x1="177.8" y1="218.44" x2="177.8" y2="220.98" width="0.1524" layer="91"/>
<pinref part="V9" gate="G$1" pin="G"/>
<wire x1="177.8" y1="220.98" x2="180.34" y2="220.98" width="0.1524" layer="91"/>
<wire x1="177.8" y1="220.98" x2="160.02" y2="220.98" width="0.1524" layer="91"/>
<junction x="177.8" y="220.98"/>
<label x="160.02" y="221.234" size="1.778" layer="95"/>
</segment>
</net>
<net name="V0-PWM" class="0">
<segment>
<wire x1="22.86" y1="68.58" x2="25.4" y2="71.12" width="0.1524" layer="91"/>
<wire x1="25.4" y1="71.12" x2="25.4" y2="93.98" width="0.1524" layer="91"/>
<pinref part="R34" gate="G$1" pin="1"/>
<wire x1="25.4" y1="93.98" x2="27.94" y2="93.98" width="0.1524" layer="91"/>
<label x="25.146" y="71.374" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="101,1,370.84,25.4,J4,P$1,,,,"/>
<approved hash="101,1,355.6,25.4,J3,P$1,,,,"/>
<approved hash="101,1,340.36,25.4,J2,P$1,,,,"/>
<approved hash="101,1,325.12,25.4,J1,P$1,,,,"/>
<approved hash="104,1,198.12,106.68,D2,VCC,+3.3V,,,"/>
<approved hash="104,1,198.12,111.76,D2,AVCC,N$31,,,"/>
<approved hash="104,1,198.12,104.14,D2,VCC,+3.3V,,,"/>
<approved hash="104,1,198.12,101.6,D2,VCC,+3.3V,,,"/>
<approved hash="104,1,198.12,99.06,D2,VCC,+3.3V,,,"/>
<approved hash="104,1,198.12,96.52,D2,VCC,+3.3V,,,"/>
<approved hash="117,1,304.8,149.86,VCC,,,,,"/>
<approved hash="110,3,93.98,53.34,DB4,N$48,,,,"/>
<approved hash="110,3,93.98,53.34,DB4,N$48,,,,"/>
<approved hash="113,1,370.84,29.6545,J4,,,,,"/>
<approved hash="113,1,355.6,29.6545,J3,,,,,"/>
<approved hash="113,1,340.36,29.6545,J2,,,,,"/>
<approved hash="113,1,325.12,29.6545,J1,,,,,"/>
</errors>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
