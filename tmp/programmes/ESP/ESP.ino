#include "Wire.h"    // imports the wire library for talking over I2C 
#include "ESP8266WiFi.h"
#include "Adafruit_BMP085.h"  // import the Pressure Sensor Library
Adafruit_BMP085 mySensor;  // create sensor object called mySensor


const char* ssid  = "Social Startup";     //  your network SSID (name)
const char* password = "Certoclav"; 
float tempC;  // Variable for holding temp in C
float tempF;  // Variable for holding temp in F
float pressure; //Variable for holding pressure reading
void getSensorData(void);
char thingSpeakAddress[] = "api.thingspeak.com";
String writeAPIKey = "Z7THD2V2H1R1STLR";


void setup() {
  // put your setup code here, to run once:
mySensor.begin();   //initialize mySensor
Serial.begin(9600); //turn on serial monitor


}

void loop() {
    WiFiClient client;
   if(WiFi.status() != WL_CONNECTED){
     WiFi.begin(ssid, password);
     while (WiFi.status() != WL_CONNECTED) {
     delay(500);
     Serial.print(".");
   } 
   }
    getSensorData();
    client.connect(thingSpeakAddress, 80);
    String str = "field1=";
    str += tempC;
    str += "&field2=";
    str += pressure; 
    
    client.print("POST /update HTTP/1.1\n");
    client.print("Host: api.thingspeak.com\n");
    client.print("Connection: close\n");
    client.print("X-THINGSPEAKAPIKEY:"+writeAPIKey+"\n");
    client.print("Content-Type: application/x-www-form-urlencoded\n");
    client.print("Content-Length: ");
    client.print(str.length());
    client.print("\n\n");
    client.print(str);

    Serial.print("POST: ");
    Serial.println(str);



}
void getSensorData(void)
{
  tempC = mySensor.readTemperature(); //  Be sure to declare your variables
tempF = tempC*1.8 + 32.; // Convert degrees C to F
pressure=mySensor.readPressure(); //Read Pressure

}
