'use strict';
var express = require('express');
var router = express.Router();

var device = require('../controllers/v0/device.js'); 
var error = require('../controllers/v0/error.js'); 
var events = require('../controllers/v0/events.js'); 
var ifttt = require('../controllers/v0/ifttt.js'); 
var support = require('../controllers/v0/support.js'); 
var twilio = require('../controllers/v0/twilio.js'); 
var context = require('../controllers/v0/context.js'); 
var pairing = require('../controllers/v0/pairing.js'); 
var history = require('../controllers/v0/history.js'); 

//var mongoose = require("mongoose");
//var DeviceModel = require('../models/device');
//var request = require("request");
//var twilio = require('twilio');
//var client = new twilio.RestClient('ACea6822203e981e236396333551366d88', '9db5f4330d53a88bf9a341dacc466092');
//var resp = new twilio.TwimlResponse();

///////// V0 - protopyting API/////////////
//////////////////////////////////////////
router.get('/', function(req, res, next) { res.render('index', { title: 'Welcome to FreeSensor API Documentation !' }); });
router.get('/pairing', pairing.status);
router.post('/pairing', pairing.request); 
router.put('/pairing', pairing.ackn); 
router.delete('/device/', device.delete);
router.put('/device/', device.create);
router.post('/update', device.update); 
router.put('/update', device.update); 
router.get('/data', device.data); 
router.get('/context', context.get); 
router.put('/context', context.put); 
router.put('/event', events.put); 
router.get('/event', events.get);	
router.post('/event', events.trigger);	
router.get('/history', history.get); 

// DEV ROUTES - DONT USE IN PROD /ONLY DEV 
router.get('/register', device.create);
router.get('/triggerevent', events.trigger);
router.get('/update', device.update); 
//router.get('/putcontext', context.put); 
router.get('/putevent', events.put); 
router.get('/pairingrequest', pairing.request); 
router.get('/pairingackn', pairing.ackn); 
/////// END DEV ROUTES

//////////////////////////////////////////
/////END V0 //////////////////////////////


///////// V1 . production API/////////////
/// PLACEHOLDER
/////END V1 //////////////////////////////


/*
router.get('/del', function(req, res, next) {
	var devID = req.query.devID; 
	console.log(devID);
	if (!devID) {
		res.json({
			"status": 403,
			"message": "Invalid input"
		});
		return;
	}
	DeviceModel.remove({deviceID : devID}, function(err){
		
		if(err){
		console.log(err);
		res.json({
		"status": 404,
		"message": "Device not found"
		});
		return;
	}else{
		res.json({
		"status": 200,
		"message": "Device removed"
		});
	}
	})
});

router.get('/register', function(req, res, next) {
	
	var devID = req.query.devID; 
	if (!devID) {
	res.json({
		"status": 403,
		"message": "Invalid input"
	});
	return;
	}
	DeviceModel.findOne({deviceID : devID}, function(err, device){
	if(device){
		res.json({
		"status": 403,
		"message": "Device already exists"
		});
		return;
	}else{
		DeviceModel.create({deviceID : devID}, function (err, small) {
		if (err) console.log(err);
		});
		res.json({
		"status": 400,
		"message": "Device created"
		});
	}
	})
}); */

/*router.post('/pairing', function(req, res, next) {
	var devID = req.body.devID;
	var pairing = req.body.pairingStatus;
	if(!devID || !pairing){
		res.json({
		"status": 403,
		"message": "Faulty request. No/wrong devID or pairingStatus"
		});
		return;		
	}
	DeviceModel.update(  {deviceID: devID}, { $set:
		{pairing: pairing} }, function (err) {
			if (err) console.log(err);
		});
	
	res.json({
		"status":200, 
		"message":("pairing updatet to: "+pairing)
	});
});



router.get('/pairing', function(req, res, next) {
	var devID = req.query.devID;
	if ( devID ){
	DeviceModel.findOne(  {deviceID: devID},function (err, device) {
		if (err){
			res.json({ 
				"status":400,
				"message": "Device not found"
			});			
			console.log(err);
			return;

		}else{
			res.json({
			"status":200,
			"pairingStatus":device.pairing,
			
			});
		}
		});
	}

});


router.post('/pairingRequest', function(req, res, next) {
	var devID = req.body.devID;
	var requestPossible = "0"
	
	if ( devID ){

		DeviceModel.findOne(  {deviceID: devID}, function (err, device) {
			if (err){
				res.json({ 
					"status":400,
					"message": "devID not found"
				});			
				console.log(err);
				
				return;

			}else{
				var requestPossible= (device.pairing == "false");
				// thats a crappy workaround - check TTL
			}
			});
		console.log(requestPossible);
		if( requestPossible ){
			DeviceModel.update(  {deviceID: devID}, { 
				$set: { pairing: "request"
				}
		
			}, function (err, small) {
				if (err) {
					res.json({
					"status": 403,
					"message": "error "
					});
				return;
				}
			});
		}else{
			res.json({
				"status": 400,
				"message": "device cant be paired at the moment"
			});
			return;
		}	
	}else{
		res.json({
			"status": 400,
			"message": "devID not handled"
		});
		return;
	}


});
*/

/*	
router.post('/context', function(req, res, next) {
	var devID = req.body.devID;
	var newContext = req.body.context;
	if(!devID || !newContext){
		res.json({
		"status": 403,
		"message": "Faulty request. No/wrong devID "
		});
		return;		
	}
	DeviceModel.update(  {deviceID: devID}, { $set:
		{
			context: newContext,
			news: true


		} }, function (err) {
			if (err) console.log(err);
		});
	
	res.json({
		"status":200, 
		"message":("contxt updatet")
	});
});

router.get('/context', function(req, res, next) {
	var devID = req.query.devID;
	if ( devID ){

			
		DeviceModel.update(  {deviceID: devID}, { $set:
			{  news :false } 
		}, function (err) {
			if (err) console.log(err);
		});

	DeviceModel.findOne(  {deviceID: devID},function (err, device) {
		if (err){
			res.json({ 
				"status":400,
				"message": "error"
			});
			
			console.log(err);
			return;
		}else if( device){
			res.json({
			"status":200,
			"context": device.context
			});
		}
		else{
			res.json({ 
				"status":400,
				"message": "error"
			});	
		}
		});
	}

});*/

/*
router.post('/eventaction', function(req, res, next) {
	var devID = req.body.devID;
	var neweventaction = req.body.eventaction;
	if(!devID || !neweventaction){
		res.json({
		"status": 403,
		"message": "Faulty request. No/wrong devID "
		});
		return;		
	}
	DeviceModel.update(  {deviceID: devID}, { $set:
		{
			eventaction: neweventaction
		} }, function (err) {
			if (err) console.log(err);
		});
	
	res.json({
		"status":200, 
		"message":("contxt updatet")
	});
});

router.get('/eventaction', function(req, res, next) {
	var devID = req.query.devID;
	if ( devID ){
	DeviceModel.findOne(  {deviceID: devID},function (err, device) {
		if (err){
			res.json({ 
				"status":400,
				"message": "error"
			});
			
			console.log(err);
			return;
		}else if( device){
			res.json({
			"status":200,
			"eventaction": device.eventaction
			});
		}
		else{
			res.json({ 
				"status":400,
				"message": "error"
			});	
		}
		});
	}

}); */


/*
router.post('/update', function(req, res, next) {
	var devID = req.body.devID;
	//var tmp_data  = parseFloat(req.query.field);
	
	var tmp_data = null;
	var tmp_data2 = null;
	var tmp_data3 = null;
	var tmp_data4 = null;

	tmp_data  = req.body.field1;
	if(req.body.field2) tmp_data2 = req.body.field2; 
	if(req.body.field3) tmp_data3 = req.body.field3; 
	if(req.body.field4) tmp_data4 = req.body.field4; 

	console.log(req.body);
	if ( devID && tmp_data){

	var date = new Date();
	var obje = {};
	obje['values.' + date.getHours().toString() + '.'+ date.getMinutes().toString() ] = tmp_data;

	DeviceModel.update(  {deviceID: devID}, { 
		$set: { recentData:{
				"1":{
					value: tmp_data,
					unit:"°C"
				},
				"2":{
					value: tmp_data2,
					unit: "mbar"
				},
				"3":{
					value: tmp_data3,
					unit: "mbar"
				},
				"4":{
					value: tmp_data4,
					unit: "mbar"
				}				
			},
			lastUpdate : Date.now()
		} 
		}, function (err, small) {
		if (err) console.log(err);
		});
	

	DeviceModel.update(  {deviceID: devID}, { $set:obje
		}, function (err, small) {
		if (err) console.log(err);
		}); 

	}
	DeviceModel.findOne(  {deviceID: devID}, function (err, device) {
		if (err){
			res.json({ 
				"status":400,
				"message": "error"
			});
			
			console.log(err);
			return;
		}else if( device){
			res.json({
			"status":200,
			"news": device.news
			});
		}
		else{
			res.json({ 
				"status":400,
				"message": "error"
			});	
		}
		});
});  

router.get('/data', function(req, res, next) {
	var devID = req.query.devID;
	if ( devID ){
	DeviceModel.findOne(  {deviceID: devID},function (err, device) {
		if (err){
			res.json({ 
				"status":400,
				"message": "error"
			});
			
			console.log(err);
			return;
		}else if( device){
			res.json({
			"status":200,
			"field1":device.recentData["1"].value,
			"field2":device.recentData["2"].value,
			lastUpdate : device.lastUpdate
			});
		}
		else{
			res.json({ 
				"status":400,
				"message": "error"
			});	
		}
		});
	}

});*/

/*
router.get('/datalist', function(req, res, next) {
	var devID = req.query.devID;
	if ( devID ){
	DeviceModel.findOne(  {deviceID: devID},function (err, device) {
		if (err){
			res.json({ 
				"status":400,
				"message": err
			});
			
			console.log(err);
			return;
		}else if( device){
			res.json({
			"status":200,
			"values":device.values
			});
		}
		else{
			res.json({ 
				"status":400,
				"message": "not dev found"
			});	
		}
		});
	}

});
*/
/*
router.post('/event', function(req, res, next) {
	var devID = req.body.devID;
	var eventlvl = req.body.eventlvl;
	if(!devID || !eventlvl){
		res.json({
		"status": 403,
		"message": "Faulty request. No/wrong devID "
		});
		return;		
	}
		
	DeviceModel.findOne(  {deviceID: devID},function (err, device) {
		if (err){
			res.json({ 
				"status":400,
				"message": "error"
			});
			
			console.log(err);
			return;


		}else if( device){

			//console.log(eventlvl);

			if( eventlvl == "A"){
				console.log("Button EVENT");
				console.log(devID);

////////////////////////B&P INSERT
				if (devID == "B001"){
					console.log(" send call"); 
					eventlvl = "4"; 
				}
				if (devID == "B002"){
					console.log("Writed to wunderlist"); 
					eventlvl = "5";
				}
				if (devID == "B003"){g
					console.log("Switch Wemo lights"); 
					eventlvl = "5";
				}
				if (devID == "B004"){
					console.log("send notification"); 
					eventlvl = "5";
				}
				if (devID == "B005"){
					console.log("track temp in spread sheet"); 
					eventlvl = "5";
				}
				if (devID == "B006"){
					console.log("send sms"); 
					eventlvl = "3"
				}
//// END OF B&P INSERT ////////////////////// 

			}

			if( eventlvl == "0"){
				///// just LOG entry
				console.log("Just a Log");
			}

			if( eventlvl=="1"){
				////// App Notification
				// currently its a IFTT workaround - native ionic push is bit buggy atm //
				// we can also switch to IBM push
				console.log( " Notification event");

				var apiKey = "b4f96237851e8dcd36b7ef4550239dc150550fcb6dbb40cc";
				var aut = "Basic " + new Buffer(apiKey + ":").toString("base64");
				request({
			    	url: 'https://push.ionic.io/api/v1/push', //URL to hit
			    	method: 'POST', //Specify the method
			    	headers: { //We can define headers too
			        'Content-Type': 'application/json',
			        'X-Ionic-Application-Id': '9EFE49DF',
			        "Authorization": aut
			    	},
			    	json: {
					  "user_ids": "36da595b-3e6c-421d-ba8f-4b74fd900657",
					  "notification":{
					    "alert":"Hello World!",
					    "ios":{
					      "badge":"1",
					      "sound":"ping.aiff",
					      "expiry": "1423238641",
					      "priority": "10",
					      "contentAvailable": "true",
					      "payload":{
					        "key1":"value",
					        "key2":"value"
					      }
					    },
					    "android":{
					      "collapseKey":"foo",
					      "delayWhileIdle":"true",
					      "timeToLive":"300",
					      "payload":{
					        "key1":"value",
					        "key2":"value"
					      }
					    }
					  }
					}

			}, function(error, response, body){
			    if(error) {
			        console.log(error);
			    } else {
			        console.log(response.statusCode, body);
			    }
			});

			}

			if( eventlvl=="2"){
				////// EMAIL !
				console.log("eventlvl:2");
			}
			if( eventlvl=="3"){
				///// TWILIO SMS
				console.log("eventlvl:3");
				console.log(device.eventaction.l3.actiondata.number);

				client.sms.messages.create({
    					to:device.eventaction.l3.actiondata.number,
    					from:'+4915735986444', // Rysta number 
   		 			body:device.eventaction.l3.actiondata.text
   		 			//body:"Hallo"

				}, function(error, message) {
    				if (!error) {
        				console.log('Success! The SID for this SMS message is:');
        				console.log(message.sid); 
        				console.log('Message sent on:');
        				console.log(message.dateCreated);
    				} else {
    				}
				});	
				
			}

			if( eventlvl=="4"){
				/////// TWILIO CALL 
				//console.log("eventlvl:4");
				//console.log(device.eventaction.l4.actiondata.number);
				
				resp.say(device.eventaction.l4.actiondata.text, {
    				voice:'woman',
    				language:'de'
				});
				var rspURL = 'http://twimlets.com/echo?Twiml='+ encodeURIComponent((resp.toString()));
				console.log(rspURL);

				client.makeCall({
				    to: device.eventaction.l4.actiondata.number, // Any number Twilio can call
				    //to : '+4915115502187',
				    from: '+4915735986444', // our Twillio number
				    url: rspURL
				    // 	+4915115502187
				}, function(err, responseData) {
				    console.log(responseData.from); // outputs "+14506667788"
				});
			}

			if (eventlvl == "5"){
				////// IFTTT

				console.log( "eventlevel: " + eventlvl + "  IFTTT event");
				//var URL = "https://maker.ifttt.com/trigger/" + device.IFTTT.eventName_sensor + "/with/key/" + device.IFTTT.key;
				var URL = "https://maker.ifttt.com/trigger/" + devID +  "/with/key/" + device.IFTTT.key;

				console.log(URL);
				//request.post(URL);


				request({
			    url: URL, 
			    method: 'POST', 
			    headers: { 'Content-Type': 'application/json' },
			    json: {
			    	"value1": device.recentData["1"].value,
			    	"value2": device.recentData["2"].value
					}

			}, function(error, response, body){
			    if(error) {
			        console.log(error);
			    } else {
			        console.log(response.statusCode, body);
			    }
			});



			}

			else {
				console.log("unknown eventlvl detected: ");
				console.log(eventlvl); 
			}




			res.json({ 
				"status":200,
				"message": "event send!"
			});

		}
		else{
			res.json({ 
				"status":400,
				"message": "error"
			});	
		}
		});
});*/


module.exports = router;


/*
 client.sms.messages.create({
    to:'+16512223344',
    from:'TWILIO_NUMBER',
    body:'ahoy hoy! Testing Twilio and node.js'
}, function(error, message) {
    if (!error) {
        console.log('Success! The SID for this SMS message is:');
        console.log(message.sid);
 
        console.log('Message sent on:');
        console.log(message.dateCreated);
    } else {
        console.log('Oops! There was an error.');
    }
});

*/