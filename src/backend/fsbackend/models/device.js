var mongoose = require('mongoose');

var opt = {
	min_type: { type: Number, default: ""}
}

var deviceSchema = mongoose.Schema({
	__v : {type: String, default: " "+ Math.random() + Math.random()},
	deviceID:{type: String, default: " "+ Math.random() + Math.random()},
	model:{type: String, default:  "CertoFridge" },
	fwtype:{type:String, default:  "Vers 0.1" },
	devicename:{type:String, default:  "New Certoclav"},
	devicekey: {type: String, unique: true, trim: true},
	news : {type:Boolean, default: false}, 

	pairing: 	{type:String, default:  "false" },
	lastUpdate: {type : Date , default : Date.now},
	
	metadata : {
		"firmware_version": { type:String, default: "v0.1"}
	},


	recentData:	{
		max:{type: Number , default:"-99.99"},
		"1":{
			value:{type: Number , default:"-99.99"},
			unit: {type: String , default:"°C" },
		},
		"2":{
			value:{ type: Number , default:"-99.99"},
			unit: { type: String , default:"°mbar" },
		},
		"3":{
			value:{ type: Number , default:"-99.99"},
			unit: { type: String , default:"%" },
		},
		"4":{
			value:{ type: Number , default:"-99.99"},
			unit: { type: String , default:"°C" },
		}
	},

	IFTTT : {
		key : {type:String, default: "b4f96237851e8dcd36b7ef4550239dc150550fcb6dbb40cc"},
		eventName_sensor : { type:String, default: "wemo"},
		eventName_button : { type:String, default: "wemo"}
	},


	events: 
		{	
			string: {type: String , default:"somewhere, something happend" },
			time  : {type : Date , default : Date.now},
			level :  {type: Number , default: 1 }
		},

	eventaction:{ 
		l1:{
			status: {type: String , default: "false" },
			action: {type: String , default: "none" },
			actiondata: {
				text: {type: String , default:"Sensor threshold reached!"}
			}

		},
		l2:{
			status: {type: String , default: "false" },
			action: {type: String , default: "Email" },
			actiondata: {
				email:{type: String , default:"Sven.Eliasson@Certclav.com"},
				text: {type: String , default:" Hello, this is the automatic generated Email from your CertoFridge Monitor! The preset limit of 20°C is reached. Please check the functionallaty of your Fridge or CertoFridge Sensor. Kind regards, Your CertoFrige Team"}
			}
			
		},
		l3:{
			status: {type: String , default: "false" },
			action: {type: String , default: "SMS" },
			actiondata: {
				number:{type: String , default:"+4917630638775"},
				text: {type: String , default:" Hello, this is the automatic generated Email from your CertoFridge Monitor! The preset limit of 20°C is reached. Please check the functionallaty of your Fridge or CertoFridge Sensor. Kind regards, Your CertoFrige Team"}

			}
			
		},
		l4:{
			status: {type: String , default: "false" },
			action: {type: String , default: "Call" },
			actiondata: {
				number:{type: String , default:"+4917630638775"},
				text: {type: String , default:" Hello, this is the automatic generated Email from your CertoFridge Monitor! The preset limit of 20°C is reached. Please check the functionallaty of your Fridge or CertoFridge Sensor. Kind regards, Your CertoFrige Team"}

			}
			
		},

	},


	APIkeys: [
		{ 
			key: {type:String, default:        "DEFAULT" },
			created: {type: String , default:  "°C" }, 
			MAC :{type: String , default:      "1a:2b:3c:4d" }
		},
		{ 
			key: {type:String, default:        "sudo12345" },
			created: {type: String , default:  "" }, 
			MAC :{type: String , default:      "sudo" }
		}
	],

	userContext: 
	{
		accountLimit: {
			meas_sampleRate: {type: Number , default: 60}, 
			data_sampleRate: {type: Number , default: 60},
			sensorName : {type: String , default: "FridgeLogger"}
		},
		accountData: {
			status: {type: String , default: "freeAccount"}
		}
	},

	context: 
	{	
		measObj:{					 
			"1":{	name: {type: String , default: "Temperature"},
				meas_sampleRate: {type: Number , default:"60"}, 
				data_sampleRate: {type: Number , default:"60"}, 
				unit : {type:String, default: "deg"},
				historicalData : {type: Boolean , default:true},
				i2cObj: {type: mongoose.Schema.Types.Mixed , default: undefined}
			},
			"2":{
				name: {type: String , default: "Pressure"},
				meas_sampleRate: {type: Number , default:60}, 
				data_sampleRate: {type: Number , default:60},  
				unit : {type:String, default: "deg"},
				historicalData : {type: Boolean , default:false},
				unit : {type: String, default: "mbar"}, 
				i2cObj: {type: mongoose.Schema.Types.Mixed , default: undefined}
			}
		},
		
		eventObj : {
			"A":{
				string : {type: String, default: "button pressed"},
				eventlvl: {type: Number, default: 1}
			},

			"1":{
				C1:{type: String, default: 	">"},
				V1:{type: Number , default: 	"10"},
				D1:{type: Number , default: 	"10"},
				S1:{type: Number , default: 	"0"}, 

				C2: {type: String,default: 	">"},
				V2:{type: Number , default: 	"15"},
				D2:{type: Number , default: 	"10"},
				S2: {type: Number , default:   "0"},

				C3: {type: String, default: 	">"},
				V3: {type: Number , default: 	"20"},
				D3: {type: Number , default: 	"10"},
				S3: {type: Number , default:  "1"},

				C4: {type: String,  default:	">"},
				V4: {type: Number , default: 	"23"},
				D4: {type: Number , default: 	"10"},
				S4: {type: Number , default:   "0"}
			},
			"2":{
				C1:{type: String, default: 	">"},
				V1:{type: Number , default: 	"10"},
				D1:{type: Number , default: 	"10"},
				S1:{type: Number , default: 	"0"}, 

				C2: {type: String, default: 	">"},
				V2:{type: Number , default: 	"15"},
				D2:{type: Number , default: 	"10"},
				S2: {type: Number , default:   "0"},

				C3: {type: String, default: 	">"},
				V3: {type: Number , default: 	"20"},
				D3: {type: Number , default: 	"10"},
				S3: {type: Number , default:  "1"},

				C4: {type: String,  default:	">"},
				V4: {type: Number , default: 	"23"},
				D4: {type: Number , default: 	"10"},
				S4: {type: Number , default:   "0"}
			}
		},


		sysObj: {
			communication: {type: String , default:"REST"}
		}

	},


	values : {
	 "1" :{
		 "0"  :opt.min_type,
		 "1"  :opt.min_type, "2"  :opt.min_type, "3"  :opt.min_type, "4"  :opt.min_type,"5":opt.min_type,
		 "6"  :opt.min_type, "7"  :opt.min_type, "8"  :opt.min_type, "9"  :opt.min_type, "10" :opt.min_type,
		 "11" :opt.min_type, "12" :opt.min_type, "13" :opt.min_type, "14" :opt.min_type,"15":opt.min_type,
		 "16" :opt.min_type, "17" :opt.min_type, "18" :opt.min_type, "19" :opt.min_type,"20":opt.min_type,
		 "21" :opt.min_type, "22" :opt.min_type, "23" :opt.min_type, "24" :opt.min_type,"25" :opt.min_type,
		 "26" :opt.min_type, "27" :opt.min_type, "28" :opt.min_type, "29" :opt.min_type, "30" :opt.min_type,
		 "31" :opt.min_type, "32" :opt.min_type, "33" :opt.min_type, "34" :opt.min_type, "35" :opt.min_type,
		 "36" :opt.min_type, "37" :opt.min_type, "38" :opt.min_type, "39" :opt.min_type, "40" :opt.min_type,
		 "41" :opt.min_type, "42" :opt.min_type, "43" :opt.min_type, "44" :opt.min_type, "45" :opt.min_type,
		 "46" :opt.min_type, "47" :opt.min_type, "48" :opt.min_type, "49" :opt.min_type, "50" :opt.min_type,
		 "51" :opt.min_type, "52" :opt.min_type, "53" :opt.min_type, "54" :opt.min_type, "55" :opt.min_type,
		 "56" :opt.min_type, "57" :opt.min_type, "58" :opt.min_type, "59" :opt.min_type, 
		} ,
	 "2" : {
		 "0"  :opt.min_type,
		 "1"  :opt.min_type, "2"  :opt.min_type, "3"  :opt.min_type, "4"  :opt.min_type,"5":opt.min_type,
		 "6"  :opt.min_type, "7"  :opt.min_type, "8"  :opt.min_type, "9"  :opt.min_type,"10":opt.min_type,
		 "11" :opt.min_type, "12" :opt.min_type, "13" :opt.min_type, "14" :opt.min_type,"15":opt.min_type,
		 "16" :opt.min_type, "17" :opt.min_type, "18" :opt.min_type, "19" :opt.min_type,"20":opt.min_type,
		 "21" :opt.min_type, "22" :opt.min_type, "23" :opt.min_type, "24" :opt.min_type,"25" :opt.min_type,
		 "26" :opt.min_type, "27" :opt.min_type, "28" :opt.min_type, "29" :opt.min_type, "30" :opt.min_type,
		 "31" :opt.min_type, "32" :opt.min_type, "33" :opt.min_type, "34" :opt.min_type, "35" :opt.min_type,
		 "36" :opt.min_type, "37" :opt.min_type, "38" :opt.min_type, "39" :opt.min_type, "40" :opt.min_type,
		 "41" :opt.min_type, "42" :opt.min_type, "43" :opt.min_type, "44" :opt.min_type, "45" :opt.min_type,
		 "46" :opt.min_type, "47" :opt.min_type, "48" :opt.min_type, "49" :opt.min_type, "50" :opt.min_type,
		 "51" :opt.min_type, "52" :opt.min_type, "53" :opt.min_type, "54" :opt.min_type, "55" :opt.min_type,
		 "56" :opt.min_type, "57" :opt.min_type, "58" :opt.min_type, "59" :opt.min_type,   
		} ,
	 "3" : {
		 "0"  :opt.min_type,
		 "1"  :opt.min_type, "2"  :opt.min_type, "3"  :opt.min_type, "4"  :opt.min_type,"5":opt.min_type,
		 "6"  :opt.min_type, "7"  :opt.min_type, "8"  :opt.min_type, "9"  :opt.min_type,"10":opt.min_type,
		 "11" :opt.min_type, "12" :opt.min_type, "13" :opt.min_type, "14" :opt.min_type,"15":opt.min_type,
		 "16" :opt.min_type, "17" :opt.min_type, "18" :opt.min_type, "19" :opt.min_type,"20":opt.min_type,
		 "21" :opt.min_type, "22" :opt.min_type, "23" :opt.min_type, "24" :opt.min_type,"25" :opt.min_type,
		 "26" :opt.min_type, "27" :opt.min_type, "28" :opt.min_type, "29" :opt.min_type, "30" :opt.min_type,
		 "31" :opt.min_type, "32" :opt.min_type, "33" :opt.min_type, "34" :opt.min_type, "35" :opt.min_type,
		 "36" :opt.min_type, "37" :opt.min_type, "38" :opt.min_type, "39" :opt.min_type, "40" :opt.min_type,
		 "41" :opt.min_type, "42" :opt.min_type, "43" :opt.min_type, "44" :opt.min_type, "45" :opt.min_type,
		 "46" :opt.min_type, "47" :opt.min_type, "48" :opt.min_type, "49" :opt.min_type, "50" :opt.min_type,
		 "51" :opt.min_type, "52" :opt.min_type, "53" :opt.min_type, "54" :opt.min_type, "55" :opt.min_type,
		 "56" :opt.min_type, "57" :opt.min_type, "58" :opt.min_type, "59" :opt.min_type,   
		} ,
	 "4" : {
		 "0"  :opt.min_type,
		 "1"  :opt.min_type, "2"  :opt.min_type, "3"  :opt.min_type, "4"  :opt.min_type,"5":opt.min_type,
		 "6"  :opt.min_type, "7"  :opt.min_type, "8"  :opt.min_type, "9"  :opt.min_type,"10":opt.min_type,
		 "11" :opt.min_type, "12" :opt.min_type, "13" :opt.min_type, "14" :opt.min_type,"15":opt.min_type,
		 "16" :opt.min_type, "17" :opt.min_type, "18" :opt.min_type, "19" :opt.min_type,"20":opt.min_type,
		 "21" :opt.min_type, "22" :opt.min_type, "23" :opt.min_type, "24" :opt.min_type,"25" :opt.min_type,
		 "26" :opt.min_type, "27" :opt.min_type, "28" :opt.min_type, "29" :opt.min_type, "30" :opt.min_type,
		 "31" :opt.min_type, "32" :opt.min_type, "33" :opt.min_type, "34" :opt.min_type, "35" :opt.min_type,
		 "36" :opt.min_type, "37" :opt.min_type, "38" :opt.min_type, "39" :opt.min_type, "40" :opt.min_type,
		 "41" :opt.min_type, "42" :opt.min_type, "43" :opt.min_type, "44" :opt.min_type, "45" :opt.min_type,
		 "46" :opt.min_type, "47" :opt.min_type, "48" :opt.min_type, "49" :opt.min_type, "50" :opt.min_type,
		 "51" :opt.min_type, "52" :opt.min_type, "53" :opt.min_type, "54" :opt.min_type, "55" :opt.min_type,
		 "56" :opt.min_type, "57" :opt.min_type, "58" :opt.min_type, "59" :opt.min_type,   
		} ,
	 "5" : {
		 "0"  :opt.min_type,
		 "1"  :opt.min_type, "2"  :opt.min_type, "3"  :opt.min_type, "4"  :opt.min_type,"5":opt.min_type,
		 "6"  :opt.min_type, "7"  :opt.min_type, "8"  :opt.min_type, "9"  :opt.min_type,"10":opt.min_type,
		 "11" :opt.min_type, "12" :opt.min_type, "13" :opt.min_type, "14" :opt.min_type,"15":opt.min_type,
		 "16" :opt.min_type, "17" :opt.min_type, "18" :opt.min_type, "19" :opt.min_type,"20":opt.min_type,
		 "21" :opt.min_type, "22" :opt.min_type, "23" :opt.min_type, "24" :opt.min_type,"25" :opt.min_type,
		 "26" :opt.min_type, "27" :opt.min_type, "28" :opt.min_type, "29" :opt.min_type, "30" :opt.min_type,
		 "31" :opt.min_type, "32" :opt.min_type, "33" :opt.min_type, "34" :opt.min_type, "35" :opt.min_type,
		 "36" :opt.min_type, "37" :opt.min_type, "38" :opt.min_type, "39" :opt.min_type, "40" :opt.min_type,
		 "41" :opt.min_type, "42" :opt.min_type, "43" :opt.min_type, "44" :opt.min_type, "45" :opt.min_type,
		 "46" :opt.min_type, "47" :opt.min_type, "48" :opt.min_type, "49" :opt.min_type, "50" :opt.min_type,
		 "51" :opt.min_type, "52" :opt.min_type, "53" :opt.min_type, "54" :opt.min_type, "55" :opt.min_type,
		 "56" :opt.min_type, "57" :opt.min_type, "58" :opt.min_type, "59" :opt.min_type,   
		} ,       
	 "6" : {
		 "0"  :opt.min_type,
		 "1"  :opt.min_type, "2"  :opt.min_type, "3"  :opt.min_type, "4"  :opt.min_type,"5":opt.min_type,
		 "6"  :opt.min_type, "7"  :opt.min_type, "8"  :opt.min_type, "9"  :opt.min_type,"10":opt.min_type,
		 "11" :opt.min_type, "12" :opt.min_type, "13" :opt.min_type, "14" :opt.min_type,"15":opt.min_type,
		 "16" :opt.min_type, "17" :opt.min_type, "18" :opt.min_type, "19" :opt.min_type,"20":opt.min_type,
		 "21" :opt.min_type, "22" :opt.min_type, "23" :opt.min_type, "24" :opt.min_type,"25" :opt.min_type,
		 "26" :opt.min_type, "27" :opt.min_type, "28" :opt.min_type, "29" :opt.min_type, "30" :opt.min_type,
		 "31" :opt.min_type, "32" :opt.min_type, "33" :opt.min_type, "34" :opt.min_type, "35" :opt.min_type,
		 "36" :opt.min_type, "37" :opt.min_type, "38" :opt.min_type, "39" :opt.min_type, "40" :opt.min_type,
		 "41" :opt.min_type, "42" :opt.min_type, "43" :opt.min_type, "44" :opt.min_type, "45" :opt.min_type,
		 "46" :opt.min_type, "47" :opt.min_type, "48" :opt.min_type, "49" :opt.min_type, "50" :opt.min_type,
		 "51" :opt.min_type, "52" :opt.min_type, "53" :opt.min_type, "54" :opt.min_type, "55" :opt.min_type,
		 "56" :opt.min_type, "57" :opt.min_type, "58" :opt.min_type, "59" :opt.min_type,   
		} ,       
	 "7" : {
		 "0"  :opt.min_type,
		 "1"  :opt.min_type, "2"  :opt.min_type, "3"  :opt.min_type, "4"  :opt.min_type,"5":opt.min_type,
		 "6"  :opt.min_type, "7"  :opt.min_type, "8"  :opt.min_type, "9"  :opt.min_type,"10":opt.min_type,
		 "11" :opt.min_type, "12" :opt.min_type, "13" :opt.min_type, "14" :opt.min_type,"15":opt.min_type,
		 "16" :opt.min_type, "17" :opt.min_type, "18" :opt.min_type, "19" :opt.min_type,"20":opt.min_type,
		 "21" :opt.min_type, "22" :opt.min_type, "23" :opt.min_type, "24" :opt.min_type,"25" :opt.min_type,
		 "26" :opt.min_type, "27" :opt.min_type, "28" :opt.min_type, "29" :opt.min_type, "30" :opt.min_type,
		 "31" :opt.min_type, "32" :opt.min_type, "33" :opt.min_type, "34" :opt.min_type, "35" :opt.min_type,
		 "36" :opt.min_type, "37" :opt.min_type, "38" :opt.min_type, "39" :opt.min_type, "40" :opt.min_type,
		 "41" :opt.min_type, "42" :opt.min_type, "43" :opt.min_type, "44" :opt.min_type, "45" :opt.min_type,
		 "46" :opt.min_type, "47" :opt.min_type, "48" :opt.min_type, "49" :opt.min_type, "50" :opt.min_type,
		 "51" :opt.min_type, "52" :opt.min_type, "53" :opt.min_type, "54" :opt.min_type, "55" :opt.min_type,
		 "56" :opt.min_type, "57" :opt.min_type, "58" :opt.min_type, "59" :opt.min_type,   
		} ,
	 "8" : {
		 "0"  :opt.min_type,
		 "1"  :opt.min_type, "2"  :opt.min_type, "3"  :opt.min_type, "4"  :opt.min_type,"5":opt.min_type,
		 "6"  :opt.min_type, "7"  :opt.min_type, "8"  :opt.min_type, "9"  :opt.min_type,"10":opt.min_type,
		 "11" :opt.min_type, "12" :opt.min_type, "13" :opt.min_type, "14" :opt.min_type,"15":opt.min_type,
		 "16" :opt.min_type, "17" :opt.min_type, "18" :opt.min_type, "19" :opt.min_type,"20":opt.min_type,
		 "21" :opt.min_type, "22" :opt.min_type, "23" :opt.min_type, "24" :opt.min_type,"25" :opt.min_type,
		 "26" :opt.min_type, "27" :opt.min_type, "28" :opt.min_type, "29" :opt.min_type, "30" :opt.min_type,
		 "31" :opt.min_type, "32" :opt.min_type, "33" :opt.min_type, "34" :opt.min_type, "35" :opt.min_type,
		 "36" :opt.min_type, "37" :opt.min_type, "38" :opt.min_type, "39" :opt.min_type, "40" :opt.min_type,
		 "41" :opt.min_type, "42" :opt.min_type, "43" :opt.min_type, "44" :opt.min_type, "45" :opt.min_type,
		 "46" :opt.min_type, "47" :opt.min_type, "48" :opt.min_type, "49" :opt.min_type, "50" :opt.min_type,
		 "51" :opt.min_type, "52" :opt.min_type, "53" :opt.min_type, "54" :opt.min_type, "55" :opt.min_type,
		 "56" :opt.min_type, "57" :opt.min_type, "58" :opt.min_type, "59" :opt.min_type,   
		} ,
	 "9" : {
		 "0"  :opt.min_type,
		 "1"  :opt.min_type, "2"  :opt.min_type, "3"  :opt.min_type, "4"  :opt.min_type,"5":opt.min_type,
		 "6"  :opt.min_type, "7"  :opt.min_type, "8"  :opt.min_type, "9"  :opt.min_type,"10":opt.min_type,
		 "11" :opt.min_type, "12" :opt.min_type, "13" :opt.min_type, "14" :opt.min_type,"15":opt.min_type,
		 "16" :opt.min_type, "17" :opt.min_type, "18" :opt.min_type, "19" :opt.min_type,"20":opt.min_type,
		 "21" :opt.min_type, "22" :opt.min_type, "23" :opt.min_type, "24" :opt.min_type,"25" :opt.min_type,
		 "26" :opt.min_type, "27" :opt.min_type, "28" :opt.min_type, "29" :opt.min_type, "30" :opt.min_type,
		 "31" :opt.min_type, "32" :opt.min_type, "33" :opt.min_type, "34" :opt.min_type, "35" :opt.min_type,
		 "36" :opt.min_type, "37" :opt.min_type, "38" :opt.min_type, "39" :opt.min_type, "40" :opt.min_type,
		 "41" :opt.min_type, "42" :opt.min_type, "43" :opt.min_type, "44" :opt.min_type, "45" :opt.min_type,
		 "46" :opt.min_type, "47" :opt.min_type, "48" :opt.min_type, "49" :opt.min_type, "50" :opt.min_type,
		 "51" :opt.min_type, "52" :opt.min_type, "53" :opt.min_type, "54" :opt.min_type, "55" :opt.min_type,
		 "56" :opt.min_type, "57" :opt.min_type, "58" :opt.min_type, "59" :opt.min_type,   
		} ,
	 "10" : {
		 "0"  :opt.min_type,
		 "1"  :opt.min_type, "2"  :opt.min_type, "3"  :opt.min_type, "4"  :opt.min_type,"5":opt.min_type,
		 "6"  :opt.min_type, "7"  :opt.min_type, "8"  :opt.min_type, "9"  :opt.min_type,"10":opt.min_type,
		 "11" :opt.min_type, "12" :opt.min_type, "13" :opt.min_type, "14" :opt.min_type,"15":opt.min_type,
		 "16" :opt.min_type, "17" :opt.min_type, "18" :opt.min_type, "19" :opt.min_type,"20":opt.min_type,
		 "21" :opt.min_type, "22" :opt.min_type, "23" :opt.min_type, "24" :opt.min_type,"25" :opt.min_type,
		 "26" :opt.min_type, "27" :opt.min_type, "28" :opt.min_type, "29" :opt.min_type, "30" :opt.min_type,
		 "31" :opt.min_type, "32" :opt.min_type, "33" :opt.min_type, "34" :opt.min_type, "35" :opt.min_type,
		 "36" :opt.min_type, "37" :opt.min_type, "38" :opt.min_type, "39" :opt.min_type, "40" :opt.min_type,
		 "41" :opt.min_type, "42" :opt.min_type, "43" :opt.min_type, "44" :opt.min_type, "45" :opt.min_type,
		 "46" :opt.min_type, "47" :opt.min_type, "48" :opt.min_type, "49" :opt.min_type, "50" :opt.min_type,
		 "51" :opt.min_type, "52" :opt.min_type, "53" :opt.min_type, "54" :opt.min_type, "55" :opt.min_type,
		 "56" :opt.min_type, "57" :opt.min_type, "58" :opt.min_type, "59" :opt.min_type,   
		} ,
	 "11" : {
		 "0"  :opt.min_type,
		 "1"  :opt.min_type, "2"  :opt.min_type, "3"  :opt.min_type, "4"  :opt.min_type,"5":opt.min_type,
		 "6"  :opt.min_type, "7"  :opt.min_type, "8"  :opt.min_type, "9"  :opt.min_type,"10":opt.min_type,
		 "11" :opt.min_type, "12" :opt.min_type, "13" :opt.min_type, "14" :opt.min_type,"15":opt.min_type,
		 "16" :opt.min_type, "17" :opt.min_type, "18" :opt.min_type, "19" :opt.min_type,"20":opt.min_type,
		 "21" :opt.min_type, "22" :opt.min_type, "23" :opt.min_type, "24" :opt.min_type,"25" :opt.min_type,
		 "26" :opt.min_type, "27" :opt.min_type, "28" :opt.min_type, "29" :opt.min_type, "30" :opt.min_type,
		 "31" :opt.min_type, "32" :opt.min_type, "33" :opt.min_type, "34" :opt.min_type, "35" :opt.min_type,
		 "36" :opt.min_type, "37" :opt.min_type, "38" :opt.min_type, "39" :opt.min_type, "40" :opt.min_type,
		 "41" :opt.min_type, "42" :opt.min_type, "43" :opt.min_type, "44" :opt.min_type, "45" :opt.min_type,
		 "46" :opt.min_type, "47" :opt.min_type, "48" :opt.min_type, "49" :opt.min_type, "50" :opt.min_type,
		 "51" :opt.min_type, "52" :opt.min_type, "53" :opt.min_type, "54" :opt.min_type, "55" :opt.min_type,
		 "56" :opt.min_type, "57" :opt.min_type, "58" :opt.min_type, "59" :opt.min_type,   
		} ,     
	 "12" : {
		 "0"  :opt.min_type,
		 "1"  :opt.min_type, "2"  :opt.min_type, "3"  :opt.min_type, "4"  :opt.min_type,"5":opt.min_type,
		 "6"  :opt.min_type, "7"  :opt.min_type, "8"  :opt.min_type, "9"  :opt.min_type,"10":opt.min_type,
		 "11" :opt.min_type, "12" :opt.min_type, "13" :opt.min_type, "14" :opt.min_type,"15":opt.min_type,
		 "16" :opt.min_type, "17" :opt.min_type, "18" :opt.min_type, "19" :opt.min_type,"20":opt.min_type,
		 "21" :opt.min_type, "22" :opt.min_type, "23" :opt.min_type, "24" :opt.min_type,"25" :opt.min_type,
		 "26" :opt.min_type, "27" :opt.min_type, "28" :opt.min_type, "29" :opt.min_type, "30" :opt.min_type,
		 "31" :opt.min_type, "32" :opt.min_type, "33" :opt.min_type, "34" :opt.min_type, "35" :opt.min_type,
		 "36" :opt.min_type, "37" :opt.min_type, "38" :opt.min_type, "39" :opt.min_type, "40" :opt.min_type,
		 "41" :opt.min_type, "42" :opt.min_type, "43" :opt.min_type, "44" :opt.min_type, "45" :opt.min_type,
		 "46" :opt.min_type, "47" :opt.min_type, "48" :opt.min_type, "49" :opt.min_type, "50" :opt.min_type,
		 "51" :opt.min_type, "52" :opt.min_type, "53" :opt.min_type, "54" :opt.min_type, "55" :opt.min_type,
		 "56" :opt.min_type, "57" :opt.min_type, "58" :opt.min_type, "59" :opt.min_type,   
		} , 
	 "13" : {
		 "0"  :opt.min_type,
		 "1"  :opt.min_type, "2"  :opt.min_type, "3"  :opt.min_type, "4"  :opt.min_type,"5":opt.min_type,
		 "6"  :opt.min_type, "7"  :opt.min_type, "8"  :opt.min_type, "9"  :opt.min_type,"10":opt.min_type,
		 "11" :opt.min_type, "12" :opt.min_type, "13" :opt.min_type, "14" :opt.min_type,"15":opt.min_type,
		 "16" :opt.min_type, "17" :opt.min_type, "18" :opt.min_type, "19" :opt.min_type,"20":opt.min_type,
		 "21" :opt.min_type, "22" :opt.min_type, "23" :opt.min_type, "24" :opt.min_type,"25" :opt.min_type,
		 "26" :opt.min_type, "27" :opt.min_type, "28" :opt.min_type, "29" :opt.min_type, "30" :opt.min_type,
		 "31" :opt.min_type, "32" :opt.min_type, "33" :opt.min_type, "34" :opt.min_type, "35" :opt.min_type,
		 "36" :opt.min_type, "37" :opt.min_type, "38" :opt.min_type, "39" :opt.min_type, "40" :opt.min_type,
		 "41" :opt.min_type, "42" :opt.min_type, "43" :opt.min_type, "44" :opt.min_type, "45" :opt.min_type,
		 "46" :opt.min_type, "47" :opt.min_type, "48" :opt.min_type, "49" :opt.min_type, "50" :opt.min_type,
		 "51" :opt.min_type, "52" :opt.min_type, "53" :opt.min_type, "54" :opt.min_type, "55" :opt.min_type,
		 "56" :opt.min_type, "57" :opt.min_type, "58" :opt.min_type, "59" :opt.min_type,   
		} ,
	 "14" : {
		 "0"  :opt.min_type,
		 "1"  :opt.min_type, "2"  :opt.min_type, "3"  :opt.min_type, "4"  :opt.min_type,"5":opt.min_type,
		 "6"  :opt.min_type, "7"  :opt.min_type, "8"  :opt.min_type, "9"  :opt.min_type,"10":opt.min_type,
		 "11" :opt.min_type, "12" :opt.min_type, "13" :opt.min_type, "14" :opt.min_type,"15":opt.min_type,
		 "16" :opt.min_type, "17" :opt.min_type, "18" :opt.min_type, "19" :opt.min_type,"20":opt.min_type,
		 "21" :opt.min_type, "22" :opt.min_type, "23" :opt.min_type, "24" :opt.min_type,"25" :opt.min_type,
		 "26" :opt.min_type, "27" :opt.min_type, "28" :opt.min_type, "29" :opt.min_type, "30" :opt.min_type,
		 "31" :opt.min_type, "32" :opt.min_type, "33" :opt.min_type, "34" :opt.min_type, "35" :opt.min_type,
		 "36" :opt.min_type, "37" :opt.min_type, "38" :opt.min_type, "39" :opt.min_type, "40" :opt.min_type,
		 "41" :opt.min_type, "42" :opt.min_type, "43" :opt.min_type, "44" :opt.min_type, "45" :opt.min_type,
		 "46" :opt.min_type, "47" :opt.min_type, "48" :opt.min_type, "49" :opt.min_type, "50" :opt.min_type,
		 "51" :opt.min_type, "52" :opt.min_type, "53" :opt.min_type, "54" :opt.min_type, "55" :opt.min_type,
		 "56" :opt.min_type, "57" :opt.min_type, "58" :opt.min_type, "59" :opt.min_type,   
		} ,
	 "15" : {
		 "0"  :opt.min_type,
		 "1"  :opt.min_type, "2"  :opt.min_type, "3"  :opt.min_type, "4"  :opt.min_type,"5":opt.min_type,
		 "6"  :opt.min_type, "7"  :opt.min_type, "8"  :opt.min_type, "9"  :opt.min_type,"10":opt.min_type,
		 "11" :opt.min_type, "12" :opt.min_type, "13" :opt.min_type, "14" :opt.min_type,"15":opt.min_type,
		 "16" :opt.min_type, "17" :opt.min_type, "18" :opt.min_type, "19" :opt.min_type,"20":opt.min_type,
		 "21" :opt.min_type, "22" :opt.min_type, "23" :opt.min_type, "24" :opt.min_type,"25" :opt.min_type,
		 "26" :opt.min_type, "27" :opt.min_type, "28" :opt.min_type, "29" :opt.min_type, "30" :opt.min_type,
		 "31" :opt.min_type, "32" :opt.min_type, "33" :opt.min_type, "34" :opt.min_type, "35" :opt.min_type,
		 "36" :opt.min_type, "37" :opt.min_type, "38" :opt.min_type, "39" :opt.min_type, "40" :opt.min_type,
		 "41" :opt.min_type, "42" :opt.min_type, "43" :opt.min_type, "44" :opt.min_type, "45" :opt.min_type,
		 "46" :opt.min_type, "47" :opt.min_type, "48" :opt.min_type, "49" :opt.min_type, "50" :opt.min_type,
		 "51" :opt.min_type, "52" :opt.min_type, "53" :opt.min_type, "54" :opt.min_type, "55" :opt.min_type,
		 "56" :opt.min_type, "57" :opt.min_type, "58" :opt.min_type, "59" :opt.min_type,   
		} ,
	 "16" : {
		 "0"  :opt.min_type,
		 "1"  :opt.min_type, "2"  :opt.min_type, "3"  :opt.min_type, "4"  :opt.min_type,"5":opt.min_type,
		 "6"  :opt.min_type, "7"  :opt.min_type, "8"  :opt.min_type, "9"  :opt.min_type,"10":opt.min_type,
		 "11" :opt.min_type, "12" :opt.min_type, "13" :opt.min_type, "14" :opt.min_type,"15":opt.min_type,
		 "16" :opt.min_type, "17" :opt.min_type, "18" :opt.min_type, "19" :opt.min_type,"20":opt.min_type,
		 "21" :opt.min_type, "22" :opt.min_type, "23" :opt.min_type, "24" :opt.min_type,"25" :opt.min_type,
		 "26" :opt.min_type, "27" :opt.min_type, "28" :opt.min_type, "29" :opt.min_type, "30" :opt.min_type,
		 "31" :opt.min_type, "32" :opt.min_type, "33" :opt.min_type, "34" :opt.min_type, "35" :opt.min_type,
		 "36" :opt.min_type, "37" :opt.min_type, "38" :opt.min_type, "39" :opt.min_type, "40" :opt.min_type,
		 "41" :opt.min_type, "42" :opt.min_type, "43" :opt.min_type, "44" :opt.min_type, "45" :opt.min_type,
		 "46" :opt.min_type, "47" :opt.min_type, "48" :opt.min_type, "49" :opt.min_type, "50" :opt.min_type,
		 "51" :opt.min_type, "52" :opt.min_type, "53" :opt.min_type, "54" :opt.min_type, "55" :opt.min_type,
		 "56" :opt.min_type, "57" :opt.min_type, "58" :opt.min_type, "59" :opt.min_type,   
		} ,
	 "17" : {
		 "0"  :opt.min_type,
		 "1"  :opt.min_type, "2"  :opt.min_type, "3"  :opt.min_type, "4"  :opt.min_type,"5":opt.min_type,
		 "6"  :opt.min_type, "7"  :opt.min_type, "8"  :opt.min_type, "9"  :opt.min_type,"10":opt.min_type,
		 "11" :opt.min_type, "12" :opt.min_type, "13" :opt.min_type, "14" :opt.min_type,"15":opt.min_type,
		 "16" :opt.min_type, "17" :opt.min_type, "18" :opt.min_type, "19" :opt.min_type,"20":opt.min_type,
		 "21" :opt.min_type, "22" :opt.min_type, "23" :opt.min_type, "24" :opt.min_type,"25" :opt.min_type,
		 "26" :opt.min_type, "27" :opt.min_type, "28" :opt.min_type, "29" :opt.min_type, "30" :opt.min_type,
		 "31" :opt.min_type, "32" :opt.min_type, "33" :opt.min_type, "34" :opt.min_type, "35" :opt.min_type,
		 "36" :opt.min_type, "37" :opt.min_type, "38" :opt.min_type, "39" :opt.min_type, "40" :opt.min_type,
		 "41" :opt.min_type, "42" :opt.min_type, "43" :opt.min_type, "44" :opt.min_type, "45" :opt.min_type,
		 "46" :opt.min_type, "47" :opt.min_type, "48" :opt.min_type, "49" :opt.min_type, "50" :opt.min_type,
		 "51" :opt.min_type, "52" :opt.min_type, "53" :opt.min_type, "54" :opt.min_type, "55" :opt.min_type,
		 "56" :opt.min_type, "57" :opt.min_type, "58" :opt.min_type, "59" :opt.min_type,   
		} ,    
	 "18" : {
		 "0"  :opt.min_type,
		 "1"  :opt.min_type, "2"  :opt.min_type, "3"  :opt.min_type, "4"  :opt.min_type,"5":opt.min_type,
		 "6"  :opt.min_type, "7"  :opt.min_type, "8"  :opt.min_type, "9"  :opt.min_type,"10":opt.min_type,
		 "11" :opt.min_type, "12" :opt.min_type, "13" :opt.min_type, "14" :opt.min_type,"15":opt.min_type,
		 "16" :opt.min_type, "17" :opt.min_type, "18" :opt.min_type, "19" :opt.min_type,"20":opt.min_type,
		 "21" :opt.min_type, "22" :opt.min_type, "23" :opt.min_type, "24" :opt.min_type,"25" :opt.min_type,
		 "26" :opt.min_type, "27" :opt.min_type, "28" :opt.min_type, "29" :opt.min_type, "30" :opt.min_type,
		 "31" :opt.min_type, "32" :opt.min_type, "33" :opt.min_type, "34" :opt.min_type, "35" :opt.min_type,
		 "36" :opt.min_type, "37" :opt.min_type, "38" :opt.min_type, "39" :opt.min_type, "40" :opt.min_type,
		 "41" :opt.min_type, "42" :opt.min_type, "43" :opt.min_type, "44" :opt.min_type, "45" :opt.min_type,
		 "46" :opt.min_type, "47" :opt.min_type, "48" :opt.min_type, "49" :opt.min_type, "50" :opt.min_type,
		 "51" :opt.min_type, "52" :opt.min_type, "53" :opt.min_type, "54" :opt.min_type, "55" :opt.min_type,
		 "56" :opt.min_type, "57" :opt.min_type, "58" :opt.min_type, "59" :opt.min_type,   
		} ,     
	 "19" : {
		 "0"  :opt.min_type,
		 "1"  :opt.min_type, "2"  :opt.min_type, "3"  :opt.min_type, "4"  :opt.min_type,"5":opt.min_type,
		 "6"  :opt.min_type, "7"  :opt.min_type, "8"  :opt.min_type, "9"  :opt.min_type,"10":opt.min_type,
		 "11" :opt.min_type, "12" :opt.min_type, "13" :opt.min_type, "14" :opt.min_type,"15":opt.min_type,
		 "16" :opt.min_type, "17" :opt.min_type, "18" :opt.min_type, "19" :opt.min_type,"20":opt.min_type,
		 "21" :opt.min_type, "22" :opt.min_type, "23" :opt.min_type, "24" :opt.min_type,"25" :opt.min_type,
		 "26" :opt.min_type, "27" :opt.min_type, "28" :opt.min_type, "29" :opt.min_type, "30" :opt.min_type,
		 "31" :opt.min_type, "32" :opt.min_type, "33" :opt.min_type, "34" :opt.min_type, "35" :opt.min_type,
		 "36" :opt.min_type, "37" :opt.min_type, "38" :opt.min_type, "39" :opt.min_type, "40" :opt.min_type,
		 "41" :opt.min_type, "42" :opt.min_type, "43" :opt.min_type, "44" :opt.min_type, "45" :opt.min_type,
		 "46" :opt.min_type, "47" :opt.min_type, "48" :opt.min_type, "49" :opt.min_type, "50" :opt.min_type,
		 "51" :opt.min_type, "52" :opt.min_type, "53" :opt.min_type, "54" :opt.min_type, "55" :opt.min_type,
		 "56" :opt.min_type, "57" :opt.min_type, "58" :opt.min_type, "59" :opt.min_type,   
		} ,
	 "20" : {
		 "0"  :opt.min_type,
		 "1"  :opt.min_type, "2"  :opt.min_type, "3"  :opt.min_type, "4"  :opt.min_type,"5":opt.min_type,
		 "6"  :opt.min_type, "7"  :opt.min_type, "8"  :opt.min_type, "9"  :opt.min_type,"10":opt.min_type,
		 "11" :opt.min_type, "12" :opt.min_type, "13" :opt.min_type, "14" :opt.min_type,"15":opt.min_type,
		 "16" :opt.min_type, "17" :opt.min_type, "18" :opt.min_type, "19" :opt.min_type,"20":opt.min_type,
		 "21" :opt.min_type, "22" :opt.min_type, "23" :opt.min_type, "24" :opt.min_type,"25" :opt.min_type,
		 "26" :opt.min_type, "27" :opt.min_type, "28" :opt.min_type, "29" :opt.min_type, "30" :opt.min_type,
		 "31" :opt.min_type, "32" :opt.min_type, "33" :opt.min_type, "34" :opt.min_type, "35" :opt.min_type,
		 "36" :opt.min_type, "37" :opt.min_type, "38" :opt.min_type, "39" :opt.min_type, "40" :opt.min_type,
		 "41" :opt.min_type, "42" :opt.min_type, "43" :opt.min_type, "44" :opt.min_type, "45" :opt.min_type,
		 "46" :opt.min_type, "47" :opt.min_type, "48" :opt.min_type, "49" :opt.min_type, "50" :opt.min_type,
		 "51" :opt.min_type, "52" :opt.min_type, "53" :opt.min_type, "54" :opt.min_type, "55" :opt.min_type,
		 "56" :opt.min_type, "57" :opt.min_type, "58" :opt.min_type, "59" :opt.min_type,   
		} ,
	 "21" : {
		 "0"  :opt.min_type,
		 "1"  :opt.min_type, "2"  :opt.min_type, "3"  :opt.min_type, "4"  :opt.min_type,"5":opt.min_type,
		 "6"  :opt.min_type, "7"  :opt.min_type, "8"  :opt.min_type, "9"  :opt.min_type,"10":opt.min_type,
		 "11" :opt.min_type, "12" :opt.min_type, "13" :opt.min_type, "14" :opt.min_type,"15":opt.min_type,
		 "16" :opt.min_type, "17" :opt.min_type, "18" :opt.min_type, "19" :opt.min_type,"20":opt.min_type,
		 "21" :opt.min_type, "22" :opt.min_type, "23" :opt.min_type, "24" :opt.min_type,"25" :opt.min_type,
		 "26" :opt.min_type, "27" :opt.min_type, "28" :opt.min_type, "29" :opt.min_type, "30" :opt.min_type,
		 "31" :opt.min_type, "32" :opt.min_type, "33" :opt.min_type, "34" :opt.min_type, "35" :opt.min_type,
		 "36" :opt.min_type, "37" :opt.min_type, "38" :opt.min_type, "39" :opt.min_type, "40" :opt.min_type,
		 "41" :opt.min_type, "42" :opt.min_type, "43" :opt.min_type, "44" :opt.min_type, "45" :opt.min_type,
		 "46" :opt.min_type, "47" :opt.min_type, "48" :opt.min_type, "49" :opt.min_type, "50" :opt.min_type,
		 "51" :opt.min_type, "52" :opt.min_type, "53" :opt.min_type, "54" :opt.min_type, "55" :opt.min_type,
		 "56" :opt.min_type, "57" :opt.min_type, "58" :opt.min_type, "59" :opt.min_type,   
		} ,
	 "22" : {
		 "0"  :opt.min_type,
		 "1"  :opt.min_type, "2"  :opt.min_type, "3"  :opt.min_type, "4"  :opt.min_type,"5":opt.min_type,
		 "6"  :opt.min_type, "7"  :opt.min_type, "8"  :opt.min_type, "9"  :opt.min_type,"10":opt.min_type,
		 "11" :opt.min_type, "12" :opt.min_type, "13" :opt.min_type, "14" :opt.min_type,"15":opt.min_type,
		 "16" :opt.min_type, "17" :opt.min_type, "18" :opt.min_type, "19" :opt.min_type,"20":opt.min_type,
		 "21" :opt.min_type, "22" :opt.min_type, "23" :opt.min_type, "24" :opt.min_type,"25" :opt.min_type,
		 "26" :opt.min_type, "27" :opt.min_type, "28" :opt.min_type, "29" :opt.min_type, "30" :opt.min_type,
		 "31" :opt.min_type, "32" :opt.min_type, "33" :opt.min_type, "34" :opt.min_type, "35" :opt.min_type,
		 "36" :opt.min_type, "37" :opt.min_type, "38" :opt.min_type, "39" :opt.min_type, "40" :opt.min_type,
		 "41" :opt.min_type, "42" :opt.min_type, "43" :opt.min_type, "44" :opt.min_type, "45" :opt.min_type,
		 "46" :opt.min_type, "47" :opt.min_type, "48" :opt.min_type, "49" :opt.min_type, "50" :opt.min_type,
		 "51" :opt.min_type, "52" :opt.min_type, "53" :opt.min_type, "54" :opt.min_type, "55" :opt.min_type,
		 "56" :opt.min_type, "57" :opt.min_type, "58" :opt.min_type, "59" :opt.min_type,   
		} ,
	 "23" : {
		 "0"  :opt.min_type,
		 "1"  :opt.min_type, "2"  :opt.min_type, "3"  :opt.min_type, "4"  :opt.min_type,"5":opt.min_type,
		 "6"  :opt.min_type, "7"  :opt.min_type, "8"  :opt.min_type, "9"  :opt.min_type,"10":opt.min_type,
		 "11" :opt.min_type, "12" :opt.min_type, "13" :opt.min_type, "14" :opt.min_type,"15":opt.min_type,
		 "16" :opt.min_type, "17" :opt.min_type, "18" :opt.min_type, "19" :opt.min_type,"20":opt.min_type,
		 "21" :opt.min_type, "22" :opt.min_type, "23" :opt.min_type, "24" :opt.min_type,"25" :opt.min_type,
		 "26" :opt.min_type, "27" :opt.min_type, "28" :opt.min_type, "29" :opt.min_type, "30" :opt.min_type,
		 "31" :opt.min_type, "32" :opt.min_type, "33" :opt.min_type, "34" :opt.min_type, "35" :opt.min_type,
		 "36" :opt.min_type, "37" :opt.min_type, "38" :opt.min_type, "39" :opt.min_type, "40" :opt.min_type,
		 "41" :opt.min_type, "42" :opt.min_type, "43" :opt.min_type, "44" :opt.min_type, "45" :opt.min_type,
		 "46" :opt.min_type, "47" :opt.min_type, "48" :opt.min_type, "49" :opt.min_type, "50" :opt.min_type,
		 "51" :opt.min_type, "52" :opt.min_type, "53" :opt.min_type, "54" :opt.min_type, "55" :opt.min_type,
		 "56" :opt.min_type, "57" :opt.min_type, "58" :opt.min_type, "59" :opt.min_type,   
		} ,       
	 "0" : {
		 "0"  :opt.min_type,
		 "1"  :opt.min_type, "2"  :opt.min_type, "3"  :opt.min_type, "4"  :opt.min_type,"5":opt.min_type,
		 "6"  :opt.min_type, "7"  :opt.min_type, "8"  :opt.min_type, "9"  :opt.min_type,"10":opt.min_type,
		 "11" :opt.min_type, "12" :opt.min_type, "13" :opt.min_type, "14" :opt.min_type,"15":opt.min_type,
		 "16" :opt.min_type, "17" :opt.min_type, "18" :opt.min_type, "19" :opt.min_type,"20":opt.min_type,
		 "21" :opt.min_type, "22" :opt.min_type, "23" :opt.min_type, "24" :opt.min_type,"25" :opt.min_type,
		 "26" :opt.min_type, "27" :opt.min_type, "28" :opt.min_type, "29" :opt.min_type, "30" :opt.min_type,
		 "31" :opt.min_type, "32" :opt.min_type, "33" :opt.min_type, "34" :opt.min_type, "35" :opt.min_type,
		 "36" :opt.min_type, "37" :opt.min_type, "38" :opt.min_type, "39" :opt.min_type, "40" :opt.min_type,
		 "41" :opt.min_type, "42" :opt.min_type, "43" :opt.min_type, "44" :opt.min_type, "45" :opt.min_type,
		 "46" :opt.min_type, "47" :opt.min_type, "48" :opt.min_type, "49" :opt.min_type, "50" :opt.min_type,
		 "51" :opt.min_type, "52" :opt.min_type, "53" :opt.min_type, "54" :opt.min_type, "55" :opt.min_type,
		 "56" :opt.min_type, "57" :opt.min_type, "58" :opt.min_type, "59" :opt.min_type,   
		} ,
		     
	}
});

module.exports = mongoose.model('Devices', deviceSchema);


