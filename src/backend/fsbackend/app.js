var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose'); 
var compress = require('compression');
var request = require('request');  
 
var app = express();

var config = { 
  db : { 
    uri : "mongodb://fsbackend:honeybee@dogen.mongohq.com:10099/test_mongo",
    //uri : "mongodb://rystabackend:CertoclavRysta@ds055862.mongolab.com:55862/rystadb",
    opt : ""
  }
};


/// MongoD conenction 
var db = mongoose.connect(config.db.uri, config.db.options, function(err) {
  if (err) {
    console.error('Could not connect to MongoDB!'); console.log(err);
  }
});
mongoose.connection.on("open", function(ref) {console.log("Connected to mongo server.");});




// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');


/**
 * middleware function to set the headers for CORS.
 *
 * @method app.all
 */
app.all('/*', function(req, res, next) {
  // CORS headers
  res.header("Access-Control-Allow-Origin", "*"); // restrict it to the required domain
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  // Set custom headers for CORS
  res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key');
  // When performing a cross domain request, you will recieve
  // an OPTION request first to check if the application is available
  if (req.method == 'OPTIONS') {
    res.status(200).end();
  } else {
    next();
  }
});

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.disable("X-powered-by");


app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

/**
 * Auth Middleware - This will check if the token is valid
 * Only the requests that start with /api/* will be checked for the token.
 *
 * @method /api/* middleware
 */
app.all('/api/*', [require('./middlewares/validateRequest')]);


/**
 * setting routes
 *
 * @method / routes middleware
 */
app.use('/', require('./routes'));


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
