'use strict';
var mongoose = require("mongoose");
var DeviceModel = require('../../models/device');

var pairing = {

	request: function( req,res){
		if( req.method == "GET"){
			var devID = req.query.devID; 
		}else if( req.method == "POST" ){
			var devID = req.body.devID;
		}else{
			var devID = req.query.devID; 
		}
	
		if ( devID ){
			DeviceModel.findOne(  {deviceID: devID}, function (err, device) {
				if (err){
					res.json({ 
						"status":400,
						"message": "devID not found"
					});			
					console.log(err);
					
					return;
				}else{
					if (device.pairing == "false"){
						DeviceModel.update(  {deviceID: devID}, { 
							$set: { pairing: "request"
							}
					
						}, function (err, small) {
							if (err) {
								res.json({
								"status": 403,
								"message": "error "
								});
							return;
							}
						});
					}else{
						res.json({
						"status": 400,
						"message": "device cant be paired at the moment"
						});
						return;
					}
				}
			});
			
				
	
		}else{
			res.json({
				"status": 400,
				"message": "devID not handled"
			});
			return;
		}
	}, 
	status: function(req, res){
		var devID = req.query.devID;
		if ( devID ){
		DeviceModel.findOne(  {deviceID: devID},function (err, device) {
			if (err){
				res.json({ 
					"status":400,
					"message": "Device not found"
				});			
				console.log(err);
				return;

			}else{
				res.json({
				"status":200,
				"pairingStatus":device.pairing,
				
				});
			}
			});
		}
	},
	ackn: function(req, res){
		var devID = req.body.devID;
		var pairing = req.body.pairingStatus;
		if(!devID || !pairing){
			res.json({
			"status": 403,
			"message": "Faulty request. No/wrong devID or pairingStatus"
			});
			return;		
		}
		DeviceModel.update(  {deviceID: devID}, { $set:
			{pairing: pairing} }, function (err) {
				if (err) console.log(err);
			});
		
		res.json({
			"status":200, 
			"message":("pairing updatet to: "+pairing)
		});
	}


};

module.exports = pairing;