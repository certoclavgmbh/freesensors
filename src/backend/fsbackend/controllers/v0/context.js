'use strict';
var mongoose = require("mongoose");
var DeviceModel = require('../../models/device');

var context = {
	put : function(req, res) {
		var devID = req.body.devID;
		var newContext = req.body.context;
		if(!devID || !newContext){
			res.json({
				"status": 403,
				"message": "Faulty request. No/wrong devID "
			});
			return;		
		}
		DeviceModel.update(  {deviceID: devID}, { $set:
			{
				context: newContext,
				news: true


			} }, function (err) {
				if (err) console.log(err);
			});
		
		res.json({
			"status":200, 
			"message":("contxt updatet")
		});
	},

	get : function(req, res) {
		var devID = req.query.devID;
		if ( devID){				
			DeviceModel.update(  {deviceID: devID}, { $set:
				{  news :false } 
			}, function (err) {
				if (err) console.log(err);
			});

			DeviceModel.findOne(  {deviceID: devID},function (err, device) {
				if (err){
					res.json({ 
						"status":400,
						"message": "error"
					});
					
					console.log(err);
					return;
				}else if( device){
					res.json({
					"status":200,
					"context": device.context
					});
				}
				else{
					res.json({ 
						"status":400,
						"message": "error"
					});	
				}
			});
		}

	}
};

module.exports = context;