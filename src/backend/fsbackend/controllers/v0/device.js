'use strict';
var mongoose = require("mongoose");
var DeviceModel = require('../../models/device');
var request = require("request");

var device = {
	delete : function(req, res) {
		if( req.method == "GET"){
			var devID = req.query.devID; 
			console.log("devID = " + devID);
		}else if( req.method == "POST" ){
			var devID = req.body.devID;
			console.log("devID = " + devID);
		}else{
			var devID = req.query.devID; 
			console.log("devID = " + devID);
		}

		if (!devID) {
			res.json({
				"status": 403,
				"message": "Invalid request, check devID and request format"
			});
			return;
		}
		DeviceModel.remove({deviceID : devID}, function(err){		
			if(err){
				console.log(err);
				res.json({
					"status": 404,
					"message": "Device not found"
				});
				return;
			}else{
				res.json({
					"status": 200,
					"message": "Device removed"
				});
			}
		})
	},/// END OF DELETE
	create: function(req, res) {
	
		var devID = req.query.devID; 
		if (!devID) {
			res.json({
				"status": 403,
				"message": "Invalid input"
			});
			return;
		}
		DeviceModel.findOne({deviceID : devID}, function(err, device){
		if(device){
			res.json({
				"status": 403,
				"message": "Device already exists"
			});
			return;
		}else{
			DeviceModel.create({deviceID : devID}, function (err, small) {
			if (err) console.log(err);
			});
			res.json({
				"status": 400,
				"message": "Device created"
			});
		}
		})
	}, /// END OF CREATE
	data: function(req, res){

		if (devID == "CX0009"){ devID = "CX009";}
		if (devID == "CX0010"){ devID = "CX010";}
		if (devID == "B001"  ){ devID = "CX007";}

		var devID = req.query.devID;
			if ( devID ){
				DeviceModel.findOne(  {deviceID: devID},function (err, device) {
					if (err){
						res.json({ 
							"status":400,
							"message": "error"
						});
					console.log(err);
					return;
					}else if( device){
						res.json({
							"status":200,
							"field1":device.recentData["1"].value,
							"field2":device.recentData["2"].value,
							lastUpdate : device.lastUpdate
						});
					}else{
						res.json({ 
							"status":400,
							"message": "error"
						});	
					}
				});
			}
	}, /// END OF DATA

 	update: function(req, res){

 			
 		if ( req.method == "POST"){
			var devID = req.body.devID;
			var tmp_data = null;
			var tmp_data2 = null;
			var tmp_data3 = null;
			var tmp_data4 = null;
			tmp_data  = req.body.field1;
			if(req.body.field2) tmp_data2 = req.body.field2; 
			if(req.body.field3) tmp_data3 = req.body.field3; 
			if(req.body.field4) tmp_data4 = req.body.field4; 

		}else if (req.method == "GET"){
			var devID = req.query.devID;
			var tmp_data = null;
			var tmp_data2 = null;
			var tmp_data3 = null;
			var tmp_data4 = null;
			tmp_data  = req.query.field1;
			if(req.query.field2) tmp_data2 = req.query.field2; 
			if(req.query.field3) tmp_data3 = req.query.field3; 
			if(req.query.field4) tmp_data4 = req.query.field4; 
		}
		
		if (devID == "CX0009"){ devID = "CX009";}
		if (devID == "CX0010"){ devID = "CX010";}
		if (devID == "B001"  ){ devID = "CX007";}


		DeviceModel.findOne( {deviceID: devID}, function (err, device) {
			if (err){
				console.log(err); 
				res.json({ 
					"status":400,
					"message": "Database error"
				});
				
				console.log(err);
				return;
			}else if(device){
				res.json({
				"status":200,
				"message": "success",
				"news": device.news
				});
			}
			else{
				res.json({ 
					"status":400,
					"message": "Device not found"
				});	
			}
		});

		if ( devID && tmp_data){
			var date = new Date();
			var obje = {};
			obje['values.' + date.getHours().toString() + '.'+ date.getMinutes().toString() ] = tmp_data;

			DeviceModel.update(  {deviceID: devID}, { 
				$set: { 
					recentData:{
						"1":{ value: tmp_data, unit:"°C"},
						"2":{ value: tmp_data2, unit: "mbar"},
						"3":{ value: tmp_data3, unit: "mbar"},
						"4":{ value: tmp_data4, unit: "mbar"}				
					},
					lastUpdate : Date.now()
				} 
			}, function (err, small) {
				if (err)  {
					console.log(" update error"); 
					console.log(err)
				};
			});	

			DeviceModel.update(  {deviceID: devID}, { $set:obje}, function (err, small) {
				if (err) {
					console.log(" update list error"); 
					console.log(err);
					}
			}); 
		}

	}, /// END OF update(); 


};

module.exports = device;