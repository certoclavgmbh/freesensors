'use strict';
var mongoose = require("mongoose");
var DeviceModel = require('../../models/device');

var history = {

	get : function( req,res){
		var devID = req.query.devID;
		if ( devID ){
		DeviceModel.findOne(  {deviceID: devID},function (err, device) {
			if (err){
				res.json({ 
					"status":400,
					"message": err
				});
				
				console.log(err);
				return;
			}else if( device){
				res.json({
				"status":200,
				"values":device.values
				});
			}
			else{
				res.json({ 
					"status":400,
					"message": "not dev found"
				});	
			}
			});
		}
	}

};

module.exports = history;



