'use strict';
var mongoose = require("mongoose");
var DeviceModel = require('../../models/device');
var twilio = require('twilio');
var client = new twilio.RestClient('ACea6822203e981e236396333551366d88', '9db5f4330d53a88bf9a341dacc466092');
var resp = new twilio.TwimlResponse();
var request = require("request");

var events = {
	put : function( req,res){
		var devID = req.body.devID;


		var neweventaction = req.body.eventaction;
		if(!devID || !neweventaction){
			res.json({
			"status": 403,
			"message": "Faulty request. No/wrong devID "
			});
			return;		
		}
		DeviceModel.update(  {deviceID: devID}, { $set:
			{
				eventaction: neweventaction
			} }, function (err) {
				if (err) console.log(err);
			});
		
		res.json({
			"status":200, 
			"message":("contxt updatet")
		});
	}, 
	get : function( req,res){
		var devID = req.query.devID;
		if ( devID ){
		DeviceModel.findOne(  {deviceID: devID},function (err, device) {
			if (err){
				res.json({ 
					"status":400,
					"message": "error"
				});
				
				console.log(err);
				return;
			}else if( device){
				res.json({
				"status":200,
				"eventaction": device.eventaction
				});
			}
			else{
				res.json({ 
					"status":400,
					"message": "error"
				});	
			}
			});
		}
	},
	trigger : function(req, res){
			var devID = req.body.devID;

			if (devID == "CX0009"){ devID = "CX009";}
			if (devID == "CX0010"){ devID = "CX010";}
			if (devID == "B001"  ){ devID = "CX007";}

			var eventlvl = req.body.eventlvl;
			if(!devID || !eventlvl){
				res.json({
				"status": 403,
				"message": "Faulty request. No/wrong devID "
				});
				return;		
			}
				
			DeviceModel.findOne(  {deviceID: devID},function (err, device) {
				if (err){
					res.json({ 
						"status":400,
						"message": "error"
					});
					console.log(err);
					return;
				}else if(device){
					if( eventlvl == "A"){
						console.log("Button EVENT");
						console.log(devID);

		////////////////////////B&P INSERT
						if (devID == "B001"){
							console.log(" send call"); 
							eventlvl = "4"; 
						}
						if (devID == "B002"){
							console.log("Writed to wunderlist"); 
							eventlvl = "5";
						}
						if (devID == "B003"){g
							console.log("Switch Wemo lights"); 
							eventlvl = "5";
						}
						if (devID == "B004"){
							console.log("send notification"); 
							eventlvl = "5";
						}
						if (devID == "B005"){
							console.log("track temp in spread sheet"); 
							eventlvl = "5";
						}
						if (devID == "B006"){
							console.log("send sms"); 
							eventlvl = "3"
						}

						if (devID == "CX007"){
							console.log("send sms"); 
							eventlvl = "5"
						}
						if (devID == "CX008"){
							console.log("send sms"); 
							eventlvl = "5"
						}

						if (devID == "CX009"){
							console.log("send sms"); 
							eventlvl = "5"
						}
						if (devID == "CX010"){
							console.log("send sms"); 
							eventlvl = "5"
						}



		//// END OF B&P INSERT ////////////////////// 
					}

					if( eventlvl == "0"){
						///// just LOG entry
						console.log("Just a Log!");
					}

					if( eventlvl=="1"){
						////// App Notification
						// currently its a IFTT workaround - native ionic push is bit buggy atm //
						// we can also switch to IBM push
						console.log( " Notification event");
						var apiKey = "b4f96237851e8dcd36b7ef4550239dc150550fcb6dbb40cc";
						var aut = "Basic " + new Buffer(apiKey + ":").toString("base64");
						request({
					    	url: 'https://push.ionic.io/api/v1/push', //URL to hit
					    	method: 'POST', //Specify the method
					    	headers: { //We can define headers too
					        'Content-Type': 'application/json',
					        'X-Ionic-Application-Id': '9EFE49DF',
					        "Authorization": aut
					    	},
					    	json: {
							  "user_ids": "36da595b-3e6c-421d-ba8f-4b74fd900657",
							  "notification":{
							    "alert":"Hello World!",
							    "ios":{
							      "badge":"1",
							      "sound":"ping.aiff",
							      "expiry": "1423238641",
							      "priority": "10",
							      "contentAvailable": "true",
							      "payload":{
							        "key1":"value",
							        "key2":"value"
							      }
							    },
							    "android":{
							      "collapseKey":"foo",
							      "delayWhileIdle":"true",
							      "timeToLive":"300",
							      "payload":{
							        "key1":"value",
							        "key2":"value"
							      }
							    }
							  }
							}
					}, function(error, response, body){
						if(error) {
							console.log(error);
						} else {
							console.log(response.statusCode, body);
						}
					});
					}

					if( eventlvl=="2"){    ////// EMAIL TBD!
						console.log("eventlvl:2");
					}
					if( eventlvl=="3a"){    ///// TWILIO SMS
						console.log("eventlvl:3");
						console.log(device.eventaction.l3.actiondata.number);

						client.sms.messages.create({
		    					to:device.eventaction.l3.actiondata.number,
		    					from:'+4915735986444', // Rysta number 
		   		 			body:device.eventaction.l3.actiondata.text
		   		 			//body:"Hallo"

						}, function(error, message) {
		    				if (!error) {
		        				console.log('Success! The SID for this SMS message is:');
		        				console.log(message.sid); 
		        				console.log('Message sent on:');
		        				console.log(message.dateCreated);
		    				} else {
		    				}
						});	
						
					}

					if( eventlvl=="4a"){
						/////// TWILIO CALL 
						//console.log("eventlvl:4");
						//console.log(device.eventaction.l4.actiondata.number);
						
						resp.say(device.eventaction.l4.actiondata.text, {
		    				voice:'woman',
		    				language:'de'
						});
						var rspURL = 'http://twimlets.com/echo?Twiml='+ encodeURIComponent((resp.toString()));
						console.log(rspURL);

						client.makeCall({
						    //to: device.eventaction.l4.actiondata.number, // Any number Twilio can call
						    //to : '+4915115502187',
						    to : '+4917630638775',
						    from: '+4915735986444', // our Twillio number
						    url: rspURL
						    // 	+4915115502187
						}, function(err, responseData) {
						    console.log(err); // outputs "+14506667788"
						});
					}

					if (eventlvl == "5"){
						////// IFTTT

						console.log( "eventlevel: " + eventlvl + "  IFTTT event");
						//var URL = "https://maker.ifttt.com/trigger/" + device.IFTTT.eventName_sensor + "/with/key/" + device.IFTTT.key;
						//var URL = "https://maker.ifttt.com/trigger/" + devID +  "/with/key/" + device.IFTTT.key;
						var URL = "https://maker.ifttt.com/trigger/" + devID +  "/with/key/" + "f8Z5rLSvRzTgIgORCOO4EGtmq8_2VCBjUtRXtKHA1Jo";
						console.log(URL);
						//request.post(URL);


						request({
					    url: URL, 
					    method: 'POST', 
					    headers: { 'Content-Type': 'application/json' },
					    json: {
					    	"value1": device.recentData["1"].value,
					    	"value2": device.recentData["2"].value
							}

					}, function(error, response, body){
					    if(error) {
					        console.log(error);
					    } else {
					        console.log(response.statusCode, body);
					    }
					});



					}

					else {
						console.log("unknown eventlvl detected: ");
						console.log(eventlvl); 
					}




					res.json({ 
						"status":200,
						"message": "event send!"
					});

				}
				else{
					res.json({ 
						"status":400,
						"message": "error"
					});	
				}
			});
		
	}
};

module.exports = events;
