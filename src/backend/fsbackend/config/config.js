/**
 * Module dependencies.
 */
var _ = require('underscore');

/**
 * Load app configurations
 */
if (!process.env.NODE_ENV) {
    console.error('NODE_ENV is not defined! Using default development environment');
    process.env.NODE_ENV = 'development';
}

module.exports = _.extend(
    require('./env/all'),
    require('./env/' + process.env.NODE_ENV) || {}
);