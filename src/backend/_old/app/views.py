import os
import cloudant

USERNAME = '3e34b0ea-988c-42d9-a4a5-da0f97a253b5-bluemix'
PASSWORD = 'c294e0fc9d4c29660617f6326bc8ab4ccf2d533c31711362358d91ebf1c93531'
account = cloudant.Account(USERNAME)

login = account.login(USERNAME, PASSWORD)
assert login.status_code == 200

db = account.database('freesensor')


@app.route('/')
def Welcome():
	return 'Welcome to my app running on Bluemix!'

@app.route('/test_put')
def test_put():
	resp = doc.put(params={
  			'_id': 'hello_world',
  			'herp': 'derp'
  	})
    return 'test_put ACK'

@app.route('/test_delete')
def test_delete():
	rev = resp.json()['_rev']
	doc.delete(rev).raise_for_status()

    return 'test_delete ACK'

port = os.getenv('VCAP_APP_PORT', '5000')
if __name__ == "__main__":
	app.run(host='0.0.0.0', port=int(port))