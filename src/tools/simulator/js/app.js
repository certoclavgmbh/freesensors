var app = angular.module("app", ['ja.qr']);

	
app.controller("myCtrl", function($scope,$http, $interval){
		

		var interval_promise;
		//$scope.URL = "https://api.thethings.io/v2/things/";
		//$scope.KEY = "cXik_T2GW-yhkVMcDLB7vwP-ZuocQhCPrGZ0wgX_yLY";
		$scope.status = "none"; 
		$scope.URL = "http://192.168.178.80:3000"
		$scope.devID = "12345";
		$scope.secretAPI = "sf2b cbxo q94n 8210";
		$scope.QrCodePayload= ({ "devID" : $scope.devID,"secretAPI" :$scope.secretAPI }).toString();

		$scope.data = { "values":  { "key": "fun", "value": "5000" }  };
		$scope.testURL = "https://api.thethings.io/v2/things/cXik_T2GW-yhkVMcDLB7vwP-ZuocQhCPrGZ0wgX_yLY/";
		
		$scope.varName1 = "field1";
		$scope.sampleRate1 = 60;
		$scope.noiseLevel1 = 5;
		$scope.dataValue1 = 10;

		$scope.varName2 = "field2";
		$scope.sampleRate2 = 60;
		$scope.noiseLevel2 = 50;
		$scope.dataValue2 = 9854;

		$scope.pairingRequestDetected = false; 

		$scope.button_eventString = "Enter a String";

		$scope.backendstatus = "idle";
		$scope.news = "idle"; 

		$scope.contextStatus = "idle";
		$scope.context= {};

		$scope.numCalls = 0;
		$scope.simulationRunning = false; 

		$scope.cmdResponse = "none";

		$scope.pairingStatus= "OFF"; 

		$scope.send = function(){
			$scope.status = "preparing...";
			var dataToSend1 = ($scope.dataValue1 +( Math.random().toFixed(2) * $scope.noiseLevel1)).toString();
			var dataToSend2 = ($scope.dataValue2 +( Math.random().toFixed(2) * $scope.noiseLevel2)).toString();
			$scope.data = { "devID": $scope.devID, "field1": dataToSend1.toString(), "field2": dataToSend2.toString() };
			$scope.status = "sending...";
			$http.post(($scope.URL+"/update"), $scope.data).then(function(response) {
				$scope.status = response;
				$scope.news = response.data.news;
				$scope.backendstatus = "ok";
				$scope.numCalls++;
			}, function(err) {
			$scope.status = err;
			$scope.backendstatus = "error";
			});
		};

		$scope.startOver = function(){
			$interval.cancel(interval_promise);
			interval_promise = $interval( function(){$scope.send();}, 60000/$scope.sampleRate1);
		};

		$scope.register = function() {
			$http.get(($scope.URL+"/register"),{ "params":{"devID": $scope.devID}} ).then(function(response,err) {
				$scope.cmdResponse = response.data.message;
			}, function(err) {
			$scope.cmdResponse = JSON.stringify(err);
			});
		};

	/*	$scope.pairingModeEnable = function() {
			if( $scope.pairingStatus == "OFF" || $scope.pairingStatus == "ACK"  ){
				$scope.pairingStatus = "WAITING"
				$http.post(($scope.URL+"/pairing"),{"devID": $scope.devID, "pairingStatus": $scope.pairingStatus} ).then(function(response,err) {
					$scope.cmdResponse = response.data.message;
					interval_promise_pairing = $interval( function(){$scope.pairingCheck();}, 1000);
				}, function(err) {
				$scope.cmdResponse = JSON.stringify(err);pairingStatus
				});
			}

		};*/

		$scope.pairingModeReset = function() {
			if( true ){
				$scope.pairingStatus = "false"
				$http.post(($scope.URL+"/pairing"),{"devID": $scope.devID, "pairingStatus": $scope.pairingStatus} ).then(function(response,err) {
					$scope.cmdResponse = response.data.message;
					$interval.cancel(interval_promise_pairing);
				}, function(err) {
				$scope.cmdResponse = JSON.stringify(err);pairingStatus
				});
			}

		};

		$scope.pairingCheck = function (){
			$http.get(($scope.URL+"/pairing"),{ "params":{"devID": $scope.devID}} ).then(function(response,err) {
				$scope.pairingStatus = response.data.pairingStatus;
				$scope.cmdResponse = response.data.pairingStatus;

			}, function(err) {
			$scope.cmdResponse = JSON.stringify(err);
			});

		};
		$scope.ackPairing = function() {
			if($scope.pairingStatus == "request"){
				$scope.pairingStatus= "ACK"; 
				$http.post(($scope.URL+"/pairing"),{"devID": $scope.devID, "pairingStatus": $scope.pairingStatus} ).then(function(response,err) {
					$scope.cmdResponse = response.data.message;
				}, function(err) {
				$scope.cmdResponse = JSON.stringify(err);
			});
		}
		};

		$scope.getContext = function() {};
		$scope.putContext = function() {};

		$scope.sendEvent = function(eventlvl) {
			$http.post(($scope.URL+"/event"),{"devID": $scope.devID, "eventlvl": eventlvl} ).then(function(response,err) {
					$scope.contextStatus = "send!";
				}, function(err) {
				$scope.contextStatus = JSON.stringify(err);
				});
		};

		

		$scope.clearResponse = function() {
			$scope.cmdResponse = "none";	
		};
		$scope.stop = function() {
			$scope.simulationRunning =false;
			$interval.cancel(interval_promise);
		};

		$scope.start = function() {
			$scope.simulationRunning = true;
			$interval.cancel(interval_promise);
			interval_promise = $interval( function(){$scope.send();}, 60000/$scope.sampleRate1);
		};

		$scope.delete = function() {
			$http.get(($scope.URL+"/del"),{ "params":{"devID": $scope.devID}} ).then(function(response,err) {
				$scope.cmdResponse = response.data.message;
			}, function(err) {
			$scope.cmdResponse = JSON.stringify(err);
			});
		};

		$scope.getContext = function() {
			$http.get(($scope.URL+"/context"),{ "params":{"devID": $scope.devID}} ).then(function(response,err) {
				$scope.context = response.data.context;
				$scope.contextStatus = "got!";
				console.log( $scope.context["eventObj"]["1"]["V1"]);
			}, function(err) {
			$scope.contextStatus = JSON.stringify(err);
			});
		};

		$scope.postContext = function() {
			$http.post(($scope.URL+"/context"),{"devID": $scope.devID, "context": $scope.context} ).then(function(response,err) {
					$scope.contextStatus = "send!";
				}, function(err) {
				$scope.contextStatus = JSON.stringify(err);
				});
		};


	});
