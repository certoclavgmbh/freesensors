
var app = angular.module("app",['uiSwitch'] );

	
app.controller("myCtrl", function($scope,$http, $interval){
		
		$scope.interval_promise ="";
		$scope.started = false; 
		$scope.numCalls = 0;

		$scope.URL = "http://192.168.178.80:3000";
		$scope.devID = "12345";
		$scope.status = "status";
		$scope.eventStatus = "idle";

		$scope.pairing_status = "idle"



		$scope.enabled= false;
	

		$scope.labels= ["1","2","3","4","5","6"];
  		$scope.data = [[65, 59, 80, 56, 55, 40]];
  		$scope.eventaction = false;
		$scope.context= false;


		$scope.updateData = function(){
			Chart.defaults.global.animation = false;
			$scope.req = { params:  { 'devID' : $scope.devID  } };
			$http.get(($scope.URL+"/datalist"), $scope.req).then(function(res) {
    				$scope.datalist = res.data.values;
    				$scope.data = [[]];
    				$scope.labels=[];
    				var num = 0;
    				for (hours in $scope.datalist) {
    					for (minutes in $scope.datalist[hours]){
    						if( $scope.datalist[hours][minutes] != null ){
    						num++;
    						$scope.labels.push( (hours*60+minutes).toString());
    						$scope.data.push( $scope.datalist[hours][minutes]);
    						}
    					}

    				}
    				$scope.data =[$scope.data];
    				console.log($scope.data); 
    				$scope.numCalls++;
  			}, function(err) {
    	  		$scope.status = err;
    	  	});
		};

		$scope.start = function(){
			if( $scope.started == false){
				//$interval.cancel($scope.interval_promise);
				//$scope.interval_promise = $interval( function(){$scope.updateData();}, 2000);
				$scope.updateData();
				$scope.started = true;
			}
		};
		$scope.stop = function(){
			//if( $scope.started == true){
				$interval.cancel($scope.interval_promise);
				$scope.started = false; 
			//}
		};

		$scope.getEventAction = function() {
			$http.get(($scope.URL+"/eventaction"),{ "params":{"devID": $scope.devID}} ).then(function(response,err) {
				$scope.eventaction = response.data.eventaction;
				$scope.eventStatus = "got!";
			}, function(err) {
			$scope.eventStatus = JSON.stringify(err);
			});
			$http.get(($scope.URL+"/context"),{ "params":{"devID": $scope.devID}} ).then(function(response,err) {
				$scope.context = response.data.context;
			}, function(err) {
			$scope.eventStatus = JSON.stringify(err);
			});
		};

		$scope.postEventAction  = function() {
			$http.post(($scope.URL+"/eventaction"),{"devID": $scope.devID, "eventaction": $scope.eventaction} ).then(function(response,err) {
					$scope.eventStatus = "send!";
				}, function(err) {
				$scope.eventStatus = JSON.stringify(err);
				});
			$http.post(($scope.URL+"/context"),{"devID": $scope.devID, "context": $scope.context} ).then(function(response,err) {
					$scope.eventStatus = "send!";
				}, function(err) {
				$scope.eventStatus = JSON.stringify(err);
				});
		};


		$scope.pairingCheck = function (){
			$scope.pairing_status = "send check"
			$http.get(($scope.URL+"/pairing"),{ "params":{"devID": $scope.devID} }).then(function(response,err) {
				$scope.pairing_status = response.data.pairingStatus;
				}
			, function(err) {
			$scope.pairing_status = JSON.stringify(err);
			});
		};

		$scope.pairingRequest = function (){
			$scope.pairing_status = "send request"
			$http.post(($scope.URL+"/pairingRequest"),{"devID": $scope.devID} ).then(function(response,err) {
				$scope.pairing_status = response.data.message;
				console.log(response);
				}
			, function(err) {
			$scope.pairing_status = JSON.stringify(err);
			});
		};

		




		//interval_promise= $interval( function(){$scope.send();}, 2000);

	});