import serial # import Serial Library
import numpy  # Import numpy
import matplotlib.pyplot as plt #import matplotlib library
from drawnow import *

tempC=[]
pressure=[]
buf = ""
EspData = serial.Serial('com3', 115200) #Creating our serial object named EspData
plt.ion() #Tell matplotlib you want interactive mode to plot live data
cnt=0

def makeFig(): #Create a function that makes our desired plot
    plt.ylim(10,40)                                 #Set y min and max values
    plt.title('My Live Streaming Sensor Data')      #Plot the title
    plt.grid(True)                                  #Turn the grid on
    plt.ylabel('Temp C')                            #Set ylabels
    plt.plot(tempC, 'ro-', label='Degrees C')       #plot the temperature
    plt.legend(loc='upper left')                    #plot the legend
    plt2=plt.twinx()                                #Create a second y axis
    plt.ylim(96000,96500)                           #Set limits of second y axis- adjust to readings you are getting
    plt2.plot(pressure, 'b^-', label='Pressure (Pa)') #plot pressure data
    plt2.set_ylabel('Pressrue (Pa)')                    #label second y axis
    plt2.ticklabel_format(useOffset=False)           #Force matplotlib to NOT autoscale y axis
    plt2.legend(loc='upper right')                  #plot the legend

            

    # wait if some data arrived
    
    while( EspData.inWaiting() == 504):
            # buffer = Get the current buffer
        buf = EspData.read(504) ## read the whole data???
        print buf
        #print type(buf)    #STRING
            # datapackages = split the buffer into packages splitting at "\r\n" -->like  100 datapackages
        #buf = buf.split('\r\n')
        #buf =filter(None, buf)
        #print buf
        #print type(buf)    #list
            # loop through the packages
      # for i in buf:
    #seq=i.split(',')
            #print seq
            #print type(seq)
            #temp = float(seq[0])
           # p =  float(seq[1])
           # tempC.append(temp)
           # pressure.append(p)
            #print tempC
           # print pressure
            #tsmoothed=numpy.convolve(temp,numpy.ones(3)/3)
            #psmoothed=numpy.convolve(p,numpy.ones(3)/3)
            #print temp
            #print p
            #print tsmoothed
            #print psmoothed
            
        #"""drawnow(makeFig)                       #Call drawnow to update our live graph
           # plt.pause(.000001)                     #Pause Briefly. Important to keep drawnow from crashing
           # cnt=cnt+1
            #if(cnt>50):                            #If you have 50 or more points, delete the first one from the array
             #   tempC.pop(0)                       #This allows us to just see the last 50 data points
             #   pressure.pop(0)"""
                                   #Call drawnow to update our live graph
            
            
            #print type(temp)
            #print type(p)
            
                #datalist = buf [i:i+n]
                #print datalist
                #print type(datalist)
               # for i in range(0,len(datalist)):
                    
                   # q=i.split(',')
                   # print q
                    #temp = a[0]
                    #pre = a[1]
                    #tempC.append(temp)
                    #pressure.append(pre)
                    

                    
          
        #print buf.split( );"""
                        #Pause Briefly. Important to keep drawnow from crashing
