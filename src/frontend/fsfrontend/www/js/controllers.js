angular.module('starter.controllers', ['ngCordova'])

.controller('DashCtrl', function($scope, Sensors,$state, $interval,$cordovaBarcodeScanner, $ionicPopup) {
	
	$scope.sensors = Sensors.all();

	$scope.updateSensor = function(){
	Sensors.all();
	console.log("updated");
	};
  	$scope.current = $state.current

  	$scope.tmp_ID = "";

  	$scope.testname = "test";
 

	$scope.scanBarcode = function() {
		$cordovaBarcodeScanner.scan().then(function(imageData) {
				
			//Sensors.add(parseInt(imageData.text));
			$scope.tmp_id = imageData.text;
			$scope.testname = imageData.text;
			$scope.data ={};
			var myPopup = $ionicPopup.show({
			template: '<input type="text" ng-model="data.rysta">',
			title: 'Enter a Name for your RYSTA',
			subTitle: 'Please use normal things',
			scope: $scope,
				buttons: [
					{ text: 'Oh wait, no!',
					  type: 'button-warning' },
					{ text: '<b>Take this!</b>',
					type: 'button-positive',
				onTap: function(e) {
		          if (!$scope.data.rysta) {
		            return "MY RYSTA"
		          } else {
		            return $scope.data.rysta;
		          }
		        }
		      }
		    ]
		  });
		  myPopup.then(function(res) {
		  	Sensors.add($scope.tmp_id,res);
		    	$scope.testname = imageData.text;
		  });
			}, function(error) {
				alert("An error happened -> " + error);
			}); 
		};
	

	interval_promise= $interval( function(){$scope.updateSensor();}, 60000/30);

	})

.controller('ChatsCtrl', function($scope, Chats,$cordovaBarcodeScanner) {


	$scope.chats = Chats.all();
	$scope.remove = function(chat) {
	Chats.remove(chat);
	}

	$scope.scanBarcode = function() {
			$cordovaBarcodeScanner.scan().then(function(imageData) {
				alert(imageData.text);
				console.log("Barcode Format -> " + imageData.format);
				console.log("Cancelled -> " + imageData.cancelled);
			}, function(error) {
				console.log("An error happened -> " + error);
			});
		};

})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
	$scope.chat = Chats.get($stateParams.chatId);
})

.controller('SensorDetailCtrl', function($scope, $stateParams, $http,Sensors) {
	$scope.sensor = Sensors.get($stateParams.sensorId);
	
	// this is a workaround since there is a bug with ng-true-value // ng-false-value
	$scope.cond_state = true;
	$scope.cond = "smaller";
	$scope.value = 22;
	$scope.timeout = 1;

	$scope.updatecondition = function () {
		
		if($scope.cond_state){
			return "smaller";
		}else{
			return "bigger"; 	
		}
	};
 
  
  $scope.getdata = function(ID){
    $http.get("http://rysta.io:3000/datalist", {params:{devID:ID}}).
    then( function(resp){
      $scope.rawData = resp.data.values;
      alert("ok");      
    }, function(err){
      alert("no");
    });
  };
  
    $scope.rawData={};
  

  
  $scope.options = {
            chart: {
                forceY : [-10,30],
                type: 'lineChart',
                height: 500,
                width :600,
                margin : {
                    top: 20,
                    right: 20,
                    bottom: 40,
                    left: 55
                },
                x: function(d){ return d.x; },
                y: function(d){ return d.y; },
                useInteractiveGuideline: false,
                dispatch: {
                    stateChange: function(e){ console.log("stateChange"); },
                    changeState: function(e){ console.log("changeState"); },
                    tooltipShow: function(e){ console.log("tooltipShow"); },
                    tooltipHide: function(e){ console.log("tooltipHide"); }
                },
                xAxis: {
                    axisLabel: 'Time [min]'
                },
                yAxis: {
                    axisLabel: 'Temperature (°C)',
                    tickFormat: function(d){
                        return d3.format('.02f')(d);
                    },
                    axisLabelDistance: 30
                },
                callback: function(chart){
                    console.log("!!! lineChart callback !!!");
                }
            },
            title: {
                enable: false,
                text: 'Title for Line Chart'
            }
        };

        $scope.data =[
                {
                    values: [ {x: 3, y:1 },{x: 2, y:1 },{x: 1, y:1 }],      //values - represents the array of {x,y} data points
                    key: 'RYSTA', //key  - the name of the series.
                    color: '#ff7f0e'  //color - optional: choose your own line color.
                }
            ];
            
        /*Random Data Generator */
        $scope.update = function() {

            var val = []

            //Data is represented as an array of {x,y} pairs.
            for (var hour = 0; hour < 5; hour++) {
              for (var min = 0; min < 61; min++) {  
                if( $scope.rawData[hour][min] !== undefined || $scope.rawData[hour][min] !== null ){
                  val.push({x: (hour*60)+min, y:$scope.rawData[hour][min] });
                }
              }
            }
        
            //Line chart data should be sent as an array of series objects.
            $scope.data =  [
                {
                    values: val,      //values - represents the array of {x,y} data points
                    key: 'RYSTA', //key  - the name of the series.
                    color: '#ff7f0e'  //color - optional: choose your own line color.
                }
            ];
        } 

})


.controller('PlugHelpCtrl', function() {
})

.controller('AccountCtrl', function($scope) {
	$scope.enableAlarms = {
	enableFriends: true
	};
	$scope.enableNoti = {
	enableFriends: true
	};
	$scope.enableEmail = {
	enableFriends: true
	};
	$scope.enableButtons = {
	enableFriends: true
	};
	$scope.UserMail = {
	enableFriends: true
	};
});
