angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope,$http) {

   $scope.stuff2 = "nothing";
  
    $scope.stuff = function() {
    $http.get('http://192.168.178.42:3000/getData?devID=12345').then(
      function(resp) {
        console.log('Success', resp.data.data);
        $scope.stuff2 = resp.data.data;
        return resp.data.data
    })
  }
})
  
.controller('ChatsCtrl', function($scope, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };



});
