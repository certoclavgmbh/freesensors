angular.module('starter.services', [])

.factory('Chats', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var chats = [{
    id: 0,
    name: 'Ben Sparrow',
    lastText: 'Are you on the way?',
    face: 'https://pbs.twimg.com/profile_images/514549811765211136/9SgAuHeY.png'
  }, {
    id: 1,
    name: 'Max Lynx',
    lastText: 'Hey, it\'s me',
    face: 'https://avatars3.githubusercontent.com/u/11214?v=3&s=460'
  },{
    id: 2,
    name: 'Adam Bradleyson',
    lastText: 'I should buy a boat',
    face: 'https://pbs.twimg.com/profile_images/479090794058379264/84TKj_qa.jpeg'
  }, {
    id: 3,
    name: 'Perry Governor',
    lastText: 'Look at my mukluks!',
    face: 'https://pbs.twimg.com/profile_images/598205061232103424/3j5HUXMY.png'
  }, {
    id: 4,
    name: 'Mike Harrington',
    lastText: 'This is wicked good ice cream.',
    face: 'https://pbs.twimg.com/profile_images/578237281384841216/R3ae1n61.png'
  }];

  return {
    all: function() {
      return chats;
    },
    remove: function(chat) {
      chats.splice(chats.indexOf(chat), 1);
    },
    get: function(chatId) {
      for (var i = 0; i < chats.length; i++) {
        if (chats[i].id === parseInt(chatId)) {
          return chats[i];
        }
      }
      return null;
    }
  };
})

.factory('Sensors', function() {
  var sensors = [{
    id: 0,
    name: 'Weather station',
    status: "OK",
    last_update: "26.07.2015",
    value:"29.2",
    unit:"C",
    symbol:"ion_temperature",

  }, {
    id: 2,
    name: 'Max Lynx',
  },{
    id: 3,
    name: 'Adam Bradleyson',
  }];

  return {
    all: function() {
      return sensors;
    },
    remove: function(sensor) {
      sensors.splice(sensors.indexOf(sensor), 1);
    },
    get: function(sensorId) {
      for (var i = 0; i < sensors.length; i++) {
        if (sensors[i].id === parseInt(sensorId)) {
          return sensors[i];
        }
      }
      return null;
    }
  };
});

