angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope, Sensors) {
  $scope.sensors = Sensors.all();
  }
)

.controller('ChatsCtrl', function($scope, Chats) {

  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  }
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('SensorDetailCtrl', function($scope, $stateParams, Sensors) {
  $scope.sensor = Sensors.get($stateParams.sensorId);
})


.controller('PlugHelpCtrl', function() {
})

.controller('AccountCtrl', function($scope) {
  $scope.enableAlarms = {
    enableFriends: true
  };
  $scope.enableNoti = {
    enableFriends: true
  };
  $scope.enableEmail = {
    enableFriends: true
  };
  $scope.enableButtons = {
    enableFriends: true
  };
  $scope.UserMail = {
    enableFriends: true
  };
});
