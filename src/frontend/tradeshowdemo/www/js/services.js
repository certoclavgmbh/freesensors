angular.module('starter.services', [])

.factory('Chats', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data

  return {
    all: function() {
      return chats;
    },
    remove: function(chat) {
      chats.splice(chats.indexOf(chat), 1);
    },
    get: function(chatId) {
      for (var i = 0; i < chats.length; i++) {
        if (chats[i].id === parseInt(chatId)) {
          return chats[i];
        }
      }
      return null;
    }
  };
})

.factory('Sensors', function($http) {
  var sensors = [{
    devID: 1111,
    name: 'Beer@Certoclav',
    status: "OK",
    last_update: "26.07.2015 - 13:01:01",
    value:"29.2",
    value2:"1000",
    unit:"°C",
    unit2:"mbar",
    symbol:"ion_temperature",} ,
{
    devID: 2222,
    name: 'Kitchen@Certoclav',
    status: "OK",
    last_update: "26.07.2015 - 13:01:01",
    value:"12.2",
    value2:"1000",
    unit:"°C",
    unit2:"mbar",
    symbol:"ion_temperature",} ,
{
    devID: 3333,
    name: 'Beer@LLG',
    status: "OK",
    last_update: "26.07.2015 - 13:01:01",
    value:"32.2",
    value2:"1000",
    unit:"°C",
    symbol:"ion_temperature",} ,
    {
    devID: 4444,
    name: 'Beer@LLG',
    status: "OK",
    last_update: "26.07.2015 - 13:01:01",
    value:"-99",
    value2:"1000",
    unit:"°C",
    symbol:"ion_temperature",}  
  ];
    //$scope.URL = "https://api.thethings.io/v2/things/";
    URL = "http://sveneliasson.de:3000/getData";
    //$scope.KEY = "cXik_T2GW-yhkVMcDLB7vwP-ZuocQhCPrGZ0wgX_yLY";
    KEY = "";
  return {
    all: function() {

      $http.get(  "http://rysta.io:3000/data", 
          {params: {devID: "1111"}}
        ).then(function(response) {

          sensors[0].value = response.data.field1;
          sensors[0].value2 = response.data.field2;
          sensors[0].lastUpdate = response.data.lastUpdate;
          var tdiff = (Date.now() - Date.parse(response.data.lastUpdate))/ 60000; 
          if (tdiff >= 120){
              sensors[0].status = "Offline since " +Math.floor(tdiff/60) + " hours";
            }else if( tdiff>=1 ){
              ssensors[0].status = "Offline since " (Math.floor(tdiff/60)).toString() + "hours";
           }else{
              sensors[3].status = "OK";
           }
      }, function(err) {
          console.log(err);
          });


      $http.get(  "http://rysta.io:3000/data", 
          {params: {devID: "2222"}}
        ).then(function(response) {

          sensors[1].value = response.data.field1;
          sensors[1].value2 = response.data.field2;
          sensors[1].lastUpdate = response.data.lastUpdate;
          var tdiff = (Date.now() - Date.parse(response.data.lastUpdate))/ 60000; 
          if (tdiff >= 120){
              sensors[1].status = "Offline since " +Math.floor(tdiff/60) + " hours";
            }else if( tdiff>=1 ){
              ssensors[1].status = "Offline since " (Math.floor(tdiff/60)) + "hours";
           }else{
              sensors[1].status = "OK"
           }
      }, function(err) {
          console.log(err);
          });



      $http.get(  "http://rysta.io:3000/data", 
          {params: {devID: "3333"}}
        ).then(function(response) {

          sensors[2].value = response.data.field1;
          sensors[2].value2 = response.data.field2;
          sensors[2].lastUpdate = response.data.lastUpdate;
          var tdiff = (Date.now() - Date.parse(response.data.lastUpdate))/ 60000; 
          if (tdiff >= 120){
              sensors[2].status = "Offline since " +Math.floor(tdiff/60) + " hours";
            }else if( tdiff>=1 ){
              ssensors[2].status = "Offline since " (Math.floor(tdiff/60)).toString() + "hours";
           }else{
              sensors[3].status = "OK";
           }
      }, function(err) {
          console.log(err);
          });

      $http.get(  "http://rysta.io:3000/data", 
          {params: {devID: "4444"}}
        ).then(function(response) {

          sensors[3].value = response.data.field1;
          sensors[3].value2 = response.data.field2;
          sensors[3].lastUpdate = response.data.lastUpdate;
          var tdiff = (Date.now() - Date.parse(response.data.lastUpdate))/ 60000; 
          if (tdiff >= 120){
              sensors[3].status = "Offline since " +Math.floor(tdiff/60) + " hours";
            }else if( tdiff>=1 ){
              ssensors[3].status = "Offline since " (Math.floor(tdiff/60)).toString() + "hours";
           }else{
              sensors[3].status = "OK";
           }
      }, function(err) {
          console.log(err);
          });



      return sensors;
    },
    remove: function(sensor) {
      sensors.splice(sensors.indexOf(sensor), 1);
    },
    asdd: function(sensor) {
      sensors.splice(sensors.indexOf(sensor), 1);
    },
    get: function(sensorId) {
      for (var i = 0; i < sensors.length; i++) {
        if (sensors[i].devID === parseInt(sensorId)) {
          return sensors[i];
        }
      }
      return null;
    }
  };
});

