angular.module('starter.controllers', ['ngCordova'])

.controller('DashCtrl', function($scope, Sensors,$state, $interval,$cordovaBarcodeScanner) {
	
	$scope.sensors = Sensors.all();

	$scope.updateSensor = function(){
	Sensors.all();
	console.log("updated");
	};
  	$scope.current = $state.current



	$scope.scanBarcode = function() {
			$cordovaBarcodeScanner.scan().then(function(imageData) {
				alert(imageData.text);
				console.log("Barcode Format -> " + imageData.format);
				console.log("Cancelled -> " + imageData.cancelled);
			}, function(error) {
				alert("An error happened -> " + error);
			});
		};


	interval_promise= $interval( function(){$scope.updateSensor();}, 60000/30);

	/*$scope.registerWithPushService = function() {
    		$ionicPush.register({
      		canShowAlert: true, // Can pushes show an alert on your screen?
      		canSetBadge: true, // Can pushes update app icon badges?
      		canPlaySound: true, // Can notifications play a sound?
      		canRunActionsOnWake: true, // Can run actions outside the app,
      		onNotification: function(notification) {
        // Handle push notifications here
        			console.log(notification);
        			return true;
      		}
    		});
  	};*/



	}
	)

.controller('ChatsCtrl', function($scope, Chats,$cordovaBarcodeScanner) {


	$scope.chats = Chats.all();
	$scope.remove = function(chat) {
	Chats.remove(chat);
	}

	$scope.scanBarcode = function() {
			$cordovaBarcodeScanner.scan().then(function(imageData) {
				alert(imageData.text);
				console.log("Barcode Format -> " + imageData.format);
				console.log("Cancelled -> " + imageData.cancelled);
			}, function(error) {
				console.log("An error happened -> " + error);
			});
		};

})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
	$scope.chat = Chats.get($stateParams.chatId);
})

.controller('SensorDetailCtrl', function($scope, $stateParams, Sensors) {
	$scope.sensor = Sensors.get($stateParams.sensorId);
})


.controller('PlugHelpCtrl', function() {
})

.controller('AccountCtrl', function($scope) {
	$scope.enableAlarms = {
	enableFriends: true
	};
	$scope.enableNoti = {
	enableFriends: true
	};
	$scope.enableEmail = {
	enableFriends: true
	};
	$scope.enableButtons = {
	enableFriends: true
	};
	$scope.UserMail = {
	enableFriends: true
	};
});
